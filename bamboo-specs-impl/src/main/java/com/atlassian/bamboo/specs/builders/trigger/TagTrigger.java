package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.model.trigger.TagTriggerProperties;

/**
 * Represents tag trigger.
 */
public class TagTrigger extends Trigger<TagTrigger, TagTriggerProperties> {

    private String filter;
    private boolean checkingIfTagIsInBranch = true;

    /**
     * Sets the filter (regular expresion).
     */
    public TagTrigger filter(final String filter) {
        this.filter = filter;
        return this;
    }

    /**
     * Sets the flag checkingIfTagIsInBranch.
     */
    public TagTrigger checkingIfTagIsInBranch(boolean enable) {
        this.checkingIfTagIsInBranch = enable;
        return this;
    }

    @Override
    protected TagTriggerProperties build() {
        return new TagTriggerProperties(name, description, triggerEnabled, conditions, filter, checkingIfTagIsInBranch);
    }
}

