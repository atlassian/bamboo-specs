package com.atlassian.bamboo.specs.api.util;

import com.atlassian.bamboo.specs.api.builders.CallEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

public final class EntityPropertiesBuilders {
    private EntityPropertiesBuilders() {
    }

    @NotNull
    public static <T extends EntityProperties, B extends EntityPropertiesBuilder<T>> T build(@NotNull final B builder) {
        return CallEntityPropertiesBuilder.build(builder);
    }
}
