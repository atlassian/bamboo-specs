package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.model.trigger.GitHubTriggerProperties;

/**
 * Represents Github trigger that indicates the integration of webhooks.
 *
 * @since 10.1
 */
public class GitHubTrigger extends RepositoryBasedTrigger<GitHubTrigger, GitHubTriggerProperties> {

    @Override
    protected GitHubTriggerProperties build() {
        return new GitHubTriggerProperties(name, description, triggerEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
    }
}
