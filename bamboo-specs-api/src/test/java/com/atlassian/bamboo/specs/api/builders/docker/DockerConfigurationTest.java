package com.atlassian.bamboo.specs.api.builders.docker;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DockerConfigurationTest {

    @Test
    public void testDisabledConfigurationIsValid() {
        new DockerConfiguration()
                .enabled(false)
                .build()
                .validate();
    }

    @Test
    @Parameters({
            "my-image",
            "atlassian/my-image",
            "atlassian/my-image:6.3.0",
            "localhost:6990/atlassian/my-image:version5"
    })
    public void testEnabledConfigurationWithImageIsValid(String image) {
        new DockerConfiguration()
                .image(image)
                .build()
                .validate();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testEnabledConfigurationWithoutImageIsInvalid() {
        new DockerConfiguration()
                .build()
                .validate();
    }

    @Test
    public void testDefaultVolumes() {
        DockerConfigurationProperties dockerConfigurationProperties = new DockerConfiguration()
                .image("myImage")
                .build();
        assertEquals(dockerConfigurationProperties.getVolumes(), DockerConfigurationProperties.DEFAULT_VOLUMES);
    }

    @Test
    public void testRemovingDefaultVolumes() {
        DockerConfigurationProperties dockerConfigurationProperties = new DockerConfiguration()
                .image("myImage")
                .withoutDefaultVolumes()
                .build();
        assertThat(dockerConfigurationProperties.getVolumes().entrySet(), hasSize(0));
    }

    @Test
    public void testCustomVolumes() {
        DockerConfigurationProperties dockerConfigurationProperties = new DockerConfiguration()
                .image("myImage")
                .withoutDefaultVolumes()
                .volume("/home/user", "/tmpdata")
                .build();
        final Map<String, String> volumes = dockerConfigurationProperties.getVolumes();
        assertThat(volumes.entrySet(), hasSize(1));
        assertThat(volumes.get("/home/user"), is("/tmpdata"));
    }

    @Test
    public void testMergingDefaultAndCustomVolumes() {
        DockerConfigurationProperties dockerConfigurationProperties = new DockerConfiguration()
                .image("myImage")
                .volume("/home/user/", "/tmpdata")
                .build();
        final Map<String, String> volumes = dockerConfigurationProperties.getVolumes();
        assertThat(volumes.entrySet(),
                hasSize(1 + DockerConfigurationProperties.DEFAULT_VOLUMES.size()));
        assertThat(volumes.get("/home/user/"), is("/tmpdata"));
        DockerConfigurationProperties.DEFAULT_VOLUMES.forEach((key, value) ->
                assertThat(volumes.get(key), is(value)));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testShouldDoNotAllowDuplicateContainerInVolumes() {
        new DockerConfiguration()
                .image("myImage")
                .volume("/home/user/", "/tmpdata")
                .volume("/home/user2/", "/tmpdata")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testShouldDoNotAllowDuplicateHostInVolumes() {
        new DockerConfiguration()
                .image("myImage")
                .volume("/home/user/", "/tmpdata-1")
                .volume("/home/user/", "/tmpdata-2")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(method = "emptyVolumeDirs")
    public void testShouldNotAllowEmptyDirsForVolume(String hostDir, String containerDir) {
        new DockerConfiguration()
                .image("myImage")
                .volume(hostDir, containerDir)
                .build();
    }

    private Object[] emptyVolumeDirs() {
        return new Object[][]{
                {null, "/tmp"},
                {"/tmp", null},
                {null, null},
                {"     ", ""},
                {"", ""}
        };
    }
}
