package com.atlassian.bamboo.specs.api.model.label;

import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.LabelValidator;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;

@Immutable
public class LabelProperties implements EntityProperties {
    private final String name;

    protected LabelProperties() {
        name = null;
    }

    public LabelProperties(String name) {
        this.name = name;
        validate();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LabelProperties)) {
            return false;
        }
        final LabelProperties that = (LabelProperties) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public void validate() {
        checkNoErrors(LabelValidator.validateLabel(name));
    }
}
