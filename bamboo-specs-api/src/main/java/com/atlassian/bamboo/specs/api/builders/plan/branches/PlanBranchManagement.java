package com.atlassian.bamboo.specs.api.builders.plan.branches;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchCleanupProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchIntegrationProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.CreatePlanBranchesProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents configuration of automatic branch management.
 * These settings are only applied on the master plan and as defaults for non-Specs branches.
 */
@SuppressWarnings("UnusedReturnValue")
public class PlanBranchManagement extends EntityPropertiesBuilder<PlanBranchManagementProperties> {
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Plan Branch Management");
    private CreatePlanBranchesProperties createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.MANUAL, null);
    private BranchCleanupProperties deletePlanBranches = new BranchCleanupProperties(false, Duration.ZERO, false, Duration.ZERO);
    private PlanBranchManagementProperties.TriggeringOption triggeringOption = PlanBranchManagementProperties.TriggeringOption.INHERITED;
    private PlanBranchManagementProperties.NotificationStrategy notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.NONE;
    private BranchIntegrationProperties branchIntegration = new BranchIntegrationProperties(false, null, false, false);
    private TriggerProperties defaultTrigger;

    private boolean issueLinkingEnabled = true;

    /**
     * Disable automatic plan branch creation.
     * <p>
     * In RSS mode, the branch creation is controlled by the master plan. This setting has no effect on a branch.
     */
    public PlanBranchManagement createManually() {
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.MANUAL, null);
        return this;
    }

    /**
     * Create new plan branches for new pull requests - pull requests either from branches or from both branches and forks.
     * <p>
     * In RSS mode, the branch creation is controlled by the master plan. This setting has no effect on a branch.
     *
     * @param allowPullRequestFromForks if pull requests from forks are allowed. If false, then plan branch will be created only for PRs from default repository
     */
    public PlanBranchManagement createForPullRequest(boolean allowPullRequestFromForks) {
        if (allowPullRequestFromForks) {
            this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.FORK_ENABLED_PULL_REQUEST, null);
        } else {
            this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.PULL_REQUEST, null);
        }
        return this;
    }

    /**
     * Create new plan branches for new pull requests - pull requests only from branches.
     * <p>
     * In RSS mode, the branch creation is controlled by the master plan. This setting has no effect on a branch.
     */
    public PlanBranchManagement createForPullRequest() {
        return createForPullRequest(false);
    }

    /**
     * Create new plan branches for new branches in default repository.
     * <p>
     * In RSS mode, the branch creation is controlled by the master plan. This setting has no effect on a branch.
     */
    public PlanBranchManagement createForVcsBranch() {
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.BRANCH, null);
        return this;
    }

    /**
     * Create new plan branches for new branches with name which matches pattern.
     * <p>
     * In RSS mode, the branch creation is controlled by the master plan. This setting has no effect on a branch.
     *
     * @param pattern regexp to match branch name
     */
    public PlanBranchManagement createForVcsBranchMatching(final String pattern) {
        ImporterUtils.checkNotBlank(VALIDATION_CONTEXT, "to createForVcsBranchMatching()", pattern);
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.BRANCH, pattern);
        return this;
    }

    /**
     * Sets configuration of automatic removal of branches. The feature is turned off by default.
     * <p>
     * In RSS mode, the branch expiry is controlled by the master plan. This setting has no effect on a branch.
     *
     * @see BranchCleanup
     */
    public PlanBranchManagement delete(@NotNull BranchCleanup removedBranchCleanup) throws PropertiesValidationException {
        checkNotNull("removedBranchCleanup", removedBranchCleanup);
        this.deletePlanBranches = removedBranchCleanup.build();
        return this;
    }

    /**
     * Created plan branch can only be triggered manually only.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#triggers(Trigger[])} instead.
     */
    public PlanBranchManagement triggerBuildsManually() {
        this.triggeringOption = PlanBranchManagementProperties.TriggeringOption.MANUAL;
        this.defaultTrigger = null;
        return this;
    }

    /**
     * Created plan branch will use the same triggers as master plan.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#triggers(Trigger[])} instead.
     */
    public PlanBranchManagement triggerBuildsLikeParentPlan() {
        this.triggeringOption = PlanBranchManagementProperties.TriggeringOption.INHERITED;
        this.defaultTrigger = null;
        return this;
    }

    /**
     * Created plan branch will use a custom trigger.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#triggers(Trigger[])} instead.
     */
    public PlanBranchManagement defaultTrigger(@NotNull Trigger<?, ?> defaultTrigger) {
        this.triggeringOption = PlanBranchManagementProperties.TriggeringOption.CUSTOM;
        this.defaultTrigger = EntityPropertiesBuilders.build(defaultTrigger);
        return this;
    }

    /**
     * All committers and people who have favourited the branch will be notified for all build failures and the first successful build.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#notifications(Notification...)} instead.
     */
    public PlanBranchManagement notificationForCommitters() {
        this.notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.NOTIFY_COMMITTERS;
        return this;
    }

    /**
     * Use the same notification rules as configured for the master plan.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#notifications(Notification...)} instead.
     */
    public PlanBranchManagement notificationLikeParentPlan() {
        this.notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.INHERIT;
        return this;
    }

    /**
     * No notifications will be sent for the created branch.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#notifications(Notification...)} instead.
     */
    public PlanBranchManagement notificationDisabled() {
        this.notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.NONE;
        return this;
    }

    /**
     * Sets default merge strategy for new branches. By default merging is turned off.
     * <p>
     * This setting is ignored for Specs branches. Use {@link Plan#branchConfiguration(PlanBranchConfiguration)} instead.
     * @see BranchIntegration
     */
    public PlanBranchManagement branchIntegration(@NotNull BranchIntegration branchIntegration) throws PropertiesValidationException {
        checkNotNull("branchIntegration", branchIntegration);
        this.branchIntegration = branchIntegration.build();
        return this;
    }

    /**
     * Enables/disables automatic JIRA issue link creation when new branch is created. Enabled by default.
     */
    public PlanBranchManagement issueLinkingEnabled(boolean issueLinkingEnabled) throws PropertiesValidationException {
        this.issueLinkingEnabled = issueLinkingEnabled;
        return this;
    }

    protected PlanBranchManagementProperties build() throws PropertiesValidationException {
        return new PlanBranchManagementProperties(
                createPlanBranches,
                deletePlanBranches,
                triggeringOption,
                defaultTrigger,
                notificationStrategy,
                branchIntegration,
                issueLinkingEnabled);
    }
}
