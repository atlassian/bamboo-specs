package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.RepositoryBasedTriggerProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Immutable
public final class BitbucketServerTriggerProperties extends RepositoryBasedTriggerProperties {
    private static final String NAME = "Bitbucket Server repository triggered";
    private static final String MODULE_KEY = "com.atlassian.bamboo.plugins.stash.atlassian-bamboo-plugin-stash:stashTrigger";

    private static final AtlassianModuleProperties MODULE = new AtlassianModuleProperties(MODULE_KEY);

    @SuppressWarnings("unused")
    private BitbucketServerTriggerProperties() {
        super(NAME, null, true, Collections.emptySet(), TriggeringRepositoriesType.ALL, Collections.emptyList());
    }

    /**
     * Deprecated. Use {@link BitbucketServerTriggerProperties#BitbucketServerTriggerProperties(String, String, boolean, Set, TriggeringRepositoriesType, List)}
     * @deprecated since 10.0
     */
    @Deprecated
    public BitbucketServerTriggerProperties(final String description,
                                            final boolean isEnabled,
                                            final Set<TriggerConditionProperties> conditions,
                                            final TriggeringRepositoriesType triggeringRepositoriesType,
                                            final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(NAME, description, isEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
        validate();
    }

    public BitbucketServerTriggerProperties(final String name,
                                            final String description,
                                            final boolean isEnabled,
                                            final Set<TriggerConditionProperties> conditions,
                                            final TriggeringRepositoriesType triggeringRepositoriesType,
                                            final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(StringUtils.defaultIfBlank(name, NAME), description, isEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE;
    }
}
