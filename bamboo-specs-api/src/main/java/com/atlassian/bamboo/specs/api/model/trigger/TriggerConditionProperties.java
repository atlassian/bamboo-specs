package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.Nullable;

public abstract class TriggerConditionProperties implements EntityProperties {
    @Override
    public void validate() {
    }

    @Nullable
    public abstract AtlassianModuleProperties getAtlassianPlugin();
}
