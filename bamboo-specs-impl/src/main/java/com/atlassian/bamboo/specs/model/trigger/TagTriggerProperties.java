package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Immutable
public final class TagTriggerProperties extends TriggerProperties {
    @SuppressWarnings("WeakerAccess") //used in Bamboo tests
    public static final String NAME = "Tag trigger";
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:tag";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));

    private final String filter;
    private final boolean checkingIfTagIsInBranch;

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    private TagTriggerProperties() {
        this(NAME, null, true, Collections.emptySet(), null, true);
    }

    /**
     * Deprecated. Use {@link TagTriggerProperties#TagTriggerProperties(String, String, boolean, Set, String, boolean)}
     * @deprecated since 10.0
     */
    @Deprecated
    public TagTriggerProperties(final String description,
                                final boolean isEnabled,
                                final Set<TriggerConditionProperties> conditions,
                                final String filter,
                                final boolean checkingIfTagIsInBranch) {
        super(NAME, description, isEnabled, conditions);
        this.filter = filter;
        this.checkingIfTagIsInBranch = checkingIfTagIsInBranch;
        validate();
    }

    public TagTriggerProperties(final String name,
                                final String description,
                                final boolean isEnabled,
                                final Set<TriggerConditionProperties> conditions,
                                final String filter,
                                final boolean checkingIfTagIsInBranch) {
        super(StringUtils.defaultIfBlank(name, NAME), description, isEnabled, conditions);
        this.filter = filter;
        this.checkingIfTagIsInBranch = checkingIfTagIsInBranch;
        validate();
    }

    public void validate() throws PropertiesValidationException {
        super.validate();
        String filterExpression = StringUtils.trimToNull(filter);
        if (filterExpression != null) {
            try {
                Pattern.compile(filterExpression);
            } catch (PatternSyntaxException e) {
                throw new PropertiesValidationException(String.format("Invalid regex filter: %s", filter));
            }
        }
    }

    public String getFilter() {
        return filter;
    }

    public boolean isCheckingIfTagIsInBranch() {
        return checkingIfTagIsInBranch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TagTriggerProperties that = (TagTriggerProperties) o;
        return checkingIfTagIsInBranch == that.checkingIfTagIsInBranch &&
                Objects.equals(filter, that.filter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), filter, checkingIfTagIsInBranch);
    }
}
