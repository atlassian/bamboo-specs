package com.atlassian.bamboo.specs.api.model;

import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;

@Immutable
public interface RootEntityProperties extends EntityProperties {
    /**
     * Entity type that is shown to human.
     */
    @NotNull
    String humanReadableType();

    /**
     * Entity type and id representation that is shown to human.
     */
    @NotNull
    String humanReadableId();
}
