package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.AntTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Ant build task builder.
 */
public class AntTask extends Task<AntTask, AntTaskProperties> {
    @NotNull
    private String target;
    @Nullable
    private String buildFile;
    @Nullable
    private String environmentVariables;
    @Nullable
    private String jdk;
    @Nullable
    private String label;
    private boolean hasTests;
    @Nullable
    private String testResultsDirectory;
    @Nullable
    private String workingSubdirectory;

    /**
     * Specifies Ant target(s).
     */
    public AntTask target(@NotNull String target) {
        this.target = target;
        return this;
    }

    /**
     * Specifies Ant's build file.
     */
    public AntTask buildFile(String buildFile) {
        this.buildFile = buildFile;
        return this;
    }

    /**
     * Sets environment variables to be set when Ant is executed.
     */
    public AntTask environmentVariables(String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return this;
    }

    /**
     * Specifies which JDK to use. If not set default Bamboo JDK will be used.
     */
    public AntTask jdk(String jdk) {
        this.jdk = jdk;
        return this;
    }

    /**
     * Sets label of Ant executable.
     */
    public AntTask executableLabel(@NotNull String label) {
        this.label = label;
        return this;
    }

    /**
     * Specifies if given task produces tests. If set to <i>true</i> the task will fail if no tests are found.
     * If task is used for Deployment project validation will fail if it has value true.
     * Test output must be in JUnit XML format. Defaults to <i>false</i>.
     */
    public AntTask hasTests(boolean hasTests) {
        this.hasTests = hasTests;
        return this;
    }

    /**
     * Sets location where Bamboo will look for test results. This is a coma separated list of directories. You can also use
     * Ant style patterns such as {@code test-reports/*.xml}.
     * Defaults to <i>**\/test-reports/*.xml</i>
     */
    public AntTask testResultsPath(String testResultsDirectory) {
        this.testResultsDirectory = testResultsDirectory;
        return this;
    }

    /**
     * Sets a directory the script should be executed in.
     */
    public AntTask workingSubdirectory(String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    @NotNull
    @Override
    protected AntTaskProperties build() {
        return new AntTaskProperties(description,
                taskEnabled,
                target,
                buildFile,
                environmentVariables,
                jdk,
                label,
                hasTests,
                testResultsDirectory,
                workingSubdirectory,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AntTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AntTask antTask = (AntTask) o;
        return hasTests == antTask.hasTests &&
                target.equals(antTask.target) &&
                Objects.equals(buildFile, antTask.buildFile) &&
                Objects.equals(environmentVariables, antTask.environmentVariables) &&
                Objects.equals(jdk, antTask.jdk) &&
                Objects.equals(label, antTask.label) &&
                Objects.equals(testResultsDirectory, antTask.testResultsDirectory) &&
                Objects.equals(workingSubdirectory, antTask.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), target, buildFile, environmentVariables, jdk, label, hasTests, testResultsDirectory, workingSubdirectory);
    }
}
