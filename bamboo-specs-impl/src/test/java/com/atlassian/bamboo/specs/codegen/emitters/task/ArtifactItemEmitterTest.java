package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.ArtifactItemProperties;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ArtifactItemEmitterTest {

    private CodeGenerationContext context;
    private ArtifactItemEmitter emitter;

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new ArtifactItemEmitter();
    }

    @Test
    public void testAllArtifacts() throws CodeGenerationException {
        final ArtifactItemProperties properties = new ArtifactItemProperties(
                new PlanIdentifierProperties(
                        new BambooKeyProperties("PROJ"),
                        new BambooKeyProperties("PLAN"),
                        null),
                true,
                null);
        final String expectedCode =
                "new ArtifactItem()\n" +
                "    .sourcePlan(new PlanIdentifier(\"PROJ\", \"PLAN\"))\n" +
                "    .allArtifacts()";
        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void testOneArtifact() throws CodeGenerationException {
        final ArtifactItemProperties properties = new ArtifactItemProperties(
                new PlanIdentifierProperties(
                        new BambooKeyProperties("PROJ"),
                        new BambooKeyProperties("PLAN"),
                        null),
                false,
                "One Artifact");
        final String expectedCode =
                "new ArtifactItem()\n" +
                "    .sourcePlan(new PlanIdentifier(\"PROJ\", \"PLAN\"))\n" +
                "    .artifact(\"One Artifact\")";
        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void testOneArtifactWithNoSourcePlan() throws CodeGenerationException {
        final ArtifactItemProperties properties = new ArtifactItemProperties(null, false, "One Artifact");
        final String expectedCode =
                "new ArtifactItem()\n" +
                "    .artifact(\"One Artifact\")";
        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

}