package com.atlassian.bamboo.specs.api.model.credentials;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public class AnySharedCredentialsProperties extends SharedCredentialsProperties {
    private final AtlassianModuleProperties atlassianPlugin;
    private final Map<String, Object> configuration;

    private AnySharedCredentialsProperties() {
        atlassianPlugin = null;
        configuration = Collections.emptyMap();
    }

    public AnySharedCredentialsProperties(final AtlassianModuleProperties atlassianPlugin,
                                          final String name,
                                          final BambooOidProperties oid,
                                          final Map<String, Object> configuration) {
        this(atlassianPlugin, name, oid, configuration, null);
    }

    public AnySharedCredentialsProperties(final AtlassianModuleProperties atlassianPlugin,
                                          final String name,
                                          final BambooOidProperties oid,
                                          final Map<String, Object> configuration,
                                          final ProjectProperties project) {
        super(name, oid, project);
        this.atlassianPlugin = atlassianPlugin;
        this.configuration = Collections.unmodifiableMap(MapUtils.copyOf(configuration));

        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AnySharedCredentialsProperties that = (AnySharedCredentialsProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getConfiguration(), that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAtlassianPlugin(), getConfiguration());
    }

    @Override
    public void validate() {
        super.validate();
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
