package com.atlassian.bamboo.specs.api.builders.task;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.condition.TaskCondition;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a Bamboo task. Task is an unit of work within Job.
 */
public abstract class Task<T extends Task<T,P>, P extends TaskProperties> extends EntityPropertiesBuilder<P> {
    protected boolean taskEnabled = true;
    protected String description = "";
    protected List<ConditionProperties> conditions = new ArrayList<>();

    protected List<RequirementProperties> requirements = new ArrayList<>();

    protected Task() {
    }

    /**
     * Sets the task description.
     */
    public T description(String description) {
        this.description = description;
        return (T) this;
    }

    /**
     * Enabled/disables that task. Task is enabled by default.
     */
    public T enabled(final boolean taskEnabled) {
        this.taskEnabled = taskEnabled;
        return (T) this;
    }

    /**
     * Adds custom requirements to this task. All the custom requirements from tasks will be added to {@link Job}'s or
     * {@link Environment}'s requirements list.
     *
     * @see Job#requirements
     * @see Environment#requirements
     */
    public T requirements(final Requirement... requirements) {
        checkNotNull("requirements", requirements);
        Arrays.stream(requirements)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.requirements::add);
        return (T) this;
    }

    /**
     * Adds conditions to this task.
     */
    public T conditions(final TaskCondition<?>... conditions) {
        checkNotNull("conditions", conditions);
        Arrays.stream(conditions)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.conditions::add);
        return (T) this;
    }

    @NotNull
    protected abstract P build();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Task)) {
            return false;
        }
        Task<?, ?> task = (Task<?, ?>) o;
        return taskEnabled == task.taskEnabled &&
                Objects.equals(description, task.description) &&
                Objects.equals(conditions, task.conditions) &&
                Objects.equals(requirements, task.requirements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskEnabled, description, conditions, requirements);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("taskEnabled", taskEnabled)
                .append("description", description)
                .append("conditions", conditions)
                .append("requirements", requirements)
                .toString();
    }
}
