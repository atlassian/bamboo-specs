package com.atlassian.bamboo.specs.model.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationTypeProperties;
import com.atlassian.bamboo.specs.codegen.emitters.notification.BuildErrorNotificationEmitter;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public class BuildErrorNotificationProperties extends NotificationTypeProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:buildError");

    @CodeGenerator(BuildErrorNotificationEmitter.class)
    private final boolean firstOccurrenceOnly;

    private BuildErrorNotificationProperties() {
        firstOccurrenceOnly = false;
    }

    public BuildErrorNotificationProperties(boolean firstOccurrenceOnly) {
        this.firstOccurrenceOnly = firstOccurrenceOnly;
    }

    public boolean isFirstOccurrenceOnly() {
        return firstOccurrenceOnly;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BuildErrorNotificationProperties that = (BuildErrorNotificationProperties) o;
        return isFirstOccurrenceOnly() == that.isFirstOccurrenceOnly();
    }

    @Override
    public int hashCode() {
        return Objects.hash(isFirstOccurrenceOnly());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public Set<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }

    @Override
    public void validate() {

    }
}
