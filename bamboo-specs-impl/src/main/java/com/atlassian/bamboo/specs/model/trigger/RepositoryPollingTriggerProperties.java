package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.RepositoryBasedTriggerProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.CronExpressionClientSideValidator;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkPositive;

@Immutable
public final class RepositoryPollingTriggerProperties extends RepositoryBasedTriggerProperties {
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:poll";
    private static final String NAME = "Repository polling";
    private static final AtlassianModuleProperties MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));

    private static final int DEFAULT_POLLING_PERIOD = preventInlining(180);

    private final Duration pollingPeriod;
    private final String cronExpression;
    private final PollType pollType;

    @SuppressWarnings("unused")
    private RepositoryPollingTriggerProperties() {
        super(NAME, null, true, Collections.emptySet(), TriggeringRepositoriesType.ALL, Collections.emptyList());
        pollingPeriod = Duration.ofSeconds(DEFAULT_POLLING_PERIOD);
        cronExpression = null;
        pollType = PollType.PERIOD;
    }

    /**
     * Deprecated. Use {@link RepositoryPollingTriggerProperties#RepositoryPollingTriggerProperties(String, String, boolean, Set, TriggeringRepositoriesType, List, String)}
     * @deprecated since 10.0
     */
    @Deprecated
    public RepositoryPollingTriggerProperties(final String description,
                                              final boolean isEnabled,
                                              final Set<TriggerConditionProperties> conditions,
                                              final TriggeringRepositoriesType triggeringRepositoriesType,
                                              final List<VcsRepositoryIdentifierProperties> triggeringRepositories,
                                              final String cronExpression) {
        super(NAME, description, isEnabled, conditions, triggeringRepositoriesType, triggeringRepositories);
        this.cronExpression = cronExpression;
        this.pollingPeriod = null;
        pollType = PollType.CRON;
        validate();
    }

    public RepositoryPollingTriggerProperties(final String name,
                                              final String description,
                                              final boolean isEnabled,
                                              final Set<TriggerConditionProperties> conditions,
                                              final TriggeringRepositoriesType triggeringRepositoriesType,
                                              final List<VcsRepositoryIdentifierProperties> triggeringRepositories,
                                              final String cronExpression) {
        super(StringUtils.defaultIfBlank(name, NAME), description, isEnabled, conditions, triggeringRepositoriesType, triggeringRepositories);
        this.cronExpression = cronExpression;
        this.pollingPeriod = null;
        pollType = PollType.CRON;
        validate();
    }

    /**
     * Deprecated. Use {@link RepositoryPollingTriggerProperties#RepositoryPollingTriggerProperties(String, String, boolean, Set, TriggeringRepositoriesType, List, Duration)}
     * @deprecated since 10.0
     */
    @Deprecated
    public RepositoryPollingTriggerProperties(final String description,
                                              final boolean isEnabled,
                                              final Set<TriggerConditionProperties> conditions,
                                              final TriggeringRepositoriesType triggeringRepositoriesType,
                                              final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories,
                                              final Duration pollingPeriod) {
        super(NAME, description, isEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
        this.cronExpression = null;
        this.pollingPeriod = pollingPeriod;
        pollType = PollType.PERIOD;
        validate();
    }

    public RepositoryPollingTriggerProperties(final String name,
                                              final String description,
                                              final boolean isEnabled,
                                              final Set<TriggerConditionProperties> conditions,
                                              final TriggeringRepositoriesType triggeringRepositoriesType,
                                              final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories,
                                              final Duration pollingPeriod) {
        super(StringUtils.defaultIfBlank(name, NAME), description, isEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
        this.cronExpression = null;
        this.pollingPeriod = pollingPeriod;
        pollType = PollType.PERIOD;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE;
    }

    public PollType getPollType() {
        return pollType;
    }

    @Nullable
    public Duration getPollingPeriod() {
        return pollingPeriod;
    }

    @Nullable
    public String getCronExpression() {
        return cronExpression;
    }

    @Override
    public void validate() {
        super.validate();

        if (pollType == null) {
            throw new PropertiesValidationException("Can't create repository polling trigger without any polling type");
        }

        switch (pollType) {
            case PERIOD:
                checkNotNull("pollingPeriod", pollingPeriod);
                checkPositive("pollingPeriod", (int) pollingPeriod.getSeconds());
                break;
            case CRON:
                CronExpressionClientSideValidator.validate(cronExpression);
                break;
            default:
                throw new PropertiesValidationException("Can't create repository polling trigger - unknown polling type: " + pollType);
        }
    }

    public enum PollType {
        PERIOD,
        CRON
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        RepositoryPollingTriggerProperties that = (RepositoryPollingTriggerProperties) o;
        return Objects.equals(pollingPeriod, that.pollingPeriod) &&
                Objects.equals(cronExpression, that.cronExpression) &&
                pollType == that.pollType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pollingPeriod, cronExpression, pollType);
    }
}
