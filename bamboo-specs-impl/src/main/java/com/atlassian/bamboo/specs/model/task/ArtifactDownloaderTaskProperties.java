package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class ArtifactDownloaderTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask");

    private final PlanIdentifierProperties sourcePlan;
    private final String customBranch;
    private final List<DownloadItemProperties> artifacts;

    private ArtifactDownloaderTaskProperties() {
        sourcePlan = null;
        customBranch = null;
        artifacts = Collections.emptyList();
    }

    public ArtifactDownloaderTaskProperties(@Nullable final String description,
                                            final boolean enabled,
                                            @Nullable final PlanIdentifierProperties sourcePlan,
                                            @NotNull final String customBranch,
                                            @NotNull final List<DownloadItemProperties> artifacts,
                                            @NotNull List<RequirementProperties> requirements,
                                            @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.sourcePlan = sourcePlan;
        this.customBranch = customBranch;
        this.artifacts = Collections.unmodifiableList(new ArrayList<>(artifacts));
        validate();
    }

    @Nullable
    public PlanIdentifierProperties getSourcePlan() {
        return sourcePlan;
    }

    public String getCustomBranch() {
        return customBranch;
    }

    public List<DownloadItemProperties> getArtifacts() {
        return artifacts;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final ArtifactDownloaderTaskProperties that = (ArtifactDownloaderTaskProperties) o;
        return Objects.equals(getSourcePlan(), that.getSourcePlan()) &&
                Objects.equals(getCustomBranch(), that.getCustomBranch()) &&
                Objects.equals(getArtifacts(), that.getArtifacts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSourcePlan(), getCustomBranch(), getArtifacts());
    }

    @Override
    public void validate() {
        super.validate();
        final ValidationContext context = ValidationContext.of("Artifact downloader task");
        checkThat(context, artifacts != null && !artifacts.isEmpty(), "No artifacts to download has been defined");
    }
}
