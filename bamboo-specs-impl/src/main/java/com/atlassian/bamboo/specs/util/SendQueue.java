package com.atlassian.bamboo.specs.util;


import java.net.URI;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

public final class SendQueue {
    private static final SynchronousQueue<RestTaskFactory.RestTask> TASK_QUEUE = new SynchronousQueue<>();
    private static final SynchronousQueue<RestTaskResult> RESULT_QUEUE = new SynchronousQueue<>();

    private static final RestHelper REST_HELPER = new RestHelper();

    static {
        final Thread thread = new Thread("Entity REST sender") {
            @Override
            public void run() {
                while (true) {
                    final RestTaskFactory.RestTask restTask = take(TASK_QUEUE);
                    try {
                        final URI restEndpointUri = restTask.getRestEndpointUri();
                        final String response =
                                REST_HELPER.post(restEndpointUri,
                                        restTask.getAuthenticationProvider(),
                                        restTask.getYamlString());
                        put(RestTaskResult.forResult(response));
                    } catch (final Exception e) {
                        put(RestTaskResult.forException(e));
                    }
                }
            }
        };
        thread.setDaemon(true);
        thread.start();
    }

    private SendQueue() {
    }

    private static RestTaskFactory.RestTask take(final BlockingQueue<RestTaskFactory.RestTask> queue) {
        try {
            return queue.take();
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static RestTaskResult put(final RestTaskFactory.RestTask restTask) {
        try {
            TASK_QUEUE.put(restTask);
            return RESULT_QUEUE.take();
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    private static void put(final RestTaskResult result) {
        try {
            RESULT_QUEUE.put(result);
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
