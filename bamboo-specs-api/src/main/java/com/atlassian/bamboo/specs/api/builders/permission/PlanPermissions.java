package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.model.permission.PlanPermissionsProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Entity representing permissions for plans.
 * Note that this object needs to be published separately from associated {@link Plan}.
 * Pre-existing permissions that are not defined in associated {@link Permissions} object are <b>revoked</b> when this object is published.
 */
public final class PlanPermissions extends RootEntityPropertiesBuilder<PlanPermissionsProperties> {

    public static final String TYPE = "plan permission";

    @NotNull
    private final PlanIdentifier planIdentifier;
    private Permissions permissions = new Permissions();


    public PlanPermissions(final BambooOid planOid) {
        this(new PlanIdentifier(planOid));
    }

    public PlanPermissions(final BambooKey projectKey,
                           final BambooKey planKey) {
        this(new PlanIdentifier(projectKey, planKey));
    }

    public PlanPermissions(final PlanIdentifier planIdentifier) {
        this.planIdentifier = planIdentifier;
    }

    public PlanPermissions permissions(@NotNull final Permissions permissions) {
        checkNotNull("permissions", permissions);
        this.permissions = permissions;
        return this;
    }

    /**
     * Adds defaults permissions, ie. {@link PermissionType#VIEW} for logged-in and anonymous users.
     */
    public PlanPermissions addDefaultPermissions() {
        permissions.loggedInUserPermissions(PermissionType.VIEW);
        permissions.anonymousUserPermissionView();
        return this;
    }

    @NotNull
    public PlanIdentifier getPlanIdentifier() {
        return planIdentifier;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    @Override
    protected PlanPermissionsProperties build() {
        return new PlanPermissionsProperties(planIdentifier, permissions);
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        return String.format("%s for plan %s", TYPE, planIdentifier);
    }
}
