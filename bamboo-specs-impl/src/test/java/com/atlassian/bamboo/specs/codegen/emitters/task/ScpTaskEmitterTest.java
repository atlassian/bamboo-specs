package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.ArtifactItem;
import com.atlassian.bamboo.specs.model.task.BaseSshTaskProperties;
import com.atlassian.bamboo.specs.model.task.ScpTaskProperties;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ScpTaskEmitterTest {

    private CodeGenerationContext context;
    private ScpTaskEmitter emitter;

    private static final List<RequirementProperties> REQS = Collections.singletonList(new RequirementProperties("key", "value", Requirement.MatchType.EXISTS));

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new ScpTaskEmitter();
    }

    @Test
    public void emitCodeForAllArtifacts() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("ABC", "ONE"))
                .allArtifacts();

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE, "null",
                "secret-key", "secret-passphrase", null, "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir", REQS, Collections.emptyList());

        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .host(\"localhost\")\n" +
                        "    .authenticateWithKeyWithPassphrase(\"secret-key\", \"secret-passphrase\")\n" +
                        "    .fromArtifact(new ArtifactItem()\n" +
                        "        .sourcePlan(new PlanIdentifier(\"ABC\", \"ONE\"))\n" +
                        "        .allArtifacts())";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForAllArtifactsWithMultipleHosts() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("ABC", "ONE"))
                .allArtifacts();

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost, 123.123.123", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE, "null",
                "secret-key", "secret-passphrase", null, "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir", REQS, Collections.emptyList());

        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .host(\"localhost\").host(\"123.123.123\")\n" +
                        "    .authenticateWithKeyWithPassphrase(\"secret-key\", \"secret-passphrase\")\n" +
                        "    .fromArtifact(new ArtifactItem()\n" +
                        "        .sourcePlan(new PlanIdentifier(\"ABC\", \"ONE\"))\n" +
                        "        .allArtifacts())";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForSingleArtifact() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("ABC", "ONE"))
                .artifact("Test Report");

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE,
                null, "secret-key", null, null, "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir", REQS, Collections.emptyList());

        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .host(\"localhost\")\n" +
                        "    .authenticateWithKey(\"secret-key\")\n" +
                        "    .fromArtifact(new ArtifactItem()\n" +
                        "        .sourcePlan(new PlanIdentifier(\"ABC\", \"ONE\"))\n" +
                        "        .artifact(\"Test Report\"))";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForArtifactFromCurrentPlan() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .artifact("Test Report"); // current plan, one artifact

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.PASSWORD, "secret-password", null, null,
                null, "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir", REQS, Collections.emptyList());

        // we expect no sourcePlan() method call
        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .host(\"localhost\")\n" +
                        "    .authenticateWithPassword(\"secret-password\")\n" +
                        "    .fromArtifact(new ArtifactItem()\n" +
                        "        .artifact(\"Test Report\"))";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForLocalFiles() throws Exception {
        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.PASSWORD, "secret-password", null, null,
                null, "hostfingerprint", 80,
                null, "abc.txt,target/abc.jar", false, "remote-dir", REQS, Collections.emptyList());

        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .host(\"localhost\")\n" +
                        "    .authenticateWithPassword(\"secret-password\")\n" +
                        "    .fromLocalPath(\"abc.txt,target/abc.jar\", false)";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForLocalFilesWithAntPatterns() throws Exception {
        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.PASSWORD, "secret-password", null, null,
                null, "hostfingerprint", 80,
                null, "abc.txt,target/**/*.jar", true, "remote-dir", REQS, Collections.emptyList());

        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .host(\"localhost\")\n" +
                        "    .authenticateWithPassword(\"secret-password\")\n" +
                        "    .fromLocalPath(\"abc.txt,target/**/*.jar\", true)";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void testAuthenticationWithSshProjectSharedCredentials() throws Exception {
        SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties = new SharedCredentialsIdentifierProperties("project-shared-credentials", null, SharedCredentialsScope.PROJECT);

        final ScpTaskProperties properties = new ScpTaskProperties(null, true,
                                                                   "localhost", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE, null, null, null,
                                                                   sharedCredentialsIdentifierProperties, null, 80,
                                                                   null, "abc.txt,target/abc.jar", false, "remote-dir", REQS, Collections.emptyList());

        final String expectedCode =
                "new ScpTask()\n" +
                "    .requirements(new Requirement(\"key\")\n" +
                "            .matchValue(\"value\"))\n" +
                "    .username(\"admin\")\n" +
                "    .port(80)\n" +
                "    .toRemotePath(\"remote-dir\")\n" +
                "    .host(\"localhost\")\n" +
                "    .authenticateWithSshSharedCredentials(new SharedCredentialsIdentifier(\"project-shared-credentials\").scope(SharedCredentialsScope.PROJECT))\n" +
                "    .fromLocalPath(\"abc.txt,target/abc.jar\", false)";
        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }
}