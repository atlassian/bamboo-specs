package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.MsBuildTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MsBuildTaskTest {
    private String executable = "executable";
    private String projectFile = "project_file";
    private String options = "options";
    private String environmentVariables = "A=1";
    private String workingSubdirectory = "/tmp";

    @Test
    public void testBuilderOK() {
        final MsBuildTask msBuildTaskBuilder = new MsBuildTask()
                .executable(executable)
                .projectFile(projectFile)
                .options(options)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);


        final MsBuildTaskProperties properties = msBuildTaskBuilder.build();

        assertThat(properties.getExecutable(), is(executable));
        assertThat(properties.getProjectFile(), is(projectFile));
        assertThat(properties.getOptions(), is(options));
        assertThat(properties.getEnvironmentVariables(), is(environmentVariables));
        assertThat(properties.getWorkingSubdirectory(), is(workingSubdirectory));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionWithEmptyExecutable() {
        final MsBuildTask msBuildTaskBuilder =
                new MsBuildTask().executable("");
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionWithEmptyProject() {
        final MsBuildTask msBuildTaskBuilder =
                new MsBuildTask().projectFile("");
    }
}
