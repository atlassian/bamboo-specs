package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequiredString;

public final class VariableValidator {
    private VariableValidator() {
    }

    public static List<ValidationProblem> validate(@NotNull final VariableProperties variableProperties) {
        final ValidationContext context = ValidationContext.of("Variable");
        final List<ValidationProblem> errors = new ArrayList<>();

        validateRequiredString(context.with("Name"), variableProperties.getName())
                .ifPresent(errors::add);

        return errors;
    }

    public static List<ValidationProblem> validateUniqueVariableNames(final List<VariableProperties> variables) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        Set<String> variableNames = new HashSet<>();
        for (VariableProperties var : variables) {
            if (!variableNames.add(var.getName())) {
                errors.add(new ValidationProblem("Duplicate variable name " + var.getName()));
            }

        }
        return errors;
    }
}
