package com.atlassian.bamboo.specs.api.model.plan.artifact;

import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Immutable
public final class ArtifactProperties implements EntityProperties {

    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Artifact");

    private final String name;
    private final List<String> copyPatterns;
    private final List<String> exclusionPatterns;
    private final String location;
    private final boolean shared;
    private final boolean required;
    private final boolean httpCompressionOn;


    private ArtifactProperties() {
        shared = Artifact.SHARED_BY_DEFAULT;
        required = Artifact.REQUIRED_BY_DEFAULT;
        httpCompressionOn = Artifact.HTTP_COMPRESSION_ON_BY_DEFAULT;
        name = null;
        location = "";
        copyPatterns = Collections.emptyList();
        exclusionPatterns = Collections.emptyList();
    }

    public ArtifactProperties(final String name,
                              final List<String> copyPatterns,
                              final List<String> exclusionPatterns,
                              final String location,
                              final boolean shared,
                              final boolean required,
                              final boolean httpCompressionOn) throws PropertiesValidationException {
        this.name = name;
        this.copyPatterns = new ArrayList<>(copyPatterns);
        this.exclusionPatterns = new ArrayList<>(exclusionPatterns);
        this.location = location;
        this.shared = shared;
        this.required = required;
        this.httpCompressionOn = httpCompressionOn;
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArtifactProperties that = (ArtifactProperties) o;
        return isShared() == that.isShared() &&
                isRequired() == that.isRequired() &&
                isHttpCompressionOn() == that.isHttpCompressionOn() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getCopyPatterns(), that.getCopyPatterns()) &&
                Objects.equals(getExclusionPatterns(), that.getExclusionPatterns()) &&
                Objects.equals(getLocation(), that.getLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCopyPatterns(), getExclusionPatterns(), getLocation(), isShared(), isRequired(), isHttpCompressionOn());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("name", name)
                .append("copyPatterns", copyPatterns)
                .append("exclusionPatterns", exclusionPatterns)
                .append("location", location)
                .append("shared", shared)
                .append("required", required)
                .append("httpCompressionOn", httpCompressionOn)
                .build();
    }

    public String getName() {
        return name;
    }

    public List<String> getCopyPatterns() {
        return copyPatterns;
    }

    public List<String> getExclusionPatterns() {
        return exclusionPatterns;
    }

    public String getLocation() {
        return location;
    }

    public boolean isShared() {
        return shared;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isHttpCompressionOn() {
        return httpCompressionOn;
    }

    @Override
    public void validate() {
        ImporterUtils.checkArgument(VALIDATION_CONTEXT, !StringUtils.contains(name, "/"), "Name can not contain '/' character");
    }
}
