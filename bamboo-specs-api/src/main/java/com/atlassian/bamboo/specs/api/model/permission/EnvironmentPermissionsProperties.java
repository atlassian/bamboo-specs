package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.EnvironmentPermissions;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.RootEntityProperties;
import com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
@ConstructFrom({"deploymentName"})
public class EnvironmentPermissionsProperties implements RootEntityProperties {
    private final BambooOidProperties deploymentOid;
    private final String deploymentName;
    private final String environmentName;
    private final PermissionsProperties permissions;

    private EnvironmentPermissionsProperties() {
        this.deploymentOid = null;
        this.deploymentName = null;
        this.environmentName = null;
        this.permissions = null;
    }

    private EnvironmentPermissionsProperties(@Nullable final BambooOidProperties deploymentOid,
                                             @Nullable final String deploymentName,
                                             @NotNull final String environmentName,
                                             @NotNull final PermissionsProperties permissions) {
        this.deploymentOid = deploymentOid;
        this.deploymentName = deploymentName;
        this.environmentName = environmentName;
        this.permissions = permissions;
        validate();
    }

    public EnvironmentPermissionsProperties(@NotNull final String deploymentName,
                                            @NotNull final String environmentName,
                                            @NotNull final PermissionsProperties permissions) {
        this(null, deploymentName, environmentName, permissions);
    }

    public EnvironmentPermissionsProperties(@NotNull final BambooOidProperties deploymentOid,
                                            @NotNull final String environmentName,
                                            @NotNull final PermissionsProperties permissions) {
        this(deploymentOid, null, environmentName, permissions);
    }

    @Override
    public void validate() throws PropertiesValidationException {
        if (deploymentName == null && deploymentOid == null) {
            throw new PropertiesValidationException("deployment oid or name should be not null");
        }
        checkNotBlank("environment name", environmentName);
        checkNotNull("permissions", permissions);
        checkNoErrors(PermissionValidator.validatePermissions(permissions, PermissionValidator.PermissionTarget.ENVIRONMENT));
    }

    public BambooOidProperties getDeploymentOid() {
        return deploymentOid;
    }

    public String getDeploymentName() {
        return deploymentName;
    }

    public String getEnvironmentName() {
        return environmentName;
    }

    public PermissionsProperties getPermissions() {
        return permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EnvironmentPermissionsProperties that = (EnvironmentPermissionsProperties) o;
        return Objects.equals(deploymentOid, that.deploymentOid) &&
                Objects.equals(deploymentName, that.deploymentName) &&
                Objects.equals(environmentName, that.environmentName) &&
                Objects.equals(permissions, that.permissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deploymentOid, deploymentName, environmentName, permissions);
    }

    @NotNull
    @Override
    public String humanReadableType() {
        return EnvironmentPermissions.TYPE;
    }

    @Override
    public String humanReadableId() {
        final StringBuilder id = new StringBuilder();
        id.append(EnvironmentPermissions.TYPE);
        id.append(" for deployment ");
        if (deploymentOid != null) {
            id.append(deploymentOid);
        } else {
            id.append(deploymentName);
        }
        id.append(" environment ");
        id.append(environmentName);
        return id.toString();
    }
}
