package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.ScpTaskEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkArgument;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@CodeGenerator(ScpTaskEmitter.class)
@Immutable
public class ScpTaskProperties extends BaseSshTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-scp-plugin:scptask");

    @Nullable
    private ArtifactItemProperties artifactItem;
    @Nullable
    private String localPath;
    private boolean localPathAntStyle = false;
    private String remotePath;


    private ScpTaskProperties() {
        this.artifactItem = null;
        this.localPath = null;
        this.remotePath = null;
    }


    public ScpTaskProperties(final String description,
                             final boolean enabled,
                             final String host,
                             final String username,
                             final AuthenticationType authenticationType,
                             @Nullable final String password,
                             @Nullable final String key,
                             @Nullable final String passphrase,
                             @Nullable final SharedCredentialsIdentifierProperties sharedCredentialsIdentifier,
                             @Nullable final String hostFingerprint,
                             final int port,
                             @Nullable final ArtifactItemProperties artifactItem,
                             @Nullable final String localPath,
                             final boolean localPathAntStyle,
                             final String remotePath,
                             @NotNull List<RequirementProperties> requirements,
                             @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, host, username, authenticationType, password, key, passphrase, sharedCredentialsIdentifier, hostFingerprint, port, requirements, conditions);
        this.artifactItem = artifactItem;
        this.localPath = localPath;
        this.localPathAntStyle = localPathAntStyle;
        this.remotePath = remotePath;
        this.validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    /**
     * Artifact name.
     * @return always null
     * @deprecated since 6.1.0, use {@link #getArtifactItem()} instead
     */
    @Deprecated
    @Nullable
    public String getArtifactName() {
        return null;
    }

    @Nullable
    public ArtifactItemProperties getArtifactItem() {
        return artifactItem;
    }

    @Nullable
    public String getLocalPath() {
        return localPath;
    }

    public boolean isLocalPathAntStyle() {
        return localPathAntStyle;
    }

    public String getRemotePath() {
        return remotePath;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        final ValidationContext context = ValidationContext.of("SCP task");
        checkArgument(context.with("artifact name or local path"),
                artifactItem != null || !StringUtils.isBlank(localPath),
                "artifact name or local path must be set");
        checkRequired(context.with("remote path"), remotePath);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final ScpTaskProperties that = (ScpTaskProperties) o;
        return localPathAntStyle == that.localPathAntStyle &&
                Objects.equals(artifactItem, that.artifactItem) &&
                Objects.equals(localPath, that.localPath) &&
                Objects.equals(remotePath, that.remotePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), artifactItem, localPath, localPathAntStyle, remotePath);
    }

}
