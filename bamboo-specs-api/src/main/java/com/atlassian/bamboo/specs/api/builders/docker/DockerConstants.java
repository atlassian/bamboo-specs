package com.atlassian.bamboo.specs.api.builders.docker;

/**
 * Docker constants which can be used for docker related classes.
 */
public final class DockerConstants {

    /**
     * Default host mapping for data volume mappings.
     */
    public static final String DEFAULT_HOST_MAPPING = "${bamboo.working.directory}";

    /**
     * Default container mapping for data volume mappings.
     */
    public static final String DEFAULT_CONTAINER_MAPPING = "/data";

    private DockerConstants() {}
}
