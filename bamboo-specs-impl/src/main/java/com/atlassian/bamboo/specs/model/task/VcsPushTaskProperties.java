package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.VcsPushTaskEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@CodeGenerator(VcsPushTaskEmitter.class)
public final class VcsPushTaskProperties extends BaseVcsTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.vcs:task.vcs.push");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("VCS Push Task");

    private VcsPushTaskProperties() {
        super();
    }

    public VcsPushTaskProperties(@Nullable String description,
                                 boolean enabled,
                                 @NotNull List<RequirementProperties> requirements,
                                 @NotNull List<? extends ConditionProperties> conditions,
                                 boolean defaultRepository,
                                 @Nullable VcsRepositoryIdentifierProperties repository,
                                 @Nullable String workingSubdirectory) {
        super(description, enabled, requirements, conditions, defaultRepository, repository, workingSubdirectory);
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }
}
