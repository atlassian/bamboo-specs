import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.util.BambooServer;

@BambooSpec
public class PlanSpec {

    public PlanPermissions rootObject() {
        final PlanPermissions rootObject = new PlanPermissions(new PlanIdentifier("PROJ1", "PLAN1"))
                .permissions(new Permissions()
                        .userPermissions("admin", PermissionType.EDIT, PermissionType.CLONE, PermissionType.ADMIN)
                        .groupPermissions("bamboo-admin", PermissionType.ADMIN)
                        .loggedInUserPermissions(PermissionType.VIEW)
                        .anonymousUserPermissionView());
        return rootObject;
    }

    public static void main(String... argv) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("http://localhost:8085");
        final PlanSpec planSpec = new PlanSpec();

        final PlanPermissions rootObject = planSpec.rootObject();
        bambooServer.publish(rootObject);
    }
}