package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

public abstract class TriggerProperties implements EntityProperties {
    private final String name;
    private final String description;
    private final boolean enabled;
    private final List<? extends TriggerConditionProperties> conditions;

    protected TriggerProperties() {
        this.name = null;
        this.description = null;
        this.enabled = true;
        this.conditions = Collections.emptyList();
    }

    public TriggerProperties(@NotNull String name, String description, boolean enabled, Set<TriggerConditionProperties> conditions) {
        this.name = name;
        this.description = description;
        this.enabled = enabled;
        this.conditions = Collections.unmodifiableList(new ArrayList<>(conditions));
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public List<? extends TriggerConditionProperties> getConditions() {
        return conditions;
    }

    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TriggerProperties that = (TriggerProperties) o;
        return enabled == that.enabled &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(conditions, that.conditions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, enabled, conditions);
    }

    @Override
    public void validate() throws PropertiesValidationException {
        checkNotBlank("name", name);
    }

    public EnumSet<Applicability> applicableTo() {
        return EnumSet.allOf(Applicability.class);
    }

    @Override
    public String toString() {
        return "TriggerProperties{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", conditions=" + conditions +
                ", enabled=" + enabled +
                '}';
    }
}
