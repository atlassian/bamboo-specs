package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.ArtifactDownloaderTaskProperties;
import com.atlassian.bamboo.specs.model.task.DownloadItemProperties;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents task that downloads artifacts created by other jobs and plans.
 */
public class ArtifactDownloaderTask extends Task<ArtifactDownloaderTask, ArtifactDownloaderTaskProperties> {
    private PlanIdentifierProperties sourcePlan;
    private String customBranch;
    private List<DownloadItemProperties> artifacts = new ArrayList<>();

    /**
     * Specifies the plan that is the source of the artifacts.
     * If this property is not set, source plan is either the current plan (if the task is used in a job) or the plan
     * associated with the deployment project (if the task is used in a deployment environment).
     */
    public ArtifactDownloaderTask sourcePlan(@NotNull PlanIdentifier planIdentifier) {
        checkNotNull("planIdentifier", planIdentifier);
        this.sourcePlan = EntityPropertiesBuilders.build(planIdentifier);
        return this;
    }

    /**
     * Specifies the plan branch name of the source plan from which the artifact must be taken.
     * If this property is set, then custom mode is enabled: use the plan branch specified.
     * If this property is not set, then default mode is enabled: first try to find a plan branch in the source plan
     * with the same name as the plan branch it belongs to. If not found, then uses the default (e.g. master) plan
     * branch.
     * @since 10.2
     */
    public ArtifactDownloaderTask customBranch(@NotNull String customBranch) {
        checkNotNull("customBranch", customBranch);
        this.customBranch = customBranch;
        return this;
    }

    /**
     * Activates the match-plan-branches-by-name mode by cleaning eventual "customBranch" setting.
     * After activating this mode, Artifact Downloader task will first try to find a plan branch in the source plan with
     * the same name as the plan branch it belongs to. If not found, then uses the default (e.g. master) plan branch.
     * @since 10.2
     */
    public ArtifactDownloaderTask matchByBranchName() {
        this.customBranch = null;
        return this;
    }

    /**
     * Adds download requests.
     */
    public ArtifactDownloaderTask artifacts(@NotNull DownloadItem... artifacts) {
        checkNotNull("artifacts", artifacts);
        Arrays.stream(artifacts)
                .map(DownloadItem::build)
                .forEach(this.artifacts::add);
        return this;
    }

    @NotNull
    @Override
    protected ArtifactDownloaderTaskProperties build() {
        return new ArtifactDownloaderTaskProperties(description,
                taskEnabled,
                sourcePlan,
                customBranch,
                artifacts,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ArtifactDownloaderTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ArtifactDownloaderTask that = (ArtifactDownloaderTask) o;
        return Objects.equals(sourcePlan, that.sourcePlan) &&
                Objects.equals(customBranch, that.customBranch) &&
                Objects.equals(artifacts, that.artifacts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sourcePlan, customBranch, artifacts);
    }
}
