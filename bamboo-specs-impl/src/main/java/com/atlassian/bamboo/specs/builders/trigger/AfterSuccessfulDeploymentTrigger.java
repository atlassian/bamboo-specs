package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.model.trigger.AfterSuccessfulDeploymentTriggerProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Trigger which schedule a deployment when other environment deployment complete.
 */
public class AfterSuccessfulDeploymentTrigger extends Trigger<AfterSuccessfulDeploymentTrigger, AfterSuccessfulDeploymentTriggerProperties> {
    private final String environment;

    /**
     * Schedule a deployment when environment deployment complete.
     * @param environment name
     */
    public AfterSuccessfulDeploymentTrigger(@NotNull final String environment) {
        this.environment = environment;
    }

    @Override
    protected AfterSuccessfulDeploymentTriggerProperties build() {
        return new AfterSuccessfulDeploymentTriggerProperties(name, description, triggerEnabled, environment);
    }
}
