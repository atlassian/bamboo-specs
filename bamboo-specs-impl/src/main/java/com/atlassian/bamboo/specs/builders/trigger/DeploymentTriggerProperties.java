package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;

import java.util.Collections;
import java.util.EnumSet;

public abstract class DeploymentTriggerProperties extends TriggerProperties {
    public DeploymentTriggerProperties() {
        super();
    }

    public DeploymentTriggerProperties(String name, String description, boolean enabled) {
        super(name, description, enabled, Collections.emptySet());
    }

    @Override
    public EnumSet<Applicability> applicableTo() {
        return EnumSet.of(Applicability.DEPLOYMENTS);
    }
}
