package com.atlassian.bamboo.specs.api.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.RepositoryBasedTriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a definition of trigger which uses a Bamboo repository to activate builds.
 * <p>
 * This class contains common data only. In order to define a specific type of trigger one should use the specialised
 * implementation or, if such is not available, {@link AnyTrigger} class.
 */
public abstract class RepositoryBasedTrigger<T extends RepositoryBasedTrigger<T, P>, P extends RepositoryBasedTriggerProperties> extends Trigger<T, P> {
    public enum TriggeringRepositoriesType {
        ALL, SELECTED
    }

    protected List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories = new ArrayList<>();
    protected TriggeringRepositoriesType triggeringRepositoriesType = TriggeringRepositoriesType.ALL;

    /**
     * Selects the mode of selecting repositories this trigger should check. Possible values:
     * <dl>
     * <dt>ALL</dt>
     * <dd>Select all repositories that work with this trigger</dd>
     * <dt>SELECTED</dt>
     * <dd>Pick repositories manually. See {@link #selectedTriggeringRepositories(VcsRepositoryIdentifier...)}</dd>
     * </dl>
     * Default is ALL.
     */
    public T triggeringRepositoriesType(@NotNull TriggeringRepositoriesType triggeringRepositoriesType) {
        checkNotNull("triggeringRepositoriesType", triggeringRepositoriesType);
        this.triggeringRepositoriesType = triggeringRepositoriesType;
        return (T) this;
    }

    /**
     * Sets all repositories available in this plan and making sense for specific trigger type (exp: all Bitbucket repositories
     * defined in plan for Bitbucket trigger) for the trigger.
     * This is the default behaviour.
     */
    public T allAvailableTriggeringRepositories() {
        this.triggeringRepositoriesType = TriggeringRepositoriesType.ALL;
        this.selectedTriggeringRepositories.clear();
        return (T) this;
    }

    /**
     * Adds the source repository for the trigger.
     * <p>
     * Bamboo trigger can check a source repository for changes in order to trigger the build execution upon new
     * commits. The detailed behaviour which specify the exact rules of detecting changes are specified by specialised
     * implementation of this class.
     *
     * @param triggeringRepositories Trigger source repositories referenced by {@link VcsRepositoryIdentifierProperties}
     */
    public T selectedTriggeringRepositories(@NotNull final VcsRepositoryIdentifier... triggeringRepositories) {
        checkNotNull("triggeringRepositories", triggeringRepositories);

        this.triggeringRepositoriesType = TriggeringRepositoriesType.SELECTED;

        for (VcsRepositoryIdentifier moreRepository : triggeringRepositories) {
            selectedTriggeringRepositories.add(EntityPropertiesBuilders.build(moreRepository));

        }
        return (T) this;
    }
}
