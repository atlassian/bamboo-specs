package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class BitbucketCloudTriggerPropertiesTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetAtlassianPlugin() {
        assertThat(new BitbucketCloudTriggerProperties("a",
                        "My Bbc trigger",
                        true,
                        Collections.emptySet(),
                        RepositoryBasedTrigger.TriggeringRepositoriesType.ALL,
                        Collections.emptyList()
                ).getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-bitbucket:bbcTrigger"));
    }

    @Test
    public void testBitbucketCloudTriggerPropertiesBaseValidation() {
        new BitbucketCloudTriggerProperties("a",
                "My Bbc trigger",
                true,
                Collections.emptySet(),
                RepositoryBasedTrigger.TriggeringRepositoriesType.ALL,
                Collections.emptyList()
        ).validate();
    }
}
