package com.atlassian.bamboo.specs.api.builders.plan.configuration;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.plan.configuration.PluginConfigurationProperties;
import org.jetbrains.annotations.NotNull;

public abstract class PluginConfiguration<T extends PluginConfigurationProperties> extends EntityPropertiesBuilder<T> {
    @NotNull
    @Override
    protected abstract T build();

    public abstract boolean equals(Object o);

    @Override
    public abstract int hashCode();
}
