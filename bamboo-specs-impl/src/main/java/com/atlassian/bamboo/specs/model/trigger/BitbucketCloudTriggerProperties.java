package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.RepositoryBasedTriggerProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Immutable
public final class BitbucketCloudTriggerProperties extends RepositoryBasedTriggerProperties {
    private static final String NAME = "Bitbucket Cloud repository trigger";
    private static final String MODULE_KEY = "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-bitbucket:bbcTrigger";

    private static final AtlassianModuleProperties MODULE = new AtlassianModuleProperties(MODULE_KEY);

    @SuppressWarnings("unused")
    private BitbucketCloudTriggerProperties() {
        super(NAME, null, true, Collections.emptySet(), RepositoryBasedTrigger.TriggeringRepositoriesType.ALL, Collections.emptyList());
    }

    /**
     * Deprecated. Use {@link BitbucketCloudTriggerProperties#BitbucketCloudTriggerProperties(String, String, boolean, Set, RepositoryBasedTrigger.TriggeringRepositoriesType, List)}
     * @deprecated since 10.0
     */
    @Deprecated
    public BitbucketCloudTriggerProperties(final String description,
                                           final boolean isEnabled,
                                           final Set<TriggerConditionProperties> conditions,
                                           final RepositoryBasedTrigger.TriggeringRepositoriesType triggeringRepositoriesType,
                                           final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(NAME, description, isEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
        validate();
    }

    public BitbucketCloudTriggerProperties(final String name,
                                           final String description,
                                           final boolean isEnabled,
                                           final Set<TriggerConditionProperties> conditions,
                                           final RepositoryBasedTrigger.TriggeringRepositoriesType triggeringRepositoriesType,
                                           final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(StringUtils.defaultIfBlank(name, NAME), description, isEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE;
    }
}
