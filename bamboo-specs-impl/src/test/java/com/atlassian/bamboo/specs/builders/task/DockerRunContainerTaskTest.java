package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.bamboo.specs.api.builders.docker.DockerConstants.DEFAULT_CONTAINER_MAPPING;
import static com.atlassian.bamboo.specs.api.builders.docker.DockerConstants.DEFAULT_HOST_MAPPING;
import static com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties.DEFAULT_SERVICE_TIMEOUT;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DockerRunContainerTaskTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String IMAGE_NAME = "registry.address:port/some/namespace/image:latest";
    private static final String CONTAINER_NAME = "containerName";
    private static final String SERVICE_URL_PATTERN = "http://service-url.org";
    private static final String CONTAINER_ENVIRONMENT_VARIABLES = "CONTAINER_JAVA_HOME=/opt/java4";
    private static final String CONTAINER_WORKING_DIRECTORY = "/tmp";
    private static final String CONTAINER_COMMAND = "/bin/bash";
    private static final String ADDITIONAL_ARGUMENTS = "--memory=64m";

    private static final String ETC = "/etc";
    private static final String ETC2 = "/etc2";
    private static final String HOME = "/home";
    private static final String HOME2 = "/home2";

    @Test
    public void testMinimal() throws Exception {
        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .build();

        assertThat(properties.getImageName(), equalTo(IMAGE_NAME));
        assertThat(properties.isDetachedContainer(), equalTo(false));
        assertThat(properties.getContainerName(), isEmptyOrNullString());
        assertThat(properties.getPortMappings(), equalTo(Collections.emptyMap()));
        assertThat(properties.isWaitToStart(), equalTo(false));
        assertThat(properties.getServiceURLPattern(), isEmptyOrNullString());
        assertThat(properties.getServiceTimeout(), equalTo(DEFAULT_SERVICE_TIMEOUT));
        assertThat(properties.isLinkToDetachedContainers(), equalTo(false));
        assertThat(properties.getContainerEnvironmentVariables(), isEmptyOrNullString());
        assertThat(properties.getContainerCommand(), isEmptyOrNullString());
        assertThat(properties.getContainerWorkingDirectory(), equalTo(DEFAULT_CONTAINER_MAPPING));
        assertThat(properties.getAdditionalArguments(), isEmptyOrNullString());
        assertThat(properties.getVolumeMappings(), hasEntry(DEFAULT_HOST_MAPPING, DEFAULT_CONTAINER_MAPPING));
    }

    @Test
    public void testTimeoutWithDuration() throws Exception {
        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .detachContainer(true)
                .containerName(CONTAINER_NAME)
                .serviceURLPattern(SERVICE_URL_PATTERN)
                .appendPortMapping(15432, 5432)
                .waitToStart(true)
                .serviceTimeout(Duration.ofHours(1))
                .build();

        assertThat(properties.getImageName(), equalTo(IMAGE_NAME));
        assertThat(properties.isDetachedContainer(), equalTo(true));
        assertThat(properties.getServiceURLPattern(), equalTo(SERVICE_URL_PATTERN));
        assertThat(properties.getContainerName(), equalTo(CONTAINER_NAME));
        assertThat(properties.getPortMappings(), hasEntry(15432, 5432));
        assertThat(properties.isWaitToStart(), equalTo(true));
        assertThat(properties.getServiceTimeout(), equalTo(3600L));
    }

    @Test
    public void testFull() throws Exception {
        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .detachContainer(true)
                .containerName(CONTAINER_NAME)
                .appendPortMapping(18080, 8080)
                .appendPortMapping(15432, 5432)
                .waitToStart(true)
                .serviceURLPattern(SERVICE_URL_PATTERN)
                .serviceTimeoutInSeconds(150L)
                .linkToDetachedContainers(true)
                .containerEnvironmentVariables(CONTAINER_ENVIRONMENT_VARIABLES)
                .containerWorkingDirectory(CONTAINER_WORKING_DIRECTORY)
                .containerCommand(CONTAINER_COMMAND)
                .additionalArguments(ADDITIONAL_ARGUMENTS)
                .clearVolumeMappings()
                .appendVolumeMapping(ETC, ETC2)
                .appendVolumeMapping(HOME, HOME2)
                .build();

        assertThat(properties.getImageName(), equalTo(IMAGE_NAME));
        assertThat(properties.isDetachedContainer(), equalTo(true));
        assertThat(properties.getContainerName(), equalTo(CONTAINER_NAME));
        assertThat(properties.getPortMappings(), allOf(
                hasEntry(18080, 8080),
                hasEntry(15432, 5432)));
        assertThat(properties.isWaitToStart(), equalTo(true));
        assertThat(properties.getServiceURLPattern(), equalTo(SERVICE_URL_PATTERN));
        assertThat(properties.getServiceTimeout(), equalTo(150L));
        assertThat(properties.isLinkToDetachedContainers(), equalTo(true));
        assertThat(properties.getContainerEnvironmentVariables(), equalTo(CONTAINER_ENVIRONMENT_VARIABLES));
        assertThat(properties.getContainerCommand(), equalTo(CONTAINER_COMMAND));
        assertThat(properties.getContainerWorkingDirectory(), equalTo(CONTAINER_WORKING_DIRECTORY));
        assertThat(properties.getAdditionalArguments(), equalTo(ADDITIONAL_ARGUMENTS));
        assertThat(properties.getVolumeMappings(), allOf(hasEntry(ETC, ETC2), hasEntry(HOME, HOME2)));
    }

    @Test
    public void testValidateNoImageName() throws Exception {
        expectedException.expectMessage("Argument imageName can not be null.");

        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(null)
                .build();
    }

    @Test
    public void testValidateDuplicatedPorts() throws Exception {
        expectedException.expectMessage("Port 30 is already defined.");

        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .appendPortMapping(10, 30)
                .appendPortMapping(20, 30)
                .build();
    }

    @Test
    public void testValidateDuplicatedVolumes() throws Exception {
        expectedException.expectMessage("Volume /data is already defined.");

        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .appendVolumeMapping("/home", "/data")
                .appendVolumeMapping("/etc", "/data")
                .build();
    }

    @Test
    public void testValidateInvalidPorts() throws Exception {
        expectedException.expectMessage("Specified port mappings are invalid: [[-1,2], [10,65536]]");

        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .appendPortMapping(-1, 2)
                .appendPortMapping(10, 65536)
                .build();
    }

    @Test
    public void testValidateInvalidVolumes() throws Exception {
        expectedException.expectMessage("Specified volume mappings are invalid: [[/home, ], [, /data]]");

        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .clearVolumeMappings()
                .appendVolumeMapping("/home", "")
                .appendVolumeMapping("", "/data")
                .build();
    }

    @Test
    public void testValidateDetachedContainerNoContainerName() throws Exception {
        expectedException.expectMessage("Argument containerName can not be null.");

        DockerRunContainerTaskProperties properties = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .detachContainer(true)
                .build();
    }

    @Parameters(method = "waitToStartParameters")
    @Test
    public void testValidateWaitToStart(String expectedErrorMessage, long timeout, String urlPattern, Map<Integer, Integer> portMappings) throws Exception {
        expectedException.expectMessage(expectedErrorMessage);

        DockerRunContainerTask task = new DockerRunContainerTask()
                .imageName(IMAGE_NAME)
                .waitToStart(true)
                .serviceTimeoutInSeconds(timeout)
                .serviceURLPattern(urlPattern);

        for (Map.Entry<Integer, Integer> entry : portMappings.entrySet()) {
            task.appendPortMapping(entry.getKey(), entry.getValue());
        }

        DockerRunContainerTaskProperties properties = task.build();
    }

    public Object[] waitToStartParameters() {
        return new Object[]{
                new Object[]{
                        "Argument serviceURLPattern can not be null.", 150, null, ImmutableMap.of(10, 10)
                },
                new Object[]{
                        "Argument serviceTimeout must be a positive number.", -20, "http://localhost", ImmutableMap.of(10, 10)
                },
                new Object[]{
                        "Argument serviceTimeout must be a positive number.", 0, "http://localhost", ImmutableMap.of(10, 10)
                },
                new Object[]{
                        "No port mappings defined", 20, "http://localhost", Collections.emptyMap()
                }

        };
    }
}