package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.CronExpressionClientSideValidator;
import com.atlassian.bamboo.specs.codegen.emitters.trigger.ScheduledTriggerEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

@Immutable
@CodeGenerator(ScheduledTriggerEmitter.class)
public final class ScheduledTriggerProperties extends TriggerProperties {
    private static final String NAME = "Scheduled";
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:schedule";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));

    public enum Container {DEPLOYMENT, PLAN}

    private final String cronExpression;
    private final String artifactBranch;
    private final boolean skipIfLatestVersionAlreadyDeployed;

    //required for code generator to produce different builders from same properties class
    @SkipCodeGen
    private final Container container;

    @SuppressWarnings("unused")
    private ScheduledTriggerProperties() {
        super(NAME, null, true, Collections.emptySet());
        this.cronExpression = "0 0 0 ? * *";
        this.artifactBranch = null;
        container = Container.PLAN;
        skipIfLatestVersionAlreadyDeployed = false;
    }

    /**
     * Deprecated. Use {@link ScheduledTriggerProperties#ScheduledTriggerProperties(String, String, boolean, Set, String, String, boolean, Container)}
     * @deprecated since 10.0
     */
    @Deprecated
    public ScheduledTriggerProperties(final String description,
                                      final boolean isEnabled,
                                      final Set<TriggerConditionProperties> conditions,
                                      @NotNull final String cronExpression,
                                      @Nullable final String artifactBranch,
                                      boolean skipIfLatestVersionAlreadyDeployed,
                                      @NotNull Container container) throws PropertiesValidationException {
        super(NAME, description, isEnabled, conditions);
        this.cronExpression = cronExpression.trim();
        this.artifactBranch = artifactBranch;
        this.container = container;
        this.skipIfLatestVersionAlreadyDeployed = skipIfLatestVersionAlreadyDeployed;
        validate();
    }

    public ScheduledTriggerProperties(final String name,
                                      final String description,
                                      final boolean isEnabled,
                                      final Set<TriggerConditionProperties> conditions,
                                      @NotNull final String cronExpression,
                                      @Nullable final String artifactBranch,
                                      boolean skipIfLatestVersionAlreadyDeployed,
                                      @NotNull Container container) throws PropertiesValidationException {
        super(StringUtils.defaultIfBlank(name, NAME), description, isEnabled, conditions);
        this.cronExpression = cronExpression.trim();
        this.artifactBranch = artifactBranch;
        this.container = container;
        this.skipIfLatestVersionAlreadyDeployed = skipIfLatestVersionAlreadyDeployed;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public String getArtifactBranch() {
        return artifactBranch;
    }

    public Container getContainer() {
        return container;
    }

    public boolean isSkipIfLatestVersionAlreadyDeployed() {
        return skipIfLatestVersionAlreadyDeployed;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        CronExpressionClientSideValidator.validate(cronExpression);
    }


    @Override
    public EnumSet<Applicability> applicableTo() {
        return container == Container.PLAN
                ? EnumSet.of(Applicability.PLANS)
                : EnumSet.of(Applicability.DEPLOYMENTS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ScheduledTriggerProperties that = (ScheduledTriggerProperties) o;
        return Objects.equals(cronExpression, that.cronExpression) &&
                Objects.equals(artifactBranch, that.artifactBranch) &&
                skipIfLatestVersionAlreadyDeployed == that.skipIfLatestVersionAlreadyDeployed &&
                container == that.container;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cronExpression, artifactBranch, skipIfLatestVersionAlreadyDeployed, container);
    }
}
