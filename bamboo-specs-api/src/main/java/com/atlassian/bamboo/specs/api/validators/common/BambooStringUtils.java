package com.atlassian.bamboo.specs.api.validators.common;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class BambooStringUtils {
    public static final char CHAR_APOSTROPHE = '\'';
    public static final char CHAR_BACKSLASH = '\\';
    public static final char CHAR_DOUBLE_QUOTE = '"';

    public static final char[] XSS_RELATED_CHARACTERS = {CHAR_DOUBLE_QUOTE, '&', CHAR_APOSTROPHE, '<', '>', CHAR_BACKSLASH};

    // pattern for XSS_RELATED_CHARACTERS
    public static final String XSS_RELATED_CHARACTERS_FIND_STR = "" + CHAR_APOSTROPHE + CHAR_DOUBLE_QUOTE + CHAR_BACKSLASH + "\\<\\>";

    // for validation repository data, CHAR_BACKSLASH could be used in Windows paths and user names (domain\\username)
    public static final char[] RELAXED_XSS_RELATED_CHARACTERS = {CHAR_DOUBLE_QUOTE, '&', CHAR_APOSTROPHE, '<', '>'};

    public static final char[] SHELL_INJECTION_RELATED_CHARACTERS = {CHAR_DOUBLE_QUOTE, '&', CHAR_APOSTROPHE, '>', '<', ';', '`'};
    public static final String SHELL_INJECTION_DOLLAR_PARENTHESIS = "$(";

    private BambooStringUtils() {
    }

    public static boolean containsRelatedCharacters(@Nullable final String string, @NotNull char[] characters) {
        return (string != null) && (StringUtils.containsAny(string, characters));
    }

    /**
     * Returns true if the supplied string contains characters that could be used to deface a page
     * or trigger an XSS exploit (characters: "{@literal &}'&lt;&gt;\).
     *
     * @param string string to check
     * @return true if string contains unsafe characters, false otherwise
     */
    public static boolean containsXssRelatedCharacters(@Nullable final String string) {
        return containsRelatedCharacters(string, XSS_RELATED_CHARACTERS);
    }

    /**
     * Relaxed xss check that allows \ character (mainly for repositories on Windows).
     *
     * @param string string to check
     * @return true if string contains unsafe characters, false otherwise
     */
    public static boolean containsRelaxedXssRelatedCharacters(@Nullable final String string) {
        return containsRelatedCharacters(string, RELAXED_XSS_RELATED_CHARACTERS);
    }

    public static boolean containsShellInjectionRelatedCharacters(@Nullable final String string) {
        return containsRelatedCharacters(string, SHELL_INJECTION_RELATED_CHARACTERS)
                || (string != null && string.contains(SHELL_INJECTION_DOLLAR_PARENTHESIS));
    }

}
