package com.atlassian.bamboo.specs.util;

import org.apache.http.HttpHeaders;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.protocol.BasicHttpContext;

/**
 * User name and password authentication provider.
 * @deprecated since 7.1.0, use {@link TokenCredentials} instead
 */
@Deprecated
public interface UserPasswordCredentials extends AuthenticationProvider {

    String getUsername();

    String getPassword();

    @Override
    default void authenticate(final HttpRequestBase request) throws AuthenticationException {
        final CredentialsProvider credsProvider = new BasicCredentialsProvider();
        final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(this.getUsername(), this.getPassword());
        credsProvider.setCredentials(new AuthScope(request.getURI().getHost(), 443), credentials);
        request.addHeader(new BasicScheme().authenticate(credentials, request, new BasicHttpContext()));
        request.addHeader(HttpHeaders.ACCEPT, "application/json");
    }

}
