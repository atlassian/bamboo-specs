package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.model.permission.ProjectPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Entity representing permissions for project.
 * Note that this object needs to be published separately from associated {@link Project}
 * Pre-existing permissions that are not defined in associated {@link Permissions} object are <b>revoked</b> when this object is published.
 */
public class ProjectPermissions extends RootEntityPropertiesBuilder<ProjectPermissionsProperties> {
    public static final String TYPE = "project permissions";

    @NotNull
    private final BambooKey projectKey;
    private Permissions projectPermissions = new Permissions();
    private Permissions projectPlanPermissions = new Permissions();
    private List<VcsRepositoryIdentifierProperties> specsRepositories = new ArrayList<>();


    public ProjectPermissions(final BambooKey projectKey) {
        checkNotNull("projectKey", projectKey);
        this.projectKey = projectKey;
    }

    public ProjectPermissions(final String projectKey) {
        checkNotNull("projectKey", projectKey);
        this.projectKey = new BambooKey(projectKey);
    }

    public ProjectPermissions projectPermissions(@NotNull final Permissions projectPermissions) {
        checkNotNull("projectPermissions", projectPermissions);
        this.projectPermissions = projectPermissions;
        return this;
    }

    public ProjectPermissions projectPlanPermissions(@NotNull final Permissions projectPlanPermissions) {
        checkNotNull("projectPlanPermissions", projectPlanPermissions);
        this.projectPlanPermissions = projectPlanPermissions;
        return this;
    }

    public ProjectPermissions specsRepositories(@NotNull String... repositoriesNames) {
        checkNotNull("repositoriesNames", repositoriesNames);
        Arrays.stream(repositoriesNames)
                .map(VcsRepositoryIdentifier::new)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.specsRepositories::add);
        return this;
    }

    @NotNull
    public BambooKey getProjectKey() {
        return projectKey;
    }

    public Permissions getProjectPermissions() {
        return projectPermissions;
    }

    public Permissions getProjectPlanPermissions() {
        return projectPlanPermissions;
    }

    @Override
    protected ProjectPermissionsProperties build() {
        return new ProjectPermissionsProperties(
                projectKey,
                projectPermissions,
                projectPlanPermissions,
                specsRepositories);
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        return String.format("%s for project %s", TYPE, projectKey);
    }

}
