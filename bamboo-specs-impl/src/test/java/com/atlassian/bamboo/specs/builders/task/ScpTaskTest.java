package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.ScpTaskProperties;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class ScpTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Rule
    public TemporaryFolder sourceDirectory = new TemporaryFolder();

    @Test
    public void testEquals() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new ScpTask()
                        .description("My SCP task")
                        .enabled(true)
                        .host("1.1.1.1")
                        .username("myusername")
                        .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                        .fromLocalPath("/local/from")
                        .toRemotePath("/remote/to")
                        .hostFingerprint("hostFingerprint")
                        .port(23)
                        .requirements(requirement)
                        .build(),
                equalTo(new ScpTaskProperties("My SCP task",
                        true,
                        "1.1.1.1",
                        "myusername",
                        SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE,
                        null,
                        "mykey",
                        "mypassphrase",
                        null,
                        "hostFingerprint",
                        23,
                        null,
                        "/local/from",
                        false,
                        "/remote/to",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList())));
    }

    @Test
    public void testEqualsWithMultipleHosts() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new ScpTask()
                        .description("My SCP task")
                        .enabled(true)
                        .host("1.1.1.1", "1.1.1.2", "1.1.1.3")
                        .username("myusername")
                        .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                        .fromLocalPath("/local/from")
                        .toRemotePath("/remote/to")
                        .hostFingerprint("hostFingerprint")
                        .port(23)
                        .requirements(requirement)
                        .build(),
                equalTo(new ScpTaskProperties("My SCP task",
                        true,
                        "1.1.1.1, 1.1.1.2, 1.1.1.3",
                        "myusername",
                        SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE,
                        null,
                        "mykey",
                        "mypassphrase",
                        null,
                        "hostFingerprint",
                        23,
                        null,
                        "/local/from",
                        false,
                        "/remote/to",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList())));
    }

    @Test
    public void testNotEqualsLocalPath() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new ScpTask()
                        .description("My SCP task")
                        .enabled(true)
                        .host("1.1.1.1")
                        .username("myusername")
                        .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                        .fromLocalPath("/local/from/1")
                        .toRemotePath("/remote/to")
                        .hostFingerprint("hostFingerprint")
                        .port(23)
                        .requirements(requirement)
                        .build(),
                not(equalTo(new ScpTaskProperties("My SSH task",
                        true,
                        "1.1.1.1",
                        "myusername",
                        SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE,
                        null,
                        "mykey",
                        "mypassphrase",
                        null,
                        "hostFingerprint",
                        23,
                        null,
                        "/local/from/2",
                        false,
                        "/remote/to",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))));
    }

    @Test
    public void testAuthenticationWithPassword() throws Exception {
        final ScpTask scpTask = new ScpTask()
                .description("My SCP task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithPassword("mypassword")
                .fromLocalPath("/local/from")
                .toRemotePath("/remote/to");
        assertThat(scpTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.PASSWORD));
        assertThat(scpTask.build().getPassword(), equalTo("mypassword"));
    }

    @Test
    public void testAuthenticationWithKeyWithPassphrase() throws Exception {
        final ScpTask scpTask = new ScpTask()
                .description("My SCP task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                .fromLocalPath("/local/from")
                .toRemotePath("/remote/to");
        assertThat(scpTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE));
        assertThat(scpTask.build().getKey(), equalTo("mykey"));
        assertThat(scpTask.build().getPassphrase(), equalTo("mypassphrase"));
    }

    @Test
    public void testAuthenticationWithKeyNoPassphrase() throws Exception {
        final ScpTask scpTask = new ScpTask()
                .description("My SCP task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKey("mykey")
                .fromLocalPath("/local/from")
                .toRemotePath("/remote/to");
        assertThat(scpTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE));
        assertThat(scpTask.build().getKey(), equalTo("mykey"));
        assertThat(scpTask.build().getPassphrase(), is(nullValue()));
    }


    @Test
    public void testAuthenticationWithKeyFromFile() throws Exception {
        final File fileWithKey = sourceDirectory.newFile("testFileWithKey.key");
        Files.write(fileWithKey.toPath(), "KKK-EEE-YYY".getBytes());

        final ScpTask scpTask = new ScpTask()
                .description("My SCP task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKey(fileWithKey.toPath())
                .fromLocalPath("/local/from")
                .toRemotePath("/remote/to");
        assertThat(scpTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE));
        assertThat(scpTask.build().getKey(), equalTo("KKK-EEE-YYY"));
        assertThat(scpTask.build().getPassphrase(), is(nullValue()));
    }

    @Test
    public void testAuthenticationWithKeyFromFileWhenFileDoesntExist() throws Exception {
        final Path pathToNonExistingKey = new File("/path/to/nonexisting/key").toPath();

        assertThat(Files.exists(pathToNonExistingKey), equalTo(false));

        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("File %s does not exist", pathToNonExistingKey));

        new ScpTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKey(pathToNonExistingKey)
                .fromLocalPath("/local/from")
                .toRemotePath("/remote/to");
    }

    @Test
    public void testArtifactAsStringOneArtifact() throws Exception {
        final ScpTask scpTaskWithOneArtifact = scpTaskWithBasicAuth()
                .fromArtifact("PROJ-PLAN: my artifact");
        final ArtifactItem oneArtifact = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("PROJ", "PLAN"))
                .artifact("my artifact");
        assertThat(scpTaskWithOneArtifact.build().getArtifactItem(),
                equalTo(EntityPropertiesBuilders.build(oneArtifact)));
    }

    @Test
    public void testArtifactAsStringOneArtifactWithColon() throws Exception {
        final ScpTask scpTaskWithColonInArtifact = scpTaskWithBasicAuth()
                .fromArtifact("PROJ-PLAN: my artifact : with colon");
        final ArtifactItem oneArtifactWithColon = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("PROJ", "PLAN"))
                .artifact("my artifact : with colon");
        assertThat(scpTaskWithColonInArtifact.build().getArtifactItem(),
                equalTo(EntityPropertiesBuilders.build(oneArtifactWithColon)));
    }

    @Test
    public void testArtifactAsStringAllArtifacts() throws Exception {
        final ScpTask scpTaskWithAllArtifacts = scpTaskWithBasicAuth()
                .fromArtifact("PROJ-PLAN: all artifacts");
        final ArtifactItem allArtifacts = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("PROJ", "PLAN"))
                .allArtifacts();
        assertThat(scpTaskWithAllArtifacts.build().getArtifactItem(),
                equalTo(EntityPropertiesBuilders.build(allArtifacts)));
    }

    @Test
    public void testArtifactAsStringNoPlanKey() throws Exception {
        final ScpTask scpTaskWithAllArtifacts = scpTaskWithBasicAuth()
                .fromArtifact("my artifact");
        final ArtifactItem allArtifacts = new ArtifactItem()
                .artifact("my artifact");
        assertThat(scpTaskWithAllArtifacts.build().getArtifactItem(),
                equalTo(EntityPropertiesBuilders.build(allArtifacts)));
    }

    private ScpTask scpTaskWithBasicAuth() {
        return new ScpTask()
                .host("1.1.1.1")
                .username("user")
                .authenticateWithPassword("mypassword")
                .toRemotePath("/remote/to");
    }

    @Test
    public void testValidationNoArtifactNoLocalPath() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("SCP task / artifact name or local path: artifact name or local path must be set");

        new ScpTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithPassword("password")
                .toRemotePath("/remote")
                .build()
                .validate();
    }

    @Test
    public void testValidationNoRemotePath() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("SCP task / remote path: Property is required."));

        new ScpTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithPassword("password")
                .fromLocalPath("/local")
                .build()
                .validate();
    }
}
