package com.atlassian.bamboo.specs.api.builders.plan.artifact;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;



public class ArtifactTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldThrowExceptionOnNullName() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Argument name can not be null.");
        final String artifactName = null;
        new Artifact(artifactName);
    }

    @Test
    public void shouldThrowExceptionOnNameWithSlash() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Name can not contain '/' character");
        final String artifactName = "MY-COMPANY/MY-APPLICATION";
        new Artifact(artifactName);
    }

    @Test
    public void shouldThrowExceptionOnNullCopyPattern(){
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Argument copyPatterns can not be null.");
        final String artifactName = "MY-APPLICATION";
        Artifact artifact = new Artifact(artifactName).copyPatterns(null);
    }

    @Test
    public void shouldThrowExceptionOnNullLocation() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Argument location can not be null.");
        final String artifactName = "MY-APPLICATION";
        Artifact artifact = new Artifact(artifactName).location(null);
    }

    @Test
    public void shouldNoThrowException() {
        final String artifactName = "MY-APPLICATION";
        Artifact artifact = new Artifact(artifactName).copyPatterns("**/*.jar").location("target");

    }

}