package com.atlassian.bamboo.specs.api.codegen.annotations;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Names builder class for entity properties.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Builder {
    Class<? extends EntityPropertiesBuilder> value();
}
