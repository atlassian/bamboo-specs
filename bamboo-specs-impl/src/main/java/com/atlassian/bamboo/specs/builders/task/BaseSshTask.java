package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.BaseSshTaskProperties;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotEmpty;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNegative;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public abstract class BaseSshTask<T extends BaseSshTask<T,E>, E extends BaseSshTaskProperties> extends Task<T, E> {

    protected List<String> hosts = new ArrayList<>();
    protected String username;
    protected SshTaskProperties.AuthenticationType authenticationType;
    @Nullable
    protected String password;
    @Nullable
    protected String key;
    @Nullable
    protected String passphrase;
    @Nullable
    protected String hostFingerprint;
    protected int port = BaseSshTaskProperties.DEFAULT_PORT;
    @Nullable
    protected SharedCredentialsIdentifierProperties sharedCredentials;

    /**
     * Hostname or IP address of the remote host.
     */
    public T host(@NotNull final String host) {
        checkNotEmpty("host", host);
        this.hosts.add(host);
        return (T) this;
    }

    /**
     * Hostnames or IP addresses of the remote hosts.
     */
    public T host(@NotNull final String... host) {
        final List<String> hosts = Arrays.asList(host);
        hosts.forEach(h -> checkNotEmpty("host", h));
        this.hosts.addAll(hosts);
        return (T) this;
    }

    /**
     * Username you want to use to access the remote host.
     */
    public T username(@NotNull final String username) {
        checkNotEmpty("username", username);
        this.username = username;
        return (T) this;
    }

    /**
     * Authenticate with password associated with username {@link BaseSshTask#username(String)}.
     */
    public T authenticateWithPassword(@NotNull final String password) {
        checkNotEmpty("password", password);
        this.authenticationType = SshTaskProperties.AuthenticationType.PASSWORD;
        this.password = password;
        return (T) this;
    }

    /**
     * Authenticate with key with passphrase.
     *
     * @param key        - SSH private key
     * @param passphrase - SSH passphrase
     */
    public T authenticateWithKeyWithPassphrase(@NotNull final String key, @NotNull final String passphrase) {
        checkNotEmpty("key", key);
        checkNotEmpty("passphrase", passphrase);
        this.authenticationType = SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE;
        this.key = key;
        this.passphrase = passphrase;
        return (T) this;
    }

    /**
     * Authenticate with key with passphrase.
     *
     * @param keyPath    - path to SSH private key
     * @param passphrase - SSH passphrase
     */
    public T authenticateWithKeyWithPassphrase(@NotNull final Path keyPath, @NotNull final String passphrase) {
        checkNotNull("key path", keyPath);
        return authenticateWithKeyWithPassphrase(keyFromPath(keyPath), passphrase);
    }

    /**
     * Authenticate with key (without passphrase).
     *
     * @param key - SSH private key
     */
    public T authenticateWithKey(@NotNull final String key) {
        checkNotEmpty("key", key);
        this.authenticationType = SshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE;
        this.key = key;
        return (T) this;
    }

    /**
     * Authenticate with key (without passphrase).
     *
     * @param keyPath - path to SSH private key
     */
    public T authenticateWithKey(@NotNull final Path keyPath) {
        checkNotNull("key path", keyPath);
        return authenticateWithKey(keyFromPath(keyPath));
    }

    public T authenticateWithSshSharedCredentials(@NotNull SharedCredentialsIdentifier sharedCredentials) {
        checkNotNull("shared credentials", sharedCredentials);
        this.sharedCredentials = EntityPropertiesBuilders.build(sharedCredentials);
        this.authenticationType = SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE;
        return (T) this;
    }

    public T authenticateWithUsernamePasswordSharedCredentials(@NotNull SharedCredentialsIdentifier sharedCredentials) {
        checkNotNull("shared credentials", sharedCredentials);
        this.sharedCredentials = EntityPropertiesBuilders.build(sharedCredentials);
        this.authenticationType = SshTaskProperties.AuthenticationType.PASSWORD;
        return (T) this;
    }

    private static String keyFromPath(@NotNull final Path keyPath) throws PropertiesValidationException {
        final File file = keyPath.toFile();
        if (!file.exists()) {
            throw new PropertiesValidationException(String.format("File %s does not exist", file.getAbsolutePath()));
        }
        try {
            return new String(Files.readAllBytes(keyPath));
        } catch (final IOException e) {
            throw new PropertiesValidationException(String.format("Error when key from path: %s", file.getAbsolutePath()), e);
        }
    }

    /**
     * Set public key fingerprint.
     */
    public T hostFingerprint(@NotNull final String hostFingerprint) {
        checkNotEmpty("host fingerprint", hostFingerprint);
        this.hostFingerprint = hostFingerprint;
        return (T) this;
    }


    /**
     * Set the port number of the remote host that is used for the SSH connection. The default value is 22.
     *
     * @see BaseSshTask#portDefault
     */
    public T port(final int port) {
        checkNotNegative("port", port);
        this.port = port;
        return (T) this;
    }


    /**
     * Set the port number of the remote host to the default value (22).
     *
     * @see BaseSshTask#portDefault
     */
    public T portDefault() {
        port(22);
        return (T) this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseSshTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BaseSshTask<?, ?> that = (BaseSshTask<?, ?>) o;
        return port == that.port &&
                Objects.equals(hosts, that.hosts) &&
                Objects.equals(username, that.username) &&
                authenticationType == that.authenticationType &&
                Objects.equals(password, that.password) &&
                Objects.equals(key, that.key) &&
                Objects.equals(passphrase, that.passphrase) &&
                Objects.equals(hostFingerprint, that.hostFingerprint) &&
                Objects.equals(sharedCredentials, that.sharedCredentials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), hosts, username, authenticationType, password, key, passphrase, hostFingerprint, port, sharedCredentials);
    }
}
