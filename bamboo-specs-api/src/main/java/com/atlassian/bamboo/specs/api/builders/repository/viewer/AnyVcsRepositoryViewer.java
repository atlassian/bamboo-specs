package com.atlassian.bamboo.specs.api.builders.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.AnyVcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a vcs repository viewer of any type.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation of a given credential type is not available.
 */
public class AnyVcsRepositoryViewer extends VcsRepositoryViewer {
    private final AtlassianModuleProperties atlassianPlugin;
    private Map<String, Object> configuration;

    /**
     * Specifies a repository viewer of given type.
     *
     * @param atlassianPlugin type of the viewer identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyVcsRepositoryViewer(@NotNull String atlassianPlugin) throws PropertiesValidationException {
        checkNotNull("atlassianPlugin", atlassianPlugin);
        this.atlassianPlugin = EntityPropertiesBuilders.build(new AtlassianModule(atlassianPlugin));
    }

    /**
     * Specifies a repository viewer of given type.
     *
     * @param atlassianPlugin type of the viewer identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyVcsRepositoryViewer(@NotNull AtlassianModule atlassianPlugin) throws PropertiesValidationException {
        checkNotNull("atlassianPlugin", atlassianPlugin);
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Specifies a repository viewer of given type.
     *
     * @param atlassianPlugin type of the viewer identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyVcsRepositoryViewer(@NotNull AtlassianModuleProperties atlassianPlugin) throws PropertiesValidationException {
        checkNotNull("atlassianPlugin", atlassianPlugin);
        this.atlassianPlugin = atlassianPlugin;
    }

    /**
     * Set configuration for the repository viewer.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is performed
     * on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyVcsRepositoryViewer configuration(@NotNull Map<String, Object> configuration) throws PropertiesValidationException {
        checkNotNull("configuration", configuration);
        this.configuration = MapUtils.copyOf(configuration);
        return this;
    }

    @NotNull
    protected AnyVcsRepositoryViewerProperties build() throws PropertiesValidationException {
        return new AnyVcsRepositoryViewerProperties(atlassianPlugin, configuration);
    }
}
