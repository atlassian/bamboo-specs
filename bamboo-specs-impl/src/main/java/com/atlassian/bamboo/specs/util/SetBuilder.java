package com.atlassian.bamboo.specs.util;

import java.util.LinkedHashSet;
import java.util.Set;

public class SetBuilder<T> {
    private final LinkedHashSet<T> set = new LinkedHashSet<>();

    public SetBuilder<T> put(T t) {
        set.add(t);
        return this;
    }

    public Set<T> build() {
        return set;
    }
}
