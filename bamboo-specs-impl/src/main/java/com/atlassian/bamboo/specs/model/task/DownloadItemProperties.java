package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@Immutable
public class DownloadItemProperties implements EntityProperties {
    @Setter("artifact")
    private final String artifactName;
    private final boolean allArtifacts;
    private final String path;

    private DownloadItemProperties() {
        artifactName = null;
        path = "";
        allArtifacts = false;
    }

    public DownloadItemProperties(final String artifactName, boolean allArtifacts, final String path) {
        this.artifactName = artifactName;
        this.allArtifacts = allArtifacts;
        this.path = path;
        validate();
    }

    public String getArtifactName() {
        return artifactName;
    }

    public boolean isAllArtifacts() {
        return allArtifacts;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DownloadItemProperties that = (DownloadItemProperties) o;
        return isAllArtifacts() == that.isAllArtifacts() &&
                Objects.equals(getArtifactName(), that.getArtifactName()) &&
                Objects.equals(getPath(), that.getPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getArtifactName(), isAllArtifacts(), getPath());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Artifact downloader task").with("Download spec");
        if (!allArtifacts && StringUtils.isBlank(artifactName)) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(context, "Artifact must be specified")));
        }
        if (allArtifacts && StringUtils.isNotBlank(artifactName)) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(context, "Cannot pick specific artifacts when all artifacts is selected")));
        }
        checkRequired(context.with("path"), path);
    }
}
