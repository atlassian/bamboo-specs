package com.atlassian.bamboo.specs.api.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public final class MapUtils {
    private MapUtils() {
    }

    /**
     * Creates a copy of a map with deterministic order.
     */
    public static <K extends Comparable<K>, V> @NotNull Map<K, V> copyOf(@Nullable final Map<K, V> map) {
        if (map == null) {
            return new LinkedHashMap<>();
        } else if (map instanceof LinkedHashMap || map instanceof TreeMap) {
            return new LinkedHashMap<>(map); //known maps with deterministic order
        } else {
            return new TreeMap<>(map); //unknown iteration order: use natural sort
        }
    }
}
