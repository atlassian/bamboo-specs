package com.atlassian.bamboo.specs.api.builders.repository;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

/**
 * Links repository to a plan. Repositories can be added to plan in two ways: as a link to a global repository or as plan-managed repository definition.
 * <p>
 * In the first case repository is managed independently from plan and needs to exist before it can be added to plan. Such repository can be used in many plans.
 * On the other hand, plan-managed repository's lifecycle is tied to plan life-cycle. In other words, they are updated (and deleted) whenever plan is updated and can only
 * be used by plan they are linked to.
 * <p>
 * Plan-managed repository definition can <i>inherit</i> configuration from global repositories.
 */
public class PlanRepositoryLink extends EntityPropertiesBuilder<PlanRepositoryLinkProperties> {
    /**
     * Helper class to represent link to global repositories.
     * <p>
     * When creating a link to a global repository Bamboo in fact creates a plan-managed repository that inherits all its data from
     * the global repository. This class represents this case.
     */
    private static class LinkedGlobalRepository extends VcsRepository<PlanRepositoryLink.LinkedGlobalRepository, PlanRepositoryLinkProperties.LinkedGlobalRepository> {
        private AtlassianModuleProperties atlassianPlugin;

        LinkedGlobalRepository(@NotNull VcsRepositoryProperties globalRepository) {
            this.parent = globalRepository.getName();
            ImporterUtils.checkArgument(ValidationContext.of("Linked global repository"),
                    !globalRepository.hasParent(), "Global repository should not have parent");
            this.atlassianPlugin = globalRepository.getAtlassianPlugin();
        }

        LinkedGlobalRepository(@NotNull VcsRepositoryIdentifierProperties globalRepository) {
            ImporterUtils.checkNotNull("parent", globalRepository);
            this.parent = globalRepository.getName();
        }

        @Override
        protected PlanRepositoryLinkProperties.LinkedGlobalRepository build() {
            return new PlanRepositoryLinkProperties.LinkedGlobalRepository(parent, atlassianPlugin);
        }
    }

    private static class ProjectRepository extends VcsRepository<ProjectRepository, PlanRepositoryLinkProperties.ProjectRepository> {
        private AtlassianModuleProperties atlassianPlugin;

        ProjectRepository(@NotNull VcsRepositoryProperties projectRepository) {
            this.parent = projectRepository.getParent();
            ImporterUtils.checkArgument(ValidationContext.of("Project repository"),
                    !projectRepository.hasParent(), "Project repository should not have parent");
            this.project = projectRepository.getProject();
            ImporterUtils.checkArgument(ValidationContext.of("Project repository"),
                    projectRepository.getProject() != null, "Project repository should have project");
            this.atlassianPlugin = projectRepository.getAtlassianPlugin();
        }

        ProjectRepository(@NotNull VcsRepositoryIdentifierProperties projectRepository) {
            ImporterUtils.checkNotNull("parent", projectRepository);
            this.parent = projectRepository.getName();
        }

        @Override
        protected PlanRepositoryLinkProperties.ProjectRepository build() throws PropertiesValidationException {
            return new PlanRepositoryLinkProperties.ProjectRepository(parent, atlassianPlugin, project);
        }
    }

    /**
     * Specifies a link to existing global repository.
     */
    public static PlanRepositoryLink linkToGlobalRepository(@NotNull VcsRepository<?,?> globalRepository) {
        return new PlanRepositoryLink().globalRepository(globalRepository);
    }

    /**
     * Specifies a link to existing global repository.
     */
    public static PlanRepositoryLink linkToGlobalRepository(@NotNull VcsRepositoryIdentifier globalRepository) {
        return new PlanRepositoryLink().localRepositoryDefinition(new LinkedGlobalRepository(globalRepository.build()));
    }

    /**
     * Specifies a link to existing project repository.
     */
    public static PlanRepositoryLink linkToProjectRepository(@NotNull VcsRepositoryIdentifier projectRepository) {
        return new PlanRepositoryLink().localRepositoryDefinition(new ProjectRepository(projectRepository.build()));
    }

    private VcsRepositoryProperties repositoryDefinition;

    /**
     * Specifies a link to a plan-managed repository. Repository is created or updated whenever this plan is saved.
     */
    public PlanRepositoryLink localRepositoryDefinition(@NotNull VcsRepository<?,?> repositoryDefinition) {
        ImporterUtils.checkNotNull("repositoryDefinition", repositoryDefinition);
        this.repositoryDefinition = repositoryDefinition.build();
        return this;
    }

    /**
     * Specifies a link to existing global repository. Global repository is managed independently from the plan and must exists.
     */
    public PlanRepositoryLink globalRepository(@NotNull VcsRepository<?,?> repositoryDefinition) {
        ImporterUtils.checkNotNull("repositoryDefinition", repositoryDefinition);
        this.repositoryDefinition = new LinkedGlobalRepository(repositoryDefinition.build()).build();
        return this;
    }

    /**
     * Specifies a link to existing project repository. Project repository is managed independently from the plan and must exists.
     */
    public PlanRepositoryLink projectRepository(@NotNull VcsRepository<?,?> repositoryDefinition) {
        ImporterUtils.checkNotNull("repositoryDefinition", repositoryDefinition);
        this.repositoryDefinition = new ProjectRepository(repositoryDefinition.build()).build();
        return this;
    }

    protected PlanRepositoryLinkProperties build() {
        return new PlanRepositoryLinkProperties(repositoryDefinition);
    }
}
