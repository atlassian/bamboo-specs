package com.atlassian.bamboo.specs.model.repository.github;

import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import com.atlassian.bamboo.specs.builders.repository.github.GitHubRepository;
import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.time.Duration;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class GitHubRepositoryPropertiesTest {
    @Test(expected = PropertiesValidationException.class)
    public void testValidation() {
        build(new GitHubRepository()
                .name("Repository name"));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateBranchShell() {
        build(new GitHubRepository()
                .name("Repository name")
                .repository("owner/name")
                .branch("a\"b")
        );
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateAccAuthNullUsername() {
        build(new GitHubRepository()
                .name("Repository name")
                .repository("owner/name")
                .authentication(new UserPasswordAuthentication(null))
                .branch("master")
        );
    }

    @Test
    public void testPublicRepository() {
        final GitHubRepositoryProperties result = build(new GitHubRepository()
                .name("Repository name")
                .repository("owner/name")
                .branch("master")
        );

        assertThat(result.getRepository(), equalTo("owner/name"));
        assertThat(result.getAuthenticationProperties(), nullValue());
        assertFalse(result.isWebhookEnabled());
    }

    @Test
    public void testOK() {
        final String name = "name";
        final String description = "description";
        final String branch = "branch";
        final String owner = "owner";
        final String repository = "repository";
        final String username = "username";
        final String key = "key";
        final String password = "password";
        final String projectKey = "ABC";
        final String projectName = "PROJECT";
        final Project project = new Project().key(projectKey).name(projectName);

        final GitHubRepositoryProperties properties = build(new GitHubRepository()
                .name(name)
                .description(description)
                .branch(branch)
                .project(project)
                .lfsEnabled(true)
                .submodulesEnabled(true)
                .submodulesUseShallowClonesEnabled(true)
                .fetchWholeRepository(true)
                .remoteAgentCacheEnabled(false)
                .shallowClonesEnabled(false)
                .verboseLogs(true)
                .webhookEnabled(true)
                .commandTimeoutInMinutes(500)
                .authentication(new UserPasswordAuthentication(username).password(password))
                .repository(owner + "/" + repository));

        assertThat(properties.getName(), equalTo(name));
        assertThat(properties.getDescription(), equalTo(description));
        assertThat(properties.getBranch(), equalTo(branch));
        assertThat(properties.isUseShallowClones(), equalTo(false));
        assertThat(properties.isUseRemoteAgentCache(), equalTo(false));
        assertThat(properties.isUseSubmodules(), equalTo(true));
        assertThat(properties.isUseSubmodulesWithShallowClones(), equalTo(true));
        assertThat(properties.getCommandTimeout(), equalTo(Duration.ofMinutes(500)));
        assertThat(properties.isVerboseLogs(), equalTo(true));
        assertThat(properties.isFetchWholeRepository(), equalTo(true));
        assertThat(properties.isUseLfs(), equalTo(true));
        assertThat(properties.isWebhookEnabled(), equalTo(true));
        MatcherAssert.assertThat(properties.getProject().getName(), is(projectName));
        MatcherAssert.assertThat(properties.getProject().getKey().getKey(), is(projectKey));
        assertThat(properties.getVcsChangeDetection(), nullValue());
        assertThat(properties.getRepository(), equalTo(owner + "/" + repository));
        assertThat(properties.getAuthenticationProperties(), equalTo(build(new UserPasswordAuthentication(username).password(password))));
    }
}
