package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.BambooKeyValidator;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.model.task.ArtifactItemProperties;
import com.atlassian.bamboo.specs.model.task.ScpTaskProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotEmpty;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents task that runs scp command.
 */
public class ScpTask extends BaseSshTask<ScpTask, ScpTaskProperties> {

    private static final ValidationContext SCP_TASK_CONTEXT = ValidationContext.of("SCP Task");
    private static final String ALL_ARTIFACTS = "all artifacts";
    private boolean localPathAntStyle = false;
    private String localPath;
    private ArtifactItemProperties artifactItem;
    private String remotePath;

    /**
     * The local path (relative to the Bamboo working directory) to the files you want to copy.
     * Use commas to separate files and directories.
     * You can also use Ant-style pattern matching to include multiple files, such as **\/target/*.jar.
     * Use {@link ScpTask#fromLocalPath(String, boolean)} to turn on Ant-style pattern matching.
     */
    public ScpTask fromLocalPath(final String localPath) {
        return fromLocalPath(localPath, false);
    }

    /**
     * The local path (relative to the Bamboo working directory) to the files you want to copy.
     * Use commas to separate files and directories.
     * You can also use Ant-style pattern matching to include multiple files, such as **\/target/*.jar.
     * Ant-style pattern matching is false by default.
     */
    public ScpTask fromLocalPath(final String localPath, final boolean antStyle) {
        checkNotEmpty(SCP_TASK_CONTEXT, "local path", localPath);
        this.localPath = localPath;
        this.localPathAntStyle = antStyle;
        return this;
    }

    /**
     * Name of the artifact to copy.
     * @deprecated since Bamboo 6.1.0, use {@link ScpTask#fromArtifact(ArtifactItem)}
     */
    @Deprecated
    public ScpTask fromArtifact(final String artifactName) {
        checkNotEmpty(SCP_TASK_CONTEXT, "artifact name", artifactName);
        return fromArtifact(parseArtifactItemString(artifactName));
    }

    /**
     * Artifact to copy.
     * @deprecated since Bamboo 6.1.0, use {@link ScpTask#fromArtifact(ArtifactItem)}
     */
    @Deprecated
    public ScpTask fromArtifact(final Artifact artifact) {
        checkNotNull(SCP_TASK_CONTEXT, "artifact", artifact);
        return fromArtifact(parseArtifactItemString(artifact.getName()));
    }

    /**
     * Artifact to copy.
     * @param artifactItem specifies which downloaded artifact shall be copied
     * @return ScpTask
     * @since Bamboo 6.1.0
     */
    public ScpTask fromArtifact(final ArtifactItem artifactItem) {
        checkNotNull(SCP_TASK_CONTEXT, "artifact", artifactItem);
        this.artifactItem = EntityPropertiesBuilders.build(artifactItem);
        return this;
    }

    /**
     * The path to the destination directory on the remote server.
     */
    public ScpTask toRemotePath(final String remotePath) {
        checkNotEmpty(SCP_TASK_CONTEXT, "remote path", remotePath);
        this.remotePath = remotePath;
        return this;
    }

    @NotNull
    @Override
    protected ScpTaskProperties build() {
        return new ScpTaskProperties(description,
                taskEnabled,
                String.join(", ", hosts),
                username,
                authenticationType,
                password,
                key,
                passphrase,
                sharedCredentials,
                hostFingerprint,
                port,
                artifactItem,
                localPath,
                localPathAntStyle,
                remotePath,
                requirements,
                conditions);
    }

    /**
     * Convert a full artifact name "PROJECT-PLAN: artifact" or a short artifact name "artifact" into an ArtifactItem.
     * @param fullName artifact name with project-plan keys
     * @return ArtifactItem
     */
    public static ArtifactItem parseArtifactItemString(final String fullName) {
        final String artifactName;
        final ArtifactItem item;

        // check if artifact name begins with "PROJECT-PLAN:" prefix
        final String planQualifierRegExp = BambooKeyValidator.KEY_REGEXP + "-" + BambooKeyValidator.KEY_REGEXP + ":.*";
        if (fullName.matches(planQualifierRegExp)) {
            // "PROJECT-PLAN: artifact"
            final String[] projectPlanArtifact = StringUtils.split(fullName, "-: ", 3);
            final String projectKey = projectPlanArtifact[0];
            final String planKey = projectPlanArtifact[1];
            artifactName = projectPlanArtifact[2];
            item = new ArtifactItem().sourcePlan(new PlanIdentifier(projectKey, planKey));
        } else {
            // "artifact"
            artifactName = fullName;
            item = new ArtifactItem();
        }

        if (ALL_ARTIFACTS.equals(artifactName)) {
            return item.allArtifacts();
        } else {
            return item.artifact(artifactName);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ScpTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ScpTask scpTask = (ScpTask) o;
        return localPathAntStyle == scpTask.localPathAntStyle &&
                Objects.equals(localPath, scpTask.localPath) &&
                Objects.equals(artifactItem, scpTask.artifactItem) &&
                Objects.equals(remotePath, scpTask.remotePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), localPathAntStyle, localPath, artifactItem, remotePath);
    }
}
