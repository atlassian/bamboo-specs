package com.atlassian.bamboo.specs.builders.credentials;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.model.credentials.SshCredentialsProperties;

/**
 * Represent shared SSH credentials.
 * Shared SSH credentials can be used when defining authorisation of VCS repositories (e.g. {@link GitRepository}) and in certain tasks.
 */
public class SshCredentials extends SharedCredentials<SshCredentials, SshCredentialsProperties> {
    private String key;
    private String passhphrase;

    /**
     * Create new SSH credentials.
     */
    public SshCredentials() {
    }

    /**
     * Create new SSH credentials with name.
     *
     * @param name name of the credentials.
     */
    public SshCredentials(final String name) {
        super(name);
    }

    /**
     * Set SSH private key.
     */
    public SshCredentials privateKey(final String key) {
        this.key = key;
        return this;
    }

    /**
     * Set passphrase to use to access SSH private key.
     */
    public SshCredentials passphrase(final String passhphrase) {
        this.passhphrase = passhphrase;
        return this;
    }

    @Override
    protected SshCredentialsProperties build() {
        return new SshCredentialsProperties(
                name,
                oid,
                key,
                passhphrase,
                project);
    }
}
