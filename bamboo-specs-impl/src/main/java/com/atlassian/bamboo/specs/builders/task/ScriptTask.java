package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotEmpty;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.util.FileUtils.readFileContent;

/**
 * Represents a task that executes shell script.
 */
public class ScriptTask extends Task<ScriptTask, ScriptTaskProperties> {
    private ScriptTaskProperties.Interpreter interpreter = ScriptTaskProperties.Interpreter.SHELL;
    private ScriptTaskProperties.Location location = ScriptTaskProperties.Location.INLINE;
    @Nullable
    private String body;
    @Nullable
    private String path;
    @Nullable
    private String argument;
    @Nullable
    private String environmentVariables;
    @Nullable
    private String workingSubdirectory;

    private boolean usePowershellErrorHandling;
    private boolean usePowershellErrorActionStop;

    /**
     * Specifies that task runs a script which is stored within this task's configuration.
     *
     * @see #location(ScriptTaskProperties.Location)
     */
    private void inline() {
        this.location = ScriptTaskProperties.Location.INLINE;
        this.path = null;
    }

    /**
     * Specifies that task runs a script form an external file.
     *
     * @see #location(ScriptTaskProperties.Location)
     */
    private void file() {
        this.location = ScriptTaskProperties.Location.FILE;
        this.body = null;
    }

    /**
     * Specifies the source of script to run. Possible options are:
     * <dl>
     * <dt>INLINE</dt>
     * <dd>task runs a script which is stored within this task's configuration</dd>
     * <dt>SCRIPT</dt>
     * <dd>task runs a script form an external file</dd>
     * </dl>
     */
    public ScriptTask location(final ScriptTaskProperties.Location location) {
        this.location = location;
        return this;
    }

    /**
     * Specifies that script will be run by an interpreter chosen based on the shebang line of the script.
     *
     * @see #interpreter(ScriptTaskProperties.Interpreter)
     */
    public ScriptTask interpreterShell() {
        return interpreter(ScriptTaskProperties.Interpreter.SHELL);
    }

    /**
     * Specifies that script will be run by Windows PowerShell.
     *
     * @see #interpreter(ScriptTaskProperties.Interpreter)
     */
    public ScriptTask interpreterWindowsPowerShell() {
        return interpreter(ScriptTaskProperties.Interpreter.WINDOWS_POWER_SHELL);
    }

    /**
     * Specifies that script will be run by /bin/sh.
     *
     * @see #interpreter(ScriptTaskProperties.Interpreter)
     */
    public ScriptTask interpreterBinSh() {
        return interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE);
    }

    /**
     * Selects that script should be run by cmd.exe.
     *
     * @see #interpreter(ScriptTaskProperties.Interpreter)
     */
    public ScriptTask interpreterCmdExe() {
        return interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE);
    }

    /**
     * Selects interpreter that will run the script. Possible choices are:
     * <dl>
     * <dt>SHELL</dt>
     * <dd>script will be run by an interpreter chosen based on the shebang line of the script</dd>
     * <dt>WINDOWS_POWER_SHELL</dt>
     * <dd>script will be run by Windows PowerShell</dd>
     * <dt>BINSH_OR_CMDEXE</dt>
     * <dd>script will be run by /bin/sh or cmd.exe, depending on the operating system</dd>
     * </dl>
     */
    public ScriptTask interpreter(final ScriptTaskProperties.Interpreter interpreter) {
        this.interpreter = interpreter;
        return this;
    }

    /**
     * Enables PowerShell error handling with $LastExitCode checking.
     * <p>
     * Applicable only when {@link ScriptTaskProperties.Interpreter#WINDOWS_POWER_SHELL} is the script interpreter.
     * </p>
     * @see #interpreter(ScriptTaskProperties.Interpreter)
     */
    public ScriptTask usePowershellErrorHandling(final boolean usePowershellErrorHandling) {
        this.usePowershellErrorHandling = usePowershellErrorHandling;
        return this;
    }

    /**
     * Sets PowerShell $ErrorActionPreference to 'Stop'.
     * <p>
     * Applicable only when {@link ScriptTaskProperties.Interpreter#WINDOWS_POWER_SHELL} is the script interpreter.
     * </p>
     * @see #interpreter(ScriptTaskProperties.Interpreter)
     */
    public ScriptTask usePowershellErrorActionStop(final boolean usePowershellErrorActionStop) {
        this.usePowershellErrorActionStop = usePowershellErrorActionStop;
        return this;
    }

    /**
     * Sets body of the script to execute. Only valid when {@link ScriptTaskProperties.Location#INLINE} script location is selected.
     */
    public ScriptTask inlineBody(@NotNull final String body) {
        checkNotEmpty("script inline body", body);
        inline();
        this.body = body;
        return this;
    }

    /**
     * Sets body of the script to execute from a file. Only valid when {@link ScriptTaskProperties.Location#INLINE} script location is selected.
     *
     * @param path path to the file with the script text
     */
    public ScriptTask inlineBodyFromPath(@NotNull final Path path) {
        this.body = readFileContent(path, "script inline file", "Error when reading script body from path: %s");
        return this;
    }

    /**
     * Sets the file path of script to execute. Only valid when {@link ScriptTaskProperties.Location#FILE} script location is selected.
     */
    public ScriptTask fileFromPath(@NotNull final Path path) {
        checkNotNull("script file path", path);
        return fileFromPath(path.toString());
    }

    /**
     * Sets the file path of script to execute. Only valid when {@link ScriptTaskProperties.Location#FILE} script location is selected.
     */
    public ScriptTask fileFromPath(@NotNull final String path) {
        checkNotEmpty("script file path", path);
        file();
        this.path = path;
        return this;
    }

    /**
     * Sets command line argument to be passed when script is executed.
     */
    public ScriptTask argument(@NotNull final String argument) {
        checkNotEmpty("argument", argument);
        this.argument = argument;
        return this;
    }

    /**
     * Sets environment variables to be set when script is executed.
     */
    public ScriptTask environmentVariables(@NotNull final String environmentVariables) {
        checkNotEmpty("environment variables", environmentVariables);
        this.environmentVariables = environmentVariables;
        return this;
    }

    /**
     * Sets a directory the script should be executed in.
     */
    public ScriptTask workingSubdirectory(@NotNull final String workingSubdirectory) {
        checkNotEmpty("working subdirectory", workingSubdirectory);
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    @NotNull
    @Override
    protected ScriptTaskProperties build() {
        return new ScriptTaskProperties(description,
                taskEnabled,
                interpreter,
                location,
                body,
                path,
                argument,
                environmentVariables,
                workingSubdirectory,
                usePowershellErrorHandling,
                usePowershellErrorActionStop,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ScriptTask)) {
            return false;
        }
        ScriptTask that = (ScriptTask) o;
        return interpreter == that.interpreter &&
                location == that.location &&
                Objects.equals(body, that.body) &&
                Objects.equals(path, that.path) &&
                Objects.equals(argument, that.argument) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory) &&
                Objects.equals(usePowershellErrorHandling, that.usePowershellErrorHandling) &&
                Objects.equals(usePowershellErrorActionStop, that.usePowershellErrorActionStop);
    }

    @Override
    public int hashCode() {
        return Objects.hash(interpreter, location, body, path, argument, environmentVariables, workingSubdirectory, usePowershellErrorHandling, usePowershellErrorActionStop);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("interpreter", interpreter)
                .append("location", location)
                .append("body", body)
                .append("path", path)
                .append("argument", argument)
                .append("environmentVariables", environmentVariables)
                .append("workingSubdirectory", workingSubdirectory)
                .append("usePowershellErrorHandling", usePowershellErrorHandling)
                .append("usePowershellErrorActionStop", usePowershellErrorActionStop)
                .toString();
    }
}
