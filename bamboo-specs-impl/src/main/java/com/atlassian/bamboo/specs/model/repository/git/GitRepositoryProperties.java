package com.atlassian.bamboo.specs.model.repository.git;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.codegen.emitters.repository.GitAuthenticationEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.containsBambooVariable;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsRelaxedXssRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;

@Immutable
public final class GitRepositoryProperties extends VcsRepositoryProperties {

    private static final Set<String> SUPPORTED_SCHEMES = Stream.of("http", "https", "ssh", "file", "git", null).collect(Collectors.toSet());

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:gitv2");

    private final String url;
    private String branch;

    @CodeGenerator(GitAuthenticationEmitter.class)
    private final AuthenticationProperties authenticationProperties;

    @Setter("shallowClonesEnabled")
    private final boolean useShallowClones;
    @Setter("remoteAgentCacheEnabled")
    private final boolean useRemoteAgentCache;
    @Setter("submodulesEnabled")
    private final boolean useSubmodules;
    @Setter("submodulesUseShallowClonesEnabled")
    private final boolean useSubmodulesWithShallowClones;
    private final boolean sshKeyAppliesToSubmodules;
    private final Duration commandTimeout;
    private final boolean verboseLogs;
    private final boolean fetchWholeRepository;
    @Setter("lfsEnabled")
    private final boolean useLfs;

    private VcsChangeDetectionProperties vcsChangeDetection;

    private GitRepositoryProperties() {
        url = null;
        branch = null;
        authenticationProperties = null;

        useShallowClones = false;
        useRemoteAgentCache = true;
        useSubmodules = false;
        useSubmodulesWithShallowClones = false;
        sshKeyAppliesToSubmodules = false;
        commandTimeout = Duration.ofMinutes(180);
        verboseLogs = false;
        fetchWholeRepository = false;
        useLfs = false;
    }

    public GitRepositoryProperties(@Nullable final String name,
                                   @Nullable final BambooOidProperties oid,
                                   @Nullable final String description,
                                   @Nullable final String parent,
                                   @Nullable final VcsRepositoryViewerProperties repositoryViewerProperties,
                                   @Nullable final String url,
                                   @Nullable final String branch,
                                   @Nullable final AuthenticationProperties authenticationProperties,
                                   @Nullable final VcsChangeDetectionProperties vcsChangeDetection,
                                   final boolean useShallowClones,
                                   final boolean useRemoteAgentCache,
                                   final boolean useSubmodules,
                                   final boolean useSubmodulesWithShallowClones,
                                   final boolean sshKeyAppliesToSubmodules,
                                   @NotNull final Duration commandTimeout,
                                   final boolean verboseLogs,
                                   final boolean fetchWholeRepository,
                                   final boolean useLfs
    ) throws PropertiesValidationException {
        this(name,
                oid,
                description,
                parent,
                repositoryViewerProperties,
                url,
                branch,
                null,
                authenticationProperties,
                vcsChangeDetection,
                useShallowClones,
                useRemoteAgentCache,
                useSubmodules,
                useSubmodulesWithShallowClones,
                sshKeyAppliesToSubmodules,
                commandTimeout,
                verboseLogs,
                fetchWholeRepository,
                useLfs);
    }

    public GitRepositoryProperties(@Nullable final String name,
                                   @Nullable final BambooOidProperties oid,
                                   @Nullable final String description,
                                   @Nullable final String parent,
                                   @Nullable final VcsRepositoryViewerProperties repositoryViewerProperties,
                                   @Nullable final String url,
                                   @Nullable final String branch,
                                   @Nullable final ProjectProperties project,
                                   @Nullable final AuthenticationProperties authenticationProperties,
                                   @Nullable final VcsChangeDetectionProperties vcsChangeDetection,
                                   final boolean useShallowClones,
                                   final boolean useRemoteAgentCache,
                                   final boolean useSubmodules,
                                   final boolean useSubmodulesWithShallowClones,
                                   final boolean sshKeyAppliesToSubmodules,
                                   @NotNull final Duration commandTimeout,
                                   final boolean verboseLogs,
                                   final boolean fetchWholeRepository,
                                   final boolean useLfs
    ) throws PropertiesValidationException {
        super(name, oid, description, parent, repositoryViewerProperties, project);

        this.url = url;
        this.branch = branch;
        this.authenticationProperties = authenticationProperties;
        this.vcsChangeDetection = vcsChangeDetection;
        this.useRemoteAgentCache = useRemoteAgentCache;
        this.useShallowClones = useShallowClones;
        this.useSubmodules = useSubmodules;
        this.useSubmodulesWithShallowClones = useSubmodulesWithShallowClones;
        this.sshKeyAppliesToSubmodules = sshKeyAppliesToSubmodules;
        this.commandTimeout = commandTimeout;
        this.verboseLogs = verboseLogs;
        this.fetchWholeRepository = fetchWholeRepository;
        this.useLfs = useLfs;

        if (!hasParent()) {
            if (StringUtils.isBlank(this.branch)) {
                this.branch = "master";
            }
        }
        validate();
    }

    @Nullable
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Nullable
    public String getUrl() {
        return url;
    }

    @Nullable
    public String getBranch() {
        return branch;
    }

    @Nullable
    public AuthenticationProperties getAuthenticationProperties() {
        return authenticationProperties;
    }

    public boolean isUseShallowClones() {
        return useShallowClones;
    }

    public boolean isUseRemoteAgentCache() {
        return useRemoteAgentCache;
    }

    public boolean isUseSubmodules() {
        return useSubmodules;
    }

    public boolean isUseSubmodulesWithShallowClones() {
        return useSubmodulesWithShallowClones;
    }

    public Duration getCommandTimeout() {
        return commandTimeout;
    }

    public boolean isVerboseLogs() {
        return verboseLogs;
    }

    public boolean isFetchWholeRepository() {
        return fetchWholeRepository;
    }

    public boolean isUseLfs() {
        return useLfs;
    }

    public boolean isSshKeyAppliesToSubmodules() {
        return sshKeyAppliesToSubmodules;
    }

    @Nullable
    public VcsChangeDetectionProperties getVcsChangeDetection() {
        return vcsChangeDetection;
    }

    @Override
    public void validate() {
        super.validate();

        final ValidationContext context = ValidationContext.of("Git repository");
        final List<ValidationProblem> errors = new ArrayList<>();

        if (!hasParent()) {
            checkRequired(context.with("URL"), url);
        }

        if (url != null && !containsBambooVariable(url)) {
            validateNotContainsRelaxedXssRelatedCharacters(context.with("URL"), url)
                    .ifPresent(errors::add);

            validateNotContainsShellInjectionRelatedCharacters(context.with("URL"), url)
                    .ifPresent(errors::add);

            checkUrl(context.with("URL"))
                    .ifPresent(errors::add);
        }

        if (branch != null) {
            validateNotContainsShellInjectionRelatedCharacters(context.with("Branch name"), branch)
                    .ifPresent(errors::add);
        }

        if (vcsChangeDetection != null && !vcsChangeDetection.getConfiguration().isEmpty()) {
            errors.add(new ValidationProblem(context.with("Change detection"),
                    "Git repository cannot have any extra change detection configuration."));
        }

        checkNoErrors(errors);
    }

    private Optional<ValidationProblem> checkUrl(@NotNull ValidationContext validationContext) {
        if (url == null) {
            return Optional.empty();
        }

        try {
            final URI uri = new URI(url);
            final String scheme = uri.getScheme();
            if (!SUPPORTED_SCHEMES.contains(uri.getScheme())) {
                return Optional.of(new ValidationProblem(validationContext,
                        "scheme '%s' is not supported - supported schemes are: %s",
                        scheme, String.join(", ", SUPPORTED_SCHEMES)));
            }

            final String userInfo = uri.getUserInfo();
            if (StringUtils.isNotBlank(userInfo)) {
                int d = userInfo.indexOf(':');
                String user = d < 0 ? userInfo : userInfo.substring(0, d);
                String pass = d < 0 ? null : userInfo.substring(d + 1);

                boolean duplicateUsername = !StringUtils.isBlank(user)
                        && authenticationProperties instanceof UserPasswordAuthenticationProperties
                        && StringUtils.isNotBlank(((UserPasswordAuthenticationProperties) authenticationProperties).getUsername());
                boolean duplicatePassword = !StringUtils.isBlank(pass)
                        && authenticationProperties instanceof UserPasswordAuthenticationProperties
                        && StringUtils.isNotBlank(((UserPasswordAuthenticationProperties) authenticationProperties).getPassword());
                if (duplicateUsername) {
                    return Optional.of(new ValidationProblem(validationContext,
                            "Duplicate username" + (duplicatePassword ? " & password" : "")));
                } else if (duplicatePassword) {
                    return Optional.of(new ValidationProblem(validationContext,
                            "Duplicate password"));

                }
            }
            if (authenticationProperties instanceof SshPrivateKeyAuthenticationProperties) {
                if ("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())) {
                    return Optional.of(new ValidationProblem(validationContext, "Ssh authentication not supported with " + uri.getScheme()));
                }
            }
        } catch (URISyntaxException e) {
            //windows network share or possibly scp format
            //scp format is not usual uri (for example git@bitbucket.org:atlassian/bamboo-docker-plugin.git)
            //we do best effort accepting everything that doesn't contain "://"
            if (url.startsWith("\\\\") || !url.contains("://")) {
                return Optional.empty();
            }

            return Optional.of(new ValidationProblem(validationContext, String.format("Malformed URL: %s", url)));
        }

        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GitRepositoryProperties that = (GitRepositoryProperties) o;
        return isUseShallowClones() == that.isUseShallowClones() &&
                isUseRemoteAgentCache() == that.isUseRemoteAgentCache() &&
                isUseSubmodules() == that.isUseSubmodules() &&
                isUseSubmodulesWithShallowClones() == that.isUseSubmodulesWithShallowClones() &&
                isSshKeyAppliesToSubmodules() == that.isSshKeyAppliesToSubmodules() &&
                isVerboseLogs() == that.isVerboseLogs() &&
                isFetchWholeRepository() == that.isFetchWholeRepository() &&
                isUseLfs() == that.isUseLfs() &&
                Objects.equals(getUrl(), that.getUrl()) &&
                Objects.equals(getBranch(), that.getBranch()) &&
                Objects.equals(getAuthenticationProperties(), that.getAuthenticationProperties()) &&
                Objects.equals(getCommandTimeout(), that.getCommandTimeout()) &&
                Objects.equals(getVcsChangeDetection(), that.getVcsChangeDetection());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getUrl(), getBranch(), getAuthenticationProperties(), isUseShallowClones(),
                isUseRemoteAgentCache(), isUseSubmodules(), isUseSubmodulesWithShallowClones(), isSshKeyAppliesToSubmodules(),
                getCommandTimeout(), isVerboseLogs(), isFetchWholeRepository(), isUseLfs(), getVcsChangeDetection());
    }
}
