package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.FastlaneTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Fastlane build task builder.
 */
public class FastlaneTask extends Task<FastlaneTask, FastlaneTaskProperties> {
    @NotNull
    private String lane;
    @NotNull
    private String label;
    @Nullable
    private String environmentVariables;
    @Nullable
    private String workingSubdirectory;

    /**
     * Fastlane lane which should be executed.
     */
    public FastlaneTask lane(@NotNull String lane) {
        this.lane = lane;
        return this;
    }

    /**
     * Environment variables which will be passed to Fastlane process.
     */
    public FastlaneTask environmentVariables(String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return this;
    }


    /**
     * Label of the Fastlane executable.
     */
    public FastlaneTask executableLabel(String label) {
        this.label = label;
        return this;
    }

    /**
     * An alternative subdirectory as working directory for this task.
     */
    public FastlaneTask workingSubdirectory(String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    @NotNull
    @Override
    protected FastlaneTaskProperties build() {
        return new FastlaneTaskProperties(description,
                taskEnabled,
                lane,
                environmentVariables,
                label,
                workingSubdirectory,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FastlaneTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        FastlaneTask that = (FastlaneTask) o;
        return lane.equals(that.lane) &&
                label.equals(that.label) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), lane, label, environmentVariables, workingSubdirectory);
    }
}
