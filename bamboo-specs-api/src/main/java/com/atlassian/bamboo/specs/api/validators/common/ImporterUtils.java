package com.atlassian.bamboo.specs.api.validators.common;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Utility validation methods for importing Bamboo Specs.
 * <p>
 * Methods from this class prefixed with {@code check} throw exceptions on failures. For validation utility
 * methods which return validation problems instead of throwing, use {@link ValidationUtils}.
 */
public final class ImporterUtils {
    private ImporterUtils() {
    }

    public static void checkPluginKey(@NotNull ValidationContext validationContext,
                                      @NotNull AtlassianModuleProperties actual,
                                      @NotNull AtlassianModuleProperties expected)
            throws PropertiesValidationException {
        if (!expected.equals(actual)) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(validationContext, "Invalid plugin key, got '%s', expected '%s'",
                            actual.getCompleteModuleKey(), expected.getCompleteModuleKey())));
        }
    }

    public static void checkThat(@NotNull ValidationContext validationContext,
                                 boolean condition,
                                 @NotNull String messageFormat,
                                 @NotNull Object... messageArgs)
            throws PropertiesValidationException {
        if (!condition) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(validationContext, messageFormat, messageArgs)));
        }
    }

    public static void checkThat(@NotNull String message,
                                 @NotNull boolean condition)
            throws PropertiesValidationException {
        if (!condition) {
            throw new PropertiesValidationException(message);
        }
    }

    public static void checkNoErrors(@NotNull List<ValidationProblem> errors) throws PropertiesValidationException {
        if (!errors.isEmpty()) {
            throw new PropertiesValidationException(errors);
        }
    }

    public static void checkNoErrors(@NotNull Optional<ValidationProblem> error) throws PropertiesValidationException {
        if (error.isPresent()) {
            throw new PropertiesValidationException(error.get());
        }
    }

    /**
     * Used during building properties object when some property is missing.
     */
    public static void checkRequired(@NotNull ValidationContext validationContext, @Nullable final Object o) throws PropertiesValidationException {
        if (o == null) {
            throw new PropertiesValidationException(validationContext, "Property is required.");
        }
    }

    /**
     * Used during building properties object when some property is missing.
     */
    public static void checkRequired(@NotNull ValidationContext validationContext, @NotNull String propertyName,
                                     @Nullable final Object o) throws PropertiesValidationException {
        if (o == null) {
            throw new PropertiesValidationException(validationContext, String.format("Property %s is required.", propertyName));
        }
    }

    /**
     * Used during building properties object when some property is missing.
     */
    public static void checkRequiredNotBlank(@NotNull ValidationContext validationContext,
                                             @Nullable final String s) throws PropertiesValidationException {
        if (s == null || StringUtils.isBlank(s)) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(validationContext, "Property must not be blank.")));
        }
    }

    /**
     * Used to extra validate argument that shouldn't be null to throw our exception instead of NPE.
     */
    public static <T> T checkNotNull(@NotNull final String argumentName, @Nullable final T o) throws PropertiesValidationException {
        return checkNotNull(ValidationContext.empty(), argumentName, o);
    }

    /**
     * Used to extra validate argument that shouldn't be null to throw our exception instead of NPE.
     */
    public static <T> T checkNotNull(@NotNull final ValidationContext validationContext, @NotNull final String argumentName, @Nullable final T o) throws PropertiesValidationException {
        if (o == null) {
            throw new PropertiesValidationException(validationContext, String.format("Argument %s can not be null.", argumentName));
        }
        return o;
    }

    public static String checkNotEmpty(@NotNull final String argumentName, @Nullable final String s) throws PropertiesValidationException {
        return checkNotEmpty(ValidationContext.empty(), argumentName, s);
    }

    public static String checkNotEmpty(@NotNull ValidationContext validationContext, @NotNull final String argumentName,
                                       @Nullable final String s) throws PropertiesValidationException {
        if (StringUtils.isEmpty(s)) {
            throw new PropertiesValidationException(
                    validationContext.with(argumentName),
                    String.format("Argument %s can not be empty.", argumentName));
        }
        return s;
    }

    public static void checkArgument(@NotNull ValidationContext validationContext,
                                     final boolean expression,
                                     final Object errorMessage) {
        if (!expression) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(validationContext, String.valueOf(errorMessage))));
        }
    }

    /**
     * Used to extra validate argument that shouldn't be blank.
     */
    public static String checkNotBlank(@NotNull final String argumentName, @Nullable final String o) throws PropertiesValidationException {
        return checkNotBlank(ValidationContext.empty(), argumentName, o);
    }

    /**
     * Used to extra validate argument that shouldn't be blank.
     */
    public static String checkNotBlank(@NotNull final ValidationContext validationContext, @NotNull final String argumentName,
                                       @Nullable final String o) throws PropertiesValidationException {
        checkNotNull(validationContext, argumentName, o);
        if (StringUtils.isBlank(o)) {
            throw new PropertiesValidationException(validationContext, String.format("Argument %s can not be blank.", argumentName));
        }
        return o;
    }

    /**
     * Used during building properties to check that a number is a non-negative integer.
     */
    public static void checkNotNegative(@NotNull final String argumentName, int value) throws PropertiesValidationException {
        checkNotNegative(ValidationContext.empty(), argumentName, value);
    }

    /**
     * Used during building properties to check that a number is a non-negative integer.
     */
    public static void checkNotNegative(@NotNull ValidationContext validationContext, @NotNull final String argumentName,
                                        int value) throws PropertiesValidationException {
        if (value < 0) {
            throw new PropertiesValidationException(validationContext, String.format("Property %s can not be a negative number.", argumentName));
        }
    }

    /**
     * Used during building properties to check that a number is a positive integer.
     */
    public static void checkPositive(@NotNull final String argumentName, int value) throws PropertiesValidationException {
        if (value <= 0) {
            throw new PropertiesValidationException(String.format("Argument %s must be a positive number.", argumentName));
        }
    }

    /**
     * Used during building properties to check that a number is a positive integer.
     */
    public static void checkPositive(@NotNull final String argumentName, long value) throws PropertiesValidationException {
        if (value <= 0) {
            throw new PropertiesValidationException("Argument " + argumentName + " must be a positive number.");
        }
    }

    /**
     * Used during building properties to check that a number is a positive integer.
     */
    public static void checkPositive(@NotNull ValidationContext validationContext, @NotNull final String argumentName, int value) throws PropertiesValidationException {
        if (value <= 0) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(validationContext, "Argument " + argumentName + " must be a positive number.")));
        }
    }

    /**
     * Used during building properties to check that a number is a positive integer.
     */
    public static void checkPositive(@NotNull ValidationContext validationContext, @NotNull final String argumentName, long value) throws PropertiesValidationException {
        if (value <= 0) {
            throw new PropertiesValidationException(Collections.singletonList(
                    new ValidationProblem(validationContext, "Argument " + argumentName + " must be a positive number.")));
        }
    }
}
