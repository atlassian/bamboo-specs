package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.inspector.TagInspector;
import org.yaml.snakeyaml.nodes.Tag;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class BambooSpecsTagInspector implements TagInspector {
    private static final Set<Pattern> WHITELISTED_CLASSES = Collections.unmodifiableSet(new HashSet<Pattern>() {{
        // our top-level entity
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.util\\.BambooSpecProperties"));
        // properties from API
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.model\\..*Properties"));
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.model\\..*Properties\\$.+"));
        // properties from IMPL
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.model\\..*Properties"));
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.model\\..*Properties\\$.+"));
        // enums from the builders package
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.builders\\.Applicability"));
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.builders\\.permission\\.PermissionType"));
        //we have handler for Duration class
        add(Pattern.compile("java\\.time\\.Duration"));
    }});

    @Override
    public boolean isGlobalTagAllowed(Tag tag) {
        String fullClassName = tag.getClassName();
        return WHITELISTED_CLASSES.stream()
                .anyMatch(pattern -> pattern.matcher(fullClassName).matches());
    }
}
