package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.model.trigger.BitbucketCloudTriggerProperties;

/**
 * Represents Bitbucket Cloud trigger that indicates the integration of webhooks.
 */
public class BitbucketCloudTrigger extends RepositoryBasedTrigger<BitbucketCloudTrigger, BitbucketCloudTriggerProperties> {
    @Override
    protected BitbucketCloudTriggerProperties build() {
        return new BitbucketCloudTriggerProperties(name, description, triggerEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
    }
}
