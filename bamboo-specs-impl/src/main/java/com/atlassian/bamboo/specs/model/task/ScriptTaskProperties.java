package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkArgument;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@Immutable
public final class ScriptTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.scripttask:task.builder.script");

    public enum Interpreter {
        SHELL, WINDOWS_POWER_SHELL, BINSH_OR_CMDEXE
    }

    public enum Location {
        INLINE, FILE
    }

    private final Interpreter interpreter;
    private final Location location;
    @Nullable
    private final String body;
    @Nullable
    private final String path;
    @Nullable
    private final String argument;
    @Nullable
    private final String environmentVariables;
    @Nullable
    private final String workingSubdirectory;

    private boolean usePowershellErrorHandling;
    private boolean usePowershellErrorActionStop;

    private ScriptTaskProperties() {
        this.interpreter = Defaults.getDefaultInterpreter();
        this.location = Defaults.getDefaultLocation();
        this.body = null;
        this.path = null;
        this.argument = null;
        this.environmentVariables = null;
        this.workingSubdirectory = null;
    }

    public ScriptTaskProperties(final String description,
                                final boolean enabled,
                                @Nullable final Interpreter interpreter,
                                @Nullable final Location location,
                                @Nullable final String body,
                                @Nullable final String path,
                                @Nullable final String argument,
                                @Nullable final String environmentVariables,
                                @Nullable final String workingSubdirectory,
                                final boolean usePowershellErrorHandling,
                                final boolean usePowershellErrorActionStop,
                                @NotNull List<RequirementProperties> requirements,
                                @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.interpreter = Defaults.getOrDefaultInterpreter(interpreter);
        this.location = Defaults.getOrDefaultLocation(location);
        this.body = body;
        this.path = path;
        this.argument = argument;
        this.environmentVariables = environmentVariables;
        this.workingSubdirectory = workingSubdirectory;
        this.usePowershellErrorHandling = usePowershellErrorHandling;
        this.usePowershellErrorActionStop = usePowershellErrorActionStop;
        validate();
    }

    private static class Defaults {
        static Interpreter getDefaultInterpreter() {
            return Interpreter.SHELL;
        }

        static Interpreter getOrDefaultInterpreter(@Nullable Interpreter interpreter) {
            return interpreter != null ? interpreter : getDefaultInterpreter();
        }

        static Location getDefaultLocation() {
            return Location.INLINE;
        }

        static Location getOrDefaultLocation(@Nullable Location location) {
            return location != null ? location : getDefaultLocation();
        }
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    public Interpreter getInterpreter() {
        return interpreter;
    }

    public Location getLocation() {
        return location;
    }

    @Nullable
    public String getBody() {
        return body;
    }

    @Nullable
    public String getPath() {
        return path;
    }

    @Nullable
    public String getArgument() {
        return argument;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    public boolean isUsePowershellErrorHandling() {
        return usePowershellErrorHandling;
    }

    public boolean isUsePowershellErrorActionStop() {
        return usePowershellErrorActionStop;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        final ValidationContext context = ValidationContext.of("Script task");
        checkRequired(context.with("location"), location);
        checkRequired(context.with("interpreter"), interpreter);
        switch (location) {
            case INLINE:
                checkArgument(context.with("body"), !StringUtils.isEmpty(body),
                        String.format("Script body cannot be empty when location is set to %s", location));
                checkArgument(context.with("path"), StringUtils.isEmpty(path),
                        String.format("Script path cannot be set when location is set to %s", location));
                break;
            case FILE:
                checkArgument(context.with("path"), !StringUtils.isEmpty(path),
                        String.format("Script path cannot be empty when location is set to %s", location));
                checkArgument(context.with("body"), StringUtils.isEmpty(body),
                        String.format("Script body cannot be set when location is set to %s", location));
                break;
            default:
                throw new PropertiesValidationException(String.format("Script location %s is not supported", location));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final ScriptTaskProperties that = (ScriptTaskProperties) o;
        return this.getInterpreter() == that.getInterpreter() &&
                this.getLocation() == that.getLocation() &&
                Objects.equals(this.getBody(), that.getBody()) &&
                Objects.equals(this.getPath(), that.getPath()) &&
                Objects.equals(this.getArgument(), that.getArgument()) &&
                Objects.equals(this.getEnvironmentVariables(), that.getEnvironmentVariables()) &&
                Objects.equals(this.isUsePowershellErrorHandling(), that.isUsePowershellErrorHandling()) &&
                Objects.equals(this.isUsePowershellErrorActionStop(), that.isUsePowershellErrorActionStop()) &&
                Objects.equals(this.getWorkingSubdirectory(), that.getWorkingSubdirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.getInterpreter(), this.getLocation(), this.getBody(), this.getPath(), this.getArgument(), this.getEnvironmentVariables(), this.getWorkingSubdirectory(), this.isUsePowershellErrorHandling(), this.isUsePowershellErrorActionStop());
    }
}
