package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Pushes docker image to the docker registry.
 */
public class DockerPushImageTask extends AbstractDockerRegistryTask<DockerPushImageTask> {

    /**
     * Specify image name which will be pushed to Docker Hub. You can also optionally
     * add namespace and tag. E.g <i>namespace/repository:tag</i>
     * <p>
     * Setting either this field or {@link #customRegistryImage(String)} is mandatory.
     */
    @Override
    public DockerPushImageTask dockerHubImage(@NotNull String repository) {
        return super.dockerHubImage(repository);
    }

    /**
     * Specify repository and image name which will be pushed to the custom registry. You can also optionally
     * add namespace and tag. E.g <i>registry.address:port/namespace/repository:tag</i>
     * <p>
     * Setting either this field or {@link #dockerHubImage(String)}} is mandatory.
     */
    @Override
    public DockerPushImageTask customRegistryImage(@NotNull String repository) {
        return super.customRegistryImage(repository);
    }

    @NotNull
    @Override
    protected DockerRegistryTaskProperties build() {
        return new DockerRegistryTaskProperties(
                description,
                taskEnabled,
                DockerRegistryTaskProperties.OperationType.PUSH,
                image,
                registryType,
                username,
                password,
                email,
                environmentVariables,
                workingSubdirectory,
                requirements,
                conditions,
                sharedCredentialsIdentifier);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof DockerPushImageTask)) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
