package com.atlassian.bamboo.specs.api.builders.plan.artifact;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents Bamboo artifact definition.
 */
public class Artifact extends EntityPropertiesBuilder<ArtifactProperties> {
    public static final boolean SHARED_BY_DEFAULT = preventInlining(false);
    public static final boolean REQUIRED_BY_DEFAULT = preventInlining(false);
    public static final boolean HTTP_COMPRESSION_ON_BY_DEFAULT = preventInlining(true);

    private String name;
    private final List<String> copyPatterns = new ArrayList<>();
    private final List<String> exclusionPatterns = new ArrayList<>();
    private String location = "";
    private boolean shared = SHARED_BY_DEFAULT;
    private boolean required = REQUIRED_BY_DEFAULT;
    private boolean httpCompressionOn = HTTP_COMPRESSION_ON_BY_DEFAULT;

    public Artifact() {
    }

    /**
     * Specify an artifact with given name. Name must be unique within the job; if artifact is shared it must be unique within the plan.
     * <p>
     * In the absence of oid, the name is used to identify the artifact.
     * If the artifact with given name does not exist, a new one is created, otherwise existing one is updated.
     */
    public Artifact(@NotNull String name) throws PropertiesValidationException {
        validateName(name);
        this.name = name;
    }

    /**
     * Sets an artifact name. Name must be unique within the job; if artifact is shared it must be unique within the plan.
     * <p>
     * In the absence of oid, the name is used to identify the artifact.
     * If the artifact with given name does not exist, a new one is created, otherwise existing one is updated.
     */
    public Artifact name(@NotNull String name) throws PropertiesValidationException {
        validateName(name);
        this.name = name;
        return this;
    }

    /**
     * Specify the pattern according to which Bamboo should copy files when creating an actual artifact.
     * This erases previously specified patterns.
     * @param copyPattern a copy pattern in Ant file pattern format
     * @deprecated since 8.3, use {@link #copyPatterns(String...)}
     */
    @Deprecated
    public Artifact copyPattern(@NotNull String copyPattern) throws PropertiesValidationException {
        checkNotNull("copyPattern", copyPattern);
        this.copyPatterns.clear();
        this.copyPatterns.add(copyPattern);
        return this;
    }

    /**
     * Specify the pattern according to which Bamboo should copy files when creating an actual artifact.
     * @param copyPatterns a set of copy patterns in Ant file pattern format
     * @since 8.3
     */
    public Artifact copyPatterns(@NotNull String... copyPatterns) throws PropertiesValidationException {
        checkNotNull("copyPatterns", copyPatterns);
        for (String copyPattern : copyPatterns) {
            checkNotNull("copyPattern", copyPattern);
            this.copyPatterns.add(copyPattern);
        }
        return this;
    }

    /**
     * Specify the pattern according to which Bamboo should exclude files when creating an actual artifact.
     * @param exclusionPatterns a set of exclusion patterns in Ant file pattern format
     * @since 8.3
     */
    public Artifact exclusionPatterns(@NotNull String... exclusionPatterns) throws PropertiesValidationException {
        checkNotNull("exclusionPatterns", exclusionPatterns);
        for (String exclusionPattern : exclusionPatterns) {
            checkNotNull("exclusionPattern", exclusionPattern);
            this.exclusionPatterns.add(exclusionPattern);
        }
        return this;
    }

    /**
     * Specifies if artifact is shared. Shared artifacts can be downloaded by jobs in subsequent stages as well as other plans and deployments.
     * False by default.
     */
    public Artifact shared(boolean shared) {
        this.shared = shared;
        return this;
    }

    /**
     * Set artifact to be required. Build will fail if can't publish required artifact.
     */
    public Artifact required() {
        this.required = true;
        return this;
    }

    /**
     * Specifies if artifact is required. Build will fail if can't publish required artifact.
     * False by default.
     */
    public Artifact required(boolean mandatory) {
        this.required = mandatory;
        return this;
    }

    /**
     * Specifies if artifact should be compressed before transfer.
     * True by default.
     */
    public Artifact httpCompressionOn(boolean httpCompressionOn) {
        this.httpCompressionOn = httpCompressionOn;
        return this;
    }

    /**
     * Specifies a path in which Bamboo should look for the files when creating an actual artifact.
     */
    public Artifact location(@NotNull String location) throws PropertiesValidationException {
        checkNotNull("location", location);
        this.location = location;
        return this;
    }

    /**
     * Returns defined artifact name, which serves as identifier for this object.
     *
     * @throws IllegalStateException if name is undefined
     */
    @NotNull
    public String getName() {
        if (StringUtils.isBlank(name)) {
            throw new IllegalStateException("Artifact name is undefined");
        }
        return name;
    }

    protected ArtifactProperties build() throws PropertiesValidationException {
        return new ArtifactProperties(name, copyPatterns, exclusionPatterns, location, shared, required, httpCompressionOn);
    }

    private void validateName(String name) {
        checkNotNull("name", name);
        ImporterUtils.checkArgument(ArtifactProperties.VALIDATION_CONTEXT,!StringUtils.contains(name,"/"),"Name can not contain '/' character");
    }
}
