package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ScriptTaskPropertiesTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testValidationForAllScriptLocation() {
        final Set<ScriptTaskProperties.Location> allLocationTypes = new HashSet<>(Arrays.asList(ScriptTaskProperties.Location.values()));

        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        final List<RequirementProperties> requirements = Collections.singletonList(EntityPropertiesBuilders.build(requirement));
        allLocationTypes.remove(ScriptTaskProperties.Location.INLINE);
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.INLINE,
                "echo Hello World",
                null,
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir",
                false,
                false,
                requirements, Collections.emptyList()).validate();

        allLocationTypes.remove(ScriptTaskProperties.Location.FILE);
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.FILE,
                null,
                "/path/to/script",
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir",
                false,
                false,
                requirements, Collections.emptyList()).validate();

        for (final ScriptTaskProperties.Location location : allLocationTypes) {
            new ScriptTaskProperties(
                    "hello script 1",
                    true,
                    ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                    location,
                    null,
                    null,
                    "arg1 arg2 arg3",
                    "SOME_HOME=/home ANOTHER_HOME=/home",
                    "work/dir",
                    false,
                    false,
                    requirements, Collections.emptyList()).validate();
        }
    }

    @Test
    public void testAtlassianId() {
        assertThat(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        Collections.emptyList(), Collections.emptyList()).getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.scripttask:task.builder.script"));
    }

    @Test
    public void testValidationWhenLocationFileAndBodySet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script body cannot be set when location is set to %s", ScriptTaskProperties.Location.FILE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.FILE,
                "echo Hello World",
                "/path/to/script",
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir",
                false,
                false,
                Collections.emptyList(), Collections.emptyList()).validate();
    }

    @Test
    public void testValidationWhenLocationFileAndPathNotSet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script path cannot be empty when location is set to %s", ScriptTaskProperties.Location.FILE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.FILE,
                null,
                null,
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir",
                false,
                false,
                Collections.emptyList(), Collections.emptyList()).validate();
    }

    @Test
    public void testValidationWhenLocationInlineAndPathSet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script path cannot be set when location is set to %s", ScriptTaskProperties.Location.INLINE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.INLINE,
                "echo Hello World",
                "/path/to/script",
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir",
                false,
                false,
                Collections.emptyList(), Collections.emptyList()).validate();
    }

    @Test
    public void testValidationWhenLocationInlineAndBodyNotSet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script body cannot be empty when location is set to %s", ScriptTaskProperties.Location.INLINE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.INLINE,
                null,
                null,
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir",
                false,
                false,
                Collections.emptyList(), Collections.emptyList()).validate();
    }

    @Test
    public void testDefaultInterpreterIsShell() {
        assertThat(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        null,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        Collections.emptyList(), Collections.emptyList()).getInterpreter(),
                equalTo(ScriptTaskProperties.Interpreter.SHELL));
    }

    @Test
    public void testDefaultLocationIsInline() {
        assertThat(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        null,
                        null,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        Collections.emptyList(), Collections.emptyList()).getLocation(),
                equalTo(ScriptTaskProperties.Location.INLINE));
    }

}