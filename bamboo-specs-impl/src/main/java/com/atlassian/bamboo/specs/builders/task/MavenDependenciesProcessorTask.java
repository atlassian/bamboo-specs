package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.MavenDependenciesProcessorTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Automatically update Plan dependencies by analysing the Maven pom file with every build.
 */
public class MavenDependenciesProcessorTask extends Task<MavenDependenciesProcessorTask, MavenDependenciesProcessorTaskProperties> {

    @Nullable
    private String overrideProjectFile;

    @Nullable
    private String workingSubdirectory;

    @Nullable
    private String alternatePathForTheGlobalSettingsFile;

    @Nullable
    private String alternatePathForTheUserSettingsFile;

    @Nullable
    private String pathToMavenLocalRepository;

    /**
     * Path to the project file, relative to the working subdirectory.
     * If left blank Maven will use the pom.xml in the root of the working subdirectory
     */
    public MavenDependenciesProcessorTask overrideProjectFile(@Nullable String overrideProjectFile) {
        this.overrideProjectFile = overrideProjectFile;
        return this;
    }

    /**
     * Specify an alternative subdirectory as working directory for the task.
     */
    public MavenDependenciesProcessorTask workingSubdirectory(@Nullable String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    /**
     * Specify alternative location for global settings file. If left blank builtin Maven settings will be used.
     */
    public MavenDependenciesProcessorTask alternatePathForTheGlobalSettingsFile(@Nullable String alternatePathForTheGlobalSettingsFile) {
        this.alternatePathForTheGlobalSettingsFile = alternatePathForTheGlobalSettingsFile;
        return this;
    }

    /**
     * Specify alternative location for user settings file. If left blank $HOME/.m2/settings.xml will be used.
     */
    public MavenDependenciesProcessorTask alternatePathForTheUserSettingsFile(@Nullable String alternatePathForTheUserSettingsFile) {
        this.alternatePathForTheUserSettingsFile = alternatePathForTheUserSettingsFile;
        return this;
    }

    /**
     * Specify alternative location for Maven local repository. If left blank location will be established
     * using configuration files and default value of $HOME/.m2/repository.
     */
    public MavenDependenciesProcessorTask pathToMavenLocalRepository(@Nullable String pathToMavenLocalRepository) {
        this.pathToMavenLocalRepository = pathToMavenLocalRepository;
        return this;
    }

    @NotNull
    @Override
    protected MavenDependenciesProcessorTaskProperties build() {
        return new MavenDependenciesProcessorTaskProperties(description, taskEnabled, overrideProjectFile,
                workingSubdirectory, alternatePathForTheGlobalSettingsFile, alternatePathForTheUserSettingsFile,
                pathToMavenLocalRepository,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MavenDependenciesProcessorTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        MavenDependenciesProcessorTask that = (MavenDependenciesProcessorTask) o;
        return Objects.equals(overrideProjectFile, that.overrideProjectFile) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory) &&
                Objects.equals(alternatePathForTheGlobalSettingsFile, that.alternatePathForTheGlobalSettingsFile) &&
                Objects.equals(alternatePathForTheUserSettingsFile, that.alternatePathForTheUserSettingsFile) &&
                Objects.equals(pathToMavenLocalRepository, that.pathToMavenLocalRepository);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), overrideProjectFile, workingSubdirectory, alternatePathForTheGlobalSettingsFile, alternatePathForTheUserSettingsFile, pathToMavenLocalRepository);
    }
}
