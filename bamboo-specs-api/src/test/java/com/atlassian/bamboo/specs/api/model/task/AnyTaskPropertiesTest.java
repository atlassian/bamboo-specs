package com.atlassian.bamboo.specs.api.model.task;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.condition.AnyTaskCondition;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class AnyTaskPropertiesTest {

    private static final String DESCRIPTION = "task description";

    @Test
    public void testSuccessfulCreation() {
        final Requirement requirement = Requirement.equals("key", "value");
        final AnyTaskCondition condition = new AnyTaskCondition(new AtlassianModule("com.atlassian.bamboo.plugin.condition:userCondition"));
        condition.configuration(getConditionConfig());

        final AnyTaskProperties properties = EntityPropertiesBuilders.build(new AnyTask(new AtlassianModule("com.atlassian.bamboo.plugin:test"))
                .enabled(false)
                .description(DESCRIPTION)
                .requirements(requirement)
                .conditions(condition));

        assertThat(properties.isEnabled(), equalTo(false));
        assertThat(properties.getDescription(), equalTo(DESCRIPTION));
        assertThat(properties.getConditions(), hasSize(1));
        assertThat(properties.getConditions().iterator().next(), equalTo(EntityPropertiesBuilders.build(condition)));
        assertThat(properties.getRequirements(), hasSize(1));
        assertThat(properties.getRequirements().iterator().next(), equalTo(EntityPropertiesBuilders.build(requirement)));
    }

    @NotNull
    private Map<String, String> getConditionConfig() {
        Map<String, String> config = new HashMap<>();
        config.put("param1", "value1");
        config.put("param2", "value2");
        return config;
    }
}