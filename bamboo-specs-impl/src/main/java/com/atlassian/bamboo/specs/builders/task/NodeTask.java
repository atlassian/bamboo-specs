package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.NodeTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Represents a generic task that executes a Node.js script.
 * <p>
 * Bamboo provides additional tasks for more specific Node.js tools, which are suggested to use instead of this task for
 * convenience. For example: {@link NpmTask}, {@link GulpTask}.
 *
 * @see <a href="https://nodejs.org">nodejs.org</a>
 */
public class NodeTask extends BaseNodeTask<NodeTask, NodeTaskProperties> {
    @NotNull
    private String script;
    @Nullable
    private String arguments;

    /**
     * Script to execute with node (e.g. 'server.js', 'application.js').
     */
    public NodeTask script(@NotNull String script) {
        ImporterUtils.checkNotNull("script", script);
        this.script = script;
        return this;
    }

    /**
     * Additional command line arguments to pass to node when executing the script.
     */
    public NodeTask arguments(@Nullable String arguments) {
        this.arguments = arguments;
        return this;
    }

    @NotNull
    @Override
    protected NodeTaskProperties build() {
        return new NodeTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                script,
                arguments,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NodeTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        NodeTask nodeTask = (NodeTask) o;
        return script.equals(nodeTask.script) &&
                Objects.equals(arguments, nodeTask.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), script, arguments);
    }
}
