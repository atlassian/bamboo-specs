package com.atlassian.bamboo.specs.codegen.emitters.credentials;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import org.jetbrains.annotations.NotNull;

public class SharedCredentialsIdentifierEmitter extends EntityPropertiesEmitter<SharedCredentialsIdentifierProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull SharedCredentialsIdentifierProperties entity) throws CodeGenerationException {
        String constructorInvocation = emitConstructorInvocation(context, entity);
        context.importClassName(SharedCredentialsScope.class);
        return String.format("%s.scope(%s.%s)",
                constructorInvocation,
                SharedCredentialsScope.class.getSimpleName(),
                entity.getScope());
    }
}
