package com.atlassian.bamboo.specs.api.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

/**
 * Runtime context for specs execution.
 * Can be altered by setting Java properties when running specs.
 * The properties are set automatically when executing specs in RSS mode.
 */
public final class RssRuntimeContext {
    private static final String CURRENT_BRANCH_PROPERTY_NAME = "rss.current.branch";
    private static final String IS_DEFAULT_BRANCH_PROPERTY_NAME = "rss.default.branch";
    private static final String SERVER_NAME_PROPERTY_NAME = "specs.bamboo.instanceName";

    private static String currentBranch;
    private static Boolean isDefaultBranch;
    private static String serverName;

    static {
        loadProperties();
    }

    /**
     * Current branch.
     * @return branch of the repository from which currently executed specs are checked out.
     */
    @NotNull
    public static Optional<String> getCurrentRssBranch() {
        return Optional.ofNullable(currentBranch);
    }

    /**
     * Check if branch is default repository branch.
     * @return true if the current branch is the default branch of the repository according to its configuration in Bamboo.
     */
    @NotNull
    public static Optional<Boolean> isDefaultRssBranch() {
        return Optional.ofNullable(isDefaultBranch);
    }

    /**
     * Bamboo server name.
     * @return Bamboo instance name as configured at General settings. Can be used to restrict publishing of Specs objects
     * to specific Bamboo instances.
     */
    @NotNull
    public static Optional<String> getServerName() {
        return Optional.ofNullable(serverName);
    }

    private static void loadProperties() {
        currentBranch = readProperty(CURRENT_BRANCH_PROPERTY_NAME);

        String isDefaultBranch = System.getProperty(IS_DEFAULT_BRANCH_PROPERTY_NAME);
        if (isDefaultBranch != null) {
            RssRuntimeContext.isDefaultBranch = Boolean.valueOf(isDefaultBranch);
        }
        serverName = readProperty(SERVER_NAME_PROPERTY_NAME);
    }

    @Nullable
    private static String readProperty(String propertyName) {
        final String propertyValue = System.getProperty(propertyName);
        if (propertyValue != null) {
            try {
                return new String(Base64.getDecoder().decode(propertyValue.getBytes(StandardCharsets.ISO_8859_1)), StandardCharsets.UTF_8);
            } catch (IllegalArgumentException e) {
                return propertyValue;
            }
        }
        return null;
    }

    private RssRuntimeContext() {
    }
}
