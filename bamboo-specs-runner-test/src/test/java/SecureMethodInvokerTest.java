import com.atlassian.bamboo.specs.maven.sandbox.BambooSpecsSecurityManager;
import com.atlassian.bamboo.specs.maven.sandbox.ClassWithAccessToPackagePrivateMethods;
import com.atlassian.bamboo.specs.maven.sandbox.LowPrivilegeThreadPermissionVerifier;
import com.atlassian.bamboo.specs.maven.sandbox.PrivilegedThreads;
import com.atlassian.bamboo.specs.maven.sandbox.SecureMethodInvoker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.AccessMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.EnumMap;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SecureMethodInvokerTest {

    @BeforeEach
    void setupSecurityManager() {
        BambooSpecsSecurityManager.setPermissionCheckers(Collections.emptyMap(), new LowPrivilegeThreadPermissionVerifier(Paths.get(""), null));
    }

    @AfterEach
    void clearPermissionVerifiers() {
        PrivilegedThreads.resetThreadPermissionCheckers();
    }

    @Test
    public void cannotElevateAccess() {
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(() -> {
                final ClassWithAccessToPackagePrivateMethods classWithAccessToPackagePrivateMethods = new ClassWithAccessToPackagePrivateMethods();
                return null;
            });
        });
    }

    @Test
    public void cannotResetPermissionCheckers() {
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(() -> {
                BambooSpecsSecurityManager.clearPermissionCheckers();
                return null;
            });
        });
    }

    @Test
    public void cannotOverwritePermissionCheckers() {
        assertThrows(IllegalStateException.class, () -> {
            SecureMethodInvoker.invoke(() -> {
                BambooSpecsSecurityManager.setPermissionCheckers(Collections.emptyMap(), null);
                return null;
            });
        });
    }

    @Test
    public void cannotAccessThreadMethods() {
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(() -> {
                Thread.currentThread().setUncaughtExceptionHandler(null);
                return null;
            });
        });
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(() -> {
                Thread.currentThread().stop();
                return null;
            });
        });
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(() -> {
                Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
                return null;
            });
        });
    }

    @Test
    public void lambdasWork() {
        SecureMethodInvoker.invoke(() -> {
            final Supplier<Integer> integer = () -> 1;
            return integer.get();
        });
    }
    
    @Test
    public void enumsWork() {
        SecureMethodInvoker.invoke(() -> new EnumMap<>(AccessMode.class));
    }

    @Test
    public void fileAccessDoesNotWork() throws IOException {
        createFile();
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(this::createFile);
        });
    }
    
    @Test
    public void reflectionDoesNotWork() {
        supressAccessChecks();
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(this::supressAccessChecks);
        });
    }

    @Test
    public void commandExecutionDoesNotWork() {
        executeCommand();
        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(this::executeCommand);
        });
    }

    @Test
    public void networkDoesNotWork() {
        try {
            openServerSocket();
        } catch (final RuntimeException e) {
            assertTrue(e.getCause() instanceof UnknownHostException);
        }

        assertThrows(SecurityException.class, () -> {
            SecureMethodInvoker.invoke(this::openServerSocket);
        });
    }

    private Object openServerSocket() {
        try {
            try (final Socket socket = new Socket()) {
                socket.connect(new InetSocketAddress("this does not matter", 80));
                return null;
            }
        }
        catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Object supressAccessChecks() {
        try {
            final Method toString = Object.class.getMethod("toString");
            toString.setAccessible(true);
            toString.invoke(this);
        }
        catch (final RuntimeException e) {
            throw e;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    private Object createFile() {
        try {
            Files.createTempFile("prefix", "suffix");
        }
        catch (final IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    private Object executeCommand() {
        try {
            Runtime.getRuntime().exec("java");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}