package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.DefaultFieldValues;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

public final class VcsCheckoutTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.vcs:task.vcs.checkout");

    private final List<CheckoutItemProperties> checkoutItems;
    private final boolean cleanCheckout;

    private VcsCheckoutTaskProperties(boolean addDefaultSpec) {
        checkoutItems = addDefaultSpec ? Collections.singletonList(CheckoutItemProperties.forDefaultRepository()) : null;
        cleanCheckout = false;
    }

    private VcsCheckoutTaskProperties() {
        this(true);
    }

    @DefaultFieldValues
    private VcsCheckoutTaskProperties defaults() {
        return new VcsCheckoutTaskProperties(false);
    }

    public VcsCheckoutTaskProperties(final String description,
                                     final boolean isEnabled,
                                     final List<CheckoutItemProperties> checkoutItems,
                                     final boolean cleanCheckout,
                                     @NotNull List<RequirementProperties> requirements,
                                     @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, isEnabled, requirements, conditions);
        this.checkoutItems = checkoutItems;
        this.cleanCheckout = cleanCheckout;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    public List<CheckoutItemProperties> getCheckoutItems() {
        return checkoutItems;
    }

    public boolean isCleanCheckout() {
        return cleanCheckout;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final VcsCheckoutTaskProperties that = (VcsCheckoutTaskProperties) o;
        return isCleanCheckout() == that.isCleanCheckout() &&
                Objects.equals(getCheckoutItems(), that.getCheckoutItems());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCheckoutItems(), isCleanCheckout());
    }

    @Override
    public void validate() {
        super.validate();

        final ValidationContext context = ValidationContext.of("VCS checkout task");
        checkThat(context, checkoutItems != null && !checkoutItems.isEmpty(), "No repositories to check out are defined");

        Set<String> usedPaths = new HashSet<>();
        List<ValidationProblem> problems = new ArrayList<>();

        for (CheckoutItemProperties checkoutItem : checkoutItems) {
            if (usedPaths.contains(checkoutItem.getPath())) {
                problems.add(new ValidationProblem("Duplicate path " + checkoutItem.getPath()));
            }
            usedPaths.add(checkoutItem.getPath());
        }
        if (!problems.isEmpty()) {
            throw new PropertiesValidationException(problems);
        }
    }
}
