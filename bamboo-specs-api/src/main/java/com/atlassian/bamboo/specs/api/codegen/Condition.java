package com.atlassian.bamboo.specs.api.codegen;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

/**
 * Represents a condition.
 */
public interface Condition<T extends EntityProperties> {
    boolean evaluate(T properties);
}
