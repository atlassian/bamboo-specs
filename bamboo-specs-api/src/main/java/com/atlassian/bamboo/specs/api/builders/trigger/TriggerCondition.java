package com.atlassian.bamboo.specs.api.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;

public abstract class TriggerCondition<T extends TriggerCondition<T, P>, P extends TriggerConditionProperties> extends EntityPropertiesBuilder<P> {
    @Override
    protected abstract P build();
}
