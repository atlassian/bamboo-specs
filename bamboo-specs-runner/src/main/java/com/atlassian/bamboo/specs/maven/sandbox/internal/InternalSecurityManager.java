package com.atlassian.bamboo.specs.maven.sandbox.internal;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

/**
 * This class defines a security policy based on the applet security policy, with stuff that pertains only to applets removed.
 */
public class InternalSecurityManager extends SecurityManager {

    private static final RuntimePermission MODIFY_THREAD_PERMISSION =
            new RuntimePermission("modifyThread");

    // java.lang.SecurityManager, sun.applet.AppletSecurity
    private static final RuntimePermission MODIFY_THREADGROUP_PERMISSION =
            new RuntimePermission("modifyThreadGroup");

    /**
     * Construct and initialize.
     */
    public InternalSecurityManager() {
        reset();
    }

    // Cache to store known restricted packages
    private HashSet<String> restrictedPackages = new HashSet<>();

    /**
     * Reset from Properties.
     */
    public void reset() {
        // Clear cache
        restrictedPackages.clear();

        AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
            // Enumerate system properties
            Enumeration<?> e = System.getProperties().propertyNames();

            while (e.hasMoreElements()) {
                String name = (String) e.nextElement();

                if (name != null && name.startsWith("package.restrict.access.")) {
                    String value = System.getProperty(name);

                    if (value != null && value.equalsIgnoreCase("true")) {
                        String pkg = name.substring(24);

                        // Cache restricted packages
                        restrictedPackages.add(pkg);
                    }
                }
            }
            return null;
        });
    }

    /**
     * Returns true if this threadgroup is in the applet's own thread
     * group. This will return false if there is no current class
     * loader.
     */
    protected boolean inThreadGroup(ThreadGroup g) {
        return getThreadGroup().parentOf(g);
    }

    /**
     * Returns true of the threadgroup of thread is in the applet's
     * own threadgroup.
     */
    protected boolean inThreadGroup(Thread thread) {
        return inThreadGroup(thread.getThreadGroup());
    }

    /**
     * Applets are not allowed to manipulate threads outside
     * applet thread groups. However a terminated thread no longer belongs
     * to any group.
     */
    public void checkAccess(Thread t) {
        /* When multiple applets is reloaded simultaneously, there will be
         * multiple invocations to this method from plugin's SecurityManager.
         * This method should not be synchronized to avoid deadlock when
         * a page with multiple applets is reloaded
         */
        if ((t.getState() != Thread.State.TERMINATED) && !inThreadGroup(t)) {
            checkPermission(MODIFY_THREAD_PERMISSION);
        }
    }

    private boolean inThreadGroupCheck = false;

    /**
     * Applets are not allowed to manipulate thread groups outside applet thread groups.
     */
    public synchronized void checkAccess(ThreadGroup g) {
        if (inThreadGroupCheck) {
            // if we are in a recursive check, it is because
            // inThreadGroup is calling appletLoader.getThreadGroup
            // in that case, only do the super check, as appletLoader
            // has a begin/endPrivileged
            checkPermission(MODIFY_THREADGROUP_PERMISSION);
        } else {
            try {
                inThreadGroupCheck = true;
                if (!inThreadGroup(g)) {
                    checkPermission(MODIFY_THREADGROUP_PERMISSION);
                }
            } finally {
                inThreadGroupCheck = false;
            }
        }
    }


    /**
     * Throws a {@code SecurityException} if the
     * calling thread is not allowed to access the package specified by
     * the argument.
     * <p>
     * This method is used by the {@code loadClass} method of class
     * loaders.
     * <p>
     * The {@code checkPackageAccess} method for class
     * {@code SecurityManager}  calls
     * {@code checkPermission} with the
     * {@code RuntimePermission("accessClassInPackage."+ pkgname)}
     * permission.
     *
     * @param pkgname the package name.
     * @throws SecurityException if the caller does not have
     *                           permission to access the specified package.
     * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
     */
    public void checkPackageAccess(final String pkgname) {

        // first see if the VM-wide policy allows access to this package
        super.checkPackageAccess(pkgname);

        // now check the list of restricted packages
        for (Iterator<String> iter = restrictedPackages.iterator(); iter.hasNext();) {
            String pkg = iter.next();

            // Prevent matching "sun" and "sunir" even if they
            // starts with similar beginning characters
            //
            if (pkgname.equals(pkg) || pkgname.startsWith(pkg + ".")) {
                checkPermission(new java.lang.RuntimePermission("accessClassInPackage." + pkgname));
            }
        }
    }
} // class AppletSecurity
