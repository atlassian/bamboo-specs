package com.atlassian.bamboo.specs.api.model.plan.dependencies;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactProperties;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;

public class ArtifactPropertiesTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldAllowArtifactWithoutSlashInName() {
        final String artifactName = "MY-APPLICATION";
        ArtifactProperties artifactProperties = new ArtifactProperties(artifactName, Collections.emptyList(), Collections.emptyList(),"",true, false, true);
        Assert.assertThat(artifactProperties.getName(), equalTo(artifactName));
    }

    @Test
    public void shouldThrowExceptionWhenSlashInName() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Name can not contain '/' character");
        final String artifactName = "MY-COMPANY/MY-APPLICATION";
        new ArtifactProperties(artifactName,Collections.emptyList(), Collections.emptyList(),"",true, false, false);
    }
}
