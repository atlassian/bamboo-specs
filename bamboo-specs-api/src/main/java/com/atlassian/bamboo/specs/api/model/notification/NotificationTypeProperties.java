package com.atlassian.bamboo.specs.api.model.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.EnumSet;
import java.util.Set;

@Immutable
public abstract class NotificationTypeProperties implements EntityProperties {
    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    /**
     * Determines if notification type is applicable to {@link Applicability#PLANS} or {@link Applicability#DEPLOYMENTS}.
     * @return applicable entities
     */
    public Set<Applicability> applicableTo() {
        return EnumSet.allOf(Applicability.class);
    }
}
