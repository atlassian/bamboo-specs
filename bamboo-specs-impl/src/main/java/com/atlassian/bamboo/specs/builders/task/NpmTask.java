package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.NpmTaskProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents task that executes an npm command.
 *
 * @see <a href="https://nodejs.org">nodejs.org</a>
 * @see <a href="https://www.npmjs.com">www.nmpjs.com</a>
 */
public class NpmTask extends BaseNodeTask<NpmTask, NpmTaskProperties> {
    @NotNull
    private String command;
    private boolean useIsolatedCache;

    /**
     * Sets which command to execute, e.g. {@code install}.
     */
    public NpmTask command(@NotNull String command) {
        checkNotNull("command", command);
        this.command = command;
        return this;
    }

    /**
     * Sets whether the npm task should use isolated directory for caches. Should be enabled when different incompatible
     * versions of npm may be used on one Bamboo agent. Defaults to false.
     */
    public NpmTask useIsolatedCache(boolean useIsolatedCache) {
        this.useIsolatedCache = useIsolatedCache;
        return this;
    }

    @NotNull
    @Override
    protected NpmTaskProperties build() {
        return new NpmTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                command,
                useIsolatedCache,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NpmTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        NpmTask npmTask = (NpmTask) o;
        return useIsolatedCache == npmTask.useIsolatedCache &&
                command.equals(npmTask.command);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), command, useIsolatedCache);
    }
}
