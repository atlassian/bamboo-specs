package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.util.TestFileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ScriptTaskTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @Rule
    public TemporaryFolder sourceDirectory = new TemporaryFolder();

    private static final Requirement REQ = Requirement.equals("key", "value");
    private static final List<RequirementProperties> REQ_PROP = Collections.singletonList(new RequirementProperties("key", "value", Requirement.MatchType.EQUALS));

    @Test
    public void testEqualsOnAllProperties() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterBinSh()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .requirements(REQ)
                        .build(),
                equalTo(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        REQ_PROP, Collections.emptyList()))
        );
    }

    @Test
    public void testNotEqualOnDescription() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterBinSh()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .requirements(REQ)
                        .build(),
                not(equalTo(new ScriptTaskProperties(
                        "hello script 2",
                        true,
                        ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        REQ_PROP, Collections.emptyList())))
        );
    }

    @Test
    public void testWithInterpreterShell() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterShell()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build().getInterpreter(),
                equalTo(ScriptTaskProperties.Interpreter.SHELL)
        );
    }

    @Test
    public void testWithInterpreterCmdExe() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterCmdExe()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build().getInterpreter(),
                equalTo(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
        );
    }

    @Test
    public void testWithInterpreterBinSh() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterBinSh()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build().getInterpreter(),
                equalTo(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
        );
    }

    @Test
    public void testWithInterpreterWindowsPowerShell() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterWindowsPowerShell()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build().getInterpreter(),
                equalTo(ScriptTaskProperties.Interpreter.WINDOWS_POWER_SHELL)
        );
    }

    @Test
    public void testWithInterpreterPowershellErrorHandling() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterWindowsPowerShell()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .usePowershellErrorHandling(true)
                        .build()
                        .isUsePowershellErrorHandling(),
                equalTo(true)
        );
    }

    @Test
    public void testWithInterpreterPowershellErrorActionStop() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterWindowsPowerShell()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .usePowershellErrorActionStop(true)
                        .build()
                        .isUsePowershellErrorActionStop(),
                equalTo(true)
        );
    }

    @Test
    public void testWithInterpreterPowerShellAllErrorHandlingOptionsEnabled() throws Exception {
        ScriptTaskProperties task = new ScriptTask()
                .description("hello script 1")
                .enabled(true)
                .interpreterWindowsPowerShell()
                .inlineBody("echo Hello World")
                .argument("arg1 arg2 arg3")
                .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                .workingSubdirectory("work/dir")
                .usePowershellErrorHandling(true)
                .usePowershellErrorActionStop(true)
                .build();
        assertThat(task.isUsePowershellErrorHandling(), equalTo(true));
        assertThat(task.isUsePowershellErrorActionStop(), equalTo(true));
    }

    @Test
    public void testWithInterpreterPowerShellAllErrorHandlingOptionsDisabled() throws Exception {
        ScriptTaskProperties task = new ScriptTask()
                .description("hello script 1")
                .enabled(true)
                .interpreterWindowsPowerShell()
                .inlineBody("echo Hello World")
                .argument("arg1 arg2 arg3")
                .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                .workingSubdirectory("work/dir")
                .usePowershellErrorHandling(false)
                .usePowershellErrorActionStop(false)
                .build();
        assertThat(task.isUsePowershellErrorHandling(), equalTo(false));
        assertThat(task.isUsePowershellErrorActionStop(), equalTo(false));
    }

    @Test
    public void testWithInterpreterPowerShellAllErrorHandlingOptionsNulled() throws Exception {
        ScriptTaskProperties task = new ScriptTask()
                .description("hello script 1")
                .enabled(true)
                .interpreterWindowsPowerShell()
                .inlineBody("echo Hello World")
                .argument("arg1 arg2 arg3")
                .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                .workingSubdirectory("work/dir")
                .build();
        assertThat(task.isUsePowershellErrorHandling(), not(equalTo(null)));
        assertThat(task.isUsePowershellErrorActionStop(), not(equalTo(null)));
        assertThat(task.isUsePowershellErrorHandling(), equalTo(false));
        assertThat(task.isUsePowershellErrorActionStop(), equalTo(false));
    }

    @Test
    public void testNotEqualOnArgument() throws Exception {
        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterBinSh()
                        .inlineBody("echo Hello World")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build(),
                not(equalTo(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg3 arg4 arg5",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        REQ_PROP, Collections.emptyList())))
        );
    }

    @Test
    public void testInlineBodyFromPath() throws Exception {
        final String fileContent = "echo 1\n" +
                "echo 2\n" +
                "echo 3\n" +
                "echo BOOM!";

        TestFileUtils.createFile(sourceDirectory.getRoot(), "myscript.sh", fileContent);

        assertThat(new ScriptTask()
                        .description("hello script 1")
                        .enabled(true)
                        .interpreterBinSh()
                        .inlineBodyFromPath(TestFileUtils.getPathTo(sourceDirectory.getRoot(), "myscript.sh"))
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build(),
                not(equalTo(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                        ScriptTaskProperties.Location.INLINE,
                        fileContent,
                        null,
                        "arg3 arg4 arg5",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        false,
                        false,
                        REQ_PROP, Collections.emptyList())))
        );
    }

    @Test
    public void testInlineBodyFromPathFileDoesntExist() throws Exception {
        final Path pathToNonExistingScript = TestFileUtils.getPathTo(sourceDirectory.getRoot(), "myscript.sh");
        assertThat(Files.exists(pathToNonExistingScript), equalTo(false));

        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("File %s does not exist", pathToNonExistingScript));

        new ScriptTask()
                .inlineBodyFromPath(TestFileUtils.getPathTo(sourceDirectory.getRoot(), "myscript.sh"));
    }
}