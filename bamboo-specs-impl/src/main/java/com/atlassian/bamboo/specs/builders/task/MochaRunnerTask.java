package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.MochaRunnerTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

/**
 * Runs Mocha tests using 'mocha-bamboo-reporter'.
 *
 * @see <a href="https://mochajs.org/">mochajs.org</a>
 */
public class MochaRunnerTask extends BaseNodeTask<MochaRunnerTask, MochaRunnerTaskProperties> {
    public static final String DEFAULT_MOCHA_EXECUTABLE = preventInlining("node_modules/mocha/bin/mocha");
    public static final String DEFAULT_TEST_DIRECTORY = preventInlining("test/");
    public static final boolean DEFAULT_PARSE_TEST_RESULTS = preventInlining(true);

    @NotNull
    private String mochaExecutable = DEFAULT_MOCHA_EXECUTABLE;
    @NotNull
    private String testFilesAndDirectories = DEFAULT_TEST_DIRECTORY;
    private boolean parseTestResults = DEFAULT_PARSE_TEST_RESULTS;
    @Nullable
    private String arguments;

    /**
     * Specify path to the Mocha executable for this task. Path must be relative to the working directory.
     * <p>
     * Example: {@code node_modules/mocha/bin/mocha}
     */
    public MochaRunnerTask mochaExecutable(@NotNull String mochaExecutable) {
        ImporterUtils.checkNotNull("mochaExecutable", mochaExecutable);
        this.mochaExecutable = mochaExecutable;
        return this;
    }

    /**
     * Specify files and/or directories containing Mocha tests. You can specify multiple files and directories separated
     * by a space. Defaults to 'test/' directory.
     */
    public MochaRunnerTask testFilesAndDirectories(@NotNull String testFilesAndDirectories) {
        checkNotNull("testFilesAndDirectories", testFilesAndDirectories);
        this.testFilesAndDirectories = testFilesAndDirectories;
        return this;
    }

    /**
     * Whether this task should parse test results produced by Mocha. Defaults to true. If set to false, a subsequent
     * {@link MochaParserTask} should be used to gather the results.
     */
    public MochaRunnerTask parseTestResults(boolean parseTestResults) {
        this.parseTestResults = parseTestResults;
        return this;
    }

    /**
     * Additional command line arguments to pass to Mocha. Be aware that some options may already be set by this task
     * configuration.
     */
    public MochaRunnerTask arguments(@Nullable String arguments) {
        this.arguments = arguments;
        return this;
    }

    @NotNull
    @Override
    protected MochaRunnerTaskProperties build() {
        return new MochaRunnerTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                mochaExecutable,
                testFilesAndDirectories,
                parseTestResults,
                arguments,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MochaRunnerTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        MochaRunnerTask that = (MochaRunnerTask) o;
        return parseTestResults == that.parseTestResults &&
                mochaExecutable.equals(that.mochaExecutable) &&
                testFilesAndDirectories.equals(that.testFilesAndDirectories) &&
                Objects.equals(arguments, that.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mochaExecutable, testFilesAndDirectories, parseTestResults, arguments);
    }
}
