package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.ScpTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.ArtifactItemProperties;
import com.atlassian.bamboo.specs.model.task.ScpTaskProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ScpTaskEmitter extends EntityPropertiesEmitter<ScpTaskProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity) throws CodeGenerationException {
        builderClass = ScpTask.class;
        fieldsToSkip.add("artifactItem");
        fieldsToSkip.add("localPath");
        fieldsToSkip.add("localPathAntStyle");
        fieldsToSkip.add("host");

        return emitConstructorInvocation(context, entity)
                + emitFields(context, entity)
                + context.incIndentation()
                + context.newLine()
                + emitHost(context, entity)
                + context.decIndentation()
                + context.incIndentation()
                + context.newLine()
                + new BaseSshTaskAuthenticationEmitter().emitCodeForAuthentication(entity, context)
                + context.newLine()
                + emitCodeForLocalPathOrArtifact(context, entity)
                + context.decIndentation();
    }

    private String emitHost(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity) throws CodeGenerationException {
        if (entity.getHost() != null) {
            return Arrays.stream(entity.getHost().split(","))
                    .filter(StringUtils::isNotBlank)
                    .map(String::trim)
                    .map(h -> ".host(\"" + h + "\")")
                    .collect(Collectors.joining(""));
        } else {
            throw new CodeGenerationException("Host property is null");
        }
    }

    private String emitCodeForLocalPathOrArtifact(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity) throws CodeGenerationException {
        if (entity.getLocalPath() != null) {
            // local files or directories
            return emitCodeForLocalPath(entity);
        } else if (entity.getArtifactItem() != null) {
            // artifacts
            return emitCodeForArtifact(context, entity);
        } else {
            throw new CodeGenerationException("ScpTask requires an artifact to be uploaded. "
                    + "Neither a local file/directory nor a downloaded artifact was found in the task configuration.");
        }
    }

    private String emitCodeForLocalPath(@NotNull ScpTaskProperties entity) {
        return String.format(".fromLocalPath(\"%s\", %b)", entity.getLocalPath(), entity.isLocalPathAntStyle());
    }

    private String emitCodeForArtifact(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity)
            throws CodeGenerationException {
        final ArtifactItemProperties aip = entity.getArtifactItem();
        if (aip == null) {
            throw new CodeGenerationException("Artifact property is null");
        }

        return String.format(".fromArtifact(%s)", new ArtifactItemEmitter().emitCode(context, aip));
    }
}
