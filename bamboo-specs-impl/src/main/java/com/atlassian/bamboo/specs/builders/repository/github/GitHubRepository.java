package com.atlassian.bamboo.specs.builders.repository.github;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepository;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import com.atlassian.bamboo.specs.model.repository.git.AuthenticationProperties;
import com.atlassian.bamboo.specs.model.repository.git.SharedCredentialsAuthenticationProperties;
import com.atlassian.bamboo.specs.model.repository.github.GitHubRepositoryProperties;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * GitHub repository.
 */
public class GitHubRepository extends VcsRepository<GitHubRepository, GitHubRepositoryProperties> {

    private String repository;
    private String branch;
    private AuthenticationProperties authentication;

    private boolean useShallowClones;
    private boolean useRemoteAgentCache = true;
    private boolean useSubmodules;
    private boolean useSubmodulesWithShallowClones;
    private boolean sshKeyAppliesToSubmodules;
    private Duration commandTimeout = Duration.ofMinutes(180);
    private boolean verboseLogs;
    private boolean fetchWholeRepository;
    private boolean useLfs;
    private boolean webhookEnabled = GitHubRepositoryProperties.DEFAULT_WEBHOOK_ENABLED;
    private String baseUrl = GitHubRepositoryProperties.DEFAULT_BASE_URL;

    private VcsChangeDetectionProperties vcsChangeDetection;

    public GitHubRepository() {
    }

    /**
     * Sets gitHub repository name.
     * @param repository repository name.
     */
    public GitHubRepository repository(@NotNull String repository) {
        checkNotBlank("repository", repository);
        this.repository = repository;
        return this;
    }

    /**
     * Sets branch to check out.
     */
    public GitHubRepository branch(@NotNull String branch) {
        checkNotNull("branch", branch);
        this.branch = branch;
        return this;
    }

    /**
     * Specifies username/access token authentication.
     */
    public GitHubRepository authentication(@NotNull UserPasswordAuthentication userPasswordAuthentication) {
        checkNotNull("userPasswordAuthentication", userPasswordAuthentication);
        this.authentication = EntityPropertiesBuilders.build(userPasswordAuthentication);
        return this;
    }

    /**
     * Selects a previously defined {@link SharedCredentials} to configure account authentication with Github.
     * Currently, only username/password credentials are supported.
     */
    public GitHubRepository authentication(@NotNull SharedCredentialsIdentifier sharedCredentialsIdentifier) {
        checkNotNull("sharedCredentialsIdentifier", sharedCredentialsIdentifier);
        this.authentication = new SharedCredentialsAuthenticationProperties(EntityPropertiesBuilders.build(sharedCredentialsIdentifier));
        return this;
    }

    /**
     * Enables/disables shallow clones when checking out from the repository.
     * Fetches the shallowest commit history possible. Do not use if your build depends on full repository history.
     * Shallow clones are switched off by default.
     */
    public GitHubRepository shallowClonesEnabled(boolean useShallowClones) {
        this.useShallowClones = useShallowClones;
        return this;
    }

    /**
     * Enables/disables submodule support. Turned off by default.
     */
    public GitHubRepository submodulesEnabled(boolean useSubmodules) {
        this.useSubmodules = useSubmodules;
        return this;
    }

    /**
     * Enables/disables shallow clone support for submodules. Turned off by default.
     */
    public GitHubRepository submodulesUseShallowClonesEnabled(boolean useSubmodulesWithShallowClones) {
        this.useSubmodulesWithShallowClones = useSubmodulesWithShallowClones;
        return this;
    }

    /**
     * Enables/disabled authenticating to submodules with SSH key configured for this repository.
     */
    public GitHubRepository sshKeyAppliesToSubmodules(boolean sshKeyAppliesToSubmodules) {
        this.sshKeyAppliesToSubmodules = sshKeyAppliesToSubmodules;
        return this;
    }

    /**
     * Enables/disables caching repository content on the remote and elastic agents. Bamboo uses caching to reduce bandwidth needed when retrieving
     * source code from the repository. The feature is turned on by default.
     */
    public GitHubRepository remoteAgentCacheEnabled(boolean useRemoteAgentCache) {
        this.useRemoteAgentCache = useRemoteAgentCache;
        return this;
    }

    /**
     * Specifies how much time is given for git commands to finish. Default is 180 minutes.
     */
    public GitHubRepository commandTimeout(Duration commandTimeout) {
        this.commandTimeout = commandTimeout;
        return this;
    }

    /**
     * Specifies how much time in minutes is given for git commands to finish. Default is 180 minutes.
     */
    public GitHubRepository commandTimeoutInMinutes(int commandTimeoutMinutes) {
        return commandTimeout(Duration.ofMinutes(commandTimeoutMinutes));
    }

    /**
     * Enables/disables verbose logs from git commands. Off by default.
     */
    public GitHubRepository verboseLogs(boolean verboseLogs) {
        this.verboseLogs = verboseLogs;
        return this;
    }

    /**
     * Enforces (or not) fetching all remote refs from the repository rather than single branch. Off by default.
     */
    public GitHubRepository fetchWholeRepository(boolean fetchWholeRepository) {
        this.fetchWholeRepository = fetchWholeRepository;
        return this;
    }

    /**
     * Enables/disables git lfs support. Off by default.
     */
    public GitHubRepository lfsEnabled(boolean useLfs) {
        this.useLfs = useLfs;
        return this;
    }

    /**
     * Resets all change detection options to defaults.
     */
    public GitHubRepository defaultChangeDetection() {
        this.vcsChangeDetection = null;
        return this;
    }

    /**
     * Checks base Url. Default is "https://github.com"
     */
    public GitHubRepository baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    /**
     * Sets change detection options for this repository.
     *
     * @see VcsChangeDetection
     */
    public GitHubRepository changeDetection(@NotNull VcsChangeDetection vcsChangeDetection) {
        checkNotNull("vcsChangeDetection", vcsChangeDetection);
        this.vcsChangeDetection = EntityPropertiesBuilders.build(vcsChangeDetection);
        return this;
    }

    /**
     * Enables webhooks support for the repository. Off by default.
     * @since 10.1
     */
    public GitHubRepository webhookEnabled(boolean webhookEnabled) {
        this.webhookEnabled = webhookEnabled;
        return this;
    }

    @Override
    protected GitHubRepositoryProperties build() throws PropertiesValidationException {
        return new GitHubRepositoryProperties(name,
                oid,
                description,
                parent,
                repositoryViewer,
                repository,
                branch,
                project,
                authentication,
                vcsChangeDetection,
                useShallowClones,
                useRemoteAgentCache,
                useSubmodules,
                useSubmodulesWithShallowClones,
                sshKeyAppliesToSubmodules,
                commandTimeout,
                verboseLogs,
                fetchWholeRepository,
                useLfs,
                baseUrl,
                webhookEnabled);
    }
}
