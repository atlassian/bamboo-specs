package com.atlassian.bamboo.specs.codegen.emitters.repository;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.cloud.BitbucketCloudRepository;
import com.atlassian.bamboo.specs.builders.repository.git.SshPrivateKeyAuthentication;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import com.atlassian.bamboo.specs.codegen.BambooSpecsGenerator;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import com.atlassian.bamboo.specs.model.repository.bitbucket.cloud.BitbucketCloudRepositoryProperties;
import org.junit.Test;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BitbucketCloudRepositoryEmitterTest {

    @Test
    public void testExportRepositorySlug() throws Exception {
        BitbucketCloudRepository repository = new BitbucketCloudRepository()
                .name("repository")
                .repositorySlug("owner", "slug")
                .branch("branch");

        BambooSpecsGenerator generator = new BambooSpecsGenerator(EntityPropertiesBuilders.build(repository));

        String result = generator.emitCode();

        assertTrue(result.contains(String.format(".repositorySlug(\"%s\", \"%s\")", "owner", "slug")));
    }

    @Test
    public void testAccountUserPassword() throws Exception {
        final BitbucketCloudRepository repo = new BitbucketCloudRepository()
                .name("repository")
                .repositorySlug("owner", "repo")
                .branch("master")
                .accountAuthentication(new UserPasswordAuthentication("username")
                        .password("password"));

        final BitbucketCloudRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new BitbucketCloudRepository()" +
                " .name(\"repository\")" +
                " .repositoryViewer(new BitbucketCloudRepositoryViewer())" +
                " .repositorySlug(\"owner\", \"repo\")" +
                " .accountAuthentication(new UserPasswordAuthentication(\"username\")" +
                " .password(\"password\"))" +
                " .branch(\"master\")"));
    }

    @Test
    public void testSharedCredentials() throws Exception {
        final BitbucketCloudRepository repo = new BitbucketCloudRepository()
                .name("repository")
                .repositorySlug("owner", "repo")
                .branch("master")
                .accountAuthentication(new SharedCredentialsIdentifier("shared"));

        final BitbucketCloudRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new BitbucketCloudRepository()" +
                " .name(\"repository\")" +
                " .repositoryViewer(new BitbucketCloudRepositoryViewer())" +
                " .repositorySlug(\"owner\", \"repo\")" +
                " .accountAuthentication(new SharedCredentialsIdentifier(\"shared\").scope(SharedCredentialsScope.GLOBAL))" +
                " .branch(\"master\")"));
    }

    @Test
    public void testCheckoutSSHKey() throws Exception {
        final BitbucketCloudRepository repo = new BitbucketCloudRepository()
                .name("repository")
                .repositorySlug("owner", "repo")
                .branch("master")
                .checkoutAuthentication(new SshPrivateKeyAuthentication("key").passphrase("passphrase"));

        final BitbucketCloudRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new BitbucketCloudRepository()" +
                " .name(\"repository\")" +
                " .repositoryViewer(new BitbucketCloudRepositoryViewer())" +
                " .repositorySlug(\"owner\", \"repo\")" +
                " .checkoutAuthentication(new SshPrivateKeyAuthentication(\"key\")" +
                " .passphrase(\"passphrase\"))" +
                " .branch(\"master\")"));
    }

    @Test
    public void testCheckoutShared() throws Exception {
        final BitbucketCloudRepository repo = new BitbucketCloudRepository()
                .name("repository")
                .repositorySlug("owner", "repo")
                .branch("master")
                .checkoutAuthentication(new SharedCredentialsIdentifier("shared"));

        final BitbucketCloudRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new BitbucketCloudRepository()" +
                " .name(\"repository\")" +
                " .repositoryViewer(new BitbucketCloudRepositoryViewer())" +
                " .repositorySlug(\"owner\", \"repo\")" +
                " .checkoutAuthentication(new SharedCredentialsIdentifier(\"shared\").scope(SharedCredentialsScope.GLOBAL))" +
                " .branch(\"master\")"));
    }

    @Test
    public void testCheckoutWithProjectSharedCredentials() throws Exception {
        final BitbucketCloudRepository repo = new BitbucketCloudRepository()
                .name("repository")
                .repositorySlug("owner", "repo")
                .branch("master")
                .checkoutAuthentication(new SharedCredentialsIdentifier("shared").scope(SharedCredentialsScope.PROJECT));

        final BitbucketCloudRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new BitbucketCloudRepository()" +
                " .name(\"repository\")" +
                " .repositoryViewer(new BitbucketCloudRepositoryViewer())" +
                " .repositorySlug(\"owner\", \"repo\")" +
                " .checkoutAuthentication(new SharedCredentialsIdentifier(\"shared\").scope(SharedCredentialsScope.PROJECT))" +
                " .branch(\"master\")"));
    }
}
