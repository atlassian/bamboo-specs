package com.atlassian.bamboo.specs.api.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public final class AnyTaskProperties extends TaskProperties {
    private final AtlassianModuleProperties atlassianPlugin;
    private final Map<String, String> configuration;

    private AnyTaskProperties() {
        super();
        atlassianPlugin = null;
        configuration = Collections.emptyMap();
    }

    public AnyTaskProperties(@NotNull final AtlassianModuleProperties atlassianPlugin,
                             final String description,
                             final boolean isEnabled,
                             final Map<String, String> configuration,
                             final List<RequirementProperties> requirements,
                             final List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, isEnabled, requirements, conditions);
        this.atlassianPlugin = atlassianPlugin;
        this.configuration = Collections.unmodifiableMap(MapUtils.copyOf(configuration));
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    public Map<String, String> getConfiguration() {
        return configuration;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AnyTaskProperties that = (AnyTaskProperties) o;
        return Objects.equals(getConfiguration(), that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getConfiguration());
    }

    @Override
    public void validate() {
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
