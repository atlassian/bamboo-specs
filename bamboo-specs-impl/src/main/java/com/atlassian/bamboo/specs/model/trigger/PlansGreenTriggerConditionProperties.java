package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class PlansGreenTriggerConditionProperties extends TriggerConditionProperties {
    private static final String PLUGIN_KEY = "com.atlassian.bamboo.triggercondition.internal:plansGreenCondition";
    private final List<PlanIdentifierProperties> plans;

    private PlansGreenTriggerConditionProperties() {
        plans = null;
    }

    public PlansGreenTriggerConditionProperties(@NotNull final Set<PlanIdentifierProperties> plans) {
        super();
        this.plans = Collections.unmodifiableList(new ArrayList<>(plans));
        validate();
    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return new AtlassianModuleProperties(PLUGIN_KEY);
    }

    public List<PlanIdentifierProperties> getPlans() {
        return plans;
    }

    @Override
    public void validate() {
        super.validate();
        if (plans.isEmpty()) {
            throw new PropertiesValidationException("Plans should be not empty");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlansGreenTriggerConditionProperties that = (PlansGreenTriggerConditionProperties) o;
        return Objects.equals(plans, that.plans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(PLUGIN_KEY, plans);
    }
}
