package com.atlassian.bamboo.specs.api.model.credentials;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.credentials.SharedCredentialsIdentifierEmitter")
@ConstructFrom("name")
@Immutable
public final class SharedCredentialsIdentifierProperties implements EntityProperties {
    private final BambooOidProperties oid;
    private final String name;
    private final SharedCredentialsScope scope;

    private SharedCredentialsIdentifierProperties() {
        name = null;
        oid = null;
        scope = SharedCredentialsScope.GLOBAL;
    }

    public SharedCredentialsIdentifierProperties(@Nullable final String name,
                                                 @Nullable final BambooOidProperties oid,
                                                 @NotNull SharedCredentialsScope scope) throws PropertiesValidationException {
        this.name = name;
        this.oid = oid;
        this.scope = scope;

        validate();
    }

    public SharedCredentialsIdentifierProperties(@Nullable final String name,
                                                 @Nullable final BambooOidProperties oid) throws PropertiesValidationException {
        this(name, oid, SharedCredentialsScope.GLOBAL);
    }

    @Nullable
    public String getName() {
        return name;
    }

    public boolean isNameDefined() {
        return StringUtils.isNotBlank(name);
    }

    @Nullable
    public BambooOidProperties getOid() {
        return oid;
    }

    public boolean isOidDefined() {
        return oid != null;
    }

    @NotNull
    public SharedCredentialsScope getScope() {
        return scope;
    }

    public boolean isScopeDefined() {
        return scope != null;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SharedCredentialsIdentifierProperties that = (SharedCredentialsIdentifierProperties) o;
        return Objects.equals(getOid(), that.getOid()) &&
               Objects.equals(getName(), that.getName()) &&
               Objects.equals(getScope(), that.getScope());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getName(), getScope());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("oid", oid)
                .append("name", name)
                .append("scope", scope)
                .build();
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(name) && oid == null) {
            throw new PropertiesValidationException("Either name or oid need to be defined when referencing shared credentials");
        }
        if (scope == null) {
            throw new PropertiesValidationException("Shared credentials scope need to be defined");
        }
    }
}
