package com.atlassian.bamboo.specs.api.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.trigger.AnyTriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a trigger of any type.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation of a given trigger is not available.
 */
public class AnyTrigger extends RepositoryBasedTrigger<AnyTrigger, AnyTriggerProperties> {
    private AtlassianModuleProperties atlassianPlugin;
    private Map<String, String> configuration;

    /**
     * Specifies a trigger of given type.
     *
     * @param atlassianModule type of the trigger identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyTrigger(@NotNull final AtlassianModule atlassianModule) throws PropertiesValidationException {
        checkNotNull("atlassianModule", atlassianModule);
        atlassianPlugin = EntityPropertiesBuilders.build(atlassianModule);
        configuration = new LinkedHashMap<>();
    }

    public AnyTrigger(@NotNull final AnyTrigger other) throws PropertiesValidationException {
        this(new AtlassianModule(other.atlassianPlugin.getCompleteModuleKey()));
        this.name = other.name;
        this.triggerEnabled = other.triggerEnabled;
        this.description = other.description;
        configuration(other.configuration);
    }

    /**
     * Appends the generic configuration to the trigger.
     * <p>
     * Trigger configuration typically contains data like: polling period, cron expression etc.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is
     * performed on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyTrigger configuration(final Map<String, String> configuration) {
        this.configuration.putAll(configuration);
        return this;
    }

    protected AnyTriggerProperties build() throws PropertiesValidationException {
        return new AnyTriggerProperties(atlassianPlugin, name, description, triggerEnabled, conditions, configuration, triggeringRepositoriesType, selectedTriggeringRepositories);
    }
}
