package com.atlassian.bamboo.specs.api.validators.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.model.permission.GroupPermissionProperties;
import com.atlassian.bamboo.specs.api.model.permission.LoggedInUserPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.PermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.UserPermissionProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator.PermissionTarget.DEPLOYMENT;
import static com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator.PermissionTarget.ENVIRONMENT;
import static com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator.PermissionTarget.PLAN;
import static com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator.PermissionTarget.PROJECT;

public final class PermissionValidator {
    private PermissionValidator() {
    }

    public enum PermissionTarget {
        PLAN, DEPLOYMENT, ENVIRONMENT, PROJECT
    }

    private static final Map<PermissionTarget, Set<PermissionType>> ALLOWED_PERMISSIONS = new HashMap<>();

    static {
        ALLOWED_PERMISSIONS.put(PLAN, EnumSet.of(PermissionType.VIEW, PermissionType.EDIT, PermissionType.BUILD, PermissionType.CLONE, PermissionType.ADMIN, PermissionType.VIEW_CONFIGURATION, PermissionType.CREATE_PLAN_BRANCH));
        ALLOWED_PERMISSIONS.put(DEPLOYMENT, EnumSet.of(PermissionType.VIEW, PermissionType.EDIT, PermissionType.VIEW_CONFIGURATION, PermissionType.APPROVE_RELEASE, PermissionType.CREATE_RELEASE,  PermissionType.CLONE, PermissionType.ADMIN));
        ALLOWED_PERMISSIONS.put(ENVIRONMENT, EnumSet.of(PermissionType.VIEW, PermissionType.EDIT, PermissionType.BUILD, PermissionType.VIEW_CONFIGURATION));
        ALLOWED_PERMISSIONS.put(PROJECT, EnumSet.of(PermissionType.VIEW, PermissionType.CREATE, PermissionType.CREATE_REPOSITORY, PermissionType.ADMIN));
    }

    public static List<ValidationProblem> validatePermissions(@NotNull final PermissionsProperties permission, @NotNull PermissionTarget target) {
        final List<ValidationProblem> errors = new ArrayList<>();

        errors.addAll(validateGroupPermissions(permission.getGroupPermissions(), target));
        errors.addAll(validateUserPermissions(permission.getUserPermissions(), target));
        errors.addAll(validateLoggedInUserPermissions(permission.getLoggedInUserPermissions(), target));

        return errors;
    }

    private static List<ValidationProblem> validateLoggedInUserPermissions(LoggedInUserPermissionsProperties loggedInUserPermission, PermissionTarget target) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        for (PermissionType permissionType : loggedInUserPermission.getPermissionTypes()) {
            if (!ALLOWED_PERMISSIONS.get(target).contains(permissionType)) {
                errors.add(new ValidationProblem(String.format("Cannot assign permission %s to logged in users as it's not applicable to %ss",
                        permissionType, target.name().toLowerCase())));
            }
        }
        return errors;
    }

    private static List<ValidationProblem> validateUserPermissions(List<UserPermissionProperties> userPermissions, PermissionTarget target) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        for (UserPermissionProperties userPermissionProperties : userPermissions) {
            for (PermissionType permissionType : userPermissionProperties.getPermissionTypes()) {
                if (!ALLOWED_PERMISSIONS.get(target).contains(permissionType)) {
                    errors.add(new ValidationProblem(String.format("Cannot assign permission %s to user %s as it's not applicable to %ss",
                            permissionType, userPermissionProperties.getUsername(), target.name().toLowerCase())));
                }
            }
        }
        return errors;
    }

    private static List<ValidationProblem> validateGroupPermissions(List<GroupPermissionProperties> groupPermissions, PermissionTarget target) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        for (GroupPermissionProperties groupPermissionProperties : groupPermissions) {
            for (PermissionType permissionType : groupPermissionProperties.getPermissionTypes()) {
                if (!ALLOWED_PERMISSIONS.get(target).contains(permissionType)) {
                    errors.add(new ValidationProblem(String.format("Cannot assign permission %s to group %s as it's not applicable to %ss",
                            permissionType, groupPermissionProperties.getGroup(), target.name().toLowerCase())));
                }
            }
        }
        return errors;
    }
}
