package com.atlassian.bamboo.specs.api.builders.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a web repository viewer.
 * <p>
 * Web repository viewers are responsible for rendering links to external systems for commits, files, etc.
 */
public abstract class VcsRepositoryViewer extends EntityPropertiesBuilder<VcsRepositoryViewerProperties> {
    @NotNull
    @Override
    protected abstract VcsRepositoryViewerProperties build();
}
