package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.trigger.DeploymentTriggerProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public class AfterSuccessfulBuildPlanTriggerProperties extends DeploymentTriggerProperties {
    public static final String NAME = "After successful build plan";
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:afterSuccessfulPlan";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));
    @Setter("triggerByBranch")
    private final String releaseBranch;

    private AfterSuccessfulBuildPlanTriggerProperties() {
        super(NAME, null, true);
        releaseBranch = null;
    }

    /**
     * Deprecated. Use {@link AfterSuccessfulBuildPlanTriggerProperties#AfterSuccessfulBuildPlanTriggerProperties(String, String, boolean, String)}
     * @deprecated since 10.0
     */
    @Deprecated
    public AfterSuccessfulBuildPlanTriggerProperties(String description, boolean enabled, @Nullable String releaseBranch) {
        super(NAME, description, enabled);
        this.releaseBranch = releaseBranch;
    }

    public AfterSuccessfulBuildPlanTriggerProperties(String name, String description, boolean enabled, @Nullable String releaseBranch) {
        super(StringUtils.defaultIfBlank(name, NAME), description, enabled);
        this.releaseBranch = releaseBranch;
    }

    @Nullable
    public String getReleaseBranch() {
        return releaseBranch;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AfterSuccessfulBuildPlanTriggerProperties that = (AfterSuccessfulBuildPlanTriggerProperties) o;
        return Objects.equals(releaseBranch, that.releaseBranch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), releaseBranch);
    }
}
