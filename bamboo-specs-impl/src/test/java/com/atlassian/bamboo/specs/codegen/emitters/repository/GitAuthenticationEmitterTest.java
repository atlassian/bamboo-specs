package com.atlassian.bamboo.specs.codegen.emitters.repository;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.repository.git.SshPrivateKeyAuthentication;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import com.atlassian.bamboo.specs.model.repository.git.GitRepositoryProperties;
import org.junit.Test;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.junit.Assert.assertThat;

public class GitAuthenticationEmitterTest {

    @Test
    public void testUserPassword() throws Exception {
        final GitRepository repo = new GitRepository()
                .name("repository")
                .url("https://url")
                .authentication(new UserPasswordAuthentication("username")
                        .password("password"));

        final GitRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new GitRepository()" +
                " .name(\"repository\")" +
                " .url(\"https://url\")" +
                " .branch(\"master\")" +
                " .authentication(new UserPasswordAuthentication(\"username\")" +
                " .password(\"password\"))"));
    }

    @Test
    public void testSharedCredentials() throws Exception {
        final GitRepository repo = new GitRepository()
                .name("repository")
                .url("https://url")
                .authentication(new SharedCredentialsIdentifier("shared"));
        final GitRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new GitRepository()" +
                " .name(\"repository\")" +
                " .url(\"https://url\")" +
                " .branch(\"master\")" +
                " .authentication(new SharedCredentialsIdentifier(\"shared\").scope(SharedCredentialsScope.GLOBAL))"));
    }

    @Test
    public void testSSHKey() throws Exception {
        final GitRepository repo = new GitRepository()
                .name("repository")
                .url("ssh://url")
                .authentication(new SshPrivateKeyAuthentication("key").passphrase("passphrase"));

        final GitRepositoryProperties props = build(repo);

        final CodeEmitter<EntityProperties> emitter = ValueEmitterFactory.emitterFor(props);
        String result = emitter.emitCode(new CodeGenerationContext(), props);

        assertThat(result, equalToIgnoringWhiteSpace("new GitRepository()" +
                " .name(\"repository\")" +
                " .url(\"ssh://url\")" +
                " .branch(\"master\")" +
                " .authentication(new SshPrivateKeyAuthentication(\"key\")" +
                " .passphrase(\"passphrase\"))"));
    }
}