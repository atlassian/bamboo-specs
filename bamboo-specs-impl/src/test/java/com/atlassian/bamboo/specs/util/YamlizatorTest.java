package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.permission.DeploymentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.EnvironmentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import com.atlassian.bamboo.specs.api.model.deployment.DeploymentProperties;
import com.atlassian.bamboo.specs.api.model.permission.DeploymentPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.EnvironmentPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.PlanPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanProperties;
import com.atlassian.bamboo.specs.builders.credentials.SshCredentials;
import com.atlassian.bamboo.specs.builders.notification.DeploymentFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.UserRecipient;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.trigger.AfterSuccessfulBuildPlanTrigger;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.builders.trigger.ScheduledTrigger;
import com.atlassian.bamboo.specs.model.repository.git.GitRepositoryProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.yaml.snakeyaml.composer.ComposerException;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class YamlizatorTest {

    @Rule
    public ExpectedException throwing = ExpectedException.none();

    @Test
    public void testPlanIsLoaded() {
        // dump plan into YAML
        final Plan plan = new Plan(new Project()
                .key(new BambooKey("YAML")),
                "YAML",
                new BambooKey("YAML"))
                .oid(new BambooOid("1jrui76a1rv9d"))
                .stages(new Stage("Default Stage")
                        .jobs(new Job("Default Job",
                                new BambooKey("JOB1"))
                                .artifacts(new Artifact()
                                        .name("Test Resuls")
                                        .copyPatterns("**/*.xml")
                                        .location("target")
                                        .shared(true))
                                .tasks(new ScriptTask()
                                        .inlineBody("echo hello world"))
                                .requirements(new Requirement("system.builder.ant.Ant"))
                                .dockerConfiguration(new DockerConfiguration()
                                        .enabled(true)
                                        .image("atlassian/bamboo:6.3.0"))))
                .linkedRepositories("bbs-specs")
                //.notifications introduced in 6.1
                .planBranchManagement(new PlanBranchManagement()
                        .createForVcsBranch()
                        .delete(new BranchCleanup()
                                .whenRemovedFromRepositoryAfterDays(7)
                                .whenInactiveInRepositoryAfterDays(30))
                        .notificationLikeParentPlan()
                        .issueLinkingEnabled(false))
                .triggers(
                        new RepositoryPollingTrigger()
                                .name("Repository polling"),
                        new ScheduledTrigger()
                                .scheduleWeekly(LocalTime.NOON, DayOfWeek.SATURDAY),
                        new ScheduledTrigger()
                                .scheduleEvery(24, TimeUnit.HOURS))
                .variables(new Variable("debug", "true"));

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(plan);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(PlanProperties.class));
    }

    @Test
    public void testPlanPermissionsAreLoaded() {
        // dump plan permissions into YAML
        final PlanPermissions planPermissions = new PlanPermissions(new PlanIdentifier("PROJECT", "PLAN"))
                .permissions(
                        new Permissions()
                                .userPermissions("admin", PermissionType.ADMIN, PermissionType.EDIT, PermissionType.VIEW)
                                .groupPermissions("bamboo-users", PermissionType.BUILD, PermissionType.CLONE)
                                .loggedInUserPermissions()
                                .anonymousUserPermissionView()
                );

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(planPermissions);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(PlanPermissionsProperties.class));
    }

    @Test
    public void testDeploymentProjectIsLoaded() {
        // dump deployment into YAML
        final Deployment deployment = new Deployment(new PlanIdentifier("PROJECT", "PLAN"), "Deploy")
                .description("description")
                .environments(
                        new Environment("Dev")
                                .notifications(
                                        new Notification()
                                                .type(new DeploymentFailedNotification())
                                                .recipients(new UserRecipient("admin")))
                                .requirements(
                                        new Requirement("requirement")
                                                .matchType(Requirement.MatchType.EXISTS)
                                )
                                .triggers(
                                        new AfterSuccessfulBuildPlanTrigger()
                                                .triggerByMasterBranch()
                                )
                                .variables(
                                        new Variable("variable", "value")
                                )
                                .dockerConfiguration(
                                        new DockerConfiguration()
                                                .enabled(true)
                                                .image("atlassian/bamboo:6.3.0")
                                )
                )
                .releaseNaming(
                        new ReleaseNaming("release-1")
                                .applicableToBranches(true)
                                .autoIncrement(true));

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(deployment);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(DeploymentProperties.class));
    }

    @Test
    public void testDeploymentPermissionsAreLoaded() {
        // dump deployment permissions into YAML
        final DeploymentPermissions deploymentPermissions = new DeploymentPermissions("Deploy")
                .permissions(
                        new Permissions()
                                .userPermissions("admin", PermissionType.EDIT, PermissionType.VIEW)
                                .groupPermissions("bamboo-users", PermissionType.EDIT, PermissionType.VIEW)
                                .loggedInUserPermissions()
                                .anonymousUserPermissionView()
                );

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(deploymentPermissions);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(DeploymentPermissionsProperties.class));
    }

    @Test
    public void testEnvironmentPermissionsAreLoaded() {
        // dump environment permissions into YAML
        final EnvironmentPermissions environmentPermissions = new EnvironmentPermissions("Deploy")
                .environmentName("Dev")
                .permissions(
                        new Permissions()
                                .userPermissions("admin", PermissionType.EDIT, PermissionType.VIEW)
                                .groupPermissions("bamboo-users", PermissionType.BUILD, PermissionType.VIEW)
                                .loggedInUserPermissions()
                                .anonymousUserPermissionView()
                );

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(environmentPermissions);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(EnvironmentPermissionsProperties.class));
    }

    @Test
    public void testLinkedRepositoryIsLoaded() {
        // dump a linked repository into YAML
        final GitRepository gitRepository = new GitRepository()
                .name("git")
                .url("git@localhost")
                .fetchWholeRepository(true)
                .authentication(new UserPasswordAuthentication("username")
                        .password("password"));

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(gitRepository);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(GitRepositoryProperties.class));
    }

    @Test
    public void testSharedCredentialsAreLoaded() {
        // dump shared credentials into YAML
        final SshCredentials sharedCredentials = new SshCredentials("my-credentials")
                .passphrase("passphrase")
                .privateKey("key");

        // check we can still parse it back
        final String yaml = BambooSpecSerializer.dump(sharedCredentials);
        final Object result = Yamlizator.getYaml().load(yaml);

        assertThat(result, instanceOf(BambooSpecProperties.class));
        assertThat(((BambooSpecProperties) result).getRootEntity(), instanceOf(SharedCredentialsProperties.class));
    }

    @Test
    public void testNonWhitelistedClassesAreRejectedFromRoot() throws Exception {
        throwing.expect(ComposerException.class);
        throwing.expectMessage("Global tag is not allowed: tag:yaml.org,2002:javax.script.ScriptEngineManager");
        final String yaml = "--- !!javax.script.ScriptEngineManager\n" +
                "...";

        Yamlizator.getYaml().load(yaml);
    }

    @Test
    public void testNonWhitelistedClassesAreRejectedFromProperties() throws Exception {
        throwing.expect(YAMLException.class);
        throwing.expectMessage("Global tag is not allowed: tag:yaml.org,2002:javax.script.ScriptEngineManager");
        final String yaml = "--- !!com.atlassian.bamboo.specs.util.BambooSpecProperties\n" +
                "rootEntity: !!javax.script.ScriptEngineManager\n" +
                "...";

        Yamlizator.getYaml().load(yaml);
    }

    @Test
    public void testNonWhitelistedClassesAreRejectedFromLists() throws Exception {
        throwing.expect(YAMLException.class);
        throwing.expectMessage("Global tag is not allowed: tag:yaml.org,2002:javax.script.ScriptEngineManager");
        final String yaml = "--- !!com.atlassian.bamboo.specs.util.BambooSpecProperties\n" +
                "rootEntity: !!com.atlassian.bamboo.specs.api.model.plan.PlanProperties\n" +
                "  triggers: [ " +
                "    !!javax.script.ScriptEngineManager \n" +
                "  ]\n" +
                "...";

        Yamlizator.getYaml().load(yaml);
    }

    @Test
    public void testNonWhitelistedClassesAreRejectedFromConstructorArguments() throws Exception {
        throwing.expect(YAMLException.class);
        throwing.expectMessage("Global tag is not allowed: tag:yaml.org,2002:javax.script.ScriptEngineManager");
        final String yaml = "--- !!com.atlassian.bamboo.specs.util.BambooSpecProperties\n" +
                "rootEntity: !!com.atlassian.bamboo.specs.api.model.BambooOidProperties [\n" +
                "  !!javax.script.ScriptEngineManager \n" +
                "]\n" +
                "...";

        Yamlizator.getYaml().load(yaml);
    }

    @Test
    public void testWithBasicInclude() {
        LinkedHashMap yaml = loadYaml("/yamlizator/1_root.yaml");
        assertThat(yaml, hasToString("{foo={bar={K1=K1 value, K2=K2 value}}}"));
    }

    @Test
    public void testIncludeWithCircularDependency() {
        testYamlException("/yamlizator/2_root_WithCircularDependency.yaml", "To many recursive include calls");
    }

    @Test
    public void testIncludeFromOtherDirectory() {
        testYamlException("/yamlizator/3_root_includeFromOtherDirectory.yaml", "is not in the source directory");
    }

    @Test
    public void testAnchorsWithCircularDependency() {
        testYamlException("/yamlizator/4_root_anchorsWithCircularDependency.yaml", "found undefined alias anchor");
    }

    @Test
    public void testIncludeWithManyDocuments() {
        testYamlException("/yamlizator/5_root.yaml", "expected a single document in the stream");
    }

    @Test
    public void testIncludeWithWrongFileSuffix() {
        testYamlException("/yamlizator/7_root_includeWithWrongSuffix.yaml", "has not proper suffix");
    }

    @Test
    public void testIncludeOverrideInAnchor() {
        LinkedHashMap yaml = loadYaml("/yamlizator/8_root_includeInAnchor.yaml");
        assertThat(yaml, hasToString("{foo={K1={K2=value1}}, bar={K1={K2=value2}}}"));
    }

    @Test
    public void testIncludeWithSpaces() {
        LinkedHashMap yaml = loadYaml("/yamlizator/9_root_spacesInInclude.yaml");
        assertThat(yaml, hasToString("{K1={K2=value2}}"));
    }

    @Test
    public void testAnchorFromRootYamlUsedInincludedFile() {
        LinkedHashMap yaml = loadYaml("/yamlizator/6_root_anchorInIncludes.yaml");
        assertThat(yaml, hasToString("{foo={K1=One}, bar={K2={foobar=null}}}"));
    }

    @Test
    public void testIncludeWithChineseCharacters() {
        LinkedHashMap yaml = loadYaml("/yamlizator/10_root.yaml");
        assertThat(yaml, hasToString("{Recipe={K1=餃子湯, 咖哩雞=它很好吃}}"));
    }

    private void testYamlException(final String yamlFile, final String exceptionMessage) {
        try {
            final Path yamlDirectory = getPathToYamlDirectory(yamlFile);
            String rawYaml = readFileFromResource(yamlFile);
            try {
                Yamlizator.getYamlWithRepositoryIncludes(10, yamlDirectory).load(rawYaml);
                fail();
            } catch (YAMLException e) {
                String message = e.getMessage();
                assertThat(message, containsString(exceptionMessage));
            }
        } catch (IOException ioe) {
            fail(ioe.getMessage());
        }
    }

    private LinkedHashMap loadYaml(final String yamlFile) {
        try {
            final Path yamlDirectory = getPathToYamlDirectory(yamlFile);
            String rawYaml = readFileFromResource(yamlFile);
            try {
                return (LinkedHashMap) Yamlizator.getYamlWithRepositoryIncludes(10, yamlDirectory).load(rawYaml);
            } catch (YAMLException e) {
                fail(e.getMessage());
            }
        } catch (IOException ioe) {
            fail(ioe.getMessage());
        }
        return null;
    }

    private Path getPathToYamlDirectory(final String yamlFile) {
        try {
            URI resource = this.getClass().getResource(yamlFile).toURI();
            return Paths.get(resource).getParent();
        } catch (Exception e) {
            throw new RuntimeException("", e);
        }
    }

    private String readFileFromResource(final String fileName) throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream(fileName);
        try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF8");
        } finally {
            inputStream.close();
        }
    }
}
