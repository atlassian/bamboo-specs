package com.atlassian.bamboo.specs.api.builders.plan.configuration;

import com.atlassian.bamboo.specs.api.model.plan.configuration.ForceStopBuildProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class ForceStopBuild extends PluginConfiguration<ForceStopBuildProperties> {
    private Boolean enabled;

    public ForceStopBuild enabled(@Nullable Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public ForceStopBuild useSystemDefault() {
        this.enabled = null;
        return this;
    }

    @Override
    @NotNull
    protected  ForceStopBuildProperties build() {
        return new ForceStopBuildProperties(enabled);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ForceStopBuild)) {
            return false;
        }
        ForceStopBuild that = (ForceStopBuild) o;
        return Objects.equals(enabled, that.enabled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enabled);
    }
}
