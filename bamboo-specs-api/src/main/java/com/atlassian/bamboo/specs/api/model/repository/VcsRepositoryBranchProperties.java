package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGenIf;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsXssRelatedCharacters;

@ConstructFrom({"repositoryName", "branchName"})
@Immutable
public class VcsRepositoryBranchProperties implements EntityProperties {

    private String repositoryName;
    private String branchName;
    @SkipCodeGenIf(SkipDisplayNameCondition.class)
    private String branchDisplayName;

    private VcsRepositoryBranchProperties() {
    }

    public VcsRepositoryBranchProperties(@NotNull String repositoryName, @NotNull String branchName, @Nullable String branchDisplayName) {
        this.repositoryName = repositoryName;
        this.branchName = branchName;
        this.branchDisplayName = branchDisplayName;
        validate();
    }

    @NotNull
    public String getRepositoryName() {
        return repositoryName;
    }

    @NotNull
    public String getBranchName() {
        return branchName;
    }

    @Nullable
    public String getBranchDisplayName() {
        return branchDisplayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VcsRepositoryBranchProperties that = (VcsRepositoryBranchProperties) o;
        return Objects.equals(getRepositoryName(), that.getRepositoryName()) &&
                Objects.equals(getBranchName(), that.getBranchName()) &&
                Objects.equals(getBranchDisplayName(), that.getBranchDisplayName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRepositoryName(), getBranchName(), getBranchDisplayName());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Repository branch");
        final List<ValidationProblem> errors = new ArrayList<>();

        checkNotBlank("repositoryName", repositoryName);
        checkNotBlank("branchName", branchName);

        validateNotContainsShellInjectionRelatedCharacters(context.with("Branch name"), branchName)
                .ifPresent(errors::add);

        validateNotContainsXssRelatedCharacters(context.with("Branch display name"), branchDisplayName)
                .ifPresent(errors::add);

        checkNoErrors(errors);
    }
}
