package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;


public class AnyTriggerConditionProperties extends TriggerConditionProperties {
    private final AtlassianModuleProperties atlassianPlugin;
    private final Map<String, String> configuration = new LinkedHashMap<>();

    private AnyTriggerConditionProperties() {
        atlassianPlugin = null;
    }

    public AnyTriggerConditionProperties(@Nullable AtlassianModuleProperties atlassianPlugin,
                                         @NotNull Map<String, String> configuration) {
        this.atlassianPlugin = atlassianPlugin;
        this.configuration.putAll(configuration);
        validate();
    }

    @Override
    @Nullable
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    /**
     * Configuration map.
     * @deprecated since 8.0.1. Use {{@link #getConfiguration()}}
     */
    @Deprecated
    @NotNull
    public Map<String, String> getConfig() {
        return Collections.unmodifiableMap(configuration);
    }

    @NotNull
    public Map<String, String> getConfiguration() {
        return Collections.unmodifiableMap(configuration);
    }

    @Override
    public void validate() {
        super.validate();
        checkNotNull("configuration", configuration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnyTriggerConditionProperties that = (AnyTriggerConditionProperties) o;
        return Objects.equals(atlassianPlugin, that.atlassianPlugin) &&
                Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(atlassianPlugin, configuration);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("atlassianPlugin", atlassianPlugin)
                .append("configuration", configuration)
                .toString();
    }
}
