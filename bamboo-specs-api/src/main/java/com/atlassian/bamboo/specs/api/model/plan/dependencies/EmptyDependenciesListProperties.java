package com.atlassian.bamboo.specs.api.model.plan.dependencies;


import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;

import javax.annotation.concurrent.Immutable;
import java.util.List;

@Immutable
@Deprecated
public final class EmptyDependenciesListProperties extends DependenciesProperties {

    public EmptyDependenciesListProperties() {
    }

    public DependenciesConfigurationProperties getDependenciesConfigurationProperties() {
        throw new RuntimeException("EmptyDependenciesListProperties does not have a dependenciesConfigurationProperties");
    }

    public List<PlanIdentifierProperties> getChildPlans() {
        throw new RuntimeException("EmptyDependenciesListProperties does not have a childPlans");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public void validate() {
    }

}
