package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class PlanDeploymentNotificationsTest {

    @Rule
    public ExpectedException expectedExceptionRule = ExpectedException.none();

    @Parameters(method = "planParams")
    @Test
    public void testPlanNotifications(NotificationRecipient notificationRecipient,
                                      NotificationType notificationType,
                                      Class<Exception> expectedException) {
        if (expectedException != null) {
            expectedExceptionRule.expect(expectedException);
        }

        Plan plan = new Plan(new Project()
                .name("Project name")
                .key("PROJ"), "Plan Name", "PLN")
                .notifications(new Notification()
                        .recipients(notificationRecipient)
                        .type(notificationType));
    }

    @Parameters(method = "deploymentParams")
    @Test
    public void testDeploymentParams(NotificationRecipient notificationRecipient,
                                     NotificationType notificationType,
                                     Class<Exception> expectedException) {
        if (expectedException != null) {
            expectedExceptionRule.expect(expectedException);
        }

        Environment environment = new Environment("Name")
                .notifications(new Notification()
                        .recipients(notificationRecipient)
                        .type(notificationType));
    }

    public Object[][] planParams() {
        return new Object[][]{
                { //recipients
                        new UserRecipient("user"),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new GroupRecipient("group"),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new HipChatRecipient().apiToken("token").room("room"),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new EmailRecipient("user@example.com"),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new ImRecipient("imAddress"),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new WebhookRecipient("slack", "http://atlassian.slack.com"),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new ResponsibleRecipient(),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new WatchersRecipient(),
                        new BuildErrorNotification(),
                        null
                },
                {
                        new CommittersRecipient(),
                        new BuildErrorNotification(),
                        null
                }, // types
                {
                        new UserRecipient("user"),
                        new PlanCompletedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new PlanStatusChangedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new PlanFailedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new XFailedChainsNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new CommentAddedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new JobCompletedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new JobStatusChangedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new FirstFailedJobNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new JobFailedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new JobHungNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new JobTimeoutNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new JobWithoutAgentNotification(),
                        null
                }, //deployment notifications
                {
                        new UserRecipient("user"),
                        new DeploymentFailedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new DeploymentFinishedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new DeploymentStartedAndFinishedNotification(),
                        PropertiesValidationException.class
                },
        };
    }

    public Object[][] deploymentParams() {
        return new Object[][]{
                { //recipients
                        new UserRecipient("user"),
                        new DeploymentFailedNotification(),
                        null
                },
                {
                        new GroupRecipient("group"),
                        new DeploymentFailedNotification(),
                        null
                },
                {
                        new HipChatRecipient().apiToken("token").room("room"),
                        new DeploymentFailedNotification(),
                        null
                },
                {
                        new EmailRecipient("user@example.com"),
                        new DeploymentFailedNotification(),
                        null
                },
                {
                        new ImRecipient("imAddress"),
                        new DeploymentFailedNotification(),
                        null
                },
                {
                        new ResponsibleRecipient(),
                        new DeploymentFailedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new WatchersRecipient(),
                        new DeploymentFailedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new CommittersRecipient(),
                        new DeploymentFailedNotification(),
                        PropertiesValidationException.class
                }, // types
                {
                        new UserRecipient("user"),
                        new PlanCompletedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new PlanStatusChangedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new PlanFailedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new XFailedChainsNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new CommentAddedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new JobCompletedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new JobStatusChangedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new FirstFailedJobNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new JobFailedNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new JobHungNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new JobTimeoutNotification(),
                        PropertiesValidationException.class
                },
                {
                        new UserRecipient("user"),
                        new JobWithoutAgentNotification(),
                        PropertiesValidationException.class
                }, //deployment notifications
                {
                        new UserRecipient("user"),
                        new DeploymentFailedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new DeploymentFinishedNotification(),
                        null
                },
                {
                        new UserRecipient("user"),
                        new DeploymentStartedAndFinishedNotification(),
                        null
                },
        };
    }
}