package com.atlassian.bamboo.specs.maven.sandbox;

import org.apache.maven.plugin.logging.Log;

import java.lang.annotation.Annotation;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Function;

public class FileToBambooSpecsMapper implements Function<Path, Optional<Class<?>>> {
    private final Log log;
    private final ClassLoader classLoader;
    private final Class<? extends Annotation> planAnnotation;

    public FileToBambooSpecsMapper(final Log log, final ClassLoader classLoader) {
        this.log = log;
        this.classLoader = classLoader;
        try {
            planAnnotation = (Class<? extends Annotation>) this.classLoader.loadClass("com.atlassian.bamboo.specs.api.BambooSpec");
        } catch (final ClassNotFoundException e) {
            throw new RuntimeException("Unable to find Bamboo plan marker", e);
        }
    }

    @Override
    public Optional<Class<?>> apply(final Path path) {
        try {
            final String className = path.toString()
                    .replace("/", ".")
                    .replace("\\", ".")
                    .replace(".class", "");

            log.debug("Scanning " + className + " for " + planAnnotation.getSimpleName() + " annotation.");


            final Class<?> someClass = classLoader.loadClass(className);

            if (someClass.getAnnotation(planAnnotation) == null) {
                log.debug(planAnnotation.getSimpleName() + " annotation not found on " + someClass);
                return Optional.empty();
            }

            log.info("Found @" + planAnnotation.getSimpleName() + " annotation on " + someClass);
            return Optional.of(someClass);
        } catch (final ClassNotFoundException e) {
            log.warn("Error when loading class " + path, e);
            return Optional.empty();
        }
    }
}
