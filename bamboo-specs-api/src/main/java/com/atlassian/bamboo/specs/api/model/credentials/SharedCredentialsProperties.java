package com.atlassian.bamboo.specs.api.model.credentials;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.RootEntityProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.validators.SharedCredentialsValidator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static org.apache.commons.lang3.StringUtils.defaultString;

@Immutable
public abstract class SharedCredentialsProperties implements RootEntityProperties {
    private final String name;
    private final BambooOidProperties oid;
    private final ProjectProperties project;

    protected SharedCredentialsProperties() {
        name = null;
        oid = null;
        project = null;
    }

    protected SharedCredentialsProperties(@NotNull final String name,
                                          @Nullable final BambooOidProperties oid) throws PropertiesValidationException {
        this.name = name;
        this.oid = oid;
        this.project = null;
    }

    protected SharedCredentialsProperties(@NotNull final String name,
                                          @Nullable final BambooOidProperties oid,
                                          @Nullable final ProjectProperties project) throws PropertiesValidationException {
        this.name = name;
        this.oid = oid;
        this.project = project;
    }

    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public BambooOidProperties getOid() {
        return oid;
    }

    @Nullable
    public ProjectProperties getProject() {
        return project;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SharedCredentialsProperties that = (SharedCredentialsProperties) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getProject(), that.getProject());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getOid(), getProject());
    }

    @Override
    public void validate() {
        checkNotNull("name", name);
        checkNoErrors(SharedCredentialsValidator.validate(this));
    }

    @NotNull
    @Override
    public String humanReadableType() {
        return SharedCredentials.TYPE;
    }

    @Override
    public String humanReadableId() {
        String projectReadableId = "";
        if (project != null && project.getKey() != null) {
            projectReadableId = String.format(" ProjectKey=" + project.getKey().getKey());
        }
        return String.format("%s %s%s", SharedCredentials.TYPE, defaultString(name, "<unknown>"), projectReadableId);
    }
}
