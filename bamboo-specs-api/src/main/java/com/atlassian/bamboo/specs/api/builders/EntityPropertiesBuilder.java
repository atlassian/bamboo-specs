package com.atlassian.bamboo.specs.api.builders;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

/**
 * Represents any Bamboo entity.
 */
public abstract class EntityPropertiesBuilder<T extends EntityProperties> {
    protected abstract T build();
}
