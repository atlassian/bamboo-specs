package com.atlassian.bamboo.specs.api.builders.project;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepository;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.rsbs.RepositoryStoredSpecsData;
import com.atlassian.bamboo.specs.api.rsbs.RunnerSettings;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents Bamboo project.
 */
public class Project extends RootEntityPropertiesBuilder<ProjectProperties> {
    public static final String TYPE = "project";

    private BambooKeyProperties key;
    private BambooOidProperties oid;
    private String name;
    private String description;
    private List<VariableProperties> variables = new ArrayList<>();
    private List<SharedCredentialsProperties> sharedCredentials = new ArrayList<>();
    private List<VcsRepositoryProperties> repositories = new ArrayList<>();

    /**
     * Specifies Bamboo project.
     */
    public Project() throws PropertiesValidationException {
    }

    /**
     * Sets a project name.
     */
    public Project name(@NotNull String name) throws PropertiesValidationException {
        checkNotNull("name", name);
        this.name = name;
        return this;
    }

    /**
     * Sets a project key. In the absence of oid key serves as project identifier.
     */
    public Project key(@Nullable String key) throws PropertiesValidationException {
        return key(key != null ? new BambooKey(key) : null);
    }

    /**
     * Sets a project key. In the absence of oid key serves as project identifier.
     */
    public Project key(@Nullable BambooKey key) throws PropertiesValidationException {

        this.key = key != null ? EntityPropertiesBuilders.build(key) : null;
        return this;
    }

    /**
     * Sets a project description.
     */
    public Project description(@Nullable String description) throws PropertiesValidationException {
        this.description = description;
        return this;
    }

    /**
     * Sets a project's oid.
     */
    public Project oid(@Nullable String oid) throws PropertiesValidationException {
        return oid(oid != null ? new BambooOid(oid) : null);
    }

    /**
     * Sets a project's oid.
     */
    public Project oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        this.oid = oid != null ? EntityPropertiesBuilders.build(oid) : null;
        return this;
    }

    /**
     * Adds project variables.
     */
    public Project variables(@NotNull Variable... variables) {
        checkNotNull("variables", variables);
        Arrays.stream(variables)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.variables::add);
        return this;
    }

    /**
     * Adds project shared credentials.
     */
    public Project sharedCredentials(@NotNull SharedCredentials<?, ?>... credentials) {
        checkNotNull("shared credentials", credentials);
        Arrays.stream(credentials)
                .map(c -> c.project(this))
                .map(EntityPropertiesBuilders::build)
                .forEach(this.sharedCredentials::add);
        return this;
    }

    /**
     * Adds project repositories.
     * This method will create new project level repository definitions.
     */
    public Project repositories(@NotNull VcsRepository<?, ?>... repositories) {
        checkNotNull("repositories", repositories);
        Arrays.stream(repositories)
                .map(r -> r.project(this))
                .map(EntityPropertiesBuilders::build)
                .forEach(this.repositories::add);
        return this;
    }

    /**
     * Returns project's oid.
     *
     * @throws IllegalStateException if oid is undefined
     */
    public BambooOid getOid() {
        if (oid == null) {
            throw new IllegalStateException("Project oid is undefined");
        }
        return new BambooOid(oid.getOid());
    }

    /**
     * Returns project's key.
     *
     * @throws IllegalStateException if key is undefined
     */
    public BambooKey getKey() {
        if (key == null) {
            throw new IllegalStateException("Project key is undefined");
        }
        return new BambooKey(key.getKey());
    }

    /**
     * Returns project's name.
     */
    public String getName() {
        return name;
    }

    protected ProjectProperties build() throws PropertiesValidationException {
        final RepositoryStoredSpecsData repositoryStoredSpecsData = RunnerSettings.getRepositoryStoredSpecsData();
        return new ProjectProperties(
                oid,
                key,
                name,
                description,
                variables,
                sharedCredentials,
                repositories,
                repositoryStoredSpecsData);
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        if (key != null) {
            return String.format("%s %s", TYPE, key.getKey());
        } else {
            return String.format("%s <unknown>", TYPE);
        }
    }
}
