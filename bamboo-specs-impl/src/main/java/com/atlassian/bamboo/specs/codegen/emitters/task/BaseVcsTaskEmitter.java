package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.BaseVcsTaskProperties;
import org.jetbrains.annotations.NotNull;

public class BaseVcsTaskEmitter<T extends BaseVcsTaskProperties> extends EntityPropertiesEmitter<T> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull T entity) throws CodeGenerationException {
        initBuilderClass(entity);
        fieldsToSkip.add("defaultRepository");
        if (entity.isDefaultRepository()) {
            context.incIndentation();
            final String defaultRepositoryCall = context.newLine() + ".defaultRepository()";
            context.decIndentation();
            return emitConstructorInvocation(context, entity) + defaultRepositoryCall + emitFields(context, entity);
        }
        return super.emitCode(context, entity);
    }
}
