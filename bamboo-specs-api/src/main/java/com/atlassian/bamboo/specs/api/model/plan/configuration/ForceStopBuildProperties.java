package com.atlassian.bamboo.specs.api.model.plan.configuration;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;

import java.util.Objects;

public class ForceStopBuildProperties implements PluginConfigurationProperties {

    private static final AtlassianModuleProperties ATLASSIAN_MODULE = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.additionalBuildConfiguration:hungBuildKiller.plancfg");

    private Boolean enabled;

    private ForceStopBuildProperties() {
    }

    public ForceStopBuildProperties(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public void validate() {

    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ForceStopBuildProperties)) {
            return false;
        }
        ForceStopBuildProperties that = (ForceStopBuildProperties) o;
        return Objects.equals(enabled, that.enabled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enabled);
    }
}
