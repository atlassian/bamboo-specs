package com.atlassian.bamboo.specs.api.codegen;

import org.jetbrains.annotations.NotNull;

/**
 * BambooSpecs generator for a specific field or type.
 */
public interface CodeEmitter<T> {
    @NotNull
    String emitCode(@NotNull CodeGenerationContext context, @NotNull T value) throws CodeGenerationException;
}
