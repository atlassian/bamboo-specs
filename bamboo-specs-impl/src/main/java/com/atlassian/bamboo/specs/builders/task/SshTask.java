package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;

import java.nio.file.Path;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotEmpty;
import static com.atlassian.bamboo.specs.util.FileUtils.readFileContent;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a task to run a remote command over SSH.
 */
public class SshTask extends BaseSshTask<SshTask, SshTaskProperties> {

    private String command;
    @Nullable
    protected Duration keepAliveInterval;

    /**
     * Shell command to execute on the remote host.
     */
    public SshTask command(@NotNull final String command) {
        checkNotEmpty("command", command);
        this.command = command;
        return this;
    }

    public SshTask command(@NotNull final Path path) {
        this.command = readFileContent(path, "command body file", "Error when reading ssh command body from path: %s");
        return this;
    }

    /**
     * Sets the SSH keep alive interval.
     */
    public SshTask keepAliveInterval(final Duration keepAliveInterval) {
        checkNotNull("keep alive interval", keepAliveInterval);
        this.keepAliveInterval = keepAliveInterval;
        return this;
    }

    /**
     * Sets the SSH keep alive interval in seconds.
     */
    public SshTask keepAliveIntervalInSeconds(final int keepAliveIntervalSeconds) {
        return keepAliveInterval(Duration.ofSeconds(keepAliveIntervalSeconds));
    }

    private int getKeepAliveIntervalInSec() {
        return keepAliveInterval == null ? SshTaskProperties.DEFAULT_KEEP_ALIVE_INTERVAL : (int) keepAliveInterval.getSeconds();
    }

    @NotNull
    @Override
    protected SshTaskProperties build() {
        return new SshTaskProperties(description,
                taskEnabled,
                String.join(", ", hosts),
                username,
                authenticationType,
                password,
                key,
                passphrase,
                sharedCredentials,
                hostFingerprint,
                port,
                getKeepAliveIntervalInSec(),
                command,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SshTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        SshTask sshTask = (SshTask) o;
        return Objects.equals(command, sshTask.command) &&
                Objects.equals(keepAliveInterval, sshTask.keepAliveInterval);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), command, keepAliveInterval);
    }
}
