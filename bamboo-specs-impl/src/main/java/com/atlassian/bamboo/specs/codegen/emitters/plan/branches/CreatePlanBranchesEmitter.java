package com.atlassian.bamboo.specs.codegen.emitters.plan.branches;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.CreatePlanBranchesProperties;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreatePlanBranchesEmitter implements CodeEmitter<CreatePlanBranchesProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull CreatePlanBranchesProperties value) throws CodeGenerationException {
        switch (value.getTrigger()) {
            case MANUAL:
                return ".createManually()";
            case PULL_REQUEST:
                return ".createForPullRequest()";
            case FORK_ENABLED_PULL_REQUEST:
                return ".createForPullRequest(true)";
            case BRANCH:
                final String matchingPattern = value.getMatchingPattern();
                if (StringUtils.isEmpty(matchingPattern)) {
                    return ".createForVcsBranch()";
                } else {
                    CodeEmitter matchingPatternEmitter = ValueEmitterFactory.emitterFor(matchingPattern);
                    return ".createForVcsBranchMatching(" + matchingPatternEmitter.emitCode(context, matchingPattern) + ")";
                }
            default:
                throw new CodeGenerationException("Unrecognized plan branch creation trigger: " + value.getTrigger());
        }
    }
}




