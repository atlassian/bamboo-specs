package com.atlassian.bamboo.specs.util;

import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.methods.HttpRequestBase;

public interface AuthenticationProvider {

    void authenticate(HttpRequestBase requestBase) throws AuthenticationException;

}
