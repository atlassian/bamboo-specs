package com.atlassian.bamboo.specs.api.model.deployment;

import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.model.deployment.configuration.EnvironmentPluginConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.VariableValidator;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@ConstructFrom({"name"})
public class EnvironmentProperties implements EntityProperties {

    private String name;
    private String description;
    private List<TaskProperties> tasks;
    private List<TaskProperties> finalTasks;
    private List<TriggerProperties> triggers;
    private List<VariableProperties> variables;
    private List<RequirementProperties> requirements;
    private List<NotificationProperties> notifications;
    private DockerConfigurationProperties dockerConfiguration;
    private List<EnvironmentPluginConfigurationProperties> pluginConfigurations;
    // environment prerequisites:
    private Environment.ReleaseApprovalPrerequisite releaseApprovalPrerequisite;

    private EnvironmentProperties() {
        tasks = Collections.emptyList();
        finalTasks = Collections.emptyList();
        triggers = Collections.emptyList();
        variables = Collections.emptyList();
        requirements = Collections.emptyList();
        notifications = Collections.emptyList();
        dockerConfiguration = EntityPropertiesBuilders.build(new DockerConfiguration().enabled(false));
        pluginConfigurations = Collections.emptyList();
        releaseApprovalPrerequisite = Environment.ReleaseApprovalPrerequisite.getDefault();
    }

    public EnvironmentProperties(@NotNull String name,
                                 @Nullable String description,
                                 @NotNull List<TaskProperties> tasks,
                                 @NotNull List<TaskProperties> finalTasks,
                                 @NotNull List<TriggerProperties> triggers,
                                 @NotNull List<VariableProperties> variables,
                                 @NotNull List<RequirementProperties> requirements,
                                 @NotNull List<NotificationProperties> notifications,
                                 @NotNull DockerConfigurationProperties dockerConfiguration,
                                 @NotNull Collection<EnvironmentPluginConfigurationProperties> pluginConfigurations,
                                 @NotNull Environment.ReleaseApprovalPrerequisite releaseApprovalPrerequisite) {
        this.name = name;
        this.description = description;
        this.tasks = Collections.unmodifiableList(new ArrayList<>(tasks));
        this.finalTasks = Collections.unmodifiableList(new ArrayList<>(finalTasks));
        this.triggers = Collections.unmodifiableList(new ArrayList<>(triggers));
        this.variables = Collections.unmodifiableList(new ArrayList<>(variables));
        this.requirements = Collections.unmodifiableList(new ArrayList<>(requirements));
        this.notifications = Collections.unmodifiableList(new ArrayList<>(notifications));
        this.dockerConfiguration = dockerConfiguration;
        this.pluginConfigurations = Collections.unmodifiableList(new ArrayList(pluginConfigurations));
        this.releaseApprovalPrerequisite = releaseApprovalPrerequisite;

        validate();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<TaskProperties> getTasks() {
        return tasks;
    }

    public List<TaskProperties> getFinalTasks() {
        return finalTasks;
    }

    public List<TriggerProperties> getTriggers() {
        return triggers;
    }

    public List<VariableProperties> getVariables() {
        return variables;
    }

    public List<RequirementProperties> getRequirements() {
        return requirements;
    }

    public List<NotificationProperties> getNotifications() {
        return notifications;
    }

    public DockerConfigurationProperties getDockerConfiguration() {
        return dockerConfiguration;
    }

    public List<EnvironmentPluginConfigurationProperties> getPluginConfigurations() {
        return pluginConfigurations;
    }

    public Environment.ReleaseApprovalPrerequisite getReleaseApprovalPrerequisite() {
        return releaseApprovalPrerequisite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EnvironmentProperties that = (EnvironmentProperties) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getTasks(), that.getTasks()) &&
                Objects.equals(getFinalTasks(), that.getFinalTasks()) &&
                Objects.equals(getTriggers(), that.getTriggers()) &&
                Objects.equals(getVariables(), that.getVariables()) &&
                Objects.equals(getRequirements(), that.getRequirements()) &&
                Objects.equals(getNotifications(), that.getNotifications()) &&
                Objects.equals(getDockerConfiguration(), that.getDockerConfiguration()) &&
                Objects.equals(getPluginConfigurations(), that.getPluginConfigurations()) &&
                Objects.equals(getReleaseApprovalPrerequisite(), that.getReleaseApprovalPrerequisite());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getName(),
                getDescription(),
                getTasks(),
                getFinalTasks(),
                getTriggers(),
                getVariables(),
                getRequirements(),
                getNotifications(),
                getDockerConfiguration(),
                getPluginConfigurations(),
                getReleaseApprovalPrerequisite());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Environment");
        ValidationUtils.validateName(context, name);
        checkNotNull(context.with("Docker configuration"), "docker configuration", dockerConfiguration);
        checkNoErrors(VariableValidator.validateUniqueVariableNames(variables));
    }
}
