package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationTypeProperties;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;

/**
 * Represent a notification which is sent when a job gets queued without an agent being able to build it.
 */
public class JobWithoutAgentNotification extends NotificationType<JobWithoutAgentNotification, AnyNotificationTypeProperties> {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:buildMissingCapableAgent");

    @NotNull
    @Override
    protected AnyNotificationTypeProperties build() {
        return new AnyNotificationTypeProperties(ATLASSIAN_PLUGIN, "", EnumSet.of(Applicability.PLANS));
    }
}
