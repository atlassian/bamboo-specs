package com.atlassian.bamboo.specs.codegen.emitters.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.repository.viewer.AnyVcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.builders.repository.viewer.VcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.repository.viewer.AnyVcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.builders.repository.viewer.BitbucketCloudRepositoryViewer;
import com.atlassian.bamboo.specs.builders.repository.viewer.BitbucketServerRepositoryViewer;
import com.atlassian.bamboo.specs.builders.repository.viewer.GitHubRepositoryViewer;
import com.atlassian.bamboo.specs.builders.repository.viewer.HgServeRepositoryViewer;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.util.MapBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class AnyVcsRepositoryViewerEmitter extends EntityPropertiesEmitter<AnyVcsRepositoryViewerProperties> {
    private static final Map<String, Class<? extends VcsRepositoryViewer>> VIEWER_BUILDERS = new MapBuilder<String, Class<? extends VcsRepositoryViewer>>()
            .put("com.atlassian.bamboo.plugins.stash.atlassian-bamboo-plugin-stash:bbServerViewer", BitbucketServerRepositoryViewer.class)
            .put("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-bitbucket:bbCloudViewer", BitbucketCloudRepositoryViewer.class)
            .put("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-mercurial:hgServeViewer", HgServeRepositoryViewer.class)
            .put("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:githubViewer", GitHubRepositoryViewer.class)
            .build();

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final AnyVcsRepositoryViewerProperties entity) throws CodeGenerationException {
        builderClass = AnyVcsRepositoryViewer.class;
        if (VIEWER_BUILDERS.containsKey(entity.getAtlassianPlugin().getCompleteModuleKey())) {
            builderClass = VIEWER_BUILDERS.get(entity.getAtlassianPlugin().getCompleteModuleKey());
            return "new " + context.importClassName(builderClass) + "()";
        }
        return super.emitCode(context, entity);
    }
}
