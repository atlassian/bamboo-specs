package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.AnyVcsRepository;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.repository.viewer.AnyVcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.AnyVcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class VcsRepositoryPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void vcsLinkedRepositoryPropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final String name = "name";
        final String description = "description";
        final String plugin = "A:B";
        final String oid = "1";

        final Map<String, Object> serverConfiguration = new HashMap<>();
        serverConfiguration.put("serverConfigurationKey", "serverConfigurationValue");

        final Map<String, Object> branchConfiguration = new HashMap<>();
        branchConfiguration.put("branchConfigurationKey", "branchConfigurationValue");

        final Map<String, Object> branchDetectionConfiguration = new HashMap<>();
        branchDetectionConfiguration.put("branchDetectionConfigurationKey", "branchDetectionConfigurationValue");

        final String webPlugin = "A2:B2";
        final Map<String, Object> webViewerConfiguration = new HashMap<>();
        webViewerConfiguration.put("webViewerConfigurationKey", "webViewerConfigurationValue");

        final AnyVcsRepository repositoryBuilder = new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name)
                .oid(
                        new BambooOid(oid)
                )
                .description(description)
                .serverConfiguration(serverConfiguration)
                .branchConfiguration(branchConfiguration)
                .changeDetectionConfiguration(new VcsChangeDetection())
                .branchDetectionConfiguration(branchDetectionConfiguration)
                .repositoryViewer(
                        new AnyVcsRepositoryViewer(webPlugin)
                                .configuration(webViewerConfiguration)
                );
        final AnyVcsRepositoryProperties repositoryProperties = EntityPropertiesBuilders.build(repositoryBuilder);

        assertEquals(repositoryProperties.getName(), name);
        assertEquals(repositoryProperties.getDescription(), description);
        assertEquals(repositoryProperties.getAtlassianPlugin().getCompleteModuleKey(), plugin);
        assertEquals(repositoryProperties.getOid().getOid(), oid);

        assertEquals(repositoryProperties.getServerConfiguration(), serverConfiguration);
        assertEquals(repositoryProperties.getBranchConfiguration(), branchConfiguration);
        assertEquals(repositoryProperties.getBranchDetectionConfiguration(), branchDetectionConfiguration);
        assertEquals(repositoryProperties.getRepositoryViewerProperties().getAtlassianPlugin().getCompleteModuleKey(), webPlugin);
        assertEquals(((AnyVcsRepositoryViewerProperties) repositoryProperties.getRepositoryViewerProperties()).getConfiguration(), webViewerConfiguration);

    }

    @Test
    public void vcsLinkedRepositoryPropertiesWithoutOidConstructedSuccessfully() throws PropertiesValidationException {
        final String name = "name";
        final String plugin = "A:B";

        final VcsRepositoryProperties repositoryProperties = EntityPropertiesBuilders.build(new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name));

        assertNull(repositoryProperties.getOid());
    }

    @Test
    public void vcsLinkedRepositoryPropertiesWithoutModuleThrowsException() throws PropertiesValidationException {
        final String name = "name";

        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AnyVcsRepository((AtlassianModuleProperties) null)
                .name(name));
    }

    @Test
    public void vcsLinkedRepositoryPropertiesWithEmptyNameThrowsException() throws PropertiesValidationException {
        final String name = "    ";
        final String plugin = "A:B";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name));
    }

    @Test
    public void vcsLinkedRepositoryPropertiesWithTooLongNameThrowsException() throws PropertiesValidationException {
        final String name = RandomStringUtils.random(256);
        final String plugin = "A:B";

        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name));
    }

    @Test
    public void vcsProjectRepositoryPropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final String name = "name";
        final String description = "description";
        final String plugin = "A:B";
        final String oid = "1";
        final String key = "ABC";
        final Project project = new Project().key("ABC").name(name);

        final Map<String, Object> serverConfiguration = new HashMap<>();
        serverConfiguration.put("serverConfigurationKey", "serverConfigurationValue");

        final Map<String, Object> branchConfiguration = new HashMap<>();
        branchConfiguration.put("branchConfigurationKey", "branchConfigurationValue");

        final Map<String, Object> branchDetectionConfiguration = new HashMap<>();
        branchDetectionConfiguration.put("branchDetectionConfigurationKey", "branchDetectionConfigurationValue");

        final String webPlugin = "A2:B2";
        final Map<String, Object> webViewerConfiguration = new HashMap<>();
        webViewerConfiguration.put("webViewerConfigurationKey", "webViewerConfigurationValue");

        final AnyVcsRepository repositoryBuilder = new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name)
                .oid(
                        new BambooOid(oid)
                )
                .project(project)
                .description(description)
                .serverConfiguration(serverConfiguration)
                .branchConfiguration(branchConfiguration)
                .changeDetectionConfiguration(new VcsChangeDetection())
                .branchDetectionConfiguration(branchDetectionConfiguration)
                .repositoryViewer(
                        new AnyVcsRepositoryViewer(webPlugin)
                                .configuration(webViewerConfiguration)
                );
        final AnyVcsRepositoryProperties repositoryProperties = EntityPropertiesBuilders.build(repositoryBuilder);

        assertEquals(repositoryProperties.getName(), name);
        assertEquals(repositoryProperties.getDescription(), description);
        assertEquals(repositoryProperties.getAtlassianPlugin().getCompleteModuleKey(), plugin);
        assertEquals(repositoryProperties.getOid().getOid(), oid);

        assertEquals(repositoryProperties.getProject().getKey().getKey(), key);
        assertEquals(repositoryProperties.getProject().getName(), name);

        assertEquals(repositoryProperties.getServerConfiguration(), serverConfiguration);
        assertEquals(repositoryProperties.getBranchConfiguration(), branchConfiguration);
        assertEquals(repositoryProperties.getBranchDetectionConfiguration(), branchDetectionConfiguration);
        assertEquals(repositoryProperties.getRepositoryViewerProperties().getAtlassianPlugin().getCompleteModuleKey(), webPlugin);
        assertEquals(((AnyVcsRepositoryViewerProperties) repositoryProperties.getRepositoryViewerProperties()).getConfiguration(), webViewerConfiguration);

    }

    @Test
    public void creatingVcsLinkedRepositoryPropertiesWithXssCharactersInNameThrowsException() throws PropertiesValidationException {
        final String name = "<>";
        final String plugin = "A:B";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name));
    }

    @Test
    public void creatingVcsLinkedRepositoryPropertiesWithXssCharactersInDescriptionThrowsException() throws PropertiesValidationException {
        final String name = "name";
        final String plugin = "A:B";
        final String description = "<>";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new AnyVcsRepository(new AtlassianModule(plugin))
                .name(name)
                .description(description));
    }
}