package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class DockerBuildImageEmitterTest {

    private DockerBuildImageEmitter emitter;

    @Before
    public void setUp() {
        emitter = new DockerBuildImageEmitter();
    }

    @Test
    public void emitSingleLineDockerFile() throws CodeGenerationException {
        DockerBuildImageTaskProperties properties = build(
                new DockerBuildImageTask()
                        .imageName("image")
                        .dockerfile("FROM node:12.18.1")
        );

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".useCache(true)",
                ".dockerfile(\"FROM node:12.18.1\")"

        ));
    }

    @Test
    public void emitMultiLineDockerFile() throws CodeGenerationException {
        DockerBuildImageTaskProperties properties = build(
                new DockerBuildImageTask()
                        .imageName("image")
                        .dockerfile("FROM node:12.18.1\nRUN apk add --no-cache python2 g++ make\nCMD echo \"Hello world\"\nRUN apk add --no-cache python3 g++ make")
        );

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".useCache(true)",
                ".dockerfile(\"FROM node:12.18.1\\nRUN apk add --no-cache python2 g++ make\\nCMD echo \\\"Hello world\\\"\\nRUN apk add --no-cache python3 g++ make\")"

        ));
    }

    private void assertEmittedCode(DockerBuildImageTaskProperties properties, Collection<String> expectedSetters) throws CodeGenerationException {
        List<String> result = Lists.newArrayList(emitter.emitCode(new CodeGenerationContext(), properties).split("\n"))
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());
        final String objectCreation = result.get(0);
        final List<String> objectBuilding = result.subList(1, result.size());

        assertThat(objectCreation, equalTo("new DockerBuildImageTask()"));
        assertThat(objectBuilding, containsInAnyOrder(
                expectedSetters.stream().map(Matchers::equalTo).collect(Collectors.toList())
        ));
    }
}
