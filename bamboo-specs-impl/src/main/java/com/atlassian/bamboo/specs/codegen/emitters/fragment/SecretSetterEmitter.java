package com.atlassian.bamboo.specs.codegen.emitters.fragment;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import org.jetbrains.annotations.NotNull;

public class SecretSetterEmitter implements CodeEmitter<Object> {
    public static final String SECRET_PLACEHOLDER = "/* PUT YOUR SENSITIVE INFORMATION HERE*/";
    final String methodName;

    public SecretSetterEmitter(final String methodName) {
        this.methodName = methodName;
    }

    @NotNull
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final Object argument) throws CodeGenerationException {
        return String.format(".%s(%s)", methodName, SECRET_PLACEHOLDER);
    }
}
