package com.atlassian.bamboo.specs.api.builders.task;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.task.AnyTaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Represents a task of any type.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation of a given task type is not available.
 */
public class AnyTask extends Task<AnyTask, AnyTaskProperties> {
    private final AtlassianModuleProperties atlassianPlugin;
    private Map<String, String> configuration = new LinkedHashMap<>();

    public AnyTask(@NotNull final AnyTask copy) {
        this.taskEnabled = copy.taskEnabled;
        this.description = copy.description;
        this.requirements = new ArrayList<>(copy.requirements);
        this.conditions = new ArrayList<>(copy.conditions);
        this.atlassianPlugin = copy.atlassianPlugin;
        this.configuration = MapUtils.copyOf(copy.configuration);
    }

    /**
     * Specifies a task of given type.
     *
     * @param atlassianPlugin type of the task identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyTask(@NotNull final AtlassianModule atlassianPlugin) throws PropertiesValidationException {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Appends the generic configuration to the task.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is
     * performed on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyTask configuration(final Map<String, String> configuration) {
        this.configuration.putAll(configuration);
        return this;
    }

    @NotNull
    @Override
    protected AnyTaskProperties build() throws PropertiesValidationException {
        return new AnyTaskProperties(atlassianPlugin, description, taskEnabled, configuration, requirements, conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnyTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AnyTask anyTask = (AnyTask) o;
        return Objects.equals(atlassianPlugin, anyTask.atlassianPlugin) &&
                Objects.equals(configuration, anyTask.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), atlassianPlugin, configuration);
    }
}
