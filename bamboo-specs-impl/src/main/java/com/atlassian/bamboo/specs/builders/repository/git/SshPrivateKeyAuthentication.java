package com.atlassian.bamboo.specs.builders.repository.git;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.model.repository.git.SshPrivateKeyAuthenticationProperties;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * SSH private key authentication for git repository.
 */
public class SshPrivateKeyAuthentication extends EntityPropertiesBuilder<SshPrivateKeyAuthenticationProperties> {
    private String sshPrivateKey;
    private String passphrase;

    /**
     * Specifies new ssh credentials with given private key.
     * The key can be both in Bamboo encrypted and plain form.
     *
     * @param sshPrivateKey private key
     * @see <a href="https://confluence.atlassian.com/bamboo/bamboo-specs-encryption-970268127.html">Encryption in Bamboo Specs</a>
     */
    public SshPrivateKeyAuthentication(String sshPrivateKey) {
        checkNotNull("sshPrivateKey", sshPrivateKey);

        this.sshPrivateKey = sshPrivateKey;
    }

    /**
     * Sets private key.
     * The key can be both in Bamboo encrypted and plain form.
     *
     * @param sshPrivateKey private key
     * @see <a href="https://confluence.atlassian.com/bamboo/bamboo-specs-encryption-970268127.html">Encryption in Bamboo Specs</a>
     */
    public SshPrivateKeyAuthentication sshPrivateKey(String sshPrivateKey) {
        checkNotNull("sshPrivateKey", sshPrivateKey);

        this.sshPrivateKey = sshPrivateKey;
        return this;
    }

    /**
     * Sets passphrase for the private key.
     * The passphrase can be both in Bamboo encrypted and plain form.
     *
     * @param passphrase passphrase
     * @see <a href="https://confluence.atlassian.com/bamboo/bamboo-specs-encryption-970268127.html">Encryption in Bamboo Specs</a>
     */
    public SshPrivateKeyAuthentication passphrase(@Nullable String passphrase) {
        this.passphrase = passphrase;
        return this;
    }

    protected SshPrivateKeyAuthenticationProperties build() {
        return new SshPrivateKeyAuthenticationProperties(sshPrivateKey, passphrase);
    }

}
