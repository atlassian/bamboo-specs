package com.atlassian.bamboo.specs.api.validators.repository;

import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection.FileFilteringOption;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public final class VcsChangeDetectionValidator {
    private VcsChangeDetectionValidator() {
    }

    public static List<ValidationProblem> validate(@NotNull final VcsChangeDetectionProperties vcsChangeDetectionProperties) {
        final ValidationContext context = ValidationContext.of("VCS change detection");
        final List<ValidationProblem> errors = new ArrayList<>();

        final String changesetFilterRegex = vcsChangeDetectionProperties.getChangesetFilterPatternRegex();
        if (StringUtils.isNotBlank(changesetFilterRegex)) {
            try {
                Pattern.compile(changesetFilterRegex);
            } catch (PatternSyntaxException e) {
                errors.add(new ValidationProblem(context.with("Changeset filter"),
                        "Exclude changesets regexp '%s' must contain valid regexp - %s",
                        changesetFilterRegex, e.getMessage()));
            }
        }

        final FileFilteringOption filterPatternOption = vcsChangeDetectionProperties.getFilterFilePatternOption();
        if (EnumSet.of(FileFilteringOption.INCLUDE_ONLY,
                FileFilteringOption.EXCLUDE_ALL)
                .contains(filterPatternOption)) {
            final String patternRegex = vcsChangeDetectionProperties.getFilterFilePatternRegex();

            if (StringUtils.isNotEmpty(patternRegex)) {
                try {
                    Pattern.compile(patternRegex);
                } catch (PatternSyntaxException e) {
                    errors.add(new ValidationProblem(context.with("File pattern"),
                            "File pattern regexp '%s' must contain valid regexp - %s",
                            patternRegex, e.getMessage()));
                }
            } else {
                errors.add(new ValidationProblem(context.with("File pattern"),
                        "File pattern regexp is required when filterFilePatternOption is used."));
            }
        }

        return errors;
    }

}
