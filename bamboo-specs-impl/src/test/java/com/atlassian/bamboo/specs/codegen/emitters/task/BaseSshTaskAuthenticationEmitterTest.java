package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.model.task.BaseSshTaskProperties;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class BaseSshTaskAuthenticationEmitterTest {

    private CodeGenerationContext context;
    private BaseSshTaskAuthenticationEmitter emitter;
    private static final List<RequirementProperties> REQS = Collections.singletonList(new RequirementProperties("key", "value", Requirement.MatchType.EXISTS));

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new BaseSshTaskAuthenticationEmitter();
    }

    @Test
    public void authenticationByPasswordProjectSharedCredentials() {

        final SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties = new SharedCredentialsIdentifierProperties("project-shared-credentials", null, SharedCredentialsScope.PROJECT);
        final SshTaskProperties properties = getTaskProperties(sharedCredentialsIdentifierProperties, BaseSshTaskProperties.AuthenticationType.PASSWORD);

        String expectedCode = ".authenticateWithUsernamePasswordSharedCredentials(new SharedCredentialsIdentifier(\"project-shared-credentials\")" +
                              ".scope(SharedCredentialsScope.PROJECT))";
        String result = emitter.emitCodeForAuthentication(properties, context);

        assertThat(result, equalTo(expectedCode));
    }

    @Test
    public void authenticationByKeyWithPassphraseProjectSharedCredentials() {

        final SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties = new SharedCredentialsIdentifierProperties("project-shared-credentials", null, SharedCredentialsScope.PROJECT);
        final SshTaskProperties properties = getTaskProperties(sharedCredentialsIdentifierProperties, BaseSshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE);

        String expectedCode = ".authenticateWithSshSharedCredentials(new SharedCredentialsIdentifier(\"project-shared-credentials\")" +
                              ".scope(SharedCredentialsScope.PROJECT))";
        String result = emitter.emitCodeForAuthentication(properties, context);

        assertThat(result, equalTo(expectedCode));
    }

    @Test
    public void authenticationByKeyWithoutPassphraseProjectSharedCredentials() {

        final SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties = new SharedCredentialsIdentifierProperties("global-shared-credentials", null, SharedCredentialsScope.GLOBAL);
        final SshTaskProperties properties = getTaskProperties(sharedCredentialsIdentifierProperties, BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE);

        String expectedCode = ".authenticateWithSshSharedCredentials(new SharedCredentialsIdentifier(\"global-shared-credentials\")" +
                              ".scope(SharedCredentialsScope.GLOBAL))";
        String result = emitter.emitCodeForAuthentication(properties, context);

        assertThat(result, equalTo(expectedCode));
    }

    @Test
    public void authenticationByPassword() {

        final SshTaskProperties properties = getTaskProperties(null, BaseSshTaskProperties.AuthenticationType.PASSWORD);

        String expectedCode = ".authenticateWithPassword(\"password\")";
        String result = emitter.emitCodeForAuthentication(properties, context);

        assertThat(result, equalTo(expectedCode));
    }

    @Test
    public void authenticationByKeyWithoutPassphrase() {

        final SshTaskProperties properties = getTaskProperties(null, BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE);

        String expectedCode = ".authenticateWithKey(\"key\")";
        String result = emitter.emitCodeForAuthentication(properties, context);

        assertThat(result, equalTo(expectedCode));
    }

    @Test
    public void authenticationByKeyWithPassphrase() {

        final SshTaskProperties properties = getTaskProperties(null, BaseSshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE);

        String expectedCode = ".authenticateWithKeyWithPassphrase(\"key\", \"passphrase\")";
        String result = emitter.emitCodeForAuthentication(properties, context);

        assertThat(result, equalTo(expectedCode));
    }

    @NotNull
    private SshTaskProperties getTaskProperties(SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties,
                                                BaseSshTaskProperties.AuthenticationType authenticationType) {
        return new SshTaskProperties(null, true, "localhost", "admin",
                                     authenticationType, "password",
                                     "key", "passphrase", sharedCredentialsIdentifierProperties, null,
                                     80, 0, "ls -all", REQS, Collections.emptyList());
    }
}