package com.atlassian.bamboo.specs;

import com.atlassian.bamboo.specs.api.BambooSpec;

@BambooSpec
public class TestSimpleSpec {
    public static void main(String[] args) {
        SpecsRunnerTest.specRunStartedTl.get().set(TestSimpleSpec.class);
        SpecsRunnerTest.specRunEndedTl.get().set(TestSimpleSpec.class);
    }
}
