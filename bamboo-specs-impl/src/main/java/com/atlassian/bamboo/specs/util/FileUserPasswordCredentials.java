package com.atlassian.bamboo.specs.util;

import org.jetbrains.annotations.NotNull;

import java.util.Properties;

import static com.atlassian.bamboo.specs.util.FileTokenCredentials.getPropertyOrThrow;
import static com.atlassian.bamboo.specs.util.FileTokenCredentials.loadProperties;

/**
 * Provides Bamboo credentials from text file.
 * <p>
 * It is advised not to store the file in a code repository for security reasons.
 * <p>
 * Format of the text file:
 * <pre>{@code
 *     username = some_user_name
 *     password = some_user_password
 * }</pre>
 *
 * @deprecated since 7.1.0, use {@link FileTokenCredentials}
 */
@Deprecated
public class FileUserPasswordCredentials implements UserPasswordCredentials {

    protected static final String USERNAME_PROPERTY = "username";
    protected static final String PASSWORD_PROPERTY = "password";

    private final String credentialsLocation;
    private final String username;
    private final String password;

    public FileUserPasswordCredentials() {
        this(".credentials");
    }

    public FileUserPasswordCredentials(@NotNull String credentialsLocation) {
        this.credentialsLocation = credentialsLocation;
        Properties properties = loadProperties(credentialsLocation);
        this.username = getPropertyOrThrow(properties, USERNAME_PROPERTY, credentialsLocation);
        this.password = getPropertyOrThrow(properties, PASSWORD_PROPERTY, credentialsLocation);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("[type: 'file', location: '%s', username: '%s']", credentialsLocation, username);
    }
}
