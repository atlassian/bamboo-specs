package com.atlassian.bamboo.specs.api.model.permission;


import com.atlassian.bamboo.specs.api.builders.permission.GroupPermission;
import com.atlassian.bamboo.specs.api.builders.permission.LoggedInUserPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.UserPermission;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Assert;
import org.junit.Test;

public class PermissionsPropertiesTest {

    @Test
    public void testOrderOfPermissionTypesIsNotRelevantWhenComparingGroups() {
        GroupPermissionProperties one = EntityPropertiesBuilders.build(new GroupPermission("muka").permissions(PermissionType.BUILD, PermissionType.VIEW));
        GroupPermissionProperties reversed = EntityPropertiesBuilders.build(new GroupPermission("muka").permissions(PermissionType.VIEW, PermissionType.BUILD));
        GroupPermissionProperties withADuplicate = EntityPropertiesBuilders.build(new GroupPermission("muka").permissions(PermissionType.VIEW, PermissionType.BUILD, PermissionType.VIEW));
        GroupPermissionProperties different = EntityPropertiesBuilders.build(new GroupPermission("muka").permissions(PermissionType.VIEW));

        Assert.assertEquals(one, reversed);
        Assert.assertEquals(reversed, withADuplicate);
        Assert.assertNotEquals(one, different);
    }

    @Test
    public void testOrderOfPermissionTypesIsNotRelevantWhenComparingUsers() {
        UserPermissionProperties one = EntityPropertiesBuilders.build(new UserPermission("muka").permissions(PermissionType.BUILD, PermissionType.VIEW));
        UserPermissionProperties reversed = EntityPropertiesBuilders.build(new UserPermission("muka").permissions(PermissionType.VIEW, PermissionType.BUILD));
        UserPermissionProperties withADuplicate = EntityPropertiesBuilders.build(new UserPermission("muka").permissions(PermissionType.VIEW, PermissionType.BUILD, PermissionType.VIEW));
        UserPermissionProperties different = EntityPropertiesBuilders.build(new UserPermission("muka").permissions(PermissionType.VIEW));

        Assert.assertEquals(one, reversed);
        Assert.assertEquals(reversed, withADuplicate);
        Assert.assertNotEquals(one, different);
    }

    @Test
    public void testOrderOfPermissionTypesIsNotRelevantWhenComparingLoggedInRole() {
        LoggedInUserPermissionsProperties one = EntityPropertiesBuilders.build(new LoggedInUserPermissions().permissions(PermissionType.BUILD, PermissionType.VIEW));
        LoggedInUserPermissionsProperties reversed = EntityPropertiesBuilders.build(new LoggedInUserPermissions().permissions(PermissionType.VIEW, PermissionType.BUILD));
        LoggedInUserPermissionsProperties withADuplicate = EntityPropertiesBuilders.build(new LoggedInUserPermissions().permissions(PermissionType.VIEW, PermissionType.BUILD, PermissionType.VIEW));
        LoggedInUserPermissionsProperties different = EntityPropertiesBuilders.build(new LoggedInUserPermissions().permissions(PermissionType.VIEW));

        Assert.assertEquals(one, reversed);
        Assert.assertEquals(reversed, withADuplicate);
        Assert.assertNotEquals(one, different);
    }

    @Test
    public void testOrderOfAddingGroupsIsNotRelevantWhenComparingPermissionSets() {

        PermissionsProperties one = EntityPropertiesBuilders.build(new Permissions()
                .userPermissions("one", PermissionType.BUILD, PermissionType.VIEW)
                .userPermissions("two", PermissionType.VIEW, PermissionType.BUILD));

        PermissionsProperties two = EntityPropertiesBuilders.build(new Permissions()
                .userPermissions("two", PermissionType.BUILD, PermissionType.VIEW)
                .userPermissions("one", PermissionType.VIEW, PermissionType.BUILD));

        Assert.assertEquals(one, two);
    }

    @Test
    public void testOrderOfAddingUsersIsNotRelevantWhenComparingPermissionSets() {
        PermissionsProperties one = EntityPropertiesBuilders.build(new Permissions()
                .userPermissions("one", PermissionType.BUILD, PermissionType.VIEW)
                .userPermissions("two", PermissionType.VIEW, PermissionType.BUILD));

        PermissionsProperties two = EntityPropertiesBuilders.build(new Permissions()
                .userPermissions("two", PermissionType.BUILD, PermissionType.VIEW)
                .userPermissions("one", PermissionType.VIEW, PermissionType.BUILD));

        Assert.assertEquals(one, two);
    }

    @Test
    public void testCanAddTheSameItemTwiceAndItWillMerge() {
        PermissionsProperties one = EntityPropertiesBuilders.build(new Permissions()
                .userPermissions("one", PermissionType.BUILD)
                .userPermissions("one", PermissionType.VIEW)
                .groupPermissions("one", PermissionType.BUILD)
                .groupPermissions("one", PermissionType.VIEW));

        PermissionsProperties two = EntityPropertiesBuilders.build(new Permissions()
                .userPermissions("one", PermissionType.BUILD, PermissionType.VIEW)
                .groupPermissions("one", PermissionType.BUILD, PermissionType.VIEW));

        Assert.assertEquals(one, two);
    }
}
