package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@Immutable
public class AllOtherTriggerConditionProperties extends TriggerConditionProperties {
    public static final String ALL_OTHER_TRIGGER_CONDITION_PLUGINS_MODULE_KEY = "com.atlassian.bamboo:#allothertriggerconditionplugins";

    private final Map<String, String> configuration;

    private AllOtherTriggerConditionProperties() {
        configuration = Collections.emptyMap();
    }

    public AllOtherTriggerConditionProperties(Map<String, String> configuration) {
        this.configuration = Collections.unmodifiableMap(MapUtils.copyOf(configuration));
    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return new AtlassianModuleProperties(ALL_OTHER_TRIGGER_CONDITION_PLUGINS_MODULE_KEY);
    }

    public Map<String, String> getConfiguration() {
        return configuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AllOtherTriggerConditionProperties that = (AllOtherTriggerConditionProperties) o;
        return Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configuration);
    }
}
