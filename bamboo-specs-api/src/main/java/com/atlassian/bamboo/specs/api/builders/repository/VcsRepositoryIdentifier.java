package com.atlassian.bamboo.specs.api.builders.repository;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * References a vcs repository.
 * <p>
 * The exact semantics of this class depends on the context in which it is used. In particular, possible values can be restricted only
 * to repositories that are linked to particular plan. In other contexts a reference to any global repository is allowed.
 */
public class VcsRepositoryIdentifier extends EntityPropertiesBuilder<VcsRepositoryIdentifierProperties> {
    private String name;
    private BambooOidProperties oid;

    public VcsRepositoryIdentifier() throws PropertiesValidationException {
    }

    /**
     * References repository by name.
     * Name of the repository is ignored if oid is defined.
     */
    public VcsRepositoryIdentifier(@NotNull final String name) throws PropertiesValidationException {
        checkNotBlank("name", name);
        this.name = name;
    }

    /**
     * References repository by oid.
     * <p>
     * In many contexts, e.g. when linking repository to a plan, it will be matched to both repository's oid and parent oid.
     */
    public VcsRepositoryIdentifier(@NotNull final BambooOid oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        this.oid = EntityPropertiesBuilders.build(oid);
    }

    /**
     * References repository by name.
     * Name of the repository is ignored if oid is defined.
     */
    public VcsRepositoryIdentifier name(@NotNull String name) throws PropertiesValidationException {
        checkNotBlank("name", name);
        this.name = name;
        return this;
    }

    /**
     * References repository by oid.
     * <p>
     * In many contexts, e.g. when linking repository to a plan, it will be matched to both repository's oid and parent oid.
     */
    public VcsRepositoryIdentifier oid(@NotNull final String oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        return oid(new BambooOid(oid));
    }

    /**
     * References repository by oid.
     * <p>
     * In many contexts, e.g. when linking repository to a plan, it will be matched to both repository's oid and parent oid.
     */
    public VcsRepositoryIdentifier oid(@NotNull final BambooOid oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        this.oid = EntityPropertiesBuilders.build(oid);
        return this;
    }

    protected VcsRepositoryIdentifierProperties build() throws PropertiesValidationException {
        return new VcsRepositoryIdentifierProperties(name, oid);
    }

}
