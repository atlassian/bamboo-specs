package com.atlassian.bamboo.specs.util;

import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.methods.HttpRequestBase;

import javax.annotation.Nonnull;
import java.util.Properties;

import static com.atlassian.bamboo.specs.util.FileTokenCredentials.TOKEN_PROPERTY;
import static com.atlassian.bamboo.specs.util.FileTokenCredentials.getPropertyOrThrow;
import static com.atlassian.bamboo.specs.util.FileTokenCredentials.loadProperties;
import static com.atlassian.bamboo.specs.util.FileUserPasswordCredentials.PASSWORD_PROPERTY;
import static com.atlassian.bamboo.specs.util.FileUserPasswordCredentials.USERNAME_PROPERTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * prefers token credentials, fallback to user-pass credentials for sake of backwards compat.
 *
 * introduced as deprecated, to highlight that we should move towards token based auth, to be removed along with
 * {@link FileUserPasswordCredentials} and {@link UserPasswordCredentials}
 *
 * @deprecated since 7.0.4, use {@link FileTokenCredentials}
 */
@Deprecated
public class FileAuthenticationProvider implements AuthenticationProvider {

    private final AuthenticationProvider authProvider;

    public FileAuthenticationProvider() {
        this(".credentials");
    }

    public FileAuthenticationProvider(final @Nonnull String credentialsLocation) {
        Properties properties = loadProperties(credentialsLocation);
        String token = properties.getProperty(TOKEN_PROPERTY);
        if (isNotEmpty(token)) {
            this.authProvider = new SimpleTokenCredentials(token);
        } else {
            String username = getPropertyOrThrow(properties, USERNAME_PROPERTY, credentialsLocation);
            String password = getPropertyOrThrow(properties, PASSWORD_PROPERTY, credentialsLocation);
            this.authProvider = new SimpleUserPasswordCredentials(username, password);
        }
    }

    @Override
    public void authenticate(HttpRequestBase request) throws AuthenticationException {
        this.authProvider.authenticate(request);
    }

}
