package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.SshTaskEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNegative;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@CodeGenerator(SshTaskEmitter.class)
@Immutable
public class SshTaskProperties extends BaseSshTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-scp-plugin:sshtask");

    public static final int DEFAULT_KEEP_ALIVE_INTERVAL = preventInlining(0);

    private final String command;
    private final int keepAliveIntervalInSec;

    private SshTaskProperties() {
        command = null;
        keepAliveIntervalInSec = DEFAULT_KEEP_ALIVE_INTERVAL;
    }

    public SshTaskProperties(final String description,
                             final boolean enabled,
                             final String host,
                             final String username,
                             final AuthenticationType authenticationType,
                             @Nullable final String password,
                             @Nullable final String key,
                             @Nullable final String passphrase,
                             @Nullable final SharedCredentialsIdentifierProperties sharedCredentialsIdentifier,
                             @Nullable final String hostFingerprint,
                             final int port,
                             final int keepAliveIntervalInSec,
                             final String command,
                             final List<RequirementProperties> requirements,
                             @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, host, username, authenticationType, password, key, passphrase, sharedCredentialsIdentifier,  hostFingerprint, port, requirements, conditions);
        this.command = command;
        this.keepAliveIntervalInSec = keepAliveIntervalInSec;
        this.validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    public String getCommand() {
        return command;
    }

    public int getKeepAliveIntervalInSec() {
        return keepAliveIntervalInSec;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        final ValidationContext context = ValidationContext.of("SSH task");
        checkRequired(context, "command", command);
        checkNotNegative(context, "keepAliveIntervalInSec", keepAliveIntervalInSec);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final SshTaskProperties that = (SshTaskProperties) o;
        return Objects.equals(command, that.command)
                && keepAliveIntervalInSec == that.keepAliveIntervalInSec;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), command, keepAliveIntervalInSec);
    }


}
