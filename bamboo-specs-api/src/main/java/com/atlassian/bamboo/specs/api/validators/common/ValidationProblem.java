package com.atlassian.bamboo.specs.api.validators.common;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class ValidationProblem {
    private final String message;

    public ValidationProblem(final String message) {
        this(ValidationContext.empty(), "%s", message);
    }

    public ValidationProblem(final String validatedItem,
                             final String messageFmt,
                             @Nullable final Object... args) {
        this(ValidationContext.of(validatedItem), messageFmt, args);
    }

    public ValidationProblem(final ValidationContext validationContext,
                             final String message) {
        this(validationContext, "%s", message);
    }

    public ValidationProblem(final ValidationContext validationContext,
                             final String messageFmt,
                             @Nullable final Object... args) {
        if (validationContext.isEmpty()) {
            this.message = String.format(messageFmt, args);
        } else {
            this.message = String.format("%s: " + messageFmt, ArrayUtils.add(args, 0, validationContext.toString()));
        }
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ValidationProblem that = (ValidationProblem) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }

    @Override
    public String toString() {
        return message;
    }
}
