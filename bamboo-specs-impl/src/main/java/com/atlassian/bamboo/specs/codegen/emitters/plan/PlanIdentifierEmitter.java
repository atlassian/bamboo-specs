package com.atlassian.bamboo.specs.codegen.emitters.plan;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.codegen.emitters.value.LiteralEmitter;
import org.jetbrains.annotations.NotNull;

public class PlanIdentifierEmitter extends EntityPropertiesEmitter<PlanIdentifierProperties> implements CodeEmitter<PlanIdentifierProperties> {
    @Override
    protected void initBuilderClass(@NotNull PlanIdentifierProperties entity) throws CodeGenerationException {
        builderClass = PlanIdentifier.class;
    }

    @Override
    protected String emitConstructorInvocation(@NotNull CodeGenerationContext context, @NotNull PlanIdentifierProperties entity) throws CodeGenerationException {
        context.importClassName(PlanIdentifier.class);
        LiteralEmitter literalEmitter = new LiteralEmitter();
        if (entity.isFullKeyDefined()) {
            fieldsToSkip.add("key");
            fieldsToSkip.add("projectKey");
            return "new PlanIdentifier("
                    + literalEmitter.emitCode(context, entity.getProjectKey().getKey())
                    + ", "
                    + literalEmitter.emitCode(context, entity.getKey().getKey())
                    + ")";
        } else {
            fieldsToSkip.add("oid");
            return "new PlanIdentifier("
                    + literalEmitter.emitCode(context, entity.getOid().getOid())
                    + ")";
        }
    }
}
