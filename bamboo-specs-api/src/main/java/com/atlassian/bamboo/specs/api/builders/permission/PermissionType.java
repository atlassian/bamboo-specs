package com.atlassian.bamboo.specs.api.builders.permission;

public enum PermissionType {
    VIEW,
    EDIT,
    CREATE,
    CREATE_REPOSITORY,
    DELETE,
    BUILD,
    CLONE,
    ADMIN,
    /**
     * Allows viewing configuration of plans and deployments. Bamboo DC only: converts to VIEW when used on instance with Server license.
     */
    VIEW_CONFIGURATION,
    /**
     * Allows approving deployment releases.
     */
    APPROVE_RELEASE,
    /**
     * Allows creation of new plan branches.
     */
    CREATE_PLAN_BRANCH,
    /**
     * Allows creation of new deployment releases.
     */
    CREATE_RELEASE
}
