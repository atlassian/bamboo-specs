package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;

public class BambooYamlConstructor extends Constructor {
    BambooYamlConstructor(LoaderOptions loadingConfig) {
        super(loadingConfig);
        for (final CustomYamlers.CustomYamler customYamler : CustomYamlers.YAMLERS) {
            this.yamlConstructors.put(new Tag(customYamler.getYamledClass()), customYamler.getConstructor());
        }
    }
}
