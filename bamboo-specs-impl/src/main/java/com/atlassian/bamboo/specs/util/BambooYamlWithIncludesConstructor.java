package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.nodes.Tag;

import java.nio.file.Path;

class BambooYamlWithIncludesConstructor extends BambooYamlConstructor {

    static final String INCLUDE_TAG = "!include";

    BambooYamlWithIncludesConstructor(final int maxDepth, final int depth, final Path parentPath, LoaderOptions loadingConfig) {
        super(loadingConfig);
        this.yamlConstructors.put(new Tag(INCLUDE_TAG), new IncludeTag(maxDepth, depth, parentPath));
    }
}
