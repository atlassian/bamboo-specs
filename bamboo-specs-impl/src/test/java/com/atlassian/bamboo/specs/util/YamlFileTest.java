package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.rsbs.RepositoryStoredSpecsData;
import com.atlassian.bamboo.specs.api.rsbs.RunnerSettings;
import com.google.common.base.Stopwatch;
import org.apache.commons.lang3.RandomStringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class YamlFileTest {
    private static final Pattern CANONICALISING_REGEX = Pattern.compile(".*\\sspecsSourceId:\\s.*");

    @Test
    public void generatesFileName() {
        final Path fileName = generateFileName();
        assertThat(fileName, notNullValue());
    }

    @Test
    public void generatesCanonicalHashForPlan() {
        final Plan plan = newPlan();

        generatesCanonicalHashForPlan(plan);
    }

    @Test
    public void generatesCanonicalHashForDeployment() {
        final Deployment deployment = new Deployment(new PlanIdentifier(genKey(), genKey()), genName());
        deployment.releaseNaming(new ReleaseNaming("1"));
        generatesCanonicalHashForPlan(deployment);
    }

    private void generatesCanonicalHashForPlan(final RootEntityPropertiesBuilder<?> entity) {
        RunnerSettings.setRepositoryStoredSpecsData(new RepositoryStoredSpecsData(new Random().nextLong()));
        final String yaml1 = BambooSpecSerializer.dump(entity);
        RunnerSettings.setRepositoryStoredSpecsData(new RepositoryStoredSpecsData(new Random().nextLong()));
        final String yaml2 = BambooSpecSerializer.dump(entity);

        assertThat(yaml1, not(equalTo(yaml2)));

        final YamlFile fileName1 = YamlFile.parse(Paths.get(YamlFile.getFileName(entity, yaml1)));
        final YamlFile fileName2 = YamlFile.parse(Paths.get(YamlFile.getFileName(entity, yaml2)));

        assertThat(fileName1.getHashCode(), equalTo(fileName2.getHashCode()));
    }

    private static String toCanonicalContent(final String yaml) {
        //old implementation of canonical content
        return CANONICALISING_REGEX.matcher(yaml).replaceAll("");
    }

    @Test
    public void testNewGetCanonicalContent() {
        final Deployment deployment = new Deployment(new PlanIdentifier(genKey(), genKey()), genName());
        deployment.releaseNaming(new ReleaseNaming("1"));
        RunnerSettings.setRepositoryStoredSpecsData(new RepositoryStoredSpecsData(new Random().nextLong()));
        final String yaml = BambooSpecSerializer.dump(deployment);

        final String canonicalContentNew = YamlFile.toCanonicalContent(yaml);
        final String canonicalContentOld = toCanonicalContent(yaml);
        assertEquals(canonicalContentOld, canonicalContentNew);
    }

    @Test
    @Ignore
    public void oldVsNewPerformanceTest() {
        StringBuilder content = new StringBuilder();
        for (int i=0; i <100; i++) {
            content.append(RandomStringUtils.randomAlphanumeric(1000)).append('\n');
        }
        String yaml = content.toString();
        Stopwatch stopwatch = Stopwatch.createStarted();
        final String canonicalContentOld = toCanonicalContent(yaml);
        stopwatch.stop();
        System.out.println("Old: " + stopwatch);

        Stopwatch stopwatch2 = Stopwatch.createStarted();
        final String canonicalContentNew = YamlFile.toCanonicalContent(yaml);
        stopwatch2.stop();
        System.out.println("New: " + stopwatch2);
        assertEquals(canonicalContentOld, canonicalContentNew);
    }

    @Test
    public void parsesFileName() {
        final YamlFile fileName = YamlFile.parse(generateFileName());
        assertThat(fileName, notNullValue());
        assertThat(fileName.getCounter(), notNullValue());
        assertThat(fileName.getHashCode(), notNullValue());
        assertThat(fileName.getId(), notNullValue());
    }

    private static Path generateFileName() {
        final Plan plan = newPlan();

        final String yaml = BambooSpecSerializer.dump(plan);

        return Paths.get(YamlFile.getFileName(plan, yaml));
    }

    @NotNull
    private static Plan newPlan() {
        final Project project = new Project().name(genName()).key(genKey());
        return new Plan(project, genName(), genKey());
    }

    @NotNull
    private static String genName() {
        return RandomStringUtils.randomAlphanumeric(30);
    }

    @NotNull
    private static String genKey() {
        return RandomStringUtils.randomAlphabetic(10).toUpperCase();
    }
}
