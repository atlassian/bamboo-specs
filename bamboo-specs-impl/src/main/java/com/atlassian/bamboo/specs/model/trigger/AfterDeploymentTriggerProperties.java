package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.builders.trigger.AfterDeploymentTrigger;
import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerConditionProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

@Builder(AfterDeploymentTrigger.class)
public class AfterDeploymentTriggerProperties extends TriggerProperties {
    public static final String NAME = preventInlining("After deployment");
    public static final String PLUGIN_KEY = preventInlining("com.atlassianlab.bamboo.plugins.bamboo-after-deployment-trigger-plugin:afterDeployment");

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties(PLUGIN_KEY);
    private final String deploymentProject;
    private final String environment;

    @SuppressWarnings("unused")
    private AfterDeploymentTriggerProperties() {
        super();
        deploymentProject = null;
        environment = null;
    }

    public AfterDeploymentTriggerProperties(@NotNull String deploymentProject,
                                            @Nullable String environment,
                                            @Nullable String description,
                                            boolean isEnabled,
                                            Set<TriggerConditionProperties> triggerConditionProperties) {
        super(NAME, description, isEnabled, triggerConditionProperties);
        this.deploymentProject = deploymentProject;
        this.environment = environment;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {

    }

    public String getDeploymentProject() {
        return deploymentProject;
    }

    public String getEnvironment() {
        return environment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AfterDeploymentTriggerProperties that = (AfterDeploymentTriggerProperties) o;
        return super.equals(o)
                && Objects.equals(deploymentProject, that.deploymentProject)
                && Objects.equals(environment, that.environment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), deploymentProject, environment);
    }
}
