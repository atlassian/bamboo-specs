package com.atlassian.bamboo.specs.maven.sandbox;

import com.atlassian.bamboo.specs.maven.sandbox.internal.InternalSecurityManager;

import java.security.AccessControlException;
import java.security.Permission;

/**
 * A permission checker with very limited set of allowed permissions.
 */
public abstract class AbstractThreadPermissionVerifier implements ThreadPermissionVerifier {
    private static final SecurityManager DELEGATE = new InternalSecurityManager();

    @Override
    public final void checkPermission(final Permission perm) {
        BambooSpecsSecurityManager.log("Checking: " + perm);
        try {
            DELEGATE.checkPermission(perm);
        } catch (final AccessControlException e) {
            if (checkPermissionFor(perm)) {
                BambooSpecsSecurityManager.log("Allowing " + perm + " for low privilege");
            } else {
                throw e;
            }
        }
    }

    @Override
    public void checkPermission(final Permission perm, final Object context) {
        DELEGATE.checkPermission(perm, context);
    }

    /**
     * This method will get invoked if a standard Applet security manager declined access.
     * You can use it to grant additional access on top of applet access.
     */
    protected abstract boolean checkPermissionFor(Permission perm);
}
