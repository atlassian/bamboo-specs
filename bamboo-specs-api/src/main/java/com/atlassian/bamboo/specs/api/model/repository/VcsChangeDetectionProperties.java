package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection.FileFilteringOption;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import com.atlassian.bamboo.specs.api.validators.repository.VcsChangeDetectionValidator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public final class VcsChangeDetectionProperties implements EntityProperties {
    private final boolean commitIsolationEnabled;
    private final String changesetFilterPatternRegex;
    private final FileFilteringOption filterFilePatternOption;
    private final String filterFilePatternRegex;
    private final Map<String, Object> configuration;

    private VcsChangeDetectionProperties() {
        commitIsolationEnabled = false;
        changesetFilterPatternRegex = "";
        filterFilePatternOption = FileFilteringOption.NONE;
        filterFilePatternRegex = "";
        configuration = Collections.emptyMap();
    }

    public VcsChangeDetectionProperties(boolean commitIsolationEnabled,
                                        @NotNull Map<String, Object> configuration,
                                        @Nullable String changesetFilterPatternRegex,
                                        @Nullable FileFilteringOption filterFilePatternOption,
                                        @Nullable String filterFilePatternRegex
    ) throws PropertiesValidationException {
        this.commitIsolationEnabled = commitIsolationEnabled;
        this.changesetFilterPatternRegex = changesetFilterPatternRegex;
        this.filterFilePatternOption = filterFilePatternOption;
        this.filterFilePatternRegex = filterFilePatternRegex;
        this.configuration = Collections.unmodifiableMap(MapUtils.copyOf(configuration));

        validate();
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final VcsChangeDetectionProperties that = (VcsChangeDetectionProperties) o;
        return isCommitIsolationEnabled() == that.isCommitIsolationEnabled() &&
                Objects.equals(getChangesetFilterPatternRegex(), that.getChangesetFilterPatternRegex()) &&
                Objects.equals(getFilterFilePatternOption(), that.getFilterFilePatternOption()) &&
                Objects.equals(getFilterFilePatternRegex(), that.getFilterFilePatternRegex()) &&
                Objects.equals(getConfiguration(), that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isCommitIsolationEnabled(), getChangesetFilterPatternRegex(), getFilterFilePatternOption(), getFilterFilePatternRegex(), getConfiguration());
    }

    public boolean isCommitIsolationEnabled() {
        return commitIsolationEnabled;
    }

    @NotNull
    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    @Nullable
    public String getChangesetFilterPatternRegex() {
        return changesetFilterPatternRegex;
    }

    @Nullable
    public FileFilteringOption getFilterFilePatternOption() {
        return filterFilePatternOption;
    }

    @Nullable
    public String getFilterFilePatternRegex() {
        return filterFilePatternRegex;
    }

    @Override
    public void validate() {
        checkNotNull("configuration", configuration);
        checkNoErrors(VcsChangeDetectionValidator.validate(this));
    }
}
