package com.atlassian.bamboo.specs.util;

import static java.util.Objects.requireNonNull;

public class SimpleTokenCredentials implements TokenCredentials {

    private final String token;

    public SimpleTokenCredentials(final String token) {
        this.token = requireNonNull(token);
    }

    @Override
    public String getToken() {
        return this.token;
    }

}
