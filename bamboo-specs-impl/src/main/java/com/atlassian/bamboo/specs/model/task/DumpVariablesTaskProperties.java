package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;

@Immutable
public final class DumpVariablesTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:dump");

    protected DumpVariablesTaskProperties() {
        super();
    }

    public DumpVariablesTaskProperties(@Nullable String description,
                                       boolean enabled,
                                       @NotNull List<RequirementProperties> requirements,
                                       @NotNull List<? extends ConditionProperties> conditions) {
        super(description, enabled, requirements, conditions);

        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        super.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumpVariablesTaskProperties)) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
