package com.atlassian.bamboo.specs.api.codegen;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

/**
 * Specialised code emitter that can be used to generate field setting code that handles multiple fields at once.
 * Can be used to combine class-specific handling with the generic field handling in class-level emitter.
 * Usage: put an annotation on one of the fields and ignore using {@link com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen} annotation.
 * Only used for generating setter code, i.e.: not used for constructor invocations.
 */
public interface CompoundFieldSetterEmitter<P extends EntityProperties> extends CodeEmitter<P> {
}
