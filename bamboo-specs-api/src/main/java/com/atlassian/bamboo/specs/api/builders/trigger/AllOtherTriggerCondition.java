package com.atlassian.bamboo.specs.api.builders.trigger;

import com.atlassian.bamboo.specs.api.model.trigger.AllOtherTriggerConditionProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class AllOtherTriggerCondition extends TriggerCondition<AllOtherTriggerCondition, AllOtherTriggerConditionProperties>  {
    private Map<String, String> configuration = new LinkedHashMap<>();

    public AllOtherTriggerCondition() {
    }

    public AllOtherTriggerCondition configuration(@NotNull Map<String, String> config) {
        this.configuration = Collections.unmodifiableMap(config);
        return this;
    }

    @Override
    protected AllOtherTriggerConditionProperties build() {
        return new AllOtherTriggerConditionProperties(configuration);
    }
}
