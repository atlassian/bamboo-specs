package com.atlassian.bamboo.specs.api.util;

import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.PluginConfiguration;
import com.atlassian.bamboo.specs.api.model.plan.configuration.AllOtherPluginsConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.plan.configuration.PluginConfigurationProperties;

import java.util.Map;

public final class PluginConfigurationHelper {
    private PluginConfigurationHelper() {
    }

    public static void putPluginConfiguration(Map<String, PluginConfigurationProperties> pluginConfigurations, PluginConfiguration<?> pluginConfigurationToAdd) {
        PluginConfigurationProperties pluginConfigurationProperties = EntityPropertiesBuilders.build(pluginConfigurationToAdd);
        if (!(pluginConfigurationToAdd instanceof AllOtherPluginsConfiguration) || !pluginConfigurations.containsKey(pluginConfigurationProperties.getAtlassianPlugin().getCompleteModuleKey())) {
            pluginConfigurations.put(pluginConfigurationProperties.getAtlassianPlugin().getCompleteModuleKey(), pluginConfigurationProperties);
            return;
        }

        mergeGenericPluginConfigurations(pluginConfigurations, pluginConfigurationProperties);
    }

    private static void mergeGenericPluginConfigurations(Map<String, PluginConfigurationProperties> pluginConfigurations, PluginConfigurationProperties pluginConfigurationToAdd) {
        final AllOtherPluginsConfigurationProperties aopcp = (AllOtherPluginsConfigurationProperties) pluginConfigurations.get(pluginConfigurationToAdd.getAtlassianPlugin().getCompleteModuleKey());
        final Map<String, Object> existingConfiguration = MapUtils.copyOf(aopcp.getConfiguration());
        mergeConfigurationMaps(((AllOtherPluginsConfigurationProperties) pluginConfigurationToAdd).getConfiguration(), existingConfiguration);
        pluginConfigurations.put(pluginConfigurationToAdd.getAtlassianPlugin().getCompleteModuleKey(), EntityPropertiesBuilders.build(new AllOtherPluginsConfiguration().configuration(existingConfiguration)));
    }

    private static void mergeConfigurationMaps(Map<String, Object> newConfiguration, Map<String, Object> existingConfiguration) {
        newConfiguration.forEach((k, v) -> {
            //you can erase pre-existing values by inserting null
            if (v == null) {
                existingConfiguration.remove(k);
                existingConfiguration.keySet().removeIf(ek -> ek.startsWith(k + "."));
                return;
            }
            //existing value is not a map: overwrite or add
            if (!(existingConfiguration.get(k) instanceof Map)) {
                existingConfiguration.put(k, v);
                return;
            }
            //existing value is a map: recursive call
            Map<String, Object> innerExistingMap = (Map) existingConfiguration.get(k);
            if (!(v instanceof Map)) {
                throw new IllegalStateException("Cannot merge map with value for key" + k);
            }
            Map<String, Object> innerNewMap = (Map) v;
            mergeConfigurationMaps(innerNewMap, innerExistingMap);
        });
    }
}
