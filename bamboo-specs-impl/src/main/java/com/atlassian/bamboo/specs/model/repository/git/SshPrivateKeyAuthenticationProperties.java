package com.atlassian.bamboo.specs.model.repository.git;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
@ConstructFrom("sshPrivateKey")
public final class SshPrivateKeyAuthenticationProperties implements AuthenticationProperties {
    private final String sshPrivateKey;
    private final String passphrase;

    private SshPrivateKeyAuthenticationProperties() {
        sshPrivateKey = null;
        passphrase = null;
    }

    public SshPrivateKeyAuthenticationProperties(String sshPrivateKey, @Nullable String passphrase) {
        this.sshPrivateKey = sshPrivateKey;
        this.passphrase = passphrase;

        validate();
    }

    public String getSshPrivateKey() {
        return sshPrivateKey;
    }

    @Nullable
    public String getPassphrase() {
        return passphrase;
    }

    public void validate() {
        checkNotNull("sshPrivateKey", sshPrivateKey);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SshPrivateKeyAuthenticationProperties that = (SshPrivateKeyAuthenticationProperties) o;
        return Objects.equals(getSshPrivateKey(), that.getSshPrivateKey()) &&
                Objects.equals(getPassphrase(), that.getPassphrase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSshPrivateKey(), getPassphrase());
    }
}
