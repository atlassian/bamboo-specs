package com.atlassian.bamboo.specs.api.model.plan.configuration;

import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

import java.util.Objects;

public class ConcurrentBuildsProperties implements PluginConfigurationProperties {

    private boolean useSystemWideDefault = true;
    private int maximumNumberOfConcurrentBuilds = 1;
    private ConcurrentBuilds.ConcurrentBuildsStrategy concurrentBuildsStrategy = ConcurrentBuilds.ConcurrentBuildsStrategy.getDefault();

    private static final AtlassianModuleProperties ATLASSIAN_MODULE = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.additionalBuildConfiguration:concurrentBuild");

    private ConcurrentBuildsProperties() {
    }

    public ConcurrentBuildsProperties(boolean useSystemWideDefault,
                                      int maximumNumberOfConcurrentBuilds,
                                      ConcurrentBuilds.ConcurrentBuildsStrategy concurrentBuildsStrategy) {
        this.useSystemWideDefault = useSystemWideDefault;
        this.maximumNumberOfConcurrentBuilds = maximumNumberOfConcurrentBuilds;
        this.concurrentBuildsStrategy = concurrentBuildsStrategy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConcurrentBuildsProperties that = (ConcurrentBuildsProperties) o;
        return isUseSystemWideDefault() == that.isUseSystemWideDefault()
                && getMaximumNumberOfConcurrentBuilds() == that.getMaximumNumberOfConcurrentBuilds()
                && getConcurrentBuildsStrategy() == that.getConcurrentBuildsStrategy();

    }

    @Override
    public int hashCode() {
        return Objects.hash(isUseSystemWideDefault(), getMaximumNumberOfConcurrentBuilds(), getConcurrentBuildsStrategy());
    }

    public boolean isUseSystemWideDefault() {
        return useSystemWideDefault;
    }

    public int getMaximumNumberOfConcurrentBuilds() {
        return maximumNumberOfConcurrentBuilds;
    }

    public ConcurrentBuilds.ConcurrentBuildsStrategy getConcurrentBuildsStrategy() {
        return concurrentBuildsStrategy;
    }

    @Override
    public void validate() {
        ImporterUtils.checkThat(ValidationContext.of("maximumNumberOfConcurrentBuilds"), maximumNumberOfConcurrentBuilds > 0, "Maximum number of concurrent builds must be greater than 0.");
    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }
}
