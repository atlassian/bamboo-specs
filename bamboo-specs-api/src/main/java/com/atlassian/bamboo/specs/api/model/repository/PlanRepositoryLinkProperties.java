package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class PlanRepositoryLinkProperties implements EntityProperties {
    private final VcsRepositoryProperties repositoryDefinition;

    private PlanRepositoryLinkProperties() {
        repositoryDefinition = null;
    }

    public PlanRepositoryLinkProperties(final VcsRepositoryProperties repositoryDefinition) {
        this.repositoryDefinition = repositoryDefinition;
        validate();
    }

    public static class LinkedGlobalRepository extends VcsRepositoryProperties {
        private final AtlassianModuleProperties atlassianPlugin;

        private LinkedGlobalRepository() {
            atlassianPlugin = null;
        }

        public LinkedGlobalRepository(@NotNull String parent,
                                      @Nullable AtlassianModuleProperties atlassianPlugin) {
            super(null, null, null, parent, null);
            ImporterUtils.checkNotNull("parent", parent);
            this.atlassianPlugin = atlassianPlugin;
        }

        /**
         * Returns atlassian plugin inherited from parent if known, null if it's not.
         */
        @Nullable
        @Override
        public AtlassianModuleProperties getAtlassianPlugin() {
            return atlassianPlugin;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }
            final LinkedGlobalRepository that = (LinkedGlobalRepository) o;
            return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin());
        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), getAtlassianPlugin());
        }
    }

    public static class ProjectRepository extends VcsRepositoryProperties {
        private final AtlassianModuleProperties atlassianPlugin;

        private ProjectRepository() {
            atlassianPlugin = null;
        }

        public ProjectRepository(@NotNull String parent,
                                      @Nullable AtlassianModuleProperties atlassianPlugin,
                                      @Nullable ProjectProperties project) {
            super(null, null, null, parent, null, project);
            ImporterUtils.checkNotNull("parent", parent);
            this.atlassianPlugin = atlassianPlugin;
        }

        /**
         * Returns atlassian plugin inherited from parent if known, null if it's not.
         */
        @Nullable
        @Override
        public AtlassianModuleProperties getAtlassianPlugin() {
            return atlassianPlugin;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }
            final ProjectRepository that = (ProjectRepository) o;
            return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) && Objects.equals(getProject(), that.getProject());
        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), getAtlassianPlugin(), getProject());
        }
    }

    public VcsRepositoryProperties getRepositoryDefinition() {
        return repositoryDefinition;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PlanRepositoryLinkProperties that = (PlanRepositoryLinkProperties) o;
        return Objects.equals(getRepositoryDefinition(), that.getRepositoryDefinition());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRepositoryDefinition());
    }

    @Override
    public void validate() {
        ImporterUtils.checkNotNull("repositoryDefinition", repositoryDefinition);
    }
}
