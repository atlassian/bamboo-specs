package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.Objects;

import static com.atlassian.bamboo.specs.util.FileUtils.readFileContent;

/**
 * Task to build docker image.
 */
public class DockerBuildImageTask extends AbstractDockerTask<DockerBuildImageTask, DockerBuildImageTaskProperties> {
    @NotNull
    private String imageName;
    @Nullable
    private String dockerfile;
    @NotNull
    private DockerBuildImageTaskProperties.DockerfileContent dockerfileContent = DockerBuildImageTaskProperties.DockerfileContent.INLINE;
    private boolean useCache = true;
    private boolean saveAsFile;
    @Nullable
    private String imageFilename;
    @Nullable
    private String additionalArguments;

    /**
     * Sets the image name. You can also optionally specify repository, namespace and a tag. E.g.
     * <i>registry.address:port/namespace/repository:tag</i>
     * <p>
     * This field is mandatory.
     * </p>
     */
    public DockerBuildImageTask imageName(@NotNull String imageName) {
        this.imageName = imageName;
        return this;
    }

    /**
     * Task will use <i>Dockerfile</i> which should be available in a working directory.
     */
    public DockerBuildImageTask dockerfileInWorkingDir() {
        this.dockerfileContent = DockerBuildImageTaskProperties.DockerfileContent.WORKING_DIR;
        this.dockerfile = null;
        return this;
    }

    /**
     * Specifies content of a Dockerfile.
     * <p>
     * Specifying content of a Dockerfile is mandatory, unless you use {@link #dockerfileInWorkingDir()}
     * </p>
     */
    public DockerBuildImageTask dockerfile(String dockerfile) {
        this.dockerfileContent = DockerBuildImageTaskProperties.DockerfileContent.INLINE;
        this.dockerfile = dockerfile;
        return this;
    }

    /**
     * Specifies content of a Dockerfile with content of a file. File must be available on the path when running Bamboo Specs.
     * <p>
     * Specifying content of a Dockerfile is mandatory, unless you use {@link #dockerfileInWorkingDir()}
     * </p>
     */
    public DockerBuildImageTask dockerfileFromPath(@NotNull final Path path) {
        return dockerfile(readFileContent(path, "Dockerfile path", "Error when reading Dockerfile from path: %s"));
    }

    /**
     * Specifies if cache should be used. Defaults to true.
     */
    public DockerBuildImageTask useCache(boolean useCache) {
        this.useCache = useCache;
        return this;
    }

    /**
     * If set to true Docker image will be saved to file.
     * <p>
     * If sets to true {@link #imageFilename} must be specified.
     * </p>
     */
    public DockerBuildImageTask saveAsFile(boolean saveAsFile) {
        this.saveAsFile = saveAsFile;
        return this;
    }

    /**
     * Specifies name of the file to which image will be saved.
     */
    public DockerBuildImageTask imageFilename(String imageFilename) {
        this.imageFilename = imageFilename;
        return this;
    }

    /**
     * Specifies additional build options, see https://docs.docker.com/engine/reference/commandline/build/.
     */
    public DockerBuildImageTask additionalArguments(String additionalArguments) {
        this.additionalArguments = additionalArguments;
        return this;
    }

    @NotNull
    @Override
    protected DockerBuildImageTaskProperties build() {
        return new DockerBuildImageTaskProperties(description,
                taskEnabled,
                imageName,
                dockerfileContent,
                dockerfile,
                useCache,
                saveAsFile,
                imageFilename,
                additionalArguments,
                environmentVariables,
                workingSubdirectory,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DockerBuildImageTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        DockerBuildImageTask that = (DockerBuildImageTask) o;
        return useCache == that.useCache &&
                saveAsFile == that.saveAsFile &&
                imageName.equals(that.imageName) &&
                Objects.equals(dockerfile, that.dockerfile) &&
                dockerfileContent == that.dockerfileContent &&
                Objects.equals(imageFilename, that.imageFilename) &&
                Objects.equals(additionalArguments, that.additionalArguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), imageName, dockerfile, dockerfileContent, useCache, saveAsFile, imageFilename, additionalArguments);
    }
}
