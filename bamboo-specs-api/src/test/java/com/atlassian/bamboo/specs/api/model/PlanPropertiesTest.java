package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationType;
import com.atlassian.bamboo.specs.api.builders.notification.EmptyNotificationsList;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryBranch;
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.notification.EmptyNotificationsListProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import junitparams.JUnitParamsRunner;
import junitparams.NamedParameters;
import junitparams.Parameters;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class PlanPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldValidateNotifications() {
        final Project project = new Project().key("BAR").name("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .notifications(new EmptyNotificationsList());

        //when
        final PlanProperties properties = EntityPropertiesBuilders.build(plan);

        //then
        assertThat(properties.getNotifications(), Matchers.hasItem(new EmptyNotificationsListProperties()));
    }

    @Test
    public void shouldRejectListWithEmptyListAndSomeOtherNotification() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("EmptyNotificationsList must be the only element on the notifications list");
        final Project project = new Project().key("BAR").name("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .notifications(new EmptyNotificationsList(),
                        new Notification()
                                .recipients(new AnyNotificationRecipient(new AtlassianModule("Baz:z")))
                                .type(new AnyNotificationType(new AtlassianModule("Baz:z"))));

        //when
        EntityPropertiesBuilders.build(plan);
    }

    @Test
    public void shouldRejectDoubleBranchOverride() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Plan: Duplicate branch definition for repository blah");
        final Project project = new Project().key("BAR").name("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .linkedRepositories("blah")
                .repositoryBranches(new VcsRepositoryBranch("blah", "branch1"),
                        new VcsRepositoryBranch("blah", "branch2"));

        //when
        EntityPropertiesBuilders.build(plan);
    }

    @Test
    @Parameters(named = "shouldRejectBranchOverrideIfRepoUnknown")
    public void shouldRejectBranchOverrideIfRepoUnknown(String expectedErrorMessage, Plan plan) {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage(expectedErrorMessage);

        //when
        EntityPropertiesBuilders.build(plan);
    }

    @Test
    public void shouldRemoveNotificationsOnClear() {
        final Project project = new Project().key("BAR").name("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .notifications(new Notification()
                        .recipients(new AnyNotificationRecipient(new AtlassianModule("Baz:z")))
                        .type(new AnyNotificationType(new AtlassianModule("Baz:z"))));
        assertThat(EntityPropertiesBuilders.build(plan).getNotifications(), Matchers.hasSize(1));
        plan.clearNotifications();
        assertThat(EntityPropertiesBuilders.build(plan).getNotifications(), Matchers.empty());
    }

    @Test
    public void shouldRemoveTriggersOnClear() {
        final Project project = new Project().key("BAR").name("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .triggers(
                        new AnyTrigger(new AtlassianModule("FOO:FOO"))
                                .name("FOO"));
        assertThat(EntityPropertiesBuilders.build(plan).getTriggers(), Matchers.hasSize(1));
        plan.clearTriggers();
        assertThat(EntityPropertiesBuilders.build(plan).getTriggers(), Matchers.empty());
    }


    @NamedParameters("shouldRejectBranchOverrideIfRepoUnknown")
    private Object[][] parametersForShouldRejectBranchOverrideIfRepoUnknown() {
        final Project project = new Project().key("BAR").name("BAR");
        VcsRepositoryBranch vcsRepositoryBranch = new VcsRepositoryBranch("blah", "branch1");
        return new Object[][]{
                {"Plan: Branch defined for unknown repository 'blah'. No repositories defined.",
                        new Plan(project, "foo", "FOO")
                                .repositoryBranches(vcsRepositoryBranch)},
                {"Plan: Branch defined for unknown repository 'blah'. Should be one from list: my repo,my repo2",
                        new Plan(project, "foo", "FOO")
                                .linkedRepositories("my repo", "my repo2")
                                .repositoryBranches(vcsRepositoryBranch)
                }
        };
    }

}
