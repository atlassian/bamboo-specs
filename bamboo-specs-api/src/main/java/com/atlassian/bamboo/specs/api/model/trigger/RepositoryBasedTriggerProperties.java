package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public abstract class RepositoryBasedTriggerProperties extends TriggerProperties {
    private final TriggeringRepositoriesType triggeringRepositoriesType;
    private final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories;

    protected RepositoryBasedTriggerProperties() {
        super();
        this.triggeringRepositoriesType = TriggeringRepositoriesType.ALL;
        this.selectedTriggeringRepositories = Collections.emptyList();
    }

    public RepositoryBasedTriggerProperties(final String name,
                                            final String description,
                                            final boolean isEnabled,
                                            final Set<TriggerConditionProperties> conditions,
                                            final TriggeringRepositoriesType triggeringRepositoriesType,
                                            final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(name, description, isEnabled, conditions);
        this.triggeringRepositoriesType = triggeringRepositoriesType;
        if (triggeringRepositoriesType == TriggeringRepositoriesType.SELECTED) {
            this.selectedTriggeringRepositories = Collections.unmodifiableList(new ArrayList<>(selectedTriggeringRepositories));
        } else {
            this.selectedTriggeringRepositories = Collections.emptyList();
        }
    }

    public TriggeringRepositoriesType getTriggeringRepositoriesType() {
        return triggeringRepositoriesType;
    }

    public List<VcsRepositoryIdentifierProperties> getSelectedTriggeringRepositories() {
        return selectedTriggeringRepositories;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
    }

    @Override
    public EnumSet<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        RepositoryBasedTriggerProperties that = (RepositoryBasedTriggerProperties) o;
        return triggeringRepositoriesType == that.triggeringRepositoriesType &&
                Objects.equals(selectedTriggeringRepositories, that.selectedTriggeringRepositories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), triggeringRepositoriesType, selectedTriggeringRepositories);
    }
}
