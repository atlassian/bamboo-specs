package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.model.task.BaseSshTaskProperties;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class SshTaskEmitterTest {
    private CodeGenerationContext context;
    private SshTaskEmitter emitter;

    private static final List<RequirementProperties> REQS = Collections.singletonList(new RequirementProperties("key", "value", Requirement.MatchType.EXISTS));

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new SshTaskEmitter();
    }

    @Test
    public void testAuthenticationWithSshProjectSharedCredentials() throws Exception {
        SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties = new SharedCredentialsIdentifierProperties("project-shared-credentials", null, SharedCredentialsScope.PROJECT);

        final SshTaskProperties properties = new SshTaskProperties(null, true,
                                                                   "localhost", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE, null, null, null,
                                                                   sharedCredentialsIdentifierProperties, null, 80,
                                                                   0, "ls -all", REQS, Collections.emptyList());

        final String expectedCode =
                "new SshTask()\n" +
                "    .authenticateWithSshSharedCredentials(new SharedCredentialsIdentifier(\"project-shared-credentials\").scope(SharedCredentialsScope.PROJECT))\n" +
                "    .requirements(new Requirement(\"key\")\n" +
                "            .matchValue(\"value\"))\n" +
                "    .username(\"admin\")\n" +
                "    .port(80)\n" +
                "    .command(\"ls -all\")\n" +
                "    .host(\"localhost\")";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void testAuthenticationWithSshProjectSharedCredentialsWithMultipleHosts() throws Exception {
        SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties = new SharedCredentialsIdentifierProperties("project-shared-credentials", null, SharedCredentialsScope.PROJECT);

        final SshTaskProperties properties = new SshTaskProperties(null, true,
                "localhost, 123.123.123, 0.0.0.0", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE, null, null, null,
                sharedCredentialsIdentifierProperties, null, 80,
                0, "ls -all", REQS, Collections.emptyList());

        final String expectedCode =
                "new SshTask()\n" +
                        "    .authenticateWithSshSharedCredentials(new SharedCredentialsIdentifier(\"project-shared-credentials\").scope(SharedCredentialsScope.PROJECT))\n" +
                        "    .requirements(new Requirement(\"key\")\n" +
                        "            .matchValue(\"value\"))\n" +
                        "    .username(\"admin\")\n" +
                        "    .port(80)\n" +
                        "    .command(\"ls -all\")\n" +
                        "    .host(\"localhost\").host(\"123.123.123\").host(\"0.0.0.0\")";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }
}
