package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.model.trigger.RemoteTriggerProperties;

/**
 * Represents remote trigger.
 */
public class RemoteTrigger extends RepositoryBasedTrigger<RemoteTrigger, RemoteTriggerProperties> {

    private String triggerIPAddresses;

    /**
     * Sets the IP address.
     * <p>
     * Bamboo ensures that triggers originate from IP addresses of the repository server(s). You can authorise
     * additional IP addresses or CIDRs here, separated by a comma.
     * In example: '10.0.0.0/32'
     */
    public RemoteTrigger triggerIPAddresses(final String triggerIPAddresses) {
        this.triggerIPAddresses = triggerIPAddresses;
        return this;
    }

    @Override
    protected RemoteTriggerProperties build() {
        return new RemoteTriggerProperties(name, description, triggerEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories, triggerIPAddresses);
    }
}
