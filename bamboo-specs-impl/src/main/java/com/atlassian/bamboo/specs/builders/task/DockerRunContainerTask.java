package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.docker.DockerConstants;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Task used to run a Docker container.
 */
public class DockerRunContainerTask extends AbstractDockerTask<DockerRunContainerTask, DockerRunContainerTaskProperties> {

    @NotNull
    private String imageName;
    private boolean detachContainer;
    @Nullable
    private String containerName;
    @NotNull
    private final Map<Integer, Integer> portMappings;
    private boolean waitToStart;
    @Nullable
    private String serviceURLPattern;
    private long serviceTimeout;

    private boolean linkToDetachedContainers;
    @Nullable
    private String containerEnvironmentVariables;
    @Nullable
    private String containerCommand;
    @Nullable
    private String containerWorkingDirectory;
    @Nullable
    private String additionalArguments;
    private Map<String, String> volumeMappings = new LinkedHashMap<>();

    public DockerRunContainerTask() {
        portMappings = new LinkedHashMap<>();
        serviceTimeout = DockerRunContainerTaskProperties.DEFAULT_SERVICE_TIMEOUT;

        volumeMappings = new LinkedHashMap<>();
        volumeMappings.put(DockerConstants.DEFAULT_HOST_MAPPING, DockerConstants.DEFAULT_CONTAINER_MAPPING);

        containerWorkingDirectory = DockerConstants.DEFAULT_CONTAINER_MAPPING;
    }

    /**
     * Specifies image name to run. Optionally registry name, namespace and tag may be added here,
     * e.g <i>registry.address:port/namespace/repository:tag</i>.
     * <p>
     * This field is mandatory.
     * </p>
     */
    public DockerRunContainerTask imageName(String imageName) {
        this.imageName = imageName;
        return this;
    }

    /**
     * Determines if container should be run in a detached mode. Defaults to false.
     * <p>
     * If set to true specifing a container name is mandatory.
     * </p>
     */
    public DockerRunContainerTask detachContainer(boolean detachContainer) {
        this.detachContainer = detachContainer;
        return this;
    }

    /**
     * Specifies container name.
     */
    public DockerRunContainerTask containerName(String containerName) {
        this.containerName = containerName;
        return this;
    }

    /**
     * Append a host-container port mapping.
     */
    public DockerRunContainerTask appendPortMapping(int hostPort, int containerPort) {
        this.portMappings.put(hostPort, containerPort);
        return this;
    }

    /**
     * Clears port mappings configuration.
     */
    public DockerRunContainerTask clearPortMappings() {
        this.portMappings.clear();
        return this;
    }

    /**
     * Determines if Bamboo should wait for a service to start. Defaults to false.
     * <p>
     * If sets to true service URL pattern and timeout settings are mandatory.
     * </p>
     */
    public DockerRunContainerTask waitToStart(boolean waitToStart) {
        this.waitToStart = waitToStart;
        return this;
    }

    /**
     * Bamboo will check if container service is up and running by querying this URL.
     * Defaults to <i>http://localhost:${docker.port}</i>
     * <p>
     * Note: You can use ${docker.port} to get the first exposed container port.
     * </p>
     */
    public DockerRunContainerTask serviceURLPattern(String serviceURLPattern) {
        this.serviceURLPattern = serviceURLPattern;
        return this;
    }

    /**
     * Sets timeout in seconds on waiting till container service starts. Defaults to 120.
     */
    public DockerRunContainerTask serviceTimeoutInSeconds(long serviceTimeout) {
        this.serviceTimeout = serviceTimeout;
        return this;
    }

    /**
     * Sets timeout on waiting till container service starts. Defaults to 120 seconds.
     */
    public DockerRunContainerTask serviceTimeout(Duration serviceTimeout) {
        return serviceTimeoutInSeconds(TimeUnit.MINUTES.toSeconds(serviceTimeout.toMinutes()));
    }

    /**
     * Determines whether this container should be linked to other containers which were defined prior to this one
     * via {@link DockerRunContainerTask} within the same job.
     */
    public DockerRunContainerTask linkToDetachedContainers(boolean linkToDetachedContainers) {
        this.linkToDetachedContainers = linkToDetachedContainers;
        return this;
    }

    /**
     * Specifies container environment variables.
     */
    public DockerRunContainerTask containerEnvironmentVariables(String containerEnvironmentVariables) {
        this.containerEnvironmentVariables = containerEnvironmentVariables;
        return this;
    }

    /**
     * Specifies container command.
     */
    public DockerRunContainerTask containerCommand(String containerCommand) {
        this.containerCommand = containerCommand;
        return this;
    }

    /**
     * Sets container working directory. Defaults to <i>/data</i>
     */
    public DockerRunContainerTask containerWorkingDirectory(String containerWorkingDirectory) {
        this.containerWorkingDirectory = containerWorkingDirectory;
        return this;
    }

    /**
     * Sets additional arguments to <i>docker run</i> command, e.g <i>--memory="64m"</i>.
     */
    public DockerRunContainerTask additionalArguments(String additionalArguments) {
        this.additionalArguments = additionalArguments;
        return this;
    }

    /**
     * Appends a host-container volume mapping. Default mapping entry is <i>${bamboo.working.directory} -&gt; /data</i>.
     * Note: You can clear it by running {@link #clearVolumeMappings()}.
     */
    public DockerRunContainerTask appendVolumeMapping(String hostDirectory, String containerDataVolume) {
        volumeMappings.put(hostDirectory, containerDataVolume);
        return this;
    }

    /**
     * Clears volume mappings configuration. Please remember that default container
     * working directory is <i>/data</i>. You can update it via {@link #containerWorkingDirectory(String)}.
     */
    public DockerRunContainerTask clearVolumeMappings() {
        volumeMappings.clear();
        return this;
    }

    @NotNull
    @Override
    protected DockerRunContainerTaskProperties build() {
        return new DockerRunContainerTaskProperties(description,
                taskEnabled,
                imageName,
                detachContainer,
                containerName,
                portMappings,
                waitToStart,
                serviceURLPattern,
                serviceTimeout,
                linkToDetachedContainers,
                containerEnvironmentVariables,
                containerCommand,
                containerWorkingDirectory,
                additionalArguments,
                volumeMappings,
                environmentVariables,
                workingSubdirectory,
                requirements,
                conditions
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DockerRunContainerTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        DockerRunContainerTask that = (DockerRunContainerTask) o;
        return detachContainer == that.detachContainer &&
                waitToStart == that.waitToStart &&
                serviceTimeout == that.serviceTimeout &&
                linkToDetachedContainers == that.linkToDetachedContainers &&
                imageName.equals(that.imageName) &&
                Objects.equals(containerName, that.containerName) &&
                portMappings.equals(that.portMappings) &&
                Objects.equals(serviceURLPattern, that.serviceURLPattern) &&
                Objects.equals(containerEnvironmentVariables, that.containerEnvironmentVariables) &&
                Objects.equals(containerCommand, that.containerCommand) &&
                Objects.equals(containerWorkingDirectory, that.containerWorkingDirectory) &&
                Objects.equals(additionalArguments, that.additionalArguments) &&
                Objects.equals(volumeMappings, that.volumeMappings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), imageName, detachContainer, containerName, portMappings, waitToStart, serviceURLPattern, serviceTimeout, linkToDetachedContainers, containerEnvironmentVariables, containerCommand, containerWorkingDirectory, additionalArguments, volumeMappings);
    }
}
