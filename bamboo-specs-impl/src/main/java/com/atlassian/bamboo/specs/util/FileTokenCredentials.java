package com.atlassian.bamboo.specs.util;

import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FileTokenCredentials implements TokenCredentials {

    private static final Logger log = Logger.getLogger(FileTokenCredentials.class);

    protected static final String TOKEN_PROPERTY = "token";

    private final String token;

    public FileTokenCredentials(@Nonnull String credentialsLocation) {
        Properties properties = loadProperties(credentialsLocation);
        token = getPropertyOrThrow(properties, TOKEN_PROPERTY, credentialsLocation);
    }

    @Override
    public String getToken() {
        return token;
    }

    protected static Properties loadProperties(String credentialsLocation) {
        final Properties properties = new Properties();
        try {
            final File credentialsFile = new File(credentialsLocation);
            log.debug("Looking for credentials file: %s", credentialsFile.getAbsolutePath());

            if (credentialsFile.exists()) {
                log.debug("Credentials file found: %s", credentialsFile.getAbsolutePath());
                properties.load(new FileReader(credentialsFile));
            } else {
                log.debug("Credentials file not found under '%s', scanning JVM classpath for '%s'", credentialsFile.getAbsolutePath(), credentialsLocation);
                final InputStream resourceAsStream = FileTokenCredentials.class.getClassLoader().getResourceAsStream(credentialsLocation);
                if (resourceAsStream == null) {
                    log.debug("Credentials not found on JVM classpath: %s", credentialsLocation);
                    log.info("Couldn't find credentials file '%s' to access Bamboo", credentialsLocation);
                    log.info("Please make sure that the path points to an existing file or to a resource available on JVM classpath");
                    LogUtils.hintLogLevel(log, Logger.LogLevel.DEBUG);
                    throw new RuntimeException(String.format("Couldn't find credentials file: %s", credentialsLocation));
                }

                log.debug("Found credentials file on JVM classpath: %s", credentialsLocation);
                properties.load(RestHelper.class.getClassLoader().getResourceAsStream(credentialsLocation));
            }

            return properties;
        } catch (final IOException e) {
            log.info("Couldn't read credentials file '%s' to access Bamboo", credentialsLocation);
            log.info("Please make sure that the file is accessible and that it is in correct format (Java properties)");
            LogUtils.hintLogLevel(log, Logger.LogLevel.DEBUG);
            throw new RuntimeException(e);
        }
    }

    protected static String getPropertyOrThrow(Properties properties, String propertyKey, String fileName) {
        final String propertyValue = properties.getProperty(propertyKey);
        if (StringUtils.isEmpty(propertyValue)) {
            log.info("Property '%s' must be defined in credentials file '%s' to access Bamboo", propertyKey, fileName);
            log.info("Please make sure that the file is in correct format (Java properties)");
            throw new RuntimeException(String.format("Property '%s' was not defined in credentials file: %s", propertyKey, fileName));
        } else {
            return propertyValue;
        }
    }
}
