package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.permission.AnonymousUserPermissionsProperties;

import java.util.LinkedHashSet;
import java.util.Set;

public class AnonymousUserPermissions extends EntityPropertiesBuilder<AnonymousUserPermissionsProperties> {

    private final Set<PermissionType> permissionTypes = new LinkedHashSet<>();

    public AnonymousUserPermissions view() {
        this.permissionTypes.add(PermissionType.VIEW);
        return this;
    }

    public Set<PermissionType> getPermissionTypes() {
        return permissionTypes;
    }

    @Override
    protected AnonymousUserPermissionsProperties build() {
        return new AnonymousUserPermissionsProperties(permissionTypes);
    }

}
