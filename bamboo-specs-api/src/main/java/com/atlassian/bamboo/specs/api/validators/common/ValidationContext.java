package com.atlassian.bamboo.specs.api.validators.common;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class helps to describe location of problems found by validators. Typical usage:
 * <ul><li>no context of an error:
 * <pre>ValidationContext.empty()</pre></li></ul>
 * <ul><li>a single element:
 * <pre>ValidationContext.of("key")</pre></li></ul>
 * <ul><li>a nested hierarchy:
 * <pre>ValidationContext.of("Plan P").with("Stage 1").with("Job A").with("name")</pre></li></ul>
 */
public final class ValidationContext {
    @NotNull
    private final List<String> contextPath;

    /**
     * Returns an empty context.
     *
     * @return ValidationContext
     */
    public static ValidationContext empty() {
        return new ValidationContext(Collections.emptyList());
    }

    /**
     * Returns a single-element context.
     *
     * @param context name
     * @return ValidationContext
     */
    public static ValidationContext of(@NotNull final String context) {
        return new ValidationContext(Collections.singletonList(context));
    }

    private ValidationContext(@NotNull final List<String> initialList) {
        contextPath = new ArrayList<>(initialList);
    }

    /**
     * Returns <b>new instance</b> of validation context with the {@code context} element appended to it.
     *
     * @param context name
     * @return ValidationContext
     */
    public ValidationContext with(@NotNull final String context) {
        final List<String> newContextPath = new ArrayList<>(contextPath);
        newContextPath.add(context);
        return new ValidationContext(newContextPath);
    }

    /**
     * Returns true if context is empty.
     *
     * @return boolean
     */
    public boolean isEmpty() {
        return contextPath.isEmpty();
    }

    /**
     * Returns true if context contains the given element.
     *
     * @return boolean
     */
    public boolean contains(String element) {
        return contextPath.contains(element);
    }

    public String toString() {
        return contextPath.stream().reduce((s1, s2) -> s1 + " / " + s2).orElse("");
    }

}
