package com.atlassian.bamboo.specs.api.builders.credentials;

public enum SharedCredentialsScope {
    /**
     * Global shared credentials.
     */
    GLOBAL,
    /**
     * Project shared credentials; Shared credentials which have been defined in Project. Applicable for Bamboo DC only.
     */
    PROJECT;
}
