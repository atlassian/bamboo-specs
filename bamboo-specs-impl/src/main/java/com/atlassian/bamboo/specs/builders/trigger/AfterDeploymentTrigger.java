package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.model.trigger.AfterDeploymentTriggerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a After deployment completion trigger.
 * @since 10.0
 */
public class AfterDeploymentTrigger extends Trigger<AfterDeploymentTrigger, AfterDeploymentTriggerProperties> {
    private String deploymentProject;
    private String environment;

    /**
     * Sets the deployment project name.
     */
    public AfterDeploymentTrigger deploymentProject(@NotNull String deploymentProject) {
        this.deploymentProject = deploymentProject;
        return this;
    }

    /**
     * Sets the environment name.
     */
    public AfterDeploymentTrigger environment(@Nullable String environment) {
        this.environment = environment;
        return this;
    }

    /**
     * Specifies that any environment is applicable.
     */
    public AfterDeploymentTrigger anyEnvironment() {
        this.environment = null;
        return this;
    }

    @Override
    protected AfterDeploymentTriggerProperties build() {
        return new AfterDeploymentTriggerProperties(deploymentProject, environment, description, triggerEnabled, conditions);
    }
}
