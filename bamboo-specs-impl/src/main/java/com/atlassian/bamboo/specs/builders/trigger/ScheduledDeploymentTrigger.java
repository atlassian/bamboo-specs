package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.CronExpressionCreationHelper;
import com.atlassian.bamboo.specs.model.trigger.ScheduledTriggerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a Scheduled trigger for deployment environments.
 */
public class ScheduledDeploymentTrigger extends Trigger<ScheduledDeploymentTrigger, ScheduledTriggerProperties> {
    private String artifactBranch;
    private String cronExpression = "0 0 0 ? * *";
    private boolean skipIfLatestVersionAlreadyDeployed;

    /**
     * Creates trigger which will schedule a build basing on a cron expression.
     */
    public ScheduledDeploymentTrigger() throws PropertiesValidationException {
    }

    /**
     * Schedules deployment execution every {@link TimeUnit#SECONDS}, {@link TimeUnit#MINUTES} or {@link
     * TimeUnit#HOURS}.
     */
    public ScheduledDeploymentTrigger scheduleEvery(int every, @NotNull TimeUnit at) {
        return cronExpression(CronExpressionCreationHelper.scheduleEvery(every, at));
    }

    /**
     * Schedules deployment execution to every day at specified time.
     */
    public ScheduledDeploymentTrigger scheduleOnceDaily(@NotNull LocalTime at) {
        return cronExpression(CronExpressionCreationHelper.scheduleOnceDaily(at));
    }

    /**
     * Schedules deployment execution to every week on specified week days at specified time.
     */
    public ScheduledDeploymentTrigger scheduleWeekly(@NotNull LocalTime at, DayOfWeek... onDays) {
        return cronExpression(CronExpressionCreationHelper.scheduleWeekly(at, onDays));
    }

    /**
     * Schedules deployment execution to every week on specified week days and time.
     */
    public ScheduledDeploymentTrigger scheduleWeekly(@NotNull LocalTime at, @NotNull Collection<DayOfWeek> days) {
        return cronExpression(CronExpressionCreationHelper.scheduleWeekly(at, days));
    }

    /**
     * Schedules deployment execution to every month on specified day of month and time.
     */
    public ScheduledDeploymentTrigger scheduleMonthly(@NotNull LocalTime at, int dayOfMonth) {
        return cronExpression(CronExpressionCreationHelper.scheduleMonthly(at, dayOfMonth));
    }

    /**
     * Schedules deployment execution according to the cron expression. Default value is '0 0 0 ? * *'.
     * <p>
     * Cron expression won't be properly validated until sent out to Bamboo.
     */
    public ScheduledDeploymentTrigger cronExpression(@NotNull String cronExpression) {
        checkNotNull("cronExpression", cronExpression);
        this.cronExpression = cronExpression;
        return this;
    }

    /**
     * Branch to provide artifacts for this deployment.
     *
     * @param artifactBranch name of plan branch. If value is null artifacts will be taken from plan master branch.
     */
    public ScheduledDeploymentTrigger artifactBranch(@Nullable String artifactBranch) {
        this.artifactBranch = artifactBranch;
        return this;
    }

    /**
     * Should firing of the trigger be skipped if the latest version is already deployed.
     */
    public ScheduledDeploymentTrigger skipIfLatestVersionAlreadyDeployed(boolean skipIfLatestVersionAlreadyDeployed) {
        this.skipIfLatestVersionAlreadyDeployed = skipIfLatestVersionAlreadyDeployed;
        return this;
    }

    @Override
    protected ScheduledTriggerProperties build() {
        return new ScheduledTriggerProperties(name, description, triggerEnabled, conditions, cronExpression, artifactBranch, skipIfLatestVersionAlreadyDeployed, ScheduledTriggerProperties.Container.DEPLOYMENT);
    }
}
