/**
 * Various implementations of {@link com.atlassian.bamboo.specs.api.codegen.CodeEmitter} which are used for converting
 * {@link com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties} model into Java.
 */
package com.atlassian.bamboo.specs.codegen.emitters.repository.viewer;
