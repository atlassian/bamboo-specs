package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.VisibleForTesting;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.regex.Pattern;

public final class YamlFile {
    private static final AtomicLong COUNTER = new AtomicLong();
    private static final String YAML_FILE_PATTERN = "%05d-%08x-%s.yaml";

    private final Path location;
    private final int counter;
    private final int hashCode;
    private final String id;
    private static final Pattern NON_CANONICAL_LINE_PATTERN = Pattern.compile("\\sspecsSourceId:\\s");

    private YamlFile(final Path location, final int counter, final int hashCode, final String id) {
        this.location = location;
        this.counter = counter;
        this.hashCode = hashCode;
        this.id = id;
    }

    public int getCounter() {
        return counter;
    }

    public int getHashCode() {
        return hashCode;
    }

    public String getId() {
        return id;
    }

    public Path getLocation() {
        return location;
    }

    public boolean contentCanonicallyEquals(final YamlFile other) throws IOException {
        if (other.hashCode!=hashCode) {
            return false;
        }
        final String otherContent = FileUtils.readFileToString(other.location);
        final String thisContent = FileUtils.readFileToString(location);

        return toCanonicalContent(otherContent).equals(toCanonicalContent(thisContent));
    }

    public static YamlFile parse(@NotNull final Path path) {
        final String name = path.getFileName().toString();
        final String counter = name.substring(0, 5);
        final String hashCode = name.substring(6, 14);
        String id = name.substring(15);
        if (id.endsWith(".yaml") || id.endsWith(".yml")) {
            id = id.substring(0, id.lastIndexOf('.'));
        }
        final int counterInt = Integer.parseInt(counter);
        final int hashCodeInt = Integer.parseUnsignedInt(hashCode, 16);
        return new YamlFile(path, counterInt, hashCodeInt, id);
    }

    static String getFileName(@NotNull final RootEntityPropertiesBuilder<?> entityProperties, final String content) {
        return generateFileName(entityProperties.humanReadableId(), content, COUNTER);
    }

    public static String generateFileName(@NotNull final RootEntityPropertiesBuilder<?> entityProperties, final String content, final AtomicLong fileCounter) {
        return generateFileName(entityProperties.humanReadableId(), content, fileCounter);
    }

    public static String generateFileName(@NotNull final String humanReadableId, final String content, final AtomicLong fileCounter) {
        return generateFileName(humanReadableId, content, fileCounter, YamlFile::getCanonicalContentHash);
    }

    public static String generateFileName(@NotNull final String humanReadableId, final String content, final AtomicLong fileCounter, final Function<String, Integer> hashFunction) {
        return String.format(YAML_FILE_PATTERN, fileCounter.getAndIncrement(), hashFunction.apply(content), humanReadableId.replaceAll("\\W", "-"));
    }

    private static int getCanonicalContentHash(final String yaml) {
        return toCanonicalContent(yaml).hashCode();
    }

    @VisibleForTesting
    static String toCanonicalContent(final String yaml) {
        final StringBuilder sb = new StringBuilder();
        yaml.lines()
                .map(line -> NON_CANONICAL_LINE_PATTERN.matcher(line).find() ? "" : line)
                .forEach(line -> sb.append(line).append('\n'));
        return sb.toString();
    }
}
