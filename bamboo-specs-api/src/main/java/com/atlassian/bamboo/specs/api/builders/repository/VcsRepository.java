package com.atlassian.bamboo.specs.api.builders.repository;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.viewer.VcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Represents a definition of VCS repository.
 * <p>
 * This class contains common data only. In order to define a specific type of repository one should use the specialised
 * implementation or, if such is not available, {@link AnyVcsRepository} class.
 */
public abstract class VcsRepository<T extends VcsRepository<T, E>, E extends VcsRepositoryProperties> extends RootEntityPropertiesBuilder<E> {

    public static final String TYPE = "repository";

    protected String parent;

    protected String name;
    protected BambooOidProperties oid;
    protected String description;
    protected ProjectProperties project;

    protected VcsRepositoryViewerProperties repositoryViewer;

    /**
     * Specifies a new repository definition.
     * <p>
     * A repository can be global, in which case any changes made to a repository are automatically applied to all plans or jobs that use that repository.
     * Also repository can be project, in which case it can be used only in plans from corresponding project.
     * Repository can also be defined in scope of a {@link Plan}, in which case it is only accessible in context of plan it is defined for.
     */
    public VcsRepository() throws PropertiesValidationException {
    }

    /**
     * Sets the name by which repository will be identified by Bamboo.
     * In the absence of oid, the name is used to identify the repository.
     * If the repository with given name does not exist, a new one is created, otherwise existing one is updated.
     * <p>
     * The name should be unique in scope a repository is defined in.
     * <p>
     * The name must not be set if repository <i>inherits</i> data from another repository.
     *
     * @see #parent(String)
     */
    public T name(@Nullable String name) throws PropertiesValidationException {
        this.name = name;
        return (T) this;
    }

    /**
     * Sets the repository's oid from string.
     * <p>
     * If set, it is used to identify the repository.
     * If the repository with given oid does not exist, a new one is created, otherwise existing one is updated.
     * Setting both oid and name may result in existing repository being renamed.
     */
    public T oid(@Nullable String oid) throws PropertiesValidationException {
        return oid(oid != null ? new BambooOid(oid) : null);
    }

    /**
     * Sets the repository's oid.
     * <p>
     * If set, it is used to identify the repository.
     * If the repository with given oid does not exist, a new one is created, otherwise existing one is updated.
     * Setting both oid and name may result in existing repository being renamed.
     */
    public T oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        this.oid = oid != null ? EntityPropertiesBuilders.build(oid) : null;
        return (T) this;
    }

    /**
     * Sets the parent of this repository should <i>inherit</i> data from.
     *
     * When defining repository for a plan user can decide to inherit its data from an existing global repository instead of
     * creating it from scratch. In that case, any change to parent's repository configuration will automatically be applied
     * to all its children, unless a respective data has been overridden.
     */
    public T parent(@Nullable String parent) throws PropertiesValidationException {
        this.parent = parent;
        return (T) this;
    }

    /**
     * Sets repository description.
     * <p>
     * The description must not be set if repository <i>inherits</i> from another repository.
     */
    public T description(@Nullable String description) throws PropertiesValidationException {
        this.description = description;
        return (T) this;
    }

    /**
     * Sets a web repository viewer to be used with this repository.
     * <p>
     * Web repository viewer allows Bamboo to render correct links to commits, files, etc. in the Bamboo UI.
     *
     * @see VcsRepositoryViewer
     */
    public T repositoryViewer(@Nullable VcsRepositoryViewer repositoryViewer) throws PropertiesValidationException {
        this.repositoryViewer = repositoryViewer != null ? EntityPropertiesBuilders.build(repositoryViewer) : null;
        return (T) this;
    }

    /**
     * Set the repository's project. Available for Bamboo DC only.
     * <p>
     * If set, it is Project repository, otherwise it is Global or plan repository.
     * @since 8.0
     */
    public T project(@Nullable Project project) {
        checkNotNull("project", project);
        this.project = EntityPropertiesBuilders.build(project);
        return (T) this;
    }

    /**
     * Returns repository name, which serves as identifier for this object.
     *
     * @throws IllegalStateException if name is undefined
     */
    @NotNull
    public String getName() {
        if (StringUtils.isBlank(name)) {
            throw new IllegalStateException("Repository name is undefined");
        }
        return name;
    }

    /**
     * Returns repository's oid.
     *
     * @throws IllegalStateException if oid is undefined
     */
    @NotNull
    public BambooOid getOid() {
        if (oid == null) {
            throw new IllegalStateException("Repository oid is undefined");
        }
        return new BambooOid(oid.getOid());
    }

    /**
     * Returns identifier for this object.
     *
     * @throws IllegalStateException if key is undefined
     */
    @NotNull
    public VcsRepositoryIdentifier getIdentifier() {
        if (oid != null) {
            VcsRepositoryIdentifier identifier = new VcsRepositoryIdentifier(new BambooOid(oid.getOid()));
            if (name != null) {
                identifier.name(name);
            }
            return identifier;
        } else if (StringUtils.isNotBlank(name)) {
            VcsRepositoryIdentifier identifier = new VcsRepositoryIdentifier(name);
            return identifier;
        }
        throw new IllegalStateException("Repository identifier is undefined");
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        return String.format("%s %s", TYPE, defaultString(name, "<unknown>"));
    }

    protected abstract E build() throws PropertiesValidationException;
}
