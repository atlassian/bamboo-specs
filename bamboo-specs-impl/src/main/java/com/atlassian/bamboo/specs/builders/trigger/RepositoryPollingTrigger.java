package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.api.util.CronExpressionCreationHelper;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.model.trigger.RepositoryPollingTriggerProperties;
import org.jetbrains.annotations.NotNull;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Collection;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkArgument;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkPositive;
import static com.atlassian.bamboo.specs.model.trigger.RepositoryPollingTriggerProperties.PollType.CRON;
import static com.atlassian.bamboo.specs.model.trigger.RepositoryPollingTriggerProperties.PollType.PERIOD;

/**
 * Represents repository polling trigger.
 */
public class RepositoryPollingTrigger extends RepositoryBasedTrigger<RepositoryPollingTrigger, RepositoryPollingTriggerProperties> {
    private static final EnumSet<TimeUnit> APPLICABLE_TIME_UNITS = EnumSet.of(TimeUnit.SECONDS,
            TimeUnit.MINUTES,
            TimeUnit.HOURS,
            TimeUnit.DAYS);

    private RepositoryPollingTriggerProperties.PollType pollType;
    private Duration pollingPeriod;
    private String cronExpression;

    /**
     * Creates repository polling trigger.
     */
    public RepositoryPollingTrigger() {
        triggeringRepositoriesType = TriggeringRepositoriesType.ALL;
        pollingPeriod = Duration.ofSeconds(180);
        cronExpression = null;
        pollType = RepositoryPollingTriggerProperties.PollType.PERIOD;
    }

    /**
     * Specifies how often (in {@link TimeUnit}) Bamboo should check the repository for changes.
     * Time units smaller than {@link TimeUnit#SECONDS} won't be accepted.
     * Default value is 180 seconds.
     *
     * @see #withPollingPeriod(Duration)
     */
    public RepositoryPollingTrigger pollEvery(int every, @NotNull TimeUnit timeUnit) {
        checkNotNull("timeUnit", timeUnit);
        checkPositive("pollingPeriod", every);
        checkArgument(ValidationContext.empty(), APPLICABLE_TIME_UNITS.contains(timeUnit),
                "Polling is available only with seconds, minutes and hours based period");

        pollingPeriod = Duration.ofSeconds(timeUnit.toSeconds(every));

        pollType = PERIOD;
        cronExpression = null;
        return this;
    }

    /**
     * Specifies time interval between checks for changes in the repositories.
     * Duration smaller than a second won't be accepted.
     * Default value is 180 seconds.
     *
     * @see #pollEvery(int, TimeUnit)
     */
    public RepositoryPollingTrigger withPollingPeriod(@NotNull Duration duration) {
        checkNotNull("duration", duration);
        checkArgument(ValidationContext.empty(), duration.getSeconds() > 0, "Polling interval cannot be shorted than one second");

        pollingPeriod = duration;
        pollType = PERIOD;
        cronExpression = null;
        return this;
    }

    /**
     * Selects polling type for this trigger. Possible values:
     * <dl>
     * <dt>PERIOD</dt>
     * <dd>Poll in defined intervals.</dd>
     * <dt>CRON</dt>
     * <dd>Poll according to cron expression.</dd>
     * </dl>
     */
    public RepositoryPollingTrigger withPollType(@NotNull RepositoryPollingTriggerProperties.PollType pollType) {
        checkNotNull("pollType", pollType);
        this.pollType = pollType;
        return this;
    }

    /**
     * Orders Bamboo to check repository for changes once daily at specified time.
     */
    public RepositoryPollingTrigger pollOnceDaily(@NotNull LocalTime at) {
        return pollWithCronExpression(CronExpressionCreationHelper.scheduleOnceDaily(at));
    }

    /**
     * Orders Bamboo to check repository for changes weekly at specified days of week and time.
     */
    public RepositoryPollingTrigger pollWeekly(@NotNull LocalTime at, DayOfWeek... onDays) {
        return pollWithCronExpression(CronExpressionCreationHelper.scheduleWeekly(at, onDays));
    }

    /**
     * Orders Bamboo to check repository for changes weekly at specified days of week and time.
     */
    public RepositoryPollingTrigger pollWeekly(@NotNull LocalTime at, @NotNull Collection<DayOfWeek> days) {
        return pollWithCronExpression(CronExpressionCreationHelper.scheduleWeekly(at, days));
    }

    /**
     * Orders Bamboo to check repository for changes once monthly at specified day of month and time.
     */
    public RepositoryPollingTrigger pollMonthly(@NotNull LocalTime at, int dayOfMonth) {
        return pollWithCronExpression(CronExpressionCreationHelper.scheduleMonthly(at, dayOfMonth));
    }

    /**
     * Orders Bamboo to check repository for changes based on given cron expression.
     */
    public RepositoryPollingTrigger pollWithCronExpression(@NotNull String cronExpression) {
        checkNotBlank("cronExpression", cronExpression);
        this.cronExpression = cronExpression;
        pollType = CRON;
        this.pollingPeriod = null;
        return this;
    }

    @Override
    protected RepositoryPollingTriggerProperties build() {
        switch (pollType) {
            case PERIOD:
                return new RepositoryPollingTriggerProperties(name, description, triggerEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories, pollingPeriod);
            case CRON:
                return new RepositoryPollingTriggerProperties(name, description, triggerEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories, cronExpression);
            default:
                throw new AssertionError(String.format("Don't know what %s is.", pollType));
        }
    }
}
