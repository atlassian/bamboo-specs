package com.atlassian.bamboo.specs.model.repository.git;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.containsBambooVariable;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsRelaxedXssRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;


@Immutable
@ConstructFrom("username")
public final class UserPasswordAuthenticationProperties implements AuthenticationProperties {
    private final String username;
    private final String password;

    private UserPasswordAuthenticationProperties() {
        username = null;
        password = null;
    }

    public UserPasswordAuthenticationProperties(String username, @Nullable String password) {
        this.username = username;
        this.password = password;

        validate();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void validate() {
        checkNotNull("username", username);

        final ValidationContext context = ValidationContext.of("User-password authentication");
        final List<ValidationProblem> errors = new ArrayList<>();

        if (!containsBambooVariable(username)) {
            validateNotContainsRelaxedXssRelatedCharacters(context.with("Username"), username)
                    .ifPresent(errors::add);

            validateNotContainsShellInjectionRelatedCharacters(context.with("Username"), username)
                    .ifPresent(errors::add);
        }

        checkNoErrors(errors);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserPasswordAuthenticationProperties that = (UserPasswordAuthenticationProperties) o;
        return Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getPassword());
    }
}
