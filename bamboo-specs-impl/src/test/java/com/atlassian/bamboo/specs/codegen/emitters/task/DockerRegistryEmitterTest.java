package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsScope;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class DockerRegistryEmitterTest {

    private DockerRegistryEmitter.AuthEmitter emitter;

    @Before
    public void setUp() {
        emitter = new DockerRegistryEmitter.AuthEmitter();
    }

    @Test
    public void emitUsernamePasswordAuthenticationCode() throws CodeGenerationException {

        DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPullImageTask().dockerHubImage("image").authentication("username\"WithQuotes\"", "password\"WithQuotes\"", "email\"WithQuotes\""));

        String result = emitter.emitCode(new CodeGenerationContext(), properties);

        assertFalse("Emitted code doesn't contain unescaped username", result.contains("username\"WithQuotes\""));
        assertFalse("Emitted code doesn't contain unescaped email", result.contains("email\"WithQuotes\""));

        assertTrue("Emitted code contains escaped username", result.contains("username\\\"WithQuotes\\\""));
        assertTrue("Emitted code contains escaped email", result.contains("email\\\"WithQuotes\\\""));

        assertFalse("Emitted code doesn't contain unescaped password", result.contains("password\"WithQuotes\""));
        assertTrue("Emitted code contains escaped password", result.contains("password\\\"WithQuotes\\\""));
    }

    @Test
    public void emitSharedCredentialsAuthenticationCode() throws CodeGenerationException {

        final String sharedCredentialsName = "mysharedcredentials";
        DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPullImageTask()
                        .dockerHubImage("image")
                        .authentication(new SharedCredentialsIdentifier(sharedCredentialsName)));

        String result = emitter.emitCode(new CodeGenerationContext(), properties);

        assertTrue("Emitted code should contain shared credentials name", result.contains(sharedCredentialsName));
    }

    @Test
    public void emitGlobalSharedCredentialsAuthenticationCode() throws CodeGenerationException {

        final String sharedCredentialsName = "global-shared-credentials";
        final SharedCredentialsIdentifier sharedCredentialsIdentifier = new SharedCredentialsIdentifier(sharedCredentialsName);
        sharedCredentialsIdentifier.scope(SharedCredentialsScope.GLOBAL);
        final DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPullImageTask()
                        .dockerHubImage("image")
                        .authentication(sharedCredentialsIdentifier));

        String expectedCode = ".authentication(new SharedCredentialsIdentifier(\"global-shared-credentials\").scope(SharedCredentialsScope.GLOBAL))";
        String result = emitter.emitCode(new CodeGenerationContext(), properties);

        assertThat(result, equalTo(expectedCode));
    }

    @Test
    public void emitProjectSharedCredentialsAuthenticationCode() throws CodeGenerationException {

        final String sharedCredentialsName = "project-shared-credentials";
        final SharedCredentialsIdentifier sharedCredentialsIdentifier = new SharedCredentialsIdentifier(sharedCredentialsName);
        sharedCredentialsIdentifier.scope(SharedCredentialsScope.PROJECT);
        final DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPushImageTask()
                        .dockerHubImage("image")
                        .authentication(sharedCredentialsIdentifier));

        String expectedCode = ".authentication(new SharedCredentialsIdentifier(\"project-shared-credentials\").scope(SharedCredentialsScope.PROJECT))";
        String result = emitter.emitCode(new CodeGenerationContext(), properties);

        assertThat(result, equalTo(expectedCode));
    }

    private static final String CODE = "new DockerPushImageTask()\n" +
            "    .description(\"descr\")\n" +
            "    .enabled(false)\n" +
            "    .dockerHubImage(\"image\")\n" +
            "    .authentication(new SharedCredentialsIdentifier(\"project-shared-credentials\").scope(SharedCredentialsScope.PROJECT))";


    @Test
    public void testEmitCode() throws CodeGenerationException {
        final DockerRegistryEmitter emitter = new DockerRegistryEmitter();
        final SharedCredentialsIdentifier sharedCredentialsIdentifier = new SharedCredentialsIdentifier("project-shared-credentials");
        sharedCredentialsIdentifier.scope(SharedCredentialsScope.PROJECT);
        final DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPushImageTask()
                        .description("descr")
                        .enabled(false)
                        .dockerHubImage("image")
                        .authentication(sharedCredentialsIdentifier));
        Assert.assertEquals(CODE, emitter.emitCode(new CodeGenerationContext(), properties));
    }
}