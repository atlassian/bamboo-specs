package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.GruntTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

/**
 * Represents Bamboo task which executes Grunt tasks.
 *
 * @see <a href="https://gruntjs.com/">gruntjs.com</a>
 */
public class GruntTask extends BaseNodeTask<GruntTask, GruntTaskProperties> {
    public static final String DEFAULT_GRUNT_CLI_EXECUTABLE = preventInlining("node_modules/grunt-cli/bin/grunt");

    @NotNull
    private String gruntCliExecutable = DEFAULT_GRUNT_CLI_EXECUTABLE;
    @Nullable
    private String task;
    @Nullable
    private String gruntfile;

    /**
     * Specify path to the Grunt command line interface (grunt-cli) executable for this task. Path must be relative to
     * the working directory.
     * <p>
     * Example: {@code node_modules/grunt-cli/bin/grunt}
     */
    public GruntTask gruntCliExecutable(@NotNull String gruntCliExecutable) {
        ImporterUtils.checkNotNull("gruntCliExecutable", gruntCliExecutable);
        this.gruntCliExecutable = gruntCliExecutable;
        return this;
    }

    /**
     * Grunt task to execute. If not specified, the 'default' task will be executed. Multiple tasks can be specified
     * separated by a space.
     */
    public GruntTask task(@Nullable String task) {
        this.task = task;
        return this;
    }

    /**
     * Specify path to the gruntfile, relative to the build working directory. If empty, the default gruntfile will be
     * used.
     */
    public GruntTask gruntfile(@Nullable String gruntfile) {
        this.gruntfile = gruntfile;
        return this;
    }

    @NotNull
    @Override
    protected GruntTaskProperties build() {
        return new GruntTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                gruntCliExecutable,
                task,
                gruntfile,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GruntTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GruntTask gruntTask = (GruntTask) o;
        return gruntCliExecutable.equals(gruntTask.gruntCliExecutable) &&
                Objects.equals(task, gruntTask.task) &&
                Objects.equals(gruntfile, gruntTask.gruntfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gruntCliExecutable, task, gruntfile);
    }
}
