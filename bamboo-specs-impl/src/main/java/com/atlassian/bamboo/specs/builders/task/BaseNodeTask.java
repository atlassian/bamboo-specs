package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.BaseNodeTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Base class for Node.js related tasks.
 *
 * @param <B> type of task
 * @param <P> type of properties generated by the task
 */
public abstract class BaseNodeTask<B extends BaseNodeTask<B,P>, P extends BaseNodeTaskProperties> extends Task<B, P> {
    @NotNull
    protected String nodeExecutable;
    @Nullable
    protected String environmentVariables;
    @Nullable
    protected String workingSubdirectory;

    /**
     * Sets which Node.js executable to use.
     */
    public B nodeExecutable(@NotNull String nodeExecutable) {
        checkNotNull("nodeExecutable", nodeExecutable);
        this.nodeExecutable = nodeExecutable;
        return (B) this;
    }

    /**
     * Sets environment variables for this task.
     */
    public B environmentVariables(@Nullable String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return (B) this;
    }

    /**
     * Sets the working subdirectory for this task.
     */
    public B workingSubdirectory(@Nullable String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return (B) this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseNodeTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BaseNodeTask<?, ?> that = (BaseNodeTask<?, ?>) o;
        return nodeExecutable.equals(that.nodeExecutable) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nodeExecutable, environmentVariables, workingSubdirectory);
    }
}
