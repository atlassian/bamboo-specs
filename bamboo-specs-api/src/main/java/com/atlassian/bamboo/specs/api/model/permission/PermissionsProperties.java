package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.AnonymousUserPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.GroupPermission;
import com.atlassian.bamboo.specs.api.builders.permission.LoggedInUserPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.UserPermission;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.permission.PermissionPropertiesEmitter")
@Immutable
public class PermissionsProperties implements EntityProperties {

    private Map<String, UserPermissionProperties> userPermissions;
    private Map<String, GroupPermissionProperties> groupPermissions;
    private LoggedInUserPermissionsProperties loggedInUserPermissions;
    private AnonymousUserPermissionsProperties anonymousUserPermissions;

    private PermissionsProperties() {
        this.userPermissions = null;
        this.groupPermissions = null;
        this.loggedInUserPermissions = null;
        this.loggedInUserPermissions = null;
        this.anonymousUserPermissions = null;
    }

    public PermissionsProperties(@NotNull final Collection<UserPermission> userPermissions,
                                 @NotNull final Collection<GroupPermission> groupPermissions,
                                 @NotNull final LoggedInUserPermissions loggedInUserPermissions,
                                 @NotNull final AnonymousUserPermissions anonymousUserPermissions) throws PropertiesValidationException {
        BinaryOperator<UserPermissionProperties> userPermissionsMerger = (o, n) -> {
            UserPermission merged = new UserPermission(o.getUsername());
            o.getPermissionTypes().forEach(merged::permissions);
            n.getPermissionTypes().forEach(merged::permissions);
            return EntityPropertiesBuilders.build(merged);
        };

        BinaryOperator<GroupPermissionProperties> groupPermissionsMerger = (o, n) -> {
            GroupPermission merged = new GroupPermission(o.getGroup());
            o.getPermissionTypes().forEach(merged::permissions);
            n.getPermissionTypes().forEach(merged::permissions);
            return EntityPropertiesBuilders.build(merged);
        };

        this.userPermissions = userPermissions.stream().map(EntityPropertiesBuilders::build)
                .collect(Collectors.toMap(UserPermissionProperties::getUsername, u -> u, userPermissionsMerger, TreeMap::new));
        this.groupPermissions = groupPermissions.stream().map(EntityPropertiesBuilders::build)
                .collect(Collectors.toMap(GroupPermissionProperties::getGroup, g -> g, groupPermissionsMerger, TreeMap::new));
        this.loggedInUserPermissions = EntityPropertiesBuilders.build(loggedInUserPermissions);
        this.anonymousUserPermissions = EntityPropertiesBuilders.build(anonymousUserPermissions);
        validate();
    }

    public List<UserPermissionProperties> getUserPermissions() {
        return new ArrayList<>(userPermissions.values());
    }

    public List<GroupPermissionProperties> getGroupPermissions() {
        return new ArrayList<>(groupPermissions.values());
    }

    public LoggedInUserPermissionsProperties getLoggedInUserPermissions() {
        return loggedInUserPermissions;
    }

    public AnonymousUserPermissionsProperties getAnonymousUserPermissions() {
        return anonymousUserPermissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionsProperties that = (PermissionsProperties) o;
        return Objects.equals(userPermissions, that.userPermissions) &&
                Objects.equals(groupPermissions, that.groupPermissions) &&
                Objects.equals(loggedInUserPermissions, that.loggedInUserPermissions) &&
                Objects.equals(anonymousUserPermissions, that.anonymousUserPermissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userPermissions, groupPermissions, loggedInUserPermissions, anonymousUserPermissions);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PermissionProperties{");
        sb.append("userPermissions=").append(userPermissions);
        sb.append(", groupPermissions=").append(groupPermissions);
        sb.append(", loggedInUserPermissions=").append(loggedInUserPermissions);
        sb.append(", anonymousUserPermissions=").append(anonymousUserPermissions);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void validate() throws PropertiesValidationException {
    }
}
