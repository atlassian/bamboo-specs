package com.atlassian.bamboo.specs.api.validators.plan;

import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanProperties;
import com.atlassian.bamboo.specs.api.model.plan.StageProperties;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.VariableValidator.validateUniqueVariableNames;

public final class PlanValidator {
    private PlanValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final PlanProperties planProperties) {
        final List<ValidationProblem> errors = new ArrayList<>();

        errors.addAll(validateUniqueStageNames(planProperties.getStages()));
        errors.addAll(validateUniqueJobKeysAndNames(planProperties.getStages()));
        errors.addAll(validateUniqueVariableNames(planProperties.getVariables()));
        errors.addAll(validateArtifactsAndSubscriptions(planProperties.getStages()));
        errors.addAll(validateUniqueRepositoriesNames(planProperties.getRepositories()));

        return errors;
    }

    static Collection<? extends ValidationProblem> validateUniqueRepositoriesNames(final List<PlanRepositoryLinkProperties> repositories) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        final Set<String> repositoriesNames = new HashSet<>();

        for (PlanRepositoryLinkProperties repository : repositories) {
            final String name = repository.getRepositoryDefinition().hasParent() ? repository.getRepositoryDefinition().getParentName() :
                                repository.getRepositoryDefinition().getName();
            if (repositoriesNames.contains(name)) {
                errors.add(new ValidationProblem("Duplicated repository name within a plan: " + name));
            } else {
                repositoriesNames.add(name);
            }
        }

        return errors;
    }

    private static Collection<? extends ValidationProblem> validateUniqueJobKeysAndNames(final List<StageProperties> stages) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        final Set<String> jobNames = new HashSet<>();
        final Set<BambooKeyProperties> jobKeys = new HashSet<>();

        for (StageProperties stage : stages) {
            for (JobProperties job : stage.getJobs()) {
                if (jobNames.contains(job.getName())) {
                    errors.add(new ValidationProblem("Duplicate job name " + job.getName()));
                }
                if (job.getKey() != null && jobKeys.contains(job.getKey())) {
                    errors.add(new ValidationProblem("Duplicate job key " + job.getKey().getKey()));
                }
                jobNames.add(job.getName());
                jobKeys.add(job.getKey());
            }
        }
        return errors;
    }

    private static Collection<? extends ValidationProblem> validateUniqueStageNames(final List<StageProperties> stages) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        final Set<String> stageNames = new HashSet<>();

        for (StageProperties stage : stages) {
            if (stageNames.contains(stage.getName())) {
                errors.add(new ValidationProblem("Duplicate stage name " + stage.getName()));
            }
            stageNames.add(stage.getName());
        }
        return errors;
    }

    @NotNull
    private static List<ValidationProblem> validateArtifactsAndSubscriptions(@NotNull List<StageProperties> stages) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        final Set<String> allArtifactNames = new HashSet<>();
        final Set<String> allSharedArtifactNames = new HashSet<>();

        for (StageProperties stage : stages) {
            stage.getJobs().forEach(job -> job.getArtifactSubscriptions().forEach(subscription -> {
                if (StringUtils.isBlank(subscription.getArtifactName())) {
                    errors.add(new ValidationProblem(String.format("Artifact name undefined in job %s", job.getName())));
                } else if (!allArtifactNames.contains(subscription.getArtifactName())) {
                    errors.add(new ValidationProblem(String.format("Artifact %s subscribed in job %s not defined", subscription.getArtifactName(), job.getName())));
                } else if (!allSharedArtifactNames.contains(subscription.getArtifactName())) {
                    errors.add(new ValidationProblem(String.format("Artifact %s subscribed in job %s is not a shared artifact", subscription.getArtifactName(), job.getName())));
                }
            }));

            for (JobProperties job : stage.getJobs()) {
                Set<String> jobArtifactNames = new HashSet<>();
                job.getArtifacts()
                        .forEach(artifact -> {
                            if (artifact.isShared()) {
                                if (allSharedArtifactNames.contains(artifact.getName())) {
                                    errors.add(new ValidationProblem(String.format("Duplicate shared artifact name %s", artifact.getName())));
                                }
                                allSharedArtifactNames.add(artifact.getName());
                            }
                            if (jobArtifactNames.contains(artifact.getName())) {
                                errors.add(new ValidationProblem(String.format("Duplicate artifact name %s in job %s", artifact.getName(), job.getName())));
                            }
                            jobArtifactNames.add(artifact.getName());
                            allArtifactNames.add(artifact.getName());
                        });
            }
        }
        return errors;
    }
}
