package com.atlassian.bamboo.specs.codegen.emitters.value;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.codegen.CompoundFieldSetterEmitter;
import com.atlassian.bamboo.specs.api.codegen.Condition;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.codegen.annotations.DefaultFieldValues;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGenIf;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.codegen.BuilderClassProvider;
import com.atlassian.bamboo.specs.codegen.emitters.CodeGenerationUtils;
import com.atlassian.bamboo.specs.codegen.emitters.fragment.FieldSetterEmitterFactory;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Default code generator for any {@link EntityProperties}
 * This class can be extended to implement more specialised generators.
 * Note that the implementation is stateful.
 */
public class EntityPropertiesEmitter<T extends EntityProperties> implements CodeEmitter<T> {
    /**
     * Builder class associated with given properties type.
     */
    protected Class<?> builderClass;

    /**
     * Fields that should be skipped in {@link #emitFields(CodeGenerationContext, EntityProperties)}.
     * Extending classes can use this field to be able to add specialised handling for some fields, while still
     * being able to delegate handling for the rest of the class to default implementation.
     */
    protected final Set<String> fieldsToSkip = new HashSet<>();

    @Nullable
    protected Field findField(Class<?> clazz, String fieldName) {
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if (fieldName.equals(fields[i].getName())) {
                return fields[i];
            }
        }
        Class<?> superclass = clazz.getSuperclass();
        if (EntityProperties.class.isAssignableFrom(superclass)) {
            return findField(superclass, fieldName);
        }
        return null;
    }

    protected boolean hasPublicConstructor(final Class<?> builderClass, Class<?>... argTypes) {
        try {
            builderClass.getDeclaredConstructor(argTypes);
            return true;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    /**
     * Generates constructor invocation of the builder class.
     */
    protected String emitConstructorInvocation(@NotNull CodeGenerationContext context, @NotNull T entity) throws CodeGenerationException {
        initBuilderClass(entity);
        StringBuilder builder = new StringBuilder();
        if (entity.getClass().isAnnotationPresent(ConstructFrom.class)) {
            ConstructFrom annotation = entity.getClass().getAnnotation(ConstructFrom.class);
            String[] fieldNames = annotation.value();

            builder.append("new " + context.importClassName(builderClass) + "(");
            context.incIndentation();
            List<String> valuesAndFails = new ArrayList<>();
            Set<Integer> failed = new HashSet<>();
            for (int i = 0; i < fieldNames.length; i++) {
                Field field = findField(entity.getClass(), fieldNames[i]);
                if (field == null) {
                    throw new IllegalStateException("Field " + fieldNames[i] + " not found in class " + entity.getClass().getCanonicalName());
                }
                field.setAccessible(true);
                try {
                    valuesAndFails.add(emitFieldValue(context, entity, field));
                } catch (CodeGenerationException e) {
                    valuesAndFails.add(e.getMessage());
                    failed.add(i);
                }
                fieldsToSkip.add(fieldNames[i]);
            }
            CodeGenerationUtils.appendCommaSeparatedList(context,
                    builder,
                    valuesAndFails,
                    failed);
            context.decIndentation();
            builder.append(")");
        } else if (hasPublicConstructor(builderClass)) {
            builder.append("new " + context.importClassName(builderClass) + "()");
        } else if (hasPublicConstructor(builderClass, AtlassianModule.class)) {
            try {
                builder.append("new " + context.importClassName(builderClass) + "(");
                builder.append("new " + context.importClassName(AtlassianModule.class) + "(\"");
                builder.append(getAtlassianPluginKey(entity));
                builder.append("\"))");
            } catch (Exception e) {
                throw new CodeGenerationException("Code generation failed. Could not generate constructor invocation for " + builderClass.getCanonicalName());
            }
            fieldsToSkip.add("atlassianPlugin");
        } else {
            throw new CodeGenerationException("Code generation failed. Could not find suitable constructor for " + builderClass.getCanonicalName());
        }
        return builder.toString();
    }

    private String getAtlassianPluginKey(final EntityProperties entity) throws Exception {
        Method atlassianModuleGetter = entity.getClass().getMethod("getAtlassianPlugin");
        return ((AtlassianModuleProperties) atlassianModuleGetter.invoke(entity)).getCompleteModuleKey();
    }


    /**
     * Generates value of a field. Used to generate constructor invocation.
     */
    protected String emitFieldValue(final CodeGenerationContext context, final EntityProperties entity, final Field field) throws CodeGenerationException {
        try {
            final Object fieldValue = field.get(entity);
            CodeEmitter codeEmitter = ValueEmitterFactory.emitterFor(fieldValue);
            return codeEmitter.emitCode(context, fieldValue);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Could not get value of field " + field.getName() + " from instance of " + entity.getClass().getCanonicalName());
        }
    }

    /**
     * Creates instance of the {@link EntityProperties} that contains default values. The values are then compared
     * to the actual field values of the instance the code is generated for. If the values match it is assumed that
     * invocation of the respective setter is not necessary. The object is generated by trying the following:
     * <ul>
     * <li>static method annotated with {@link DefaultFieldValues}</li>
     * <li>a parameterless constructor</li>
     * </ul>
     * If none of the above is present null value is returned and code is generated for all non-empty fields.
     */
    protected EntityProperties createDefaultObject(EntityProperties rootObject) {
        for (Method method : rootObject.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(DefaultFieldValues.class)) {
                method.setAccessible(true);
                try {
                    return (EntityProperties) method.invoke(rootObject);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    return null;
                }
            }
        }
        try {
            Constructor<?> constructor = rootObject.getClass().getDeclaredConstructor();
            constructor.setAccessible(true);
            return (EntityProperties) constructor.newInstance();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }


    private Object getFieldValue(final EntityProperties entityObject, final Field field) {
        try {
            return field.get(entityObject);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Could not get value of field " + field.getName() + " from instance of " + entityObject.getClass().getCanonicalName());
        }
    }

    private String invokeCodeEmitterForField(CodeEmitter codeEmitter, CodeGenerationContext context, EntityProperties entityObject, Object fieldValue) throws CodeGenerationException {
        if (codeEmitter instanceof CompoundFieldSetterEmitter) {
            return ((CompoundFieldSetterEmitter)codeEmitter).emitCode(context, entityObject);
        }
        return codeEmitter.emitCode(context, fieldValue);
    }

    private String emitFields(CodeGenerationContext context,
                              EntityProperties entity,
                              Class<?> entityClass,
                              Class<?> builderClass,
                              final EntityProperties entityObject,
                              final Set<String> fieldsAlreadyCovered,
                              @Nullable EntityProperties defaultObject) throws CodeGenerationException {
        StringBuilder builder = new StringBuilder();
        Class<?> superclass = entityClass.getSuperclass();
        if (EntityProperties.class.isAssignableFrom(superclass)) {
            //append fields from parent class
            builder.append(emitFields(context, entity, superclass, builderClass, entityObject, fieldsAlreadyCovered, defaultObject));
        }
        for (Field field : entityClass.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(SkipCodeGen.class)) {
                continue;
            }
            if (field.isAnnotationPresent(SkipCodeGenIf.class)) {
                final Class<? extends Condition> conditionClass = ((SkipCodeGenIf) field.getAnnotation(SkipCodeGenIf.class)).value();
                try {
                    final Condition condition = conditionClass.newInstance();
                    if (condition.evaluate(entity)) {
                        continue;
                    }
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new CodeGenerationException("Could not instantiate condition class " + conditionClass.getCanonicalName(), e);
                }
            }

            if (fieldsAlreadyCovered.contains(field.getName()) || Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            final Object fieldValue = getFieldValue(entityObject, field);
            if (CodeGenerationUtils.isEmptyField(fieldValue)) {
                continue;
            }

            if (defaultObject != null && Objects.equals(fieldValue, getFieldValue(defaultObject, field))) {
                continue;
            }


            try {
                final CodeEmitter<Object> codeEmitter = FieldSetterEmitterFactory.fieldSetterEmitterFor(context,
                                                                                                        builderClass,
                                                                                                        field,
                                                                                                        fieldValue);
                final String fieldCode = invokeCodeEmitterForField(codeEmitter, context, entityObject, fieldValue);
                if (StringUtils.isNotBlank(fieldCode)) {
                    builder.append(context.newLine());
                    builder.append(fieldCode);
                }
            } catch (CodeGenerationException e) {
                builder.append(context.newLine());
                builder.append("//todo: Could not generate code to set field " + field.getName() + ": " + e.getMessage());
                builder.append(context.newLine());
            }
        }
        return builder.toString();
    }

    /**
     * Generate code for all fields of the class that are not already covered by {@link #emitConstructorInvocation(CodeGenerationContext, EntityProperties)}
     * not contained in {@link #fieldsToSkip} and not annotated with {@link SkipCodeGen}. This involves finding a matching setter method,
     * determining the appropriate code generator and generating method invocation and its arguments.
     */
    protected String emitFields(@NotNull CodeGenerationContext context, @NotNull EntityProperties entity) throws CodeGenerationException {
        context.incIndentation();
        String code = emitFields(context, entity, entity.getClass(), builderClass, entity, fieldsToSkip, createDefaultObject(entity));
        context.decIndentation();
        return code;
    }

    /**
     * Default method of finding {@link #builderClass}. Extending class can set the field explicitly to skip the default search algorithm.
     */
    protected void initBuilderClass(@NotNull T entity) throws CodeGenerationException {
        if (builderClass == null) {
            builderClass = BuilderClassProvider.findBuilderClass(entity.getClass());
        }
        if (builderClass == null) {
            throw new IllegalStateException("Builder class not set.");
        }
    }

    /**
     * Generates code for properties class. This includes generating invocation of the constructor of the appropriate builder followed
     * by invocations of its setters.
     */
    @NotNull
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull T entity) throws CodeGenerationException {
        initBuilderClass(entity);
        return emitConstructorInvocation(context, entity) + emitFields(context, entity);
    }
}
