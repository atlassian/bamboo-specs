package com.atlassian.bamboo.specs.api.builders.credentials;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.credentials.AnySharedCredentialsProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a shared credentials data of any type.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation of a given credential type is not available.
 */
public class AnySharedCredentials extends SharedCredentials<AnySharedCredentials, AnySharedCredentialsProperties> {
    private final Map<String, Object> configuration = new LinkedHashMap<>();
    protected final AtlassianModuleProperties atlassianPlugin;

    /**
     * Create a shared credential of given name and type.
     *
     * @param name            name of the shared credential
     * @param atlassianPlugin type of the credential identified by its plugin module key
     * @see SharedCredentials#name(String)
     * @see AtlassianModule
     */
    public AnySharedCredentials(@NotNull final String name, @NotNull final AtlassianModule atlassianPlugin) throws PropertiesValidationException {
        this(name, EntityPropertiesBuilders.build(checkNotNull("atlassianPlugin", atlassianPlugin)));
    }

    private AnySharedCredentials(final String name, final AtlassianModuleProperties atlassianPlugin) {
        super(name);
        this.atlassianPlugin = checkNotNull("atlassianPlugin", atlassianPlugin);
    }

    /**
     * Set configuration for the credentials.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is performed
     * on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnySharedCredentials configuration(final Map<String, Object> configuration) {
        this.configuration.putAll(configuration);
        return this;
    }

    @Override
    public AnySharedCredentials oid(@Nullable String oid) throws PropertiesValidationException {
        super.oid(oid);
        return this;
    }

    @Override
    public AnySharedCredentials oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        super.oid(oid);
        return this;
    }

    @Override
    protected AnySharedCredentialsProperties build() {
        return new AnySharedCredentialsProperties(atlassianPlugin, name, oid, configuration, project);
    }

}
