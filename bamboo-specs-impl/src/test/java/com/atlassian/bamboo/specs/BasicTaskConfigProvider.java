package com.atlassian.bamboo.specs;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

/**
 * Provides arguments for JUnitParams for testing tasks.
 *
 * @see JUnitParamsRunner
 * @see Parameters#source()
 */
public final class BasicTaskConfigProvider {
    private BasicTaskConfigProvider() {
    }

    /**
     * Basic configuration shared by all tasks. Each test will contain the following parameters:
     * - task enabled (boolean)
     * - task description (string)
     */
    public static Object[] provideBasicTaskConfig() {
        return new Object[][]{
                {true, null},
                {true, ""},
                {false, "description"}
        };
    }
}
