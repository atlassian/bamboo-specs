package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.trigger.TriggerCondition;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.trigger.PlansGreenTriggerConditionProperties;

import java.util.LinkedHashSet;
import java.util.Set;

public class PlansGreenTriggerCondition extends TriggerCondition<PlansGreenTriggerCondition, PlansGreenTriggerConditionProperties> {
    private final Set<PlanIdentifierProperties> plans = new LinkedHashSet<>();

    public PlansGreenTriggerCondition plans(PlanIdentifier... plans) {
        for (PlanIdentifier plan : plans) {
            this.plans.add(EntityPropertiesBuilders.build(plan));
        }
        return this;
    }

    @Override
    protected PlansGreenTriggerConditionProperties build() {
        return new PlansGreenTriggerConditionProperties(plans);
    }
}
