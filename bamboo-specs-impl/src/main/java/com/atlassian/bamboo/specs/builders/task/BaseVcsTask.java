package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.BaseVcsTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public abstract class BaseVcsTask<T extends BaseVcsTask<T, E>, E extends BaseVcsTaskProperties> extends Task<T, E> {
    protected boolean defaultRepository;
    @Nullable
    protected VcsRepositoryIdentifierProperties repository;
    @Nullable
    protected String workingSubdirectory;

    /**
     * Sets the repository for this task to plan's default repository. Default repository is the repository which is the
     * first on the list of plan's repositories.
     * <p>
     * Deployment projects don't have a default repository. Because of that, this method can't be used in deployments.
     */
    public T defaultRepository() {
        this.defaultRepository = true;
        this.repository = null;
        return (T) this;
    }

    /**
     * Sets the repository for this task.
     */
    public T repository(@NotNull String repositoryName) {
        checkNotNull("repositoryName", repositoryName);
        return repository(new VcsRepositoryIdentifier().name(repositoryName));
    }

    /**
     * Sets the repository for this task.
     */
    public T repository(@NotNull VcsRepositoryIdentifier repositoryIdentifier) {
        checkNotNull("repositoryIdentifier", repositoryIdentifier);
        this.defaultRepository = false;
        this.repository = EntityPropertiesBuilders.build(repositoryIdentifier);
        return (T) this;
    }

    /**
     * Sets the working subdirectory for this task.
     * <p>
     * This method can only be used in deployments. For build plans, the working subdirectory will be extracted from
     * the checkout location of selected {@link #repository(String) repository}.
     */
    public T workingSubdirectory(@Nullable String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return (T) this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseVcsTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BaseVcsTask<?, ?> that = (BaseVcsTask<?, ?>) o;
        return defaultRepository == that.defaultRepository &&
                Objects.equals(repository, that.repository) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), defaultRepository, repository, workingSubdirectory);
    }
}
