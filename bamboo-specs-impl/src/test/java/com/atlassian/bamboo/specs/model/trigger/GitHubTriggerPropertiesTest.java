package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class GitHubTriggerPropertiesTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetAtlassianPlugin() {
        assertThat(new GitHubTriggerProperties("a",
                        "My Github trigger",
                        true,
                        Collections.emptySet(),
                        RepositoryBasedTrigger.TriggeringRepositoriesType.ALL,
                        Collections.emptyList()
                ).getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:githubTrigger"));
    }

    @Test
    public void testGitHubTriggerPropertiesBaseValidation() {
        new GitHubTriggerProperties("a",
                "My Github trigger",
                true,
                Collections.emptySet(),
                RepositoryBasedTrigger.TriggeringRepositoriesType.ALL,
                Collections.emptyList()
        ).validate();
    }
}
