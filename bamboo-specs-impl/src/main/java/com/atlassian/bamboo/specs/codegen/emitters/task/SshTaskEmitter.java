package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.SshTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SshTaskEmitter extends EntityPropertiesEmitter<SshTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull SshTaskProperties entity) throws CodeGenerationException {
        builderClass = SshTask.class;
        fieldsToSkip.add("host");

        return emitConstructorInvocation(context, entity)
                + context.incIndentation()
                + context.newLine()
                + new BaseSshTaskAuthenticationEmitter().emitCodeForAuthentication(entity, context)
                + context.decIndentation()
                + emitFields(context, entity)
                + context.incIndentation()
                + context.newLine()
                + emitHost(context, entity)
                + context.decIndentation();
    }

    private String emitHost(@NotNull CodeGenerationContext context, @NotNull SshTaskProperties entity) throws CodeGenerationException {
        if (entity.getHost() != null) {
            return Arrays.stream(entity.getHost().split(","))
                    .filter(StringUtils::isNotBlank)
                    .map(String::trim)
                    .map(h -> ".host(\"" + h + "\")")
                    .collect(Collectors.joining(""));
        } else {
            throw new CodeGenerationException("Host property is null");
        }
    }
}
