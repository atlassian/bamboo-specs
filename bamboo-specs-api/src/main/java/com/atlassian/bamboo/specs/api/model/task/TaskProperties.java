package com.atlassian.bamboo.specs.api.model.task;


import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

public abstract class TaskProperties implements EntityProperties {
    private final String description;
    private final boolean enabled;
    private final Collection<? extends RequirementProperties> requirements;
    private final Collection<? extends ConditionProperties> conditions;

    protected TaskProperties() {
        this("", true, Collections.emptyList(), Collections.emptyList());
    }

    public TaskProperties(final String description,
                          final boolean enabled,
                          @NotNull final List<RequirementProperties> requirements,
                          @NotNull final List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        this.description = description;
        this.enabled = enabled;
        this.requirements = Collections.unmodifiableList(new ArrayList<>(requirements));
        this.conditions = Collections.unmodifiableList(new ArrayList<>(conditions));
    }

    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    @Nullable
    public String getDescription() {
        return description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Collection<? extends RequirementProperties> getRequirements() {
        return requirements;
    }

    public Collection<? extends ConditionProperties> getConditions() {
        return conditions;
    }

    public EnumSet<Applicability> applicableTo() {
        return EnumSet.allOf(Applicability.class);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TaskProperties that = (TaskProperties) o;
        return isEnabled() == that.isEnabled() &&
                Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getRequirements(), that.getRequirements()) &&
                Objects.equals(getConditions(), that.getConditions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getDescription(), isEnabled());
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "TaskProperties{" +
                "description='" + description + '\'' +
                ", enabled=" + enabled +
                ", requirements=" + requirements +
                ", conditions=" + conditions +
                '}';
    }
}
