package com.atlassian.bamboo.specs.api.builders.credentials;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Represents a shared credentials data. Global shared credentials can be used by any plan. Project shared credentials
 * can be used only by plans in Project.
 * <p>
 * This class contains common data only. In order to define a specific type of credentials one should use the specialised
 * implementation or, if such is not available, {@link AnySharedCredentials} class.
 */
public abstract class SharedCredentials<B extends SharedCredentials<B, C>, C extends SharedCredentialsProperties> extends RootEntityPropertiesBuilder<C> {

    public static final String TYPE = "shared credentials";

    protected String name;
    protected BambooOidProperties oid;
    protected ProjectProperties project;

    protected SharedCredentials() {
    }

    protected SharedCredentials(@NotNull String name) throws PropertiesValidationException {
        checkNotBlank("name", name);
        this.name = name;
    }

    /**
     * Set the name by which credentials will be identified by Bamboo.
     * In the absence of oid, the name is used to identify the shared credential.
     * If the shared credential with given name does not exist, a new one is created, otherwise existing one is updated.
     *
     * @see #oid(BambooOid)
     */
    public B name(@NotNull String name) {
        checkNotBlank("name", name);
        this.name = name;
        return (B) this;
    }

    /**
     * Set the credential's oid.
     * <p>
     * If set, it is used to identify the shared credential.
     * If the shared credential with given oid does not exist, a new one is created, otherwise existing one is updated.
     * Setting both oid and name may result in existing shared credential being renamed.
     */
    public B oid(@Nullable String oid) throws PropertiesValidationException {
        return oid(oid != null ? new BambooOid(oid) : null);
    }

    /**
     * Set the credential's oid from oid string.
     * <p>
     * If set, it is used to identify the shared credential.
     * If the shared credential with given oid does not exist, a new one is created, otherwise existing one is updated.
     * Setting both oid and name may result in existing shared credential being renamed.
     */
    public B oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        this.oid = oid != null ? EntityPropertiesBuilders.build(oid) : null;
        return (B) this;
    }

    /**
     * Set the credential's project.
     * <p>
     * If set, it is Project shared credentials, otherwise it is Global Shared Credentials.
     */
    public B project(@NotNull Project project) {
        checkNotNull("project", project);
        this.project = EntityPropertiesBuilders.build(project);
        return (B) this;
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        return String.format("%s %s", TYPE, defaultString(name, "<unknown>"));
    }

    protected abstract C build();
}
