package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


public class DockerRunContainerEmitterTest {

    private DockerRunContainerEmitter emitter;

    @Before
    public void setUp() {
        emitter = new DockerRunContainerEmitter();
    }

    @Test
    public void emitRunContainerWithoutPortMappings() throws Exception {
        DockerRunContainerTaskProperties properties = build(
                new DockerRunContainerTask()
                        .imageName("image"));

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".containerWorkingDirectory(\"/data\")",
                "", //without port mappings generated code gets empty line
                ".clearVolumeMappings()",
                ".appendVolumeMapping(\"${bamboo.working.directory}\", \"/data\")"
        ));
    }

    @Test
    public void emitDetachedContainer() throws CodeGenerationException {
        DockerRunContainerTaskProperties properties = build(
                new DockerRunContainerTask()
                        .imageName("image")
                        .containerName("some name")
                        .detachContainer(true));
        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".containerWorkingDirectory(\"/data\")",
                "", //without port mappings generated code gets empty line
                ".clearVolumeMappings()",
                ".appendVolumeMapping(\"${bamboo.working.directory}\", \"/data\")",
                ".detachContainer(true)",
                ".containerName(\"some name\")"
        ));
    }

    @Test
    public void emitRunContainerWithPortMappings() throws Exception {
        DockerRunContainerTaskProperties properties = build(
                new DockerRunContainerTask()
                        .imageName("image")
                        .appendPortMapping(8080, 18080)
                        .appendPortMapping(433, 1433));

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".containerWorkingDirectory(\"/data\")",
                ".appendPortMapping(8080, 18080)",
                ".appendPortMapping(433, 1433)",
                ".clearVolumeMappings()",
                ".appendVolumeMapping(\"${bamboo.working.directory}\", \"/data\")"
        ));
    }

    @Test
    public void emitRunContainerWithoutVolume() throws Exception {
        DockerRunContainerTaskProperties properties = build(
                new DockerRunContainerTask()
                        .imageName("image")
                        .appendPortMapping(8080, 18080)
                        .appendPortMapping(433, 1433)
                        .clearVolumeMappings());

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".containerWorkingDirectory(\"/data\")",
                ".appendPortMapping(8080, 18080)",
                ".appendPortMapping(433, 1433)",
                ".clearVolumeMappings()"
        ));
    }

    @Test
    public void emitRunContainerWithMultipleVolumes() throws Exception {
        DockerRunContainerTaskProperties properties = build(
                new DockerRunContainerTask()
                        .imageName("image")
                        .appendPortMapping(8080, 18080)
                        .appendPortMapping(433, 1433)
                        .appendVolumeMapping("someHostDirectory", "/some/data")
        );

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".containerWorkingDirectory(\"/data\")",
                ".appendPortMapping(8080, 18080)",
                ".appendPortMapping(433, 1433)",
                ".clearVolumeMappings()",
                ".appendVolumeMapping(\"${bamboo.working.directory}\", \"/data\")",
                ".appendVolumeMapping(\"someHostDirectory\", \"/some/data\")"
        ));
    }

    @Test
    public void emitRunContainerWithMultipleVolumesWithoutDefault() throws Exception {
        DockerRunContainerTaskProperties properties = build(
                new DockerRunContainerTask()
                        .imageName("image")
                        .appendPortMapping(8080, 18080)
                        .appendPortMapping(433, 1433)
                        .clearVolumeMappings()
                        .appendVolumeMapping("someHostDirectory", "/some/data")
                        .appendVolumeMapping("someHostDirectory2", "/some/data/second")
        );

        assertEmittedCode(properties, Lists.newArrayList(
                ".imageName(\"image\")",
                ".containerWorkingDirectory(\"/data\")",
                ".appendPortMapping(8080, 18080)",
                ".appendPortMapping(433, 1433)",
                ".clearVolumeMappings()",
                ".appendVolumeMapping(\"someHostDirectory\", \"/some/data\")",
                ".appendVolumeMapping(\"someHostDirectory2\", \"/some/data/second\")"
        ));
    }

    private void assertEmittedCode(DockerRunContainerTaskProperties properties, Collection<String> expectedSetters) throws CodeGenerationException {
        List<String> result = Lists.newArrayList(emitter.emitCode(new CodeGenerationContext(), properties).split("\n"))
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());
        final String objectCreation = result.get(0);
        final List<String> objectBuilding = result.subList(1, result.size());

        assertThat(objectCreation, equalTo("new DockerRunContainerTask()"));
        assertThat(objectBuilding, containsInAnyOrder(
                expectedSetters.stream().map(Matchers::equalTo).collect(Collectors.toList())
        ));
    }
}