package com.atlassian.bamboo.specs.api.model.plan;

import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactProperties;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactSubscriptionProperties;
import com.atlassian.bamboo.specs.api.model.plan.configuration.PluginConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.plan.JobValidator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@ConstructFrom({"name", "key"})
@Immutable
public final class JobProperties extends AbstractPlanProperties {
    private final List<ArtifactProperties> artifacts;
    private final List<TaskProperties> tasks;
    private final List<TaskProperties> finalTasks;
    private final List<RequirementProperties> requirements;
    private final List<ArtifactSubscriptionProperties> artifactSubscriptions;
    private final boolean cleanWorkingDirectory;
    private final DockerConfigurationProperties dockerConfiguration;

    private JobProperties() {
        tasks = Collections.emptyList();
        finalTasks = Collections.emptyList();
        artifacts = Collections.emptyList();
        requirements = Collections.emptyList();
        artifactSubscriptions = Collections.emptyList();
        cleanWorkingDirectory = false;
        dockerConfiguration = EntityPropertiesBuilders.build(new DockerConfiguration().enabled(false));
    }

    public JobProperties(final BambooKeyProperties key,
                         final String name, final String description, final boolean enabled,
                         final boolean cleanWorkingDirectory,
                         final Collection<PluginConfigurationProperties> pluginConfigurations,
                         @NotNull final List<ArtifactProperties> artifacts,
                         @NotNull final List<TaskProperties> tasks,
                         @NotNull final List<TaskProperties> finalTasks,
                         @NotNull final List<RequirementProperties> requirements,
                         @NotNull final List<ArtifactSubscriptionProperties> artifactSubscriptions,
                         @NotNull final DockerConfigurationProperties dockerConfiguration) throws PropertiesValidationException {
        super(null, key, name, description, enabled, pluginConfigurations);
        this.cleanWorkingDirectory = cleanWorkingDirectory;
        this.artifacts = Collections.unmodifiableList(new ArrayList<>(artifacts));
        this.tasks = Collections.unmodifiableList(new ArrayList<>(tasks));
        this.finalTasks = Collections.unmodifiableList(new ArrayList<>(finalTasks));
        this.requirements = Collections.unmodifiableList(new ArrayList<>(requirements));
        this.artifactSubscriptions = Collections.unmodifiableList(new ArrayList<>(artifactSubscriptions));
        this.dockerConfiguration = dockerConfiguration;
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JobProperties that = (JobProperties) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getKey(), that.getKey()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                isEnabled() == that.isEnabled() &&
                isCleanWorkingDirectory() == that.isCleanWorkingDirectory() &&
                Objects.equals(getPluginConfigurations(), that.getPluginConfigurations()) &&
                Objects.equals(getArtifacts(), that.getArtifacts()) &&
                Objects.equals(getTasks(), that.getTasks()) &&
                Objects.equals(getFinalTasks(), that.getFinalTasks()) &&
                Objects.equals(getRequirements(), that.getRequirements()) &&
                Objects.equals(getArtifactSubscriptions(), that.getArtifactSubscriptions()) &&
                Objects.equals(getDockerConfiguration(), that.getDockerConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getOid(),
                getKey(),
                getName(),
                getDescription(),
                isEnabled(),
                isCleanWorkingDirectory(),
                getPluginConfigurations(),
                getArtifacts(),
                getTasks(),
                getFinalTasks(),
                getRequirements(),
                getArtifactSubscriptions(),
                getDockerConfiguration());
    }

    public boolean isCleanWorkingDirectory() {
        return cleanWorkingDirectory;
    }

    public List<ArtifactProperties> getArtifacts() {
        return artifacts;
    }

    public List<TaskProperties> getTasks() {
        return tasks;
    }

    public List<TaskProperties> getFinalTasks() {
        return finalTasks;
    }

    public List<RequirementProperties> getRequirements() {
        return requirements;
    }

    @NotNull
    public List<ArtifactSubscriptionProperties> getArtifactSubscriptions() {
        return artifactSubscriptions;
    }

    public DockerConfigurationProperties getDockerConfiguration() {
        return dockerConfiguration;
    }

    @Override
    public void validate() {
        super.validate();
        final ValidationContext context = ValidationContext.of("Job");
        checkRequired(context.with("artifacts"), artifacts);
        checkRequired(context.with("tasks"), tasks);
        checkRequired(context.with("final tasks"), finalTasks);
        checkRequired(context.with("Docker configuration"), dockerConfiguration);
        checkNoErrors(JobValidator.validate(this));
    }
}
