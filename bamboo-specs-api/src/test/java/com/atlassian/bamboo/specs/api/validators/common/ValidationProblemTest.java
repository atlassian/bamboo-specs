package com.atlassian.bamboo.specs.api.validators.common;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ValidationProblemTest {
    @Test
    public void testMessageFormatting() {
        assertThat(new ValidationProblem("simple message").getMessage(),
                equalTo("simple message"));

        assertThat(new ValidationProblem("item", "%s=%d", "key", 10).getMessage(),
                equalTo("item: key=10"));

        assertThat(new ValidationProblem(ValidationContext.of("first").with("second").with("third"), "%s=%d", "key", 10).getMessage(),
                equalTo("first / second / third: key=10"));
    }
}