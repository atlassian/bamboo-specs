package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.TestResourceHelper;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsBranchTaskProperties;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class VcsBranchTaskEmitterTest {
    @Test
    public void emitCodeWithDefaultRepository() throws Exception {
        final VcsBranchTaskProperties taskProperties = new VcsBranchTaskProperties(
                "", true, Collections.emptyList(), Collections.emptyList(), true, null, "sub/directory", "custom-branch");
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithDefaultRepository.java");
        final String emittedCode = new VcsBranchTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }

    @Test
    public void emitCodeWithCustomRepository() throws Exception {
        final VcsBranchTaskProperties taskProperties = new VcsBranchTaskProperties(
                "", true, Collections.emptyList(), Collections.emptyList(), false,
                new VcsRepositoryIdentifierProperties("bamboo-plugin", null), "sub/directory", "custom-branch");
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithCustomRepository.java");
        final String emittedCode = new VcsBranchTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }
}
