package com.atlassian.bamboo.specs.api.model.label;

import javax.annotation.concurrent.Immutable;

@Immutable
@Deprecated
public class EmptyLabelsListProperties extends LabelProperties {
    public EmptyLabelsListProperties() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o != null && getClass() == o.getClass();
    }

    @Override
    public String getName() {
        throw new RuntimeException("EmptyLabelsListProperties does not have a name");
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public void validate() {
    }
}
