package com.atlassian.bamboo.specs.model;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.builders.label.EmptyLabelsList;
import com.atlassian.bamboo.specs.api.builders.notification.EmptyNotificationsList;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.EmptyDependenciesList;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.label.EmptyLabelsListProperties;
import com.atlassian.bamboo.specs.api.model.notification.EmptyNotificationsListProperties;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.EmptyDependenciesListProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.MavenTask;
import com.atlassian.bamboo.specs.model.task.MavenTaskProperties;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.springframework.objenesis.Objenesis;
import org.springframework.objenesis.ObjenesisStd;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.fail;

/**
 * Tests which assert that all properties classes used for importing/exporting fulfill their contracts.
 */
public class PropertiesClassesTest {
    private static final Objenesis OBJENESIS = new ObjenesisStd(true);
    private static final Collection<Class<?>> CLASSES_TO_IGNORE = Arrays.asList(
            EmptyNotificationsList.class,
            EmptyDependenciesList.class,
            EmptyLabelsList.class,
            EmptyNotificationsListProperties.class,
            EmptyDependenciesListProperties.class,
            EmptyLabelsListProperties.class);

    private static Object newInstanceByNoArgConstructor(Class<?> clazz) {
        final Constructor<?> constructor;
        try {
            constructor = clazz.getDeclaredConstructor(new Class[0]);
            constructor.setAccessible(true);
            return constructor.newInstance(new Object[0]);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    private static Optional<Object> newInstanceByNoArgConstructorOrEmpty(Class<?> clazz) {
        final Constructor<?> constructor;
        try {
            constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            return Optional.of(constructor.newInstance());
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            return Optional.empty();
        }
    }

    /**
     * Verifies that all fields of properties classes are used for calculating hash codes and for checking equality.
     */
    @Test
    public void testHashCodeAndEquals() throws Exception {
        for (Class<?> clazz : getAllPropertiesClasses()) {
            final Object emptyObject = newInstanceByNoArgConstructor(clazz);
            final Object anotherObject = newInstanceByNoArgConstructor(clazz);
            assertThat(String.format("Instance of '%s' should be equal to another instance with all params in initial state", clazz.getName()),
                    anotherObject, is(equalTo(emptyObject)));
            assertThat(String.format("Instance of '%s' should have same hash code as another instance with all params in initial state", clazz.getName()),
                    anotherObject.hashCode(), is(equalTo(emptyObject.hashCode())));

            ReflectionUtils.doWithFields(clazz, field ->
            {
                if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isTransient(field.getModifiers())) {
                    field.setAccessible(true);
                    final Object modifiedObject = newInstanceByNoArgConstructor(clazz);
                    field.set(modifiedObject, instantiateField(clazz, field));
                    assertThat(String.format("Field '%s' of class '%s' should be used when checking equality", field.getName(), clazz.getName()),
                            modifiedObject, is(not(equalTo(emptyObject))));
                }
            });
        }
    }

    @Test
    public void testAllClassesHaveBuildMethod() throws Exception {
        for (Class<? extends EntityPropertiesBuilder<?>> clazz : getAllBuilderClasses()) {
            final Method buildMethod = clazz.getDeclaredMethod("build");
            if (Modifier.isPublic(buildMethod.getModifiers())) {
                fail(String.format("The build() method should not be public in %s", clazz.getName()));
            }
        }
    }

    @Test
    public void testAllClassesHaveNoParamConstructor() throws Exception {
        for (Class<? extends EntityProperties> clazz : getAllPropertiesClasses()) {
            Optional<Constructor<?>> constructor = Stream.of(clazz.getDeclaredConstructors())
                    .filter((c) -> c.getParameterCount() == 0)
                    .findAny();
            if (!constructor.isPresent()) {
                fail(String.format("Class %s must have a no-param constructor", clazz.getName()));
            } else if (Modifier.isPublic(constructor.get().getModifiers())) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (!Modifier.isStatic(field.getModifiers())) {
                        fail(String.format("The no-param constructor of %s should not be public if it has non-static fields", clazz.getName()));
                    }
                }
            }
        }
    }

    @Test
    @Ignore
    public void testBuildersAndPropertiesHaveSameDefaultValues() {
        // this test verifies for all builders with default constructor and without mandatory fields that
        // new Builder().build.equals(new BuilderProperties())
        // as of now 34 classes violates this requirement
        // test is left ignored in order not to fix them all at once
        for (Class<? extends EntityPropertiesBuilder<?>> clazz : getAllBuilderClasses()) {
            newInstanceByNoArgConstructorOrEmpty(clazz)
                    .flatMap(builder -> {
                        try {
                            return Optional.of(EntityPropertiesBuilders.build((EntityPropertiesBuilder<?>) builder));
                        } catch (PropertiesValidationException | IllegalStateException e) {
                            return Optional.empty();
                        }
                    })
                    .ifPresent(entityProperties -> {
                        final EntityProperties defaultProperties = (EntityProperties) newInstanceByNoArgConstructor(entityProperties.getClass());
                        assertThat(entityProperties, equalTo(defaultProperties));
                    });
        }
    }

    private <T> List<Class<? extends T>> getAllClasses(final Class<T> parentClass) {
        final List<Class<? extends T>> classes = new ArrayList<>();
        final Reflections reflections = new Reflections(
                ClasspathHelper.forJavaClassPath(),
                new SubTypesScanner(false));

        reflections.getSubTypesOf(parentClass).stream()
                .filter(clazz -> !clazz.isInterface())
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
                .forEach(classes::add);

        assertThat("Should have found properties classes - maybe they were refactored (package or naming scheme changed)?",
                classes, is(not(empty())));

        return classes;
    }

    /**
     * Return all classes which are used as properties for importing/exporting Bamboo entities.
     */
    private List<Class<? extends EntityPropertiesBuilder<?>>> getAllBuilderClasses() {
        final List<Class<? extends EntityPropertiesBuilder<?>>> allClasses = (List) getAllClasses(EntityPropertiesBuilder.class);
        return allClasses.stream()
                .filter(c -> !CLASSES_TO_IGNORE.contains(c))
                .collect(Collectors.toList());
    }


    /**
     * Return all classes which are used as properties for importing/exporting Bamboo entities.
     */
    private List<Class<? extends EntityProperties>> getAllPropertiesClasses() {
        return getAllClasses(EntityProperties.class).stream()
                .filter(c -> !CLASSES_TO_IGNORE.contains(c))
                .collect(Collectors.toList());
    }

    /**
     * Creates any instance of the given field. The instance should preferably yield non-zero hash code.
     */
    private Object instantiateField(Class<?> clazz, Field field) throws IllegalAccessException {
        if (clazz.equals(MavenTaskProperties.class) && field.equals(getField(MavenTaskProperties.class, "version", (RuntimeException::new)))) {
            return MavenTask.MAVEN_V2;
        } else if (field.getType() == DockerConfigurationProperties.class) {
            return EntityPropertiesBuilders.build(
                    new DockerConfiguration()
                            .enabled(true)
                            .image("atlassian/bamboo-specs-runner:6.3.0"));
        } else if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
            field.setAccessible(true);
            final Boolean value = (Boolean) field.get(newInstanceByNoArgConstructor(clazz));
            return value == null || !value;
        } else if (field.getType() == Long.class || field.getType() == Long.TYPE) {
            return 10L;
        } else if (field.getType() == Integer.class || field.getType() == Integer.TYPE) {
            return 15;
        } else if (field.getType() == String.class) {
            return "foo";
        } else if (field.getType() == Duration.class) {
            return Duration.ofDays(1);
        } else if (field.getType() == Map.class) {
            return Collections.singletonMap("foo", "bar");
        } else if (field.getType() == List.class) {
            return Collections.singletonList(100.0D);
        } else if (field.getType() == Set.class) {
            return Collections.singleton(500);
        } else if (Modifier.isAbstract(field.getType().getModifiers())) {
            return Mockito.mock(field.getType());
        } else {
            try {
                return OBJENESIS.newInstance(field.getType());
            } catch (Throwable t) {
                fail(String.format("Could not instantiate field '%s' of type '%s' from class '%s'", field.getName(), field.getType().getName(), clazz.getName()));
                return null;
            }
        }
    }

    private <T extends Exception> Field getField(Class<?> clazz, String fieldName, Function<NoSuchFieldException, T> exceptionMapper) throws T {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            throw exceptionMapper.apply(e);
        }
    }
}
