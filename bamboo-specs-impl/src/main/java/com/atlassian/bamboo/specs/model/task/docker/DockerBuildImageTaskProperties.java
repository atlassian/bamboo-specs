package com.atlassian.bamboo.specs.model.task.docker;


import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.codegen.emitters.task.DockerBuildImageEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

@Builder(DockerBuildImageTask.class)
@CodeGenerator(DockerBuildImageEmitter.class)
@Immutable
public final class DockerBuildImageTaskProperties extends AbstractDockerTaskProperties {
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Docker build image task: ");


    public enum DockerfileContent {
        WORKING_DIR,
        INLINE;
    }

    @NotNull
    private String imageName;
    @NotNull
    private DockerfileContent dockerfileContent;
    @Nullable
    private String dockerfile;
    private boolean useCache;
    private boolean saveAsFile;
    @Nullable
    private String imageFilename;
    @Nullable
    private String additionalArguments;

    protected DockerBuildImageTaskProperties() {
    }

    public DockerBuildImageTaskProperties(@Nullable String description, boolean enabled,
                                          @NotNull String imageName,
                                          @NotNull DockerfileContent dockerfileContent,
                                          @Nullable String dockerfile,
                                          boolean useCache,
                                          boolean saveAsFile,
                                          @Nullable String imageFileName,
                                          @Nullable String additionalArguments,
                                          @Nullable String environmentVariables,
                                          @Nullable String workingSubdirectory,
                                          @NotNull List<RequirementProperties> requirements,
                                          @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, environmentVariables, workingSubdirectory, requirements, conditions);

        this.imageName = imageName;
        this.dockerfileContent = dockerfileContent;
        this.dockerfile = dockerfile;
        this.useCache = useCache;
        this.saveAsFile = saveAsFile;
        this.imageFilename = imageFileName;
        this.additionalArguments = additionalArguments;
        validate();
    }

    @NotNull
    public String getImageName() {
        return imageName;
    }

    @NotNull
    public DockerfileContent getDockerfileContent() {
        return dockerfileContent;
    }

    @Nullable
    public String getDockerfile() {
        return dockerfile;
    }

    public boolean isUseCache() {
        return useCache;
    }

    public boolean isSaveAsFile() {
        return saveAsFile;
    }

    @Nullable
    public String getImageFilename() {
        return imageFilename;
    }

    @Nullable
    public String getAdditionalArguments() {
        return additionalArguments;
    }

    @Override
    public void validate() {
        super.validate();
        checkNotBlank(VALIDATION_CONTEXT, "imageName", imageName);
        if (saveAsFile) {
            checkNotBlank(VALIDATION_CONTEXT, "imageFilename", imageFilename);
        }
        if (dockerfileContent == DockerfileContent.INLINE) {
            checkNotBlank(VALIDATION_CONTEXT, "Dockerfile content", dockerfile);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        DockerBuildImageTaskProperties that = (DockerBuildImageTaskProperties) o;
        return useCache == that.useCache &&
                saveAsFile == that.saveAsFile &&
                Objects.equals(imageName, that.imageName) &&
                dockerfileContent == that.dockerfileContent &&
                Objects.equals(dockerfile, that.dockerfile) &&
                Objects.equals(additionalArguments, that.additionalArguments) &&
                Objects.equals(imageFilename, that.imageFilename);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), imageName, dockerfileContent, dockerfile, useCache,
                saveAsFile, imageFilename, additionalArguments);
    }
}
