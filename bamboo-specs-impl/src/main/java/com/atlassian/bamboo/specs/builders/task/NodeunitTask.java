package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.NodeunitTaskProperties;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties.TestType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

/**
 * Runs Nodeunit tests.
 */
public class NodeunitTask extends BaseNodeTask<NodeunitTask, NodeunitTaskProperties> {
    public static final String DEFAULT_NODEUNIT_EXECUTABLE = preventInlining("node_modules/nodeunit/bin/nodeunit");
    public static final String DEFAULT_TEST_DIRECTORY = preventInlining("test/");
    public static final String DEFAULT_RESULTS_DIRECTORY = preventInlining("test-reports/");
    public static final boolean DEFAULT_PARSE_TEST_RESULTS = preventInlining(true);

    @NotNull
    private String nodeunitExecutable = DEFAULT_NODEUNIT_EXECUTABLE;
    @NotNull
    private String testFilesAndDirectories = DEFAULT_TEST_DIRECTORY;
    @NotNull
    private String testResultsDirectory = DEFAULT_RESULTS_DIRECTORY;
    private boolean parseTestResults = DEFAULT_PARSE_TEST_RESULTS;
    @Nullable
    private String arguments;

    /**
     * Specify path to Nodeunit executable for this task. Path must be relative to the working directory.
     * <p>
     * Example: {@code node_modules/nodeunit/bin/nodeunit}
     */
    public NodeunitTask nodeunitExecutable(@NotNull String nodeunitExecutable) {
        ImporterUtils.checkNotNull("nodeunitExecutable", nodeunitExecutable);
        this.nodeunitExecutable = nodeunitExecutable;
        return this;
    }

    /**
     * Specify files and/or directories containing Nodeunit tests. You can specify multiple files and directories
     * separated by a space. Defaults to 'test/' directory.
     */
    public NodeunitTask testFilesAndDirectories(@NotNull String testFilesAndDirectories) {
        checkNotNull("testFilesAndDirectories", testFilesAndDirectories);
        this.testFilesAndDirectories = testFilesAndDirectories;
        return this;
    }

    /**
     * Name of the directory where the test results will be stored (in JUnit XML format).
     */
    public NodeunitTask testResultsDirectory(@NotNull String testResultsDirectory) {
        ImporterUtils.checkNotNull("testResultsDirectory", testResultsDirectory);
        this.testResultsDirectory = testResultsDirectory;
        return this;
    }

    /**
     * Whether this task should parse test results produced by Nodeunit. Defaults to true. If set to false, a subsequent
     * {@link TestParserTask} for {@link TestType#JUNIT} should be used to gather the results.
     */
    public NodeunitTask parseTestResults(boolean parseTestResults) {
        this.parseTestResults = parseTestResults;
        return this;
    }

    /**
     * Additional command line arguments to pass to Nodeunit. Be aware that some options may already be set by this task
     * configuration.
     */
    public NodeunitTask arguments(@Nullable String arguments) {
        this.arguments = arguments;
        return this;
    }

    @NotNull
    @Override
    protected NodeunitTaskProperties build() {
        return new NodeunitTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                nodeunitExecutable,
                testFilesAndDirectories,
                testResultsDirectory,
                parseTestResults,
                arguments,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NodeunitTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        NodeunitTask that = (NodeunitTask) o;
        return parseTestResults == that.parseTestResults &&
                nodeunitExecutable.equals(that.nodeunitExecutable) &&
                testFilesAndDirectories.equals(that.testFilesAndDirectories) &&
                testResultsDirectory.equals(that.testResultsDirectory) &&
                Objects.equals(arguments, that.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nodeunitExecutable, testFilesAndDirectories, testResultsDirectory, parseTestResults, arguments);
    }
}
