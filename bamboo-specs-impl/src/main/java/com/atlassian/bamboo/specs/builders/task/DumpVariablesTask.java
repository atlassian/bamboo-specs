package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.DumpVariablesTaskProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a task that dumps Bamboo variables to log when task is run on agent.
 */
public class DumpVariablesTask extends Task<DumpVariablesTask, DumpVariablesTaskProperties> {
    public DumpVariablesTask() {

    }

    @NotNull
    @Override
    protected DumpVariablesTaskProperties build() {
        return new DumpVariablesTaskProperties(description, taskEnabled, requirements, conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof DumpVariablesTask)) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
