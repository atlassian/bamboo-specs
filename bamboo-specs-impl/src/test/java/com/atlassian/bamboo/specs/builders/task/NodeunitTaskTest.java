package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.NodeunitTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NodeunitTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new NodeunitTask()
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testTestDirectoriesAreRequired() {
        new NodeunitTask()
                .nodeExecutable("Node.js 4.2")
                .testFilesAndDirectories("")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testTestResultsDirectoryIsRequired() {
        new NodeunitTask()
                .nodeExecutable("Node.js 4.2")
                .testResultsDirectory("")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testNodeunitExecutableIsRequired() {
        new NodeunitTask()
                .nodeExecutable("Node.js 4.2")
                .nodeunitExecutable("")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new NodeunitTask()
                .nodeExecutable("Node.js 6")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "execute Gulp";
        final boolean taskEnabled = false;
        final String nodeExecutable = "Node.js 6.0";
        final String nodeunitExecutable = "node_modules/nodeunit/bin/nodeunit";
        final String testFilesAndDirectories = "test.js test/";
        final String testResultsDirectory = "test-reports/";
        final boolean parseTestResults = false;
        final String arguments = "--run-fast";
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        final NodeunitTask NodeunitTask = new NodeunitTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .nodeunitExecutable(nodeunitExecutable)
                .testFilesAndDirectories(testFilesAndDirectories)
                .testResultsDirectory(testResultsDirectory)
                .parseTestResults(parseTestResults)
                .arguments(arguments)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory)
                .requirements(requirement);

        final NodeunitTaskProperties expectedProperties = new NodeunitTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                nodeunitExecutable,
                testFilesAndDirectories,
                testResultsDirectory,
                parseTestResults,
                arguments,
                Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                Collections.emptyList());

        assertThat(NodeunitTask.build(), is(equalTo(expectedProperties)));
    }
}
