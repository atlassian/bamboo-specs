package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection.FileFilteringOption;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class VcsChangeDetectionPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void vcsChangedDetectionPropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final Random random = new Random();
        final boolean commitIsolationEnabled = random.nextBoolean();
        final FileFilteringOption filterFilePatternOptions = FileFilteringOption.INCLUDE_ONLY;
        final String filterFilePatternRegex = "\\.txt$";
        final String changesetFilePatternRegex = "bin/.*";

        final VcsChangeDetection changeDetectionBuilder = new VcsChangeDetection()
                .commitIsolationEnabled(commitIsolationEnabled)
                .filterFilePatternOption(filterFilePatternOptions)
                .filterFilePatternRegex(filterFilePatternRegex)
                .changesetFilterPatternRegex(changesetFilePatternRegex);

        final VcsChangeDetectionProperties changeDetectionProperties = EntityPropertiesBuilders.build(changeDetectionBuilder);

        assertEquals(commitIsolationEnabled, changeDetectionProperties.isCommitIsolationEnabled());
        assertEquals(filterFilePatternOptions, changeDetectionProperties.getFilterFilePatternOption());
        assertEquals(filterFilePatternRegex, changeDetectionProperties.getFilterFilePatternRegex());
        assertEquals(changesetFilePatternRegex, changeDetectionProperties.getChangesetFilterPatternRegex());
    }

    @Test
    public void vcsChangedDetectionPropertiesChangesetFilePatternMustBeProperRegex() throws PropertiesValidationException {
        final String changesetFilePatternRegex = "*";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .changesetFilterPatternRegex(changesetFilePatternRegex));
    }

    @Test
    public void vcsChangedDetectionPropertiesFilterFilePatternMustBeProperRegex() throws PropertiesValidationException {
        final String filterFilePatternRegex = "*";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .filterFilePatternOption(FileFilteringOption.INCLUDE_ONLY)
                .filterFilePatternRegex(filterFilePatternRegex));
    }

    @Test
    public void vcsChangedDetectionPropertiesFilterFilePatternMustBeDefinedIfOptionsAre() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .filterFilePatternOption(FileFilteringOption.INCLUDE_ONLY));
    }
}