package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.CheckoutItemProperties;
import com.atlassian.bamboo.specs.model.task.VcsCheckoutTaskProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents task that checks out selected repositories to the build working directory.
 */
public class VcsCheckoutTask extends Task<VcsCheckoutTask, VcsCheckoutTaskProperties> {

    private List<CheckoutItemProperties> checkoutItems = new ArrayList<>();
    private boolean cleanCheckout;

    /**
     * Adds checkout request for the plan's default repository into the build directory. Default repository is the
     * repository which is the first on the list of plan's repositories.
     * <p>
     * The repository will be checked out to the build's working directory. For more control over checkout path, use
     * {@link VcsCheckoutTask#checkoutItems(CheckoutItem...)}.
     */
    public VcsCheckoutTask addCheckoutOfDefaultRepository() {
        return checkoutItems(new CheckoutItem().defaultRepository());
    }

    /**
     * Adds checkout request for one of plan's repositories into the build directory.
     * <p>
     * The repository will be checked out to the build's working directory. For more control over checkout path, use
     * {@link VcsCheckoutTask#checkoutItems(CheckoutItem...)}.
     */
    public VcsCheckoutTask addCheckoutOfRepository(@NotNull String repositoryName) {
        checkNotNull("repositoryName", repositoryName);
        return checkoutItems(new CheckoutItem().repository(repositoryName));
    }

    /**
     * Adds checkout request for one of plan's repositories into the build directory.
     * <p>
     * The repository will be checked out to the build's working directory. For more control over checkout path, use
     * {@link VcsCheckoutTask#checkoutItems(CheckoutItem...)}.
     */
    public VcsCheckoutTask addCheckoutOfRepository(@NotNull VcsRepositoryIdentifier repositoryIdentifier) {
        checkNotNull("repositoryIdentifier", repositoryIdentifier);
        return checkoutItems(new CheckoutItem().repository(repositoryIdentifier));
    }

    /**
     * Adds checkout requests.
     */
    public VcsCheckoutTask checkoutItems(@NotNull CheckoutItem... checkoutItems) {
        checkNotNull("checkoutItems", checkoutItems);
        Arrays.stream(checkoutItems)
                .map(CheckoutItem::build)
                .forEach(this.checkoutItems::add);
        return this;
    }

    /**
     * Enables/disabled clean checkout. If set, the task cleans the content of the checkout target directory before
     * checking out the source. Off by default.
     */
    public VcsCheckoutTask cleanCheckout(boolean cleanCheckout) {
        this.cleanCheckout = cleanCheckout;
        return this;
    }

    @Override
    @NotNull
    protected VcsCheckoutTaskProperties build() {
        return new VcsCheckoutTaskProperties(description,
                taskEnabled,
                checkoutItems,
                cleanCheckout,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VcsCheckoutTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        VcsCheckoutTask that = (VcsCheckoutTask) o;
        return cleanCheckout == that.cleanCheckout &&
                Objects.equals(checkoutItems, that.checkoutItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), checkoutItems, cleanCheckout);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("checkoutItems", checkoutItems)
                .append("cleanCheckout", cleanCheckout)
                .toString();
    }
}
