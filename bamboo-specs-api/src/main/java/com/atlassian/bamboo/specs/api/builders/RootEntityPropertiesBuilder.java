package com.atlassian.bamboo.specs.api.builders;

import com.atlassian.bamboo.specs.api.model.RootEntityProperties;

/**
 * Represents Bamboo entity that can be sent to server.
 */
public abstract class RootEntityPropertiesBuilder<T extends RootEntityProperties> extends EntityPropertiesBuilder<T> {

    /**
     * Entity type that is shown to human.
     */
    public abstract String humanReadableType();

    /**
     * Entity type and id representation that is shown to human.
     */
    public abstract String humanReadableId();
}
