package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.GulpTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

/**
 * Represents Bamboo task which executes Gulp tasks.
 *
 * @see <a href="http://gulpjs.com/">gulpjs.com</a>
 */
public class GulpTask extends BaseNodeTask<GulpTask, GulpTaskProperties> {
    public static final String DEFAULT_GULP_EXECUTABLE = preventInlining("node_modules/gulp/bin/gulp.js");

    @NotNull
    private String gulpExecutable = DEFAULT_GULP_EXECUTABLE;
    @Nullable
    private String task;
    @Nullable
    private String gulpfile;

    /**
     * Specify path to the Gulp executable for this task. Path must be relative to the working directory.
     * <p>
     * Example: {@code node_modules/gulp/bin/gulp.js}
     */
    public GulpTask gulpExecutable(@NotNull String gulpExecutable) {
        ImporterUtils.checkNotNull("gulpExecutable", gulpExecutable);
        this.gulpExecutable = gulpExecutable;
        return this;
    }

    /**
     * Gulp task to execute. If not specified, the 'default' task will be executed. Multiple tasks can be specified
     * separated by a space.
     */
    public GulpTask task(@Nullable String task) {
        this.task = task;
        return this;
    }

    /**
     * Specify path to the gulpfile, relative to the build working directory. If empty, the default gulpfile will be
     * used. Only supported for Gulp 3.3.2 or newer.
     */
    public GulpTask gulpfile(@Nullable String gulpfile) {
        this.gulpfile = gulpfile;
        return this;
    }

    @NotNull
    @Override
    protected GulpTaskProperties build() {
        return new GulpTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                gulpExecutable,
                task,
                gulpfile,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GulpTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GulpTask gulpTask = (GulpTask) o;
        return gulpExecutable.equals(gulpTask.gulpExecutable) &&
                Objects.equals(task, gulpTask.task) &&
                Objects.equals(gulpfile, gulpTask.gulpfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gulpExecutable, task, gulpfile);
    }
}
