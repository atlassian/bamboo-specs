package com.atlassian.bamboo.specs.api.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.trigger.AllOtherTriggerConditionProperties;
import com.atlassian.bamboo.specs.api.model.trigger.AnyTriggerConditionProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class AnyTriggerCondition<T extends AnyTriggerCondition<T, P>, P extends AnyTriggerConditionProperties> extends TriggerCondition<T, P> {
    private final AtlassianModuleProperties atlassianPlugin;
    private Map<String, String> configuration = new LinkedHashMap<>();

    /**
     * Deprecated default constructor. Use {@link AllOtherTriggerCondition}.
     * @deprecated since 8.0.1. Use {@link AllOtherTriggerCondition}
     */
    @Deprecated
    public AnyTriggerCondition() {
        this(new AtlassianModule(AllOtherTriggerConditionProperties.ALL_OTHER_TRIGGER_CONDITION_PLUGINS_MODULE_KEY));
    }

    public AnyTriggerCondition(@NotNull AtlassianModule atlassianPlugin) {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Configuration map.
     * @deprecated since 8.0.1. Use {{@link #configuration(Map)}}
     */
    @Deprecated
    public AnyTriggerCondition config(@NotNull Map<String, String> configuration) {
        return configuration(configuration);
    }

    public AnyTriggerCondition configuration(@NotNull Map<String, String> configuration) {
        this.configuration = Collections.unmodifiableMap(configuration);
        return this;
    }

    @Override
    protected P build() {
        return (P) new AnyTriggerConditionProperties(atlassianPlugin, configuration);
    }
}
