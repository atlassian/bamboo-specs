package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

@Immutable
public class UserPermissionProperties implements EntityProperties {

    private final String username;
    private final Set<PermissionType> permissionTypes;

    private UserPermissionProperties() {
        this.username = null;
        this.permissionTypes = null;
    }

    public UserPermissionProperties(@NotNull final String username,
                                    @NotNull final Collection<PermissionType> permissionTypes) throws PropertiesValidationException {
        this.username = username;
        this.permissionTypes = permissionTypes.isEmpty() ? Collections.emptySet() : Collections.unmodifiableSet(EnumSet.copyOf(permissionTypes));
        validate();
    }

    public String getUsername() {
        return username;
    }

    public Set<PermissionType> getPermissionTypes() {
        return permissionTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPermissionProperties that = (UserPermissionProperties) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(permissionTypes, that.permissionTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, permissionTypes);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserPermissionProperties{");
        sb.append("username='").append(username).append('\'');
        sb.append(", permissionTypes=").append(permissionTypes);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void validate() throws PropertiesValidationException {
    }
}
