package com.atlassian.bamboo.specs.api.codegen.annotations;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Custom Bamboo Specs generator for EntityProperties class or a field.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface CodeGenerator {
    Class<? extends CodeEmitter<? extends Object>> value();
}
