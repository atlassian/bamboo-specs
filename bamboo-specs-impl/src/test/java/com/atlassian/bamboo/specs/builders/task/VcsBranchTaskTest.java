package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.BasicTaskConfigProvider;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsBranchTaskProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class VcsBranchTaskTest {
    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithDefaultRepository(boolean enabled, String description) {
        assertEquals(
                new VcsBranchTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        true,
                        null,
                        null,
                        "branch-27"),
                new VcsBranchTask()
                        .description(description)
                        .enabled(enabled)
                        .defaultRepository()
                        .branchName("branch-27")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryName(boolean enabled, String description) {
        assertEquals(
                new VcsBranchTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties("bamboo-node-plugin", null),
                        null,
                        "branch-27"),
                new VcsBranchTask()
                        .description(description)
                        .enabled(enabled)
                        .repository("bamboo-node-plugin")
                        .branchName("branch-27")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryIdentifier(boolean enabled, String description) {
        assertEquals(
                new VcsBranchTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties(null, new BambooOidProperties("123456789")),
                        null,
                        "branch-27"),
                new VcsBranchTask()
                        .description(description)
                        .enabled(enabled)
                        .repository(new VcsRepositoryIdentifier().oid("123456789"))
                        .branchName("branch-27")
                        .build());
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void repositoryIsRequired(boolean enabled, String description) {
        new VcsBranchTask()
                .description(description)
                .enabled(enabled)
                .branchName("branch-27")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void branchNameIsRequired(boolean enabled, String description) {
        new VcsBranchTask()
                .description(description)
                .enabled(enabled)
                .defaultRepository()
                .build();
    }
}
