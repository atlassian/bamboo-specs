package com.atlassian.bamboo.specs.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Simples way to provide user and password credentials for Bamboo.
 * Consider using {@link FileUserPasswordCredentials} for more security.
 * <p>
 * Example of using it with user and password from main arguments:
 * <pre>{@code
 *     main(String[] args) {
 *         new BambooServer(args[0], new SimpleUserPasswordCredentials(args[1], args[2]));
 *     }
 * }</pre>
 *
 * @deprecated since 7.1.0, use {@link SimpleTokenCredentials} instead
 */
@Deprecated
public class SimpleUserPasswordCredentials implements UserPasswordCredentials {
    private static final Logger log = Logger.getLogger(SimpleUserPasswordCredentials.class);

    private final String username;
    private final String password;

    public SimpleUserPasswordCredentials(@NotNull String username, @NotNull String password) {
        this.username = checkField("Username", username);
        this.password = checkField("Password", password);
    }

    private String checkField(String fieldName, String fieldValue) {
        if (StringUtils.isEmpty(fieldValue)) {
            log.info("%s must be specified to access Bamboo", fieldName);
            throw new IllegalArgumentException(fieldName + " was not defined");
        }
        return fieldValue;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("[type: 'simple', username: '%s']", username);
    }
}
