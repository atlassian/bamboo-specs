/**
 * Artifact definitions and artifact subscriptions.
 */
package com.atlassian.bamboo.specs.api.builders.plan.artifact;
