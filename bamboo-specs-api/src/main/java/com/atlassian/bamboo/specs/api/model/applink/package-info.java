/**
 * Application Links (for instance to integrate with JIRA or Bitbucket Server).
 */
package com.atlassian.bamboo.specs.api.model.applink;
