package com.atlassian.bamboo.specs.builders.credentials;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.model.credentials.UsernamePasswordCredentialsProperties;

/**
 * Represent shared username/password credentials.
 * Shared username/password credentials can be used when defining authorisation of VCS repositories (e.g. {@link GitRepository}) and in certain tasks.
 */
public class UsernamePasswordCredentials extends SharedCredentials<UsernamePasswordCredentials, UsernamePasswordCredentialsProperties> {
    private String username;
    private String password;

    /**
     * Create new username/password credentials.
     */
    public UsernamePasswordCredentials() {
    }

    /**
     * Create new username/password credentials with name.
     *
     * @param name name of the credentials.
     */
    public UsernamePasswordCredentials(final String name) {
        super(name);
    }

    /**
     * Set username.
     */
    public UsernamePasswordCredentials username(final String username) {
        this.username = username;
        return this;
    }

    /**
     * Set password.
     */
    public UsernamePasswordCredentials password(final String password) {
        this.password = password;
        return this;
    }

    @Override
    protected UsernamePasswordCredentialsProperties build() {
        return new UsernamePasswordCredentialsProperties(
                name,
                oid,
                username,
                password,
                project);
    }
}
