package com.atlassian.bamboo.specs.codegen.emitters.repository;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.codegen.emitters.fragment.FieldSetterEmitter;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import com.atlassian.bamboo.specs.model.repository.git.AuthenticationProperties;
import com.atlassian.bamboo.specs.model.repository.git.SharedCredentialsAuthenticationProperties;
import org.jetbrains.annotations.NotNull;

public abstract class AuthenticationEmitter extends FieldSetterEmitter<AuthenticationProperties> {
    public AuthenticationEmitter(final String methodName) {
        super(methodName);
    }

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final AuthenticationProperties value) throws CodeGenerationException {
        if (value instanceof SharedCredentialsAuthenticationProperties) {
            SharedCredentialsAuthenticationProperties shProps = (SharedCredentialsAuthenticationProperties) value;
            final CodeEmitter<SharedCredentialsIdentifierProperties> codeEmitter = ValueEmitterFactory.emitterFor(shProps.getSharedCredentials());
            return String.format(".%s(%s)", methodName, codeEmitter.emitCode(context, shProps.getSharedCredentials()));
        } else {
            return super.emitCode(context, value);
        }
    }
}
