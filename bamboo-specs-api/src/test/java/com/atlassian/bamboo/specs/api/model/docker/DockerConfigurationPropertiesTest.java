package com.atlassian.bamboo.specs.api.model.docker;

import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.deployment.EnvironmentProperties;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DockerConfigurationPropertiesTest {
    /**
     * This check verifies that parameter-less constructor of {@link JobProperties} instantiates expected
     * {@link DockerConfigurationProperties}. This ensures us that there are no unnecessary calls to
     * {@link Job#dockerConfiguration(DockerConfiguration)} while generating Specs code during Specs export.
     */
    @Test
    public void testJobDefaultDockerConfiguration() throws Exception {
        final JobProperties jobProperties = instantiate(JobProperties.class);
        final DockerConfigurationProperties dockerConfigurationProperties = instantiate(DockerConfigurationProperties.class);
        assertThat(jobProperties.getDockerConfiguration(), is(dockerConfigurationProperties));
    }

    /**
     * This check verifies that parameter-less constructor of {@link EnvironmentProperties} instantiates expected
     * {@link DockerConfigurationProperties}. This ensures us that there are no unnecessary calls to
     * {@link Environment#dockerConfiguration(DockerConfiguration)} while generating Specs code during Specs export.
     */
    @Test
    public void testEnvironmentDefaultDockerConfiguration() throws Exception {
        final EnvironmentProperties environmentProperties = instantiate(EnvironmentProperties.class);
        final DockerConfigurationProperties dockerConfigurationProperties = instantiate(DockerConfigurationProperties.class);
        assertThat(environmentProperties.getDockerConfiguration(), is(dockerConfigurationProperties));
    }

    /**
     * Instantiate a properties class using parameter-less constructor.
     */
    private <T extends EntityProperties> T instantiate(Class<T> classToInstantiate) throws Exception {
        final Constructor<T> constructor = classToInstantiate.getDeclaredConstructor();
        constructor.setAccessible(true);
        return constructor.newInstance();
    }
}
