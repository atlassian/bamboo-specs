package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.VcsTagTaskProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Task which creates a new tag in a repository. For
 * <a href="https://www.atlassian.com/git/tutorials/what-is-version-control/">DVCS repositories</a> (which distinguish
 * between local and remote commits) this task will <strong>push</strong> the newly created tag to the remote
 * repository.
 */
public class VcsTagTask extends BaseVcsTask<VcsTagTask, VcsTagTaskProperties> {
    @NotNull
    private String tagName;

    /**
     * Sets the name of the tag to create.
     */
    public VcsTagTask tagName(@NotNull String branchName) {
        checkNotNull("tagName", branchName);
        this.tagName = branchName;
        return this;
    }

    @NotNull
    @Override
    protected VcsTagTaskProperties build() {
        return new VcsTagTaskProperties(
                description,
                taskEnabled,
                requirements,
                conditions,
                defaultRepository,
                repository,
                workingSubdirectory,
                tagName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VcsTagTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        VcsTagTask that = (VcsTagTask) o;
        return tagName.equals(that.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tagName);
    }
}
