package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents task that parses test results for presentation in Bamboo build results.
 */
public class TestParserTask extends Task<TestParserTask, TestParserTaskProperties> {

    private final TestParserTaskProperties.TestType testType;
    private Set<String> resultDirectories = new LinkedHashSet<>();
    @Nullable
    private Boolean pickUpTestResultsCreatedOutsideOfThisBuild;

    /**
     * Specify test parsing task that handles selected test engine. Supported test engines are JUnit and TestNG.
     */
    public TestParserTask(final TestParserTaskProperties.TestType testType) {
        this.testType = testType;
    }

    /**
     * Specify test parsing task that handles JUnit test results.
     */
    public static TestParserTask createJUnitParserTask() {
        return new TestParserTask(TestParserTaskProperties.TestType.JUNIT);
    }

    /**
     * Specify test parsing task that handles TestNG test results.
     */
    public static TestParserTask createTestNGParserTask() {
        return new TestParserTask(TestParserTaskProperties.TestType.TESTNG);
    }

    /**
     * Specify test parsing task that handles NUnit test results.
     */
    public static TestParserTask createNUnitParserTask() {
        return new TestParserTask(TestParserTaskProperties.TestType.NUNIT);
    }

    /**
     * Specify test parsing task that handles test results of Mocha executed with 'mocha-bamboo-reporter'.
     */
    public static TestParserTask createMochaParserTask() {
        return new TestParserTask(TestParserTaskProperties.TestType.MOCHA);
    }

    /**
     * Specify test parsing task that handles mstest test results.
     */
    public static TestParserTask createMSTestParserTask() {
        return new TestParserTask(TestParserTaskProperties.TestType.MSTEST);
    }

    /**
     * Gets test engine supported by this task.
     */
    public TestParserTaskProperties.TestType getTestType() {
        return testType;
    }

    /**
     * Adds directories from the argument to the list of directories in which task looks for test result files.
     */
    public TestParserTask resultDirectories(@NotNull String... resultDirectories) {
        // neither varargs array nor any of its elements can be null
        checkNotNull("resultDirectories", resultDirectories);
        Arrays.stream(resultDirectories)
                .forEach(dir -> checkNotNull("resultDirectories", dir));

        this.resultDirectories.addAll(Arrays.asList(resultDirectories));
        return this;
    }

    /**
     * Adds default directory to the list of directories in which task looks for test result files.
     * <p>
     * Default directory is defined depending on the selected test engine.
     */
    public TestParserTask defaultResultDirectory() {
        switch (testType) {
            case JUNIT:
                return resultDirectories("**/test-reports/*.xml");
            case NUNIT:
                return resultDirectories("**/test-reports/*.xml");
            case TESTNG:
                return resultDirectories("**/testng-results.xml");
            case MOCHA:
                return resultDirectories(MochaParserTask.DEFAULT_TEST_FILE_PATTERN);
            case MSTEST:
                return resultDirectories("**/*.trx");
            default:
                throw new IllegalStateException("Unsupported test type: " + testType);
        }
    }

    /**
     * Allows/disallows the task to scan test result files created before start time of the build.
     */
    public TestParserTask pickUpTestResultsCreatedOutsideOfThisBuild(@Nullable final Boolean pickUpTestResultsCreatedOutsideOfThisBuild) {
        this.pickUpTestResultsCreatedOutsideOfThisBuild = pickUpTestResultsCreatedOutsideOfThisBuild;
        return this;
    }

    @NotNull
    @Override
    protected TestParserTaskProperties build() {
        return new TestParserTaskProperties(
                testType,
                description,
                taskEnabled,
                resultDirectories,
                pickUpTestResultsCreatedOutsideOfThisBuild,
                requirements,
                conditions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestParserTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TestParserTask that = (TestParserTask) o;
        return getTestType() == that.getTestType() &&
                Objects.equals(resultDirectories, that.resultDirectories) &&
                Objects.equals(pickUpTestResultsCreatedOutsideOfThisBuild, that.pickUpTestResultsCreatedOutsideOfThisBuild);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTestType(), resultDirectories, pickUpTestResultsCreatedOutsideOfThisBuild);
    }
}
