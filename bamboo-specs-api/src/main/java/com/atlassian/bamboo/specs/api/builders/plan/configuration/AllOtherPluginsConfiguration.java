package com.atlassian.bamboo.specs.api.builders.plan.configuration;

import com.atlassian.bamboo.specs.api.model.plan.configuration.AllOtherPluginsConfigurationProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;

/***
 * Builder for all the plugins that don't have proper builders. This builder passes configuration as a map of options.
 * Please, refer to the plugin documentation to find list of keys recognized by that plugin.
 */
public class AllOtherPluginsConfiguration extends PluginConfiguration<AllOtherPluginsConfigurationProperties> {

    private Map<String, Object> configuration;

    public AllOtherPluginsConfiguration() {
    }

    /***
     * Specifies the configuration of plugins.
     */
    public AllOtherPluginsConfiguration configuration(@NotNull Map<String, Object> configuration) {
        ImporterUtils.checkNotNull("configuration", configuration);
        this.configuration = MapUtils.copyOf(configuration);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AllOtherPluginsConfiguration)) {
            return false;
        }
        AllOtherPluginsConfiguration that = (AllOtherPluginsConfiguration) o;
        return Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configuration);
    }

    @NotNull
    @Override
    protected AllOtherPluginsConfigurationProperties build() {
        return new AllOtherPluginsConfigurationProperties(configuration);
    }

}
