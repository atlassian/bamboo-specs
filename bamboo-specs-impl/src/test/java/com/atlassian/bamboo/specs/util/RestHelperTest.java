package com.atlassian.bamboo.specs.util;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class RestHelperTest {

    @Test
    @Parameters
    public void tryGetErrorMessageFromResponse(String responseJson, Optional<String> expectedResult) throws Exception {
        assertThat(RestHelper.tryGetErrorMessageFromResponse(responseJson), is(expectedResult));
    }

    private Object[] parametersForTryGetErrorMessageFromResponse() {
        final Gson gson = new Gson();

        final Map<String, String> format1 = Collections.singletonMap(
                "message", "could not move plan"
        );
        final Map<String, Object> format2 = ImmutableMap.of(
                "errors", new Object[]{"invalid plan configuration"},
                "fieldErrors", Collections.singletonMap(
                        "name", new Object[]{"value is required"}
                )
        );

        final String format1Message = "could not move plan";
        final String format2Message = "invalid plan configuration; name: value is required";

        return new Object[][]{
                {null, Optional.empty()},
                {"", Optional.empty()},
                {"foo-bar", Optional.empty()},

                {gson.toJson(format1), Optional.of(format1Message)},
                {gson.toJson(format2), Optional.of(format2Message)},
        };
    }
}