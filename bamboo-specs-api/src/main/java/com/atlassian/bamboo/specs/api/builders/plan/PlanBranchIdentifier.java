package com.atlassian.bamboo.specs.api.builders.plan;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanBranchIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public class PlanBranchIdentifier extends EntityPropertiesBuilder<PlanBranchIdentifierProperties> {
    private BambooKeyProperties key;
    private BambooOidProperties oid;

    public PlanBranchIdentifier(@NotNull final PlanBranchIdentifier copy) {
        this.key = copy.key;
        this.oid = copy.oid;
    }

    public PlanBranchIdentifier(@NotNull final BambooKey key) throws PropertiesValidationException {
        checkNotNull("key", key);
        this.key = EntityPropertiesBuilders.build(key);
    }

    public PlanBranchIdentifier(@NotNull final BambooOid oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        this.oid = EntityPropertiesBuilders.build(oid);
    }

    public PlanBranchIdentifier oid(@NotNull final String oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        return oid(new BambooOid(oid));
    }

    public PlanBranchIdentifier oid(@NotNull final BambooOid oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        this.oid = EntityPropertiesBuilders.build(oid);
        return this;
    }

    public PlanBranchIdentifier key(@NotNull final String key) throws PropertiesValidationException {
        checkNotNull("key", key);
        return key(new BambooKey(key));
    }

    public PlanBranchIdentifier key(@NotNull final BambooKey key) throws PropertiesValidationException {
        checkNotNull("key", key);
        this.key = EntityPropertiesBuilders.build(key);
        return this;
    }

    protected PlanBranchIdentifierProperties build() throws PropertiesValidationException {
        return new PlanBranchIdentifierProperties(key, oid);
    }
}
