package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.trigger.AfterSuccessfulBuildPlanTriggerProperties;
import org.jetbrains.annotations.Nullable;

/**
 * Trigger which schedule a deployment when plan build complete.
 */
public class AfterSuccessfulBuildPlanTrigger extends Trigger<AfterSuccessfulBuildPlanTrigger, AfterSuccessfulBuildPlanTriggerProperties> {
    private String releaseBranch;

    public AfterSuccessfulBuildPlanTrigger() throws PropertiesValidationException {
    }

    /**
     * Configure trigger to start deployment when plan master branch is updated.
     */
    public AfterSuccessfulBuildPlanTrigger triggerByMasterBranch() {
        releaseBranch = null;
        return this;
    }

    /**
     * Configure trigger to start deployment when plan branch is updated.
     *
     * @param planBranchName name of plan branch. If value is null it's the same as {@link #triggerByMasterBranch()}
     */
    public AfterSuccessfulBuildPlanTrigger triggerByBranch(@Nullable String planBranchName) {
        this.releaseBranch = planBranchName;
        return this;
    }

    @Override
    protected AfterSuccessfulBuildPlanTriggerProperties build() {
        return new AfterSuccessfulBuildPlanTriggerProperties(name, description, triggerEnabled, releaseBranch);
    }
}
