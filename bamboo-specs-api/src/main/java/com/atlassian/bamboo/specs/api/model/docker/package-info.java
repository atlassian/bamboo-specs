/**
 * Properties classes for Docker configuration.
 */
package com.atlassian.bamboo.specs.api.model.docker;
