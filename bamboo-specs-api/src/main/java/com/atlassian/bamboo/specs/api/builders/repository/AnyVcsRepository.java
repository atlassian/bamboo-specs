package com.atlassian.bamboo.specs.api.builders.repository;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.AnyVcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * Represents a vcs repository of any type.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation of a given credential type is not available.
 */
public final class AnyVcsRepository extends VcsRepository<AnyVcsRepository, AnyVcsRepositoryProperties> {
    private AtlassianModuleProperties atlassianPlugin;
    private Map<String, Object> serverConfiguration;
    private Map<String, Object> branchConfiguration;
    private Map<String, Object> branchDetectionConfiguration;
    private VcsChangeDetectionProperties changeDetectionConfiguration;

    /**
     * Specifies a repository of given type.
     *
     * @param atlassianPlugin type of the repository identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyVcsRepository(final AtlassianModuleProperties atlassianPlugin) {
        this.atlassianPlugin = atlassianPlugin;
    }

    /**
     * Specifies a repository of given type.
     *
     * @param atlassianPlugin type of the repository identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyVcsRepository(final AtlassianModule atlassianPlugin) {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Sets server part of the configuration. Server configuration is mandatory if no parent is defined.
     * <p>
     * Repository configuration in Bamboo is split into chunks that allow parts of data to be inherited (and to be overridden). Server configuration
     * typically contains data like: url, credentials, repository name or path, advanced options nb. command timeouts, caching etc.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is performed
     * on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyVcsRepository serverConfiguration(@Nullable Map<String, Object> serverConfiguration) throws PropertiesValidationException {
        this.serverConfiguration = serverConfiguration != null ? MapUtils.copyOf(serverConfiguration) : null;
        return this;
    }

    /**
     * Sets branch part of the configuration. Branch configuration is mandatory for most repository types if no parent is defined.
     * <p>
     * Repository configuration in Bamboo is split into chunks that allow parts of data to be inherited (and to be overridden). Branch configuration defines
     * which particular branch of the repository this object represents.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is performed
     * on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyVcsRepository branchConfiguration(@Nullable Map<String, Object> branchConfiguration) throws PropertiesValidationException {
        this.branchConfiguration = branchConfiguration != null ? MapUtils.copyOf(branchConfiguration) : null;
        return this;
    }

    /**
     * Sets branch detection part of the configuration. Branch detection options are typically optional.
     * <p>
     * Repository configuration in Bamboo is split into chunks that allow parts of data to be inherited (and to be overridden). Branch detection options contain
     * configuration specific to automatic branch management in Bamboo.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is performed
     * on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyVcsRepository branchDetectionConfiguration(@Nullable Map<String, Object> branchDetectionConfiguration) throws PropertiesValidationException {
        this.branchDetectionConfiguration = branchDetectionConfiguration != null ? MapUtils.copyOf(branchDetectionConfiguration) : null;
        return this;
    }

    /**
     * Sets change detection part of the configuration. These options are typically optional as Bamboo is usually able to set reasonable defaults.
     * <p>
     * Repository configuration in Bamboo is split into chunks that allow parts of data to be inherited (and to be overridden). Change detection options contain
     * configuration specific to change detection in Bamboo, such us commit isolation, filtering etc.
     *
     * @see VcsChangeDetection
     */
    public AnyVcsRepository changeDetectionConfiguration(@Nullable VcsChangeDetection changeDetectionConfiguration) throws PropertiesValidationException {
        this.changeDetectionConfiguration = changeDetectionConfiguration != null ? changeDetectionConfiguration.build() : null;
        return this;
    }

    @Override
    protected AnyVcsRepositoryProperties build() {
        return new AnyVcsRepositoryProperties(atlassianPlugin,
                name,
                oid,
                description,
                parent,
                project,
                serverConfiguration,
                branchConfiguration,
                changeDetectionConfiguration,
                branchDetectionConfiguration,
                repositoryViewer);
    }
}
