package com.atlassian.bamboo.specs.model.notification;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.net.MalformedURLException;
import java.net.URL;

@Immutable
@ConstructFrom({"webhookName", "url"})
public class WebhookRecipientProperties extends NotificationRecipientProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:recipient.webhook");

    private final String webhookName;
    private final String url;

    private WebhookRecipientProperties() {
        this.webhookName = null;
        this.url = null;
    }

    public WebhookRecipientProperties(@NotNull String webhookName, @NotNull String url) {
        this.webhookName = webhookName;
        this.url = url;
        validate();
    }

    public String getWebhookName() {
        return webhookName;
    }

    public String getUrl() {
        return url;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        ValidationContext context = ValidationContext.of("webhookRecipient");
        ImporterUtils.checkNotBlank(context, "webhookName", webhookName);
        ImporterUtils.checkNotBlank(context, "url", url);
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            throw new PropertiesValidationException(context, String.format("Argument %s is not valid URL", url));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WebhookRecipientProperties that = (WebhookRecipientProperties) o;

        if (webhookName != null ? !webhookName.equals(that.webhookName) : that.webhookName != null) return false;
        return url != null ? url.equals(that.url) : that.url == null;
    }

    @Override
    public int hashCode() {
        int result = webhookName != null ? webhookName.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
