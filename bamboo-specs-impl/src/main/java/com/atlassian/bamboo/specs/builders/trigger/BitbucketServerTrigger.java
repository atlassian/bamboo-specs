package com.atlassian.bamboo.specs.builders.trigger;


import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.model.trigger.BitbucketServerTriggerProperties;

/**
 * Represents Bitbucket Server trigger.
 */
public class BitbucketServerTrigger extends RepositoryBasedTrigger<BitbucketServerTrigger, BitbucketServerTriggerProperties> {
    @Override
    protected BitbucketServerTriggerProperties build() {
        return new BitbucketServerTriggerProperties(name, description, triggerEnabled, conditions, triggeringRepositoriesType, selectedTriggeringRepositories);
    }
}
