package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.MsBuildTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

/**
 * Represents task that runs MSBuild command.
 */
public class MsBuildTask extends Task<MsBuildTask, MsBuildTaskProperties> {
    public static final String DEFAULT_PROJECT = "YourSolution.sln";

    private String executable;
    private String projectFile = DEFAULT_PROJECT;
    private String options;
    private String environmentVariables;
    private String workingSubdirectory;

    /**
     * Specifies MSBuild executable to be used.
     * <p>
     * Sets label (<em>not a path</em>) of command to be executed. This label must be first
     * defined in the GUI on the Administration/Executables page.
     *
     * This field is mandatory
     */
    public MsBuildTask executable(@NotNull String executable) {
        checkNotBlank("executable", executable);
        this.executable = executable;
        return this;
    }

    /**
     * Specifies the solution, project file or MSBuild project to execute when this Job Builds.
     */
    public MsBuildTask projectFile(@NotNull String projectFile) {
        checkNotBlank("projectFile", projectFile);
        this.projectFile = projectFile;
        return this;
    }

    /**
     * Specifies command line argument to be passed when command is executed.
     */
    public MsBuildTask options(@Nullable String options) {
        this.options = options;
        return this;
    }

    /**
     * Specifies environment variables to be set when command is executed.
     */
    public MsBuildTask environmentVariables(@Nullable String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return this;
    }

    /**
     * Specifies a working directory for the task.
     */
    public MsBuildTask workingSubdirectory(@Nullable String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    @NotNull
    @Override
    protected MsBuildTaskProperties build() {
        return new MsBuildTaskProperties(
                description,
                taskEnabled,
                executable,
                projectFile,
                options,
                environmentVariables,
                workingSubdirectory,
                requirements,
                conditions
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsBuildTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        MsBuildTask that = (MsBuildTask) o;
        return Objects.equals(executable, that.executable) &&
                Objects.equals(projectFile, that.projectFile) &&
                Objects.equals(options, that.options) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), executable, projectFile, options, environmentVariables, workingSubdirectory);
    }
}
