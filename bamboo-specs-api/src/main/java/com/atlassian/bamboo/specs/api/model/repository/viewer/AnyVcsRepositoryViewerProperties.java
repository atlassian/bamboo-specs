package com.atlassian.bamboo.specs.api.model.repository.viewer;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.repository.viewer.AnyVcsRepositoryViewerEmitter")
@Immutable
public final class AnyVcsRepositoryViewerProperties implements VcsRepositoryViewerProperties {
    private final Map<String, Object> configuration;
    private final AtlassianModuleProperties atlassianPlugin;

    private AnyVcsRepositoryViewerProperties() {
        configuration = Collections.emptyMap();
        atlassianPlugin = null;
    }

    public AnyVcsRepositoryViewerProperties(@NotNull AtlassianModuleProperties atlassianPlugin,
                                            @NotNull Map<String, Object> configuration) throws PropertiesValidationException {
        this.atlassianPlugin = atlassianPlugin;
        this.configuration = Collections.unmodifiableMap(MapUtils.copyOf(configuration));
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnyVcsRepositoryViewerProperties)) {
            return false;
        }
        AnyVcsRepositoryViewerProperties that = (AnyVcsRepositoryViewerProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getConfiguration(), that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getConfiguration());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    @NotNull
    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    @Override
    public void validate() {
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
