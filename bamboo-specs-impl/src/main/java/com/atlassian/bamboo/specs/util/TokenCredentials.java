package com.atlassian.bamboo.specs.util;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpRequestBase;

import static java.lang.String.format;

public interface TokenCredentials extends AuthenticationProvider {

    String getToken();

    @Override
    default void authenticate(final HttpRequestBase request) {
        request.addHeader("Authorization", format("Bearer %s", this.getToken()));
        request.addHeader(HttpHeaders.ACCEPT, "application/json");
    }
}
