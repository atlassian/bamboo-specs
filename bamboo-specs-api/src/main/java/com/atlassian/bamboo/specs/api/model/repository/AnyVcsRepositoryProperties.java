package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.util.MapUtils;
import com.atlassian.bamboo.specs.api.validators.repository.VcsRepositoryValidator;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public final class AnyVcsRepositoryProperties extends VcsRepositoryProperties {
    private AtlassianModuleProperties atlassianPlugin;

    private Map<String, Object> serverConfiguration;
    private Map<String, Object> branchConfiguration;
    private Map<String, Object> branchDetectionConfiguration;
    private VcsChangeDetectionProperties changeDetectionConfiguration;

    private AnyVcsRepositoryProperties() {
    }

    public AnyVcsRepositoryProperties(final AtlassianModuleProperties atlassianPlugin,
                                      @Nullable final String name,
                                      @Nullable final BambooOidProperties oid,
                                      @Nullable final String description,
                                      @Nullable final String parent,
                                      @Nullable final Map<String, Object> serverConfiguration,
                                      @Nullable final Map<String, Object> branchConfiguration,
                                      @Nullable final VcsChangeDetectionProperties changeDetectionConfiguration,
                                      @Nullable final Map<String, Object> branchDetectionConfiguration,
                                      @Nullable final VcsRepositoryViewerProperties repositoryViewerProperties) throws PropertiesValidationException {
        this(atlassianPlugin, name, oid, description, parent, null, serverConfiguration, branchConfiguration,changeDetectionConfiguration, branchDetectionConfiguration, repositoryViewerProperties);
    }

    public AnyVcsRepositoryProperties(final AtlassianModuleProperties atlassianPlugin,
                                      @Nullable final String name,
                                      @Nullable final BambooOidProperties oid,
                                      @Nullable final String description,
                                      @Nullable final String parent,
                                      @Nullable final ProjectProperties project,
                                      @Nullable final Map<String, Object> serverConfiguration,
                                      @Nullable final Map<String, Object> branchConfiguration,
                                      @Nullable final VcsChangeDetectionProperties changeDetectionConfiguration,
                                      @Nullable final Map<String, Object> branchDetectionConfiguration,
                                      @Nullable final VcsRepositoryViewerProperties repositoryViewerProperties) throws PropertiesValidationException {
        super(name, oid, description, parent, repositoryViewerProperties, project);
        this.atlassianPlugin = atlassianPlugin;
        this.serverConfiguration = serverConfiguration != null ? Collections.unmodifiableMap(MapUtils.copyOf(serverConfiguration)) : null;
        this.branchConfiguration = branchConfiguration != null ? Collections.unmodifiableMap(MapUtils.copyOf(branchConfiguration)) : null;
        this.changeDetectionConfiguration = changeDetectionConfiguration;
        this.branchDetectionConfiguration = branchDetectionConfiguration != null ? Collections.unmodifiableMap(MapUtils.copyOf(branchDetectionConfiguration)) : null;
        validate();
    }

    @Nullable
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    @Nullable
    public Map<String, Object> getServerConfiguration() {
        return serverConfiguration;
    }

    @Nullable
    public Map<String, Object> getBranchConfiguration() {
        return branchConfiguration;
    }

    @Nullable
    public VcsChangeDetectionProperties getChangeDetectionConfiguration() {
        return changeDetectionConfiguration;
    }

    @Nullable
    public Map<String, Object> getBranchDetectionConfiguration() {
        return branchDetectionConfiguration;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AnyVcsRepositoryProperties that = (AnyVcsRepositoryProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getServerConfiguration(), that.getServerConfiguration()) &&
                Objects.equals(getBranchConfiguration(), that.getBranchConfiguration()) &&
                Objects.equals(getBranchDetectionConfiguration(), that.getBranchDetectionConfiguration()) &&
                Objects.equals(getChangeDetectionConfiguration(), that.getChangeDetectionConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAtlassianPlugin(), getServerConfiguration(), getBranchConfiguration(), getBranchDetectionConfiguration(), getChangeDetectionConfiguration());
    }

    @Override
    public void validate() {
        super.validate();
        if (getParentName() == null) {
            checkNotNull("atlassianPlugin", atlassianPlugin);
        }
        checkNoErrors(VcsRepositoryValidator.validate(this));
    }
}
