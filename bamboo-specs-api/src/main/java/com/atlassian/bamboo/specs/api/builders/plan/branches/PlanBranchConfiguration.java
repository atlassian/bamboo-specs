package com.atlassian.bamboo.specs.api.builders.plan.branches;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchIntegrationProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchConfigurationProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents configuration items that are specific for plan branches.
 * These settings are only applied to the current branch, if Specs branches are active.
 */
public class PlanBranchConfiguration extends EntityPropertiesBuilder<PlanBranchConfigurationProperties> {
    private boolean cleanupEnabled = true;
    private BranchIntegrationProperties branchIntegration = new BranchIntegrationProperties(false, null, false, false);

    /**
     * Enables/disables automatic removal of this plan branch. By default, automatic cleanup is enabled.
     */
    @NotNull
    public PlanBranchConfiguration cleanupEnabled(boolean cleanupEnabled) {
        this.cleanupEnabled = cleanupEnabled;
        return this;
    }

    /**
     * Disables automatic removal of this plan branch.
     */
    @NotNull
    public PlanBranchConfiguration disableAutomaticCleanup() {
        return cleanupEnabled(false);
    }

    /**
     * Enables automatic removal of this plan branch.
     */
    @NotNull
    public PlanBranchConfiguration enableAutomaticCleanup() {
        return cleanupEnabled(true);
    }

    /**
     * Sets merge strategy for this plan branch. By default, merging is turned off.
     *
     * @see BranchIntegration
     */
    public PlanBranchConfiguration branchIntegration(@NotNull BranchIntegration branchIntegration) throws PropertiesValidationException {
        checkNotNull("branchIntegration", branchIntegration);
        this.branchIntegration = branchIntegration.build();
        return this;
    }

    @Override
    protected PlanBranchConfigurationProperties build() {
        return new PlanBranchConfigurationProperties(branchIntegration, cleanupEnabled);
    }
}
