package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.notification.WebhookRecipientProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a webhook which should be called.
 */
public class WebhookRecipient  extends NotificationRecipient<WebhookRecipient, WebhookRecipientProperties> {

    private final String webhookName;
    private final String url;

    public WebhookRecipient(String webhookName, String url) {
        ImporterUtils.checkNotBlank("webhookName", webhookName);
        ImporterUtils.checkNotBlank("url", url);
        this.webhookName = webhookName;
        this.url = url;
    }

    @NotNull
    @Override
    protected WebhookRecipientProperties build() {
        return new WebhookRecipientProperties(webhookName, url);
    }
}
