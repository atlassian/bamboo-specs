package com.atlassian.bamboo.specs.codegen.emitters.plan;

import com.atlassian.bamboo.specs.api.builders.repository.PlanRepositoryLink;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.AnyVcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlanRepositoriesEmitterTest {
    private CodeGenerationContext context;
    private PlanRepositoriesEmitter emitter;

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new PlanRepositoriesEmitter();
    }

    @Test
    public void testEmitProjectRepositories() throws CodeGenerationException {
        final String code = emitter.emitCode(context, Stream.of("foo", "bar", "baz")
                .map(VcsRepositoryIdentifier::new)
                .map(PlanRepositoryLink::linkToProjectRepository)
                .map(EntityPropertiesBuilders::build)
                .collect(Collectors.toList()));
        assertThat(code, is(".projectRepositories(\"foo\",\n"
                + "    \"bar\",\n"
                + "    \"baz\")\n"));
    }

    @Test
    public void testEmitLinkedRepositories() throws CodeGenerationException {
        final String code = emitter.emitCode(context, Stream.of("foo", "bar", "baz")
                .map(VcsRepositoryIdentifier::new)
                .map(PlanRepositoryLink::linkToGlobalRepository)
                .map(EntityPropertiesBuilders::build)
                .collect(Collectors.toList()));
        assertThat(code, is(".linkedRepositories(\"foo\",\n"
                + "    \"bar\",\n"
                + "    \"baz\")\n"));
    }

    @Test
    public void testEmitPlanRepositories() throws CodeGenerationException {
        final String code = emitter.emitCode(context, Stream.of("foo", "bar", "baz")
                .map(r -> new AnyVcsRepositoryProperties(new AtlassianModuleProperties("module:module"), r, null, null, null, null, null, null, null, null))
                .map(PlanRepositoryLinkProperties::new)
                .collect(Collectors.toList()));
        assertThat(code, is(".planRepositories(new AnyVcsRepository(new AtlassianModule(\"module:module\"))\n        .name(\"foo\"),\n"
                + "    new AnyVcsRepository(new AtlassianModule(\"module:module\"))\n        .name(\"bar\"),\n"
                + "    new AnyVcsRepository(new AtlassianModule(\"module:module\"))\n        .name(\"baz\"))\n"));
    }
}