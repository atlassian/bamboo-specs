package com.atlassian.bamboo.specs.codegen.emitters.plan;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.codegen.emitters.CodeGenerationUtils;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class PlanRepositoriesEmitter implements CodeEmitter<Iterable<PlanRepositoryLinkProperties>> {

    private enum RepositoryLinkType {
        LINKED,
        PLAN,
        PROJECT
    }

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final Iterable<PlanRepositoryLinkProperties> value) throws CodeGenerationException {
        StringBuilder builder = new StringBuilder();

        List<String> valuesAndFails = new ArrayList<>();
        Set<Integer> indicesOfFails = new HashSet<>();
        int index = 0;
        RepositoryLinkType linkType = RepositoryLinkType.LINKED;

        for (PlanRepositoryLinkProperties planRepositoryLink : value) {
            VcsRepositoryProperties repository = planRepositoryLink.getRepositoryDefinition();
            if (repository instanceof PlanRepositoryLinkProperties.LinkedGlobalRepository) {
                if (linkType != RepositoryLinkType.LINKED) {
                    dumpCurrentData(builder, context, linkType, valuesAndFails, indicesOfFails);
                    index = 0;
                    linkType = RepositoryLinkType.LINKED;
                }
                valuesAndFails.add(ValueEmitterFactory.emitterFor(repository.getParent()).emitCode(context, repository.getParent()));
                index++;
            } else if (repository instanceof PlanRepositoryLinkProperties.ProjectRepository) {
                if (linkType != RepositoryLinkType.PROJECT) {
                    dumpCurrentData(builder, context, linkType, valuesAndFails, indicesOfFails);
                    index = 0;
                    linkType = RepositoryLinkType.PROJECT;
                }
                valuesAndFails.add(ValueEmitterFactory.emitterFor(repository.getParent()).emitCode(context, repository.getParent()));
                index++;
            } else {
                if (linkType != RepositoryLinkType.PLAN) {
                    dumpCurrentData(builder, context, linkType, valuesAndFails, indicesOfFails);
                    index = 0;
                    linkType = RepositoryLinkType.PLAN;
                }

                boolean fail = false;
                String repositoryCode;
                try {
                    CodeEmitter codeEmitter = ValueEmitterFactory.emitterFor(repository);
                    context.incIndentation();
                    repositoryCode = codeEmitter.emitCode(context, repository);
                    context.decIndentation();
                    valuesAndFails.add(repositoryCode);
                } catch (CodeGenerationException e) {
                    valuesAndFails.add("//" + e.getMessage());
                    indicesOfFails.add(index);
                }
                index++;
            }
        }
        dumpCurrentData(builder, context, linkType, valuesAndFails, indicesOfFails);
        return builder.toString();
    }

    private void dumpCurrentData(StringBuilder builder, CodeGenerationContext context, RepositoryLinkType linkType, List<String> valuesAndFails, Set<Integer> indicesOfFails) {
        if (valuesAndFails.isEmpty()) {
            return;
        }
        if (linkType == RepositoryLinkType.LINKED) {
            builder.append(".linkedRepositories(");
        } else if (linkType == RepositoryLinkType.PROJECT) {
            builder.append(".projectRepositories(");
        } else {
            builder.append(".planRepositories(");
        }
        context.incIndentation();
        CodeGenerationUtils.appendCommaSeparatedList(context, builder, valuesAndFails, indicesOfFails);
        context.decIndentation();
        builder.append(")").append(context.newLine());
        valuesAndFails.clear();
        indicesOfFails.clear();
    }
}
