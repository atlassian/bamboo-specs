package com.atlassian.bamboo.specs.codegen.emitters.value;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.codegen.emitters.CodeGenerationUtils;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Helper class that looks for best code generator for an object.
 */
public final class ValueEmitterFactory {
    private ValueEmitterFactory() {
    }

    public static <T extends EntityProperties> CodeEmitter<T> emitterFor(@NotNull final T entity) throws CodeGenerationException {
        final CodeEmitter<T> codeEmitter = CodeGenerationUtils.findEmitterByAnnotation(entity.getClass());
        if (codeEmitter != null) {
            return codeEmitter;
        }
        return new EntityPropertiesEmitter<>();
    }

    public static <T> CodeEmitter<T> emitterFor(@NotNull final T object) throws CodeGenerationException {
        if (object instanceof EntityProperties) {
            return (CodeEmitter<T>) emitterFor((EntityProperties) object);
        } else if (object instanceof Duration) {
            return (CodeEmitter<T>) new DurationEmitter();
        } else if (object instanceof Map) {
            return (CodeEmitter<T>) new MapEmitter();
        } else if (object instanceof Set) {
            return (CodeEmitter<T>) new SetEmitter();
        } else if (object instanceof Collection) {
            throw new CodeGenerationException("Generating code for collections is not supported yet");
        } else {
            return (CodeEmitter<T>) new LiteralEmitter();
        }
    }
}
