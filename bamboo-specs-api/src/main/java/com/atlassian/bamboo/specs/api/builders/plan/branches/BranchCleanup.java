package com.atlassian.bamboo.specs.api.builders.plan.branches;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchCleanupProperties;

import java.time.Duration;

/**
 * Represent configuration of branch cleanup.
 */
public class BranchCleanup extends EntityPropertiesBuilder<BranchCleanupProperties> {
    public static final Duration DEFAULT_REMOVED_BRANCH_EXPIRY = Duration.ofDays(7);
    public static final Duration DEFAULT_INACTIVE_BRANCH_EXPIRY = Duration.ofDays(30);

    /**
     * Removed branch expiry in days.
     * @deprecated use {@link #DEFAULT_REMOVED_BRANCH_EXPIRY} instead.
     */
    @Deprecated
    public static final int DEFAULT_REMOVED_BRANCH_EXPRIRY_IN_DAYS = (int) DEFAULT_REMOVED_BRANCH_EXPIRY.toDays();

    /**
     * Inactive branch expiry in days.
     * @deprecated use {@link #DEFAULT_INACTIVE_BRANCH_EXPIRY} instead.
     */
    @Deprecated
    public static final int DEFAULT_INACTIVE_BRANCH_EXPIRY_IN_DAYS = (int) DEFAULT_INACTIVE_BRANCH_EXPIRY.toDays();

    private boolean removeDeletedFromRepository = false;
    private boolean removeInactiveInRepository = false;
    private Duration removeDeletedFromRepositoryPeriod = DEFAULT_REMOVED_BRANCH_EXPIRY;
    private Duration removeInactiveInRepositoryPeriod = DEFAULT_INACTIVE_BRANCH_EXPIRY;

    /**
     * Enables/disables plan branch removing when branch deleted in repository. Disabled by default.
     */
    public BranchCleanup whenRemovedFromRepository(boolean removeRemovedFromRepository) throws PropertiesValidationException {
        this.removeDeletedFromRepository = removeRemovedFromRepository;
        return this;
    }

    /**
     * Enables/disables plan branch removing when branch is inactive (no commits) in repository. Disabled by default.
     */
    public BranchCleanup whenInactiveInRepository(boolean removeWhenInactiveInRepository) throws PropertiesValidationException {
        this.removeInactiveInRepository = removeWhenInactiveInRepository;
        return this;
    }

    /**
     * Defines the time after which the branch should be removed. Default is {@link #DEFAULT_REMOVED_BRANCH_EXPIRY}.
     */
    public BranchCleanup whenRemovedFromRepositoryAfterDays(int whenRemovedFromRepositoryAfterDays) throws PropertiesValidationException {
        this.removeDeletedFromRepositoryPeriod = Duration.ofDays(whenRemovedFromRepositoryAfterDays);
        this.removeDeletedFromRepository = true;
        return this;
    }

    /**
     * Defines the time after which the branch should be removed in case of inactivity. Default is {@link #DEFAULT_INACTIVE_BRANCH_EXPIRY}.
     */
    public BranchCleanup whenInactiveInRepositoryAfterDays(int whenInactiveInRepositoryAfterDays) throws PropertiesValidationException {
        this.removeInactiveInRepositoryPeriod = Duration.ofDays(whenInactiveInRepositoryAfterDays);
        this.removeInactiveInRepository = true;
        return this;
    }

    protected BranchCleanupProperties build() throws PropertiesValidationException {
        return new BranchCleanupProperties(removeDeletedFromRepository, removeDeletedFromRepositoryPeriod, removeInactiveInRepository, removeInactiveInRepositoryPeriod);
    }
}
