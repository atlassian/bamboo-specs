package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class CommandTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.scripttask:task.builder.command");

    private final String executable;
    @Nullable
    private final String argument;
    @Nullable
    private final String environmentVariables;
    @Nullable
    private final String workingSubdirectory;

    private CommandTaskProperties() {
        this.executable = null;
        this.argument = null;
        this.environmentVariables = null;
        this.workingSubdirectory = null;
    }

    public CommandTaskProperties(final String description,
                                 final boolean enabled,
                                 @NotNull final String executable,
                                 @Nullable final String argument,
                                 @Nullable final String environmentVariables,
                                 @Nullable final String workingSubdirectory,
                                 @NotNull List<RequirementProperties> requirements,
                                 @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.executable = executable;
        this.argument = argument;
        this.environmentVariables = environmentVariables;
        this.workingSubdirectory = workingSubdirectory;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    public String getExecutable() {
        return executable;
    }

    @Nullable
    public String getArgument() {
        return argument;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        final ValidationContext context = ValidationContext.of("Command task");
        checkThat(context, StringUtils.isNotBlank(executable), "Executable is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final CommandTaskProperties that = (CommandTaskProperties) o;
        return Objects.equals(this.getExecutable(), that.getExecutable()) &&
                Objects.equals(this.getArgument(), that.getArgument()) &&
                Objects.equals(this.getEnvironmentVariables(), that.getEnvironmentVariables()) &&
                Objects.equals(this.getWorkingSubdirectory(), that.getWorkingSubdirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.getExecutable(), this.getArgument(), this.getEnvironmentVariables(), this.getWorkingSubdirectory());
    }
}
