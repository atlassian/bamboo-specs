package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.ProjectPermissions;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.RootEntityProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@ConstructFrom("projectKey")
public class ProjectPermissionsProperties implements RootEntityProperties {
    private final BambooKeyProperties projectKey;
    private final PermissionsProperties projectPermissions;
    private final PermissionsProperties projectPlanPermissions;
    private final List<VcsRepositoryIdentifierProperties> specsRepositories;

    private ProjectPermissionsProperties() {
        this.projectKey = null;
        this.projectPermissions = null;
        this.projectPlanPermissions = null;
        specsRepositories = Collections.emptyList();
    }

    public ProjectPermissionsProperties(@NotNull final BambooKey projectKey,
                                        @NotNull final Permissions projectPermissions,
                                        @NotNull final Permissions projectPlanPermissions,
                                        @NotNull final List<VcsRepositoryIdentifierProperties> specsRepositories) throws PropertiesValidationException {
        this.projectKey = EntityPropertiesBuilders.build(projectKey);
        this.projectPermissions = EntityPropertiesBuilders.build(projectPermissions);
        this.projectPlanPermissions = EntityPropertiesBuilders.build(projectPlanPermissions);
        this.specsRepositories = Collections.unmodifiableList(new ArrayList<>(specsRepositories));

        validate();
    }

    @Override
    public void validate() {
        checkNotNull("projectKey", projectKey);
        checkNotNull("projectPermissions", projectPermissions);
        checkNotNull("projectPlanPermissions", projectPlanPermissions);
        checkNoErrors(PermissionValidator.validatePermissions(projectPermissions, PermissionValidator.PermissionTarget.PROJECT));
        checkNoErrors(PermissionValidator.validatePermissions(projectPlanPermissions, PermissionValidator.PermissionTarget.PLAN));
    }

    public BambooKeyProperties getProjectKey() {
        return projectKey;
    }

    public PermissionsProperties getProjectPermissions() {
        return projectPermissions;
    }

    public PermissionsProperties getProjectPlanPermissions() {
        return projectPlanPermissions;
    }

    @NotNull
    public List<VcsRepositoryIdentifierProperties> getSpecsRepositories() {
        return specsRepositories != null ? specsRepositories : Collections.emptyList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectPermissionsProperties that = (ProjectPermissionsProperties) o;
        return Objects.equals(getProjectKey(), that.getProjectKey()) &&
                Objects.equals(getProjectPermissions(), that.getProjectPermissions()) &&
                Objects.equals(getProjectPlanPermissions(), that.getProjectPlanPermissions()) &&
                Objects.equals(getSpecsRepositories(), that.getSpecsRepositories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProjectKey(), getProjectPermissions(), getProjectPlanPermissions());
    }

    @Override
    @NotNull
    public String humanReadableType() {
        return ProjectPermissions.TYPE;
    }

    @Override
    @NotNull
    public String humanReadableId() {
        return String.format("%s for project %s", ProjectPermissions.TYPE, projectKey);
    }
}
