package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.VcsCommitTaskProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * This task will <strong>commit</strong> all local changes to a repository. For
 * <a href="https://www.atlassian.com/git/tutorials/what-is-version-control/">DVCS repositories</a> (which distinguish
 * between local and remote commits) this task will also <strong>push</strong> the commits to the remote repository.
 */
public class VcsCommitTask extends BaseVcsTask<VcsCommitTask, VcsCommitTaskProperties> {
    @NotNull
    private String commitMessage;

    /**
     * Sets the message to be used for committing.
     */
    public VcsCommitTask commitMessage(@NotNull String commitMessage) {
        checkNotNull("commitMessage", commitMessage);
        this.commitMessage = commitMessage;
        return this;
    }

    @NotNull
    @Override
    protected VcsCommitTaskProperties build() {
        return new VcsCommitTaskProperties(
                description,
                taskEnabled,
                requirements,
                conditions,
                defaultRepository,
                repository,
                workingSubdirectory,
                commitMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VcsCommitTask)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        VcsCommitTask that = (VcsCommitTask) o;
        return commitMessage.equals(that.commitMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), commitMessage);
    }
}
